﻿using EntidadesLogExcepcion.Entities;
using RepositorioLogExcepcion.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioLogExcepcion.Implementacion
{
    public class ManageLogError : IManageLogError
    {
        public Context_DB_Logs_Excepcion_SAS _context;

        #region CRUD
        public void AddLogError(LogExcepcione objLogError)
        {
            objLogError.FechaError = DateTime.Now;
        }

        public async Task<bool> saveError(LogExcepcione objLog)
        {
            try
            {
                _context = new Context_DB_Logs_Excepcion_SAS();

                _context.LogExcepciones.Add(objLog);

                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                _context.Dispose();
            }
            return false;
        }

        #endregion

        #region Consultas
        public async Task<LogExcepcione> ByIDLog(Guid idLog)
        {
            throw new NotImplementedException();
        }

        public async Task<LogExcepcione> ByIDUsuario(Guid idUsuario)
        {
            throw new NotImplementedException();
        }

        public async Task<LogExcepcione> GeByMetodo(string metodo)
        {
            throw new NotImplementedException();
        }

        public async Task<List<LogExcepcione>> GetByDate(DateTime fechaInicial, DateTime fechaFinal)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
