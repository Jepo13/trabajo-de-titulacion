﻿using EntidadesLogExcepcion.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioLogExcepcion.Interface
{
    public interface IManageLogError
    {

        #region CRUD
        public void AddLogError(LogExcepcione objLogError);
        public Task<bool> saveError(LogExcepcione objLog);
        #endregion

        #region Search
        public Task<LogExcepcione> ByIDLog(Guid idLog);
        public Task<LogExcepcione> ByIDUsuario(Guid idUsuario);
        public Task<LogExcepcione> GeByMetodo(string metodo);
        public Task<List<LogExcepcione>> GetByDate(DateTime fechaInicial, DateTime fechaFinal);
        #endregion

    }
}
