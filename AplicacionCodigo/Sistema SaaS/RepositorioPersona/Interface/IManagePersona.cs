﻿using DTOs.Cliente;
using EntidadesPersonas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Persona;
using Utilitarios.Busquedas.Usuario;

namespace RepositorioPersona.Interface
{
    public interface  IManagePersona
    {

        public Persona cargarClienteFinal();
        public  Task<List<Persona>> BusquedaAvanzadaParaCliente(ClienteObjetoBusquedaDTO objBusqueda);
        public Task<List<Persona>> BusquedaAvanzadaParaUsuario(ObjetoBusquedaUsuarios objBusqueda);
        public Task<List<Persona>> BusquedaAvanzada(ObjetoBusquedaPersona objBusqueda);
        public Task<Persona> recuperarPersonaPorIdentificacion(string numeroIdentificacion);
        public Task<List<Persona>> recuperarAllPersonas();
        public  Task<Persona> recuperarPersonaPorID(Guid idPersona);
        public Task<Persona> recuperarClientePersonaPorID(Guid idPersona);
    }
}
