﻿using DTOs.EmpresaCliente;
using EntidadesPersonas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioPersona.Interface
{
    public interface IManageEmpresaCliente
    {

        public Task<List<EmpresaCliente>> obtenerEmpresaClientePorIdPersona(Guid idPersona);
        public Task<EmpresaCliente> obtenerEmpresaClientePorID(Guid idEmpresaCliente);
        public Task<EmpresaCliente> obtenerEmpresaClienteBusquedaValidar(EmpresaClienteValidadDTO objBusqueda);
    }
}
