﻿
using DTOs.Cliente;
using EntidadesPersonas.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioPersona.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Persona;
using Utilitarios.Busquedas.Usuario;

namespace RepositorioPersona.Implementacion
{
    public class ManagePersona : IManagePersona
    {
        public readonly Context_DB_Personas_SAS _context;
        public ManagePersona(Context_DB_Personas_SAS context) 
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        public Persona cargarClienteFinal()
        {
            Persona objetoPersona = new Persona();

            objetoPersona.Nombre = "Cliente";
            objetoPersona.Apellido = "Final";
            objetoPersona.NumeroIdentificacion = "9999999999";
            objetoPersona.Telefono1 = "0999999999";
            objetoPersona.Direccion = "Quito";
            objetoPersona.CorreoPersonal = "correo@prueba.com";

            /*Tipoidentificacion objetoTipoIdentificacion = new Tipoidentificacion();*/
            /*objetoTipoIdentificacion.Nombretipoidentificacion = "Cédula";*/
            return objetoPersona;
        }

        public async Task<List<Persona>> BusquedaAvanzadaParaCliente(ClienteObjetoBusquedaDTO objBusqueda) 
        {
            List<Persona> listaPersonas = new();
            if (!string.IsNullOrEmpty(objBusqueda.numeroIdentificacion)) 
            {
                listaPersonas = await _context.Personas.Where(x=>x.NumeroIdentificacion == objBusqueda.numeroIdentificacion).ToListAsync();
            }
            else 
            {
                listaPersonas = await _context.Personas.ToListAsync();
            }
            if (!string.IsNullOrEmpty(objBusqueda.nombreCliente) || !string.IsNullOrEmpty(objBusqueda.apellido))
            {
                string NombreApellido = objBusqueda.nombreCliente + " " + objBusqueda.apellido;
                listaPersonas = listaPersonas.Where(x => x.Nombre.ToUpper().Trim().Contains(NombreApellido.ToUpper().Trim()) || x.Apellido.ToUpper().Trim().Contains(NombreApellido.ToUpper().Trim())).ToList();
            }
            if (objBusqueda.idTipoIdentificacion != null) 
            {
                listaPersonas = listaPersonas.Where(x => x.IdTipoIdentificacion == objBusqueda.idTipoIdentificacion).ToList();

            }
            return listaPersonas;
        }
        public async Task<List<Persona>> BusquedaAvanzadaParaUsuario(ObjetoBusquedaUsuarios objBusqueda) 
        {
            List<Persona> listaPersonas = new();
            listaPersonas = await _context.Personas.ToListAsync();
            if (!string.IsNullOrEmpty(objBusqueda.nombres)|| !string.IsNullOrEmpty(objBusqueda.apellidos)) 
            {
                string NombreApellido = objBusqueda.nombres + " " + objBusqueda.apellidos;
                listaPersonas = listaPersonas.Where(x => x.Nombre.ToUpper().Trim().Contains(NombreApellido.ToUpper().Trim()) || x.Apellido.ToUpper().Trim().Contains(NombreApellido.ToUpper().Trim())).ToList();
            }
            if (!string.IsNullOrEmpty(objBusqueda.numeroIdentificacion))
            {
                listaPersonas = listaPersonas.Where(x => x.NumeroIdentificacion.Contains(objBusqueda.numeroIdentificacion)).ToList();
            }

            return listaPersonas;
        }

        public async Task<List<Persona>> BusquedaAvanzada(ObjetoBusquedaPersona objBusqueda) 
        {
            List<Persona> listaPersonas = new();
            listaPersonas = await _context.Personas.ToListAsync();

            if (!string.IsNullOrEmpty(objBusqueda.NombreApellido))
            {
                listaPersonas = listaPersonas.Where(x => x.Nombre.ToUpper().Trim().Contains(objBusqueda.NombreApellido.ToUpper().Trim()) || x.Apellido.ToUpper().Trim().Contains(objBusqueda.NombreApellido.ToUpper().Trim())).ToList();
            }
            if (!string.IsNullOrEmpty(objBusqueda.NumeroIdentificacion))
            {
                listaPersonas = listaPersonas.Where(x => x.NumeroIdentificacion.Contains(objBusqueda.NumeroIdentificacion)).ToList();
            }

            return listaPersonas;

        }
        public async Task<Persona> recuperarPersonaPorIdentificacion(string numeroIdentificacion)
        {
          Persona Persona = await _context.Personas.Where(p => p.NumeroIdentificacion == numeroIdentificacion).FirstOrDefaultAsync();
            return Persona;
        }


        public async Task<Persona> recuperarPersonaPorID(Guid idPersona)
        {
            Persona objetoPersona = await _context.Personas.Where(x => x.IdPersona == idPersona).FirstOrDefaultAsync();

            return objetoPersona;
        }

        public async Task<Persona> recuperarClientePersonaPorID(Guid idPersona)
        {
            Persona objetoPersona = await _context.Personas.Where(x => x.IdPersona == idPersona).Include(x=>x.EmpresaClientes).FirstOrDefaultAsync();

            return objetoPersona;
        }
        public async Task<List<Persona>> recuperarAllPersonas()
        {
            List<Persona> listaPersonas = await _context.Personas.ToListAsync();    
            return listaPersonas;
        }

        

    }
}
