﻿using DTOs.EmpresaCliente;
using EntidadesPersonas.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioPersona.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioPersona.Implementacion
{
    public class ManageEmpresaCliente : IManageEmpresaCliente
    {
        public readonly Context_DB_Personas_SAS _context;


        public ManageEmpresaCliente(Context_DB_Personas_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }


        public async Task<List<EmpresaCliente>> obtenerEmpresaClientePorIdPersona(Guid idPersona)
        {
            try
            {
                List<EmpresaCliente> ListaEmpresaCliente = await _context.EmpresaClientes
                    .Where(c => c.IdPersona == idPersona)
                    .ToListAsync();

                return ListaEmpresaCliente;
            }
            catch (Exception ex)
            {

            }
            return default;
        }

        public async Task<EmpresaCliente> obtenerEmpresaClientePorID (Guid idEmpresaCliente) 
        {
            try
            {
               EmpresaCliente EmpresaCliente = await _context.EmpresaClientes
                    .Where(c => c.IdEmpresaCliente == idEmpresaCliente)
                    .FirstOrDefaultAsync();

                return EmpresaCliente;
            }
            catch (Exception ex)
            {

            }
            return default;
        }

        public async Task<EmpresaCliente> obtenerEmpresaClienteBusquedaValidar (EmpresaClienteValidadDTO objBusqueda) 
        {
            try
            {
                EmpresaCliente EmpresaCliente = await _context.EmpresaClientes
                     .Where(c => c.IdEmpresa == objBusqueda.IdEmpresa && c.IdPersona == objBusqueda.IdPersona)
                     .FirstOrDefaultAsync();

                return EmpresaCliente;
            }
            catch (Exception ex)
            {

            }
            return default;
        }
    }
}
