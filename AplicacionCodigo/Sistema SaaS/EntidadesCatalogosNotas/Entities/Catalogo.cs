﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesCatalogosNotas.Entities
{
    public partial class Catalogo
    {
        public Catalogo()
        {
            InverseIdCatalogopadreNavigation = new HashSet<Catalogo>();
            Nota = new HashSet<Nota>();
        }

        public Guid IdCatalogo { get; set; }
        public Guid? IdCatalogopadre { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string NombreCatalogo { get; set; }
        public string CodigoCatalogo { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public string DatoAdicional { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public bool? Editable { get; set; }
        public string DatoIcono { get; set; }
        public bool? TieneVigencia { get; set; }
        public int? NivelCatalogo { get; set; }

        public virtual Catalogo IdCatalogopadreNavigation { get; set; }
        public virtual ICollection<Catalogo> InverseIdCatalogopadreNavigation { get; set; }
        public virtual ICollection<Nota> Nota { get; set; }
    }
}
