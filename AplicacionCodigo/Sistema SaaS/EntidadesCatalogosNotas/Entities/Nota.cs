﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesCatalogosNotas.Entities
{
    public partial class Nota
    {
        public Guid IdNotas { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdImportancia { get; set; }
        public string TituloNota { get; set; }
        public string ContenidoNota { get; set; }
        public bool Estado { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Catalogo IdImportanciaNavigation { get; set; }
    }
}
