﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesCatalogosNotas.Entities
{
    public partial class Context_DB_Catalogos_Nota_SAS : DbContext
    {
        public Context_DB_Catalogos_Nota_SAS()
        {
        }

        public Context_DB_Catalogos_Nota_SAS(DbContextOptions<Context_DB_Catalogos_Nota_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<Catalogo> Catalogos { get; set; }
        public virtual DbSet<Nota> Notas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Catalogos_Nota_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<Catalogo>(entity =>
            {
                entity.HasKey(e => e.IdCatalogo);

                entity.ToTable("CATALOGO");

                entity.Property(e => e.IdCatalogo)
                    .HasColumnName("ID_CATALOGO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CodigoCatalogo)
                    .IsRequired()
                    .HasMaxLength(8)
                    .HasColumnName("CODIGO_CATALOGO");

                entity.Property(e => e.DatoAdicional)
                    .HasMaxLength(80)
                    .HasColumnName("DATO_ADICIONAL");

                entity.Property(e => e.DatoIcono)
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasColumnName("DATO_ICONO");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Editable).HasColumnName("EDITABLE");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCatalogopadre).HasColumnName("ID_CATALOGOPADRE");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.NivelCatalogo).HasColumnName("NIVEL_CATALOGO");

                entity.Property(e => e.NombreCatalogo)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("NOMBRE_CATALOGO");

                entity.Property(e => e.TieneVigencia).HasColumnName("TIENE_VIGENCIA");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdCatalogopadreNavigation)
                    .WithMany(p => p.InverseIdCatalogopadreNavigation)
                    .HasForeignKey(d => d.IdCatalogopadre)
                    .HasConstraintName("FK_CATALOGO_FK_CATALO_CATALOGO");
            });

            modelBuilder.Entity<Nota>(entity =>
            {
                entity.HasKey(e => e.IdNotas);

                entity.ToTable("NOTAS");

                entity.Property(e => e.IdNotas)
                    .HasColumnName("ID_NOTAS")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ContenidoNota)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("CONTENIDO_NOTA");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.FechaVencimiento)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_VENCIMIENTO");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdImportancia)
                    .HasColumnName("ID_IMPORTANCIA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.TituloNota)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("TITULO_NOTA");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdImportanciaNavigation)
                    .WithMany(p => p.Nota)
                    .HasForeignKey(d => d.IdImportancia)
                    .HasConstraintName("FK_NOTAS_REFERENCE_CATALOGO");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
