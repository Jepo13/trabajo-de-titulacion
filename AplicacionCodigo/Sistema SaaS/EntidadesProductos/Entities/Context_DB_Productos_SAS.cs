﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class Context_DB_Productos_SAS : DbContext
    {
        public Context_DB_Productos_SAS()
        {
        }

        public Context_DB_Productos_SAS(DbContextOptions<Context_DB_Productos_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<CanjeProductoProveedor> CanjeProductoProveedors { get; set; }
        public virtual DbSet<CatalogoProducto> CatalogoProductos { get; set; }
        public virtual DbSet<CatalogoProductoImpuesto> CatalogoProductoImpuestos { get; set; }
        public virtual DbSet<CategoriaProducto> CategoriaProductos { get; set; }
        public virtual DbSet<EstadoProducto> EstadoProductos { get; set; }
        public virtual DbSet<GrupoProducto> GrupoProductos { get; set; }
        public virtual DbSet<Impuesto> Impuestos { get; set; }
        public virtual DbSet<Marca> Marcas { get; set; }
        public virtual DbSet<PrecioTipoVentaCatalogo> PrecioTipoVentaCatalogos { get; set; }
        public virtual DbSet<PrecioTipoVentaProducto> PrecioTipoVentaProductos { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<ProductoImpuesto> ProductoImpuestos { get; set; }
        public virtual DbSet<ProductoStock> ProductoStocks { get; set; }
        public virtual DbSet<UnidadCatalogoProducto> UnidadCatalogoProductos { get; set; }
        public virtual DbSet<UnidadEntrega> UnidadEntregas { get; set; }
        public virtual DbSet<UnidadProducto> UnidadProductos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Productos_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<CanjeProductoProveedor>(entity =>
            {
                entity.HasKey(e => e.IdCanjeProductoProveedor);

                entity.ToTable("CANJE_PRODUCTO_PROVEEDOR");

                entity.Property(e => e.IdCanjeProductoProveedor)
                    .HasColumnName("ID_CANJE_PRODUCTO_PROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.FechaCanje)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CANJE");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_PROVEEDOR");

                entity.Property(e => e.IdentificadorCanjeProducto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("IDENTIFICADOR_CANJE_PRODUCTO");

                entity.Property(e => e.ImagenCanje)
                    .HasColumnType("image")
                    .HasColumnName("IMAGEN_CANJE");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            modelBuilder.Entity<CatalogoProducto>(entity =>
            {
                entity.HasKey(e => e.IdCatalogoProducto);

                entity.ToTable("CATALOGO_PRODUCTO");

                entity.Property(e => e.IdCatalogoProducto)
                    .HasColumnName("ID_CATALOGO_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CantidadMaxima).HasColumnName("CANTIDAD_MAXIMA");

                entity.Property(e => e.CantidadMinima).HasColumnName("CANTIDAD_MINIMA");

                entity.Property(e => e.CodigoPersonalizado)
                    .HasMaxLength(15)
                    .HasColumnName("CODIGO_PERSONALIZADO")
                    .IsFixedLength(true);

                entity.Property(e => e.CostoUltimaAdquisicion)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("COSTO_ULTIMA_ADQUISICION");

                entity.Property(e => e.DetalleCatalogoProducto).HasColumnName("DETALLE_CATALOGO_PRODUCTO");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCaducidad)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CADUCIDAD");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCategoria).HasColumnName("ID_CATEGORIA");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdGrupoProducto).HasColumnName("ID_GRUPO_PRODUCTO");

                entity.Property(e => e.IdMarca).HasColumnName("ID_MARCA");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.IdPorcentajeIva).HasColumnName("ID_PORCENTAJE_IVA");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_PROVEEDOR");

                entity.Property(e => e.IdUnidadEntrega).HasColumnName("ID_UNIDAD_ENTREGA");

                entity.Property(e => e.IdUsuarioCreacion).HasColumnName("ID_USUARIO_CREACION");

                entity.Property(e => e.IdUsuarioModificacion).HasColumnName("ID_USUARIO_MODIFICACION");

                entity.Property(e => e.ImagenProducto).HasColumnName("IMAGEN_PRODUCTO");

                entity.Property(e => e.NombreCorto)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE_CORTO");

                entity.Property(e => e.NombreProducto)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("NOMBRE_PRODUCTO");

                entity.Property(e => e.TieneIva).HasColumnName("TIENE_IVA");

                entity.Property(e => e.UrlImagenCatalogoProducto)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("URL_IMAGEN_CATALOGO_PRODUCTO");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.CatalogoProductos)
                    .HasForeignKey(d => d.IdCategoria)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_CATEGORI");

                entity.HasOne(d => d.IdGrupoProductoNavigation)
                    .WithMany(p => p.CatalogoProductos)
                    .HasForeignKey(d => d.IdGrupoProducto)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_GRUPO_PR");

                entity.HasOne(d => d.IdMarcaNavigation)
                    .WithMany(p => p.CatalogoProductos)
                    .HasForeignKey(d => d.IdMarca)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_MARCA");

                entity.HasOne(d => d.IdUnidadEntregaNavigation)
                    .WithMany(p => p.CatalogoProductos)
                    .HasForeignKey(d => d.IdUnidadEntrega)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_UNIDAD_E");
            });

            modelBuilder.Entity<CatalogoProductoImpuesto>(entity =>
            {
                entity.HasKey(e => e.IdCatalogoproductoimpuesto);

                entity.ToTable("CATALOGO_PRODUCTO_IMPUESTO");

                entity.Property(e => e.IdCatalogoproductoimpuesto)
                    .HasColumnName("ID_CATALOGOPRODUCTOIMPUESTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdCatalogoProducto).HasColumnName("ID_CATALOGO_PRODUCTO");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_IMPUESTO");

                entity.HasOne(d => d.IdCatalogoProductoNavigation)
                    .WithMany(p => p.CatalogoProductoImpuestos)
                    .HasForeignKey(d => d.IdCatalogoProducto)
                    .HasConstraintName("FK_CATALOGO_FK_C_PROD_CATALOGO");

                entity.HasOne(d => d.IdImpuestoNavigation)
                    .WithMany(p => p.CatalogoProductoImpuestos)
                    .HasForeignKey(d => d.IdImpuesto)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_IMPUESTO");
            });

            modelBuilder.Entity<CategoriaProducto>(entity =>
            {
                entity.HasKey(e => e.IdCategoriaProducto);

                entity.ToTable("CATEGORIA_PRODUCTO");

                entity.Property(e => e.IdCategoriaProducto)
                    .HasColumnName("ID_CATEGORIA_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.DescripcionProducto)
                    .HasMaxLength(100)
                    .HasColumnName("DESCRIPCION_PRODUCTO");

                entity.Property(e => e.EstadoCategoriaProducto).HasColumnName("ESTADO_CATEGORIA_PRODUCTO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCategoriaProductoPadre).HasColumnName("ID_CATEGORIA_PRODUCTO_PADRE");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.NombreCategoriaProducto)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("NOMBRE_CATEGORIA_PRODUCTO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdCategoriaProductoPadreNavigation)
                    .WithMany(p => p.InverseIdCategoriaProductoPadreNavigation)
                    .HasForeignKey(d => d.IdCategoriaProductoPadre)
                    .HasConstraintName("FK_CATEGORI_REFERENCE_CATEGORI");
            });

            modelBuilder.Entity<EstadoProducto>(entity =>
            {
                entity.HasKey(e => e.IdEstadoProducto);

                entity.ToTable("ESTADO_PRODUCTO");

                entity.Property(e => e.IdEstadoProducto)
                    .HasColumnName("ID_ESTADO_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(20)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            modelBuilder.Entity<GrupoProducto>(entity =>
            {
                entity.HasKey(e => e.IdGrupoProducto);

                entity.ToTable("GRUPO_PRODUCTO");

                entity.Property(e => e.IdGrupoProducto)
                    .HasColumnName("ID_GRUPO_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.NombreGrupoProducto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("NOMBRE_GRUPO_PRODUCTO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            modelBuilder.Entity<Impuesto>(entity =>
            {
                entity.HasKey(e => e.IdImpuesto);

                entity.ToTable("IMPUESTO");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_IMPUESTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.NombreImpuesto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE_IMPUESTO");

                entity.Property(e => e.TipoImpuesto)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("TIPO_IMPUESTO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.Property(e => e.Valor)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR");
            });

            modelBuilder.Entity<Marca>(entity =>
            {
                entity.HasKey(e => e.IdMarca);

                entity.ToTable("MARCA");

                entity.Property(e => e.IdMarca)
                    .HasColumnName("ID_MARCA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            modelBuilder.Entity<PrecioTipoVentaCatalogo>(entity =>
            {
                entity.HasKey(e => e.IdPrecioTipoVentaCatalogo);

                entity.ToTable("PRECIO_TIPO_VENTA_CATALOGO");

                entity.Property(e => e.IdPrecioTipoVentaCatalogo)
                    .HasColumnName("ID_PRECIO_TIPO_VENTA_CATALOGO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdCatalogoProducto)
                    .HasColumnName("ID_CATALOGO_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdTipoClienteCompra).HasColumnName("ID_TIPO_CLIENTE_COMPRA");

                entity.Property(e => e.PorcentajeDescuento)
                    .HasColumnType("decimal(2, 2)")
                    .HasColumnName("PORCENTAJE_DESCUENTO");

                entity.Property(e => e.PorcentajeUtilidad)
                    .HasColumnType("decimal(2, 2)")
                    .HasColumnName("PORCENTAJE_UTILIDAD");

                entity.Property(e => e.Valor)
                    .HasColumnType("money")
                    .HasColumnName("VALOR");

                entity.Property(e => e.ValorCompraProducto)
                    .HasColumnType("money")
                    .HasColumnName("VALOR_COMPRA_PRODUCTO");

                entity.HasOne(d => d.IdCatalogoProductoNavigation)
                    .WithMany(p => p.PrecioTipoVentaCatalogos)
                    .HasForeignKey(d => d.IdCatalogoProducto)
                    .HasConstraintName("FK_PRECIO_T_REFERENCE_CATALOGO");
            });

            modelBuilder.Entity<PrecioTipoVentaProducto>(entity =>
            {
                entity.HasKey(e => e.IdPrecioTipoVentaProducto);

                entity.ToTable("PRECIO_TIPO_VENTA_PRODUCTO");

                entity.Property(e => e.IdPrecioTipoVentaProducto)
                    .HasColumnName("ID_PRECIO_TIPO_VENTA_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdProducto)
                    .HasColumnName("ID_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdTipoClienteCompra).HasColumnName("ID_TIPO_CLIENTE_COMPRA");

                entity.Property(e => e.PorcentajeDescuento)
                    .HasColumnType("decimal(2, 2)")
                    .HasColumnName("PORCENTAJE_DESCUENTO");

                entity.Property(e => e.PorcentajeUtilidad)
                    .HasColumnType("decimal(2, 2)")
                    .HasColumnName("PORCENTAJE_UTILIDAD");

                entity.Property(e => e.Valor)
                    .HasColumnType("money")
                    .HasColumnName("VALOR");

                entity.Property(e => e.ValorCompraProducto)
                    .HasColumnType("money")
                    .HasColumnName("VALOR_COMPRA_PRODUCTO");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.PrecioTipoVentaProductos)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_PRECIO_T_REFERENCE_PRODUCTO");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.IdProducto);

                entity.ToTable("PRODUCTO");

                entity.Property(e => e.IdProducto)
                    .HasColumnName("ID_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CantidadExistentes).HasColumnName("CANTIDAD_EXISTENTES");

                entity.Property(e => e.CantidadInservible).HasColumnName("CANTIDAD_INSERVIBLE");

                entity.Property(e => e.CantidadVendidos).HasColumnName("CANTIDAD_VENDIDOS");

                entity.Property(e => e.CodigoBarras)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("CODIGO_BARRAS");

                entity.Property(e => e.CodigoPersonalizado)
                    .HasMaxLength(15)
                    .HasColumnName("CODIGO_PERSONALIZADO");

                entity.Property(e => e.CostoAdquisicion)
                    .HasColumnType("money")
                    .HasColumnName("COSTO_ADQUISICION");

                entity.Property(e => e.DetalleProducto)
                    .HasColumnType("text")
                    .HasColumnName("DETALLE_PRODUCTO");

                entity.Property(e => e.EsMedicina).HasColumnName("ES_MEDICINA");

                entity.Property(e => e.FechaCaducidad)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CADUCIDAD");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCanjeProductoProveedor).HasColumnName("ID_CANJE_PRODUCTO_PROVEEDOR");

                entity.Property(e => e.IdCategoria).HasColumnName("ID_CATEGORIA");

                entity.Property(e => e.IdEstado).HasColumnName("ID_ESTADO");

                entity.Property(e => e.IdEstadoProducto).HasColumnName("ID_ESTADO_PRODUCTO");

                entity.Property(e => e.IdFacturaProveedor).HasColumnName("ID_FACTURA_PROVEEDOR");

                entity.Property(e => e.IdGrupoProducto).HasColumnName("ID_GRUPO_PRODUCTO");

                entity.Property(e => e.IdMarca).HasColumnName("ID_MARCA");

                entity.Property(e => e.IdProcedencia).HasColumnName("ID_PROCEDENCIA");

                entity.Property(e => e.IdUnidadEntrega).HasColumnName("ID_UNIDAD_ENTREGA");

                entity.Property(e => e.NombreCorto)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE_CORTO");

                entity.Property(e => e.NombreProducto)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("NOMBRE_PRODUCTO");

                entity.Property(e => e.RegistroSanitario)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("REGISTRO_SANITARIO");

                entity.Property(e => e.TieneIva).HasColumnName("TIENE_IVA");

                entity.Property(e => e.UrlImagenProducto)
                    .HasMaxLength(180)
                    .IsUnicode(false)
                    .HasColumnName("URL_IMAGEN_PRODUCTO");

                entity.Property(e => e.UsuarioCreacion)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdCanjeProductoProveedorNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdCanjeProductoProveedor)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_CANJE_PR");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdCategoria)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_CATEGORI");

                entity.HasOne(d => d.IdEstadoProductoNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdEstadoProducto)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_ESTADO_P");

                entity.HasOne(d => d.IdGrupoProductoNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdGrupoProducto)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_GRUPO_PR");

                entity.HasOne(d => d.IdMarcaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdMarca)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_MARCA");

                entity.HasOne(d => d.IdUnidadEntregaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdUnidadEntrega)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_UNIDAD_E");
            });

            modelBuilder.Entity<ProductoImpuesto>(entity =>
            {
                entity.HasKey(e => e.IdProductoImpuesto);

                entity.ToTable("PRODUCTO_IMPUESTO");

                entity.Property(e => e.IdProductoImpuesto)
                    .HasColumnName("ID_PRODUCTO_IMPUESTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_IMPUESTO");

                entity.Property(e => e.IdProducto).HasColumnName("ID_PRODUCTO");

                entity.HasOne(d => d.IdImpuestoNavigation)
                    .WithMany(p => p.ProductoImpuestos)
                    .HasForeignKey(d => d.IdImpuesto)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_IMPUESTO");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.ProductoImpuestos)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_PRODUCTO_PRODUCTO__PRODUCTO");
            });

            modelBuilder.Entity<ProductoStock>(entity =>
            {
                entity.HasKey(e => e.ProductoStock1);

                entity.ToTable("PRODUCTO_STOCK");

                entity.Property(e => e.ProductoStock1)
                    .HasColumnName("PRODUCTO_STOCK")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CantidadProductos).HasColumnName("CANTIDAD_PRODUCTOS");

                entity.Property(e => e.FechaCaducidad)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CADUCIDAD");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdBodega).HasColumnName("ID_BODEGA");

                entity.Property(e => e.IdLocal).HasColumnName("ID_LOCAL");

                entity.Property(e => e.IdProducto)
                    .HasColumnName("ID_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.UsuarioCreacion)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.ProductoStocks)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_PRODUCTO");
            });

            modelBuilder.Entity<UnidadCatalogoProducto>(entity =>
            {
                entity.HasKey(e => e.IdUnidadCatalogoProducto);

                entity.ToTable("UNIDAD_CATALOGO_PRODUCTO");

                entity.Property(e => e.IdUnidadCatalogoProducto)
                    .HasColumnName("ID_UNIDAD_CATALOGO_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CantidadInferior)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("CANTIDAD_INFERIOR");

                entity.Property(e => e.CantidadSuperior).HasColumnName("CANTIDAD_SUPERIOR");

                entity.Property(e => e.CodigoBarrasInferior)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("CODIGO_BARRAS_INFERIOR");

                entity.Property(e => e.CodigoBarrasSuperior)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("CODIGO_BARRAS_SUPERIOR");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCatalogoProducto)
                    .HasColumnName("ID_CATALOGO_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.MedidaInferior)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("MEDIDA_INFERIOR");

                entity.Property(e => e.MedidaSuperior)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("MEDIDA_SUPERIOR");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdCatalogoProductoNavigation)
                    .WithMany(p => p.UnidadCatalogoProductos)
                    .HasForeignKey(d => d.IdCatalogoProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UNIDAD_C_REFERENCE_CATALOGO");
            });

            modelBuilder.Entity<UnidadEntrega>(entity =>
            {
                entity.HasKey(e => e.IdUnidadentrega);

                entity.ToTable("UNIDAD_ENTREGA");

                entity.Property(e => e.IdUnidadentrega)
                    .HasColumnName("ID_UNIDADENTREGA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CodigoUnidad)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasColumnName("CODIGO_UNIDAD")
                    .IsFixedLength(true);

                entity.Property(e => e.DescripcionMedida)
                    .HasColumnType("text")
                    .HasColumnName("DESCRIPCION_MEDIDA");

                entity.Property(e => e.EstadoUnidad).HasColumnName("ESTADO_UNIDAD");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.NombreContenido)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE_CONTENIDO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            modelBuilder.Entity<UnidadProducto>(entity =>
            {
                entity.HasKey(e => e.IdUnidadProducto);

                entity.ToTable("UNIDAD_PRODUCTO");

                entity.Property(e => e.IdUnidadProducto)
                    .HasColumnName("ID_UNIDAD_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CantidadInferior)
                    .HasColumnType("decimal(15, 5)")
                    .HasColumnName("CANTIDAD_INFERIOR");

                entity.Property(e => e.CantidadSuperior).HasColumnName("CANTIDAD_SUPERIOR");

                entity.Property(e => e.CodigoBarrasInferior)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("CODIGO_BARRAS_INFERIOR");

                entity.Property(e => e.CodigoBarrasSuperior)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("CODIGO_BARRAS_SUPERIOR");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdProducto)
                    .HasColumnName("ID_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.MedidaInferior)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("MEDIDA_INFERIOR");

                entity.Property(e => e.MedidaSuperior)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("MEDIDA_SUPERIOR");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.UnidadProductos)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UNIDAD_P_REFERENCE_PRODUCTO");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
