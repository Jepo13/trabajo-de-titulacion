﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class Impuesto
    {
        public Impuesto()
        {
            CatalogoProductoImpuestos = new HashSet<CatalogoProductoImpuesto>();
            ProductoImpuestos = new HashSet<ProductoImpuesto>();
        }

        public Guid IdImpuesto { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string NombreImpuesto { get; set; }
        public decimal Valor { get; set; }
        public string TipoImpuesto { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<CatalogoProductoImpuesto> CatalogoProductoImpuestos { get; set; }
        public virtual ICollection<ProductoImpuesto> ProductoImpuestos { get; set; }
    }
}
