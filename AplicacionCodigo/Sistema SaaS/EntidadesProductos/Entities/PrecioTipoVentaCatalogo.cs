﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class PrecioTipoVentaCatalogo
    {
        public Guid IdPrecioTipoVentaCatalogo { get; set; }
        public Guid? IdCatalogoProducto { get; set; }
        public Guid IdTipoClienteCompra { get; set; }
        public decimal? PorcentajeUtilidad { get; set; }
        public decimal? Valor { get; set; }
        public decimal? PorcentajeDescuento { get; set; }
        public decimal ValorCompraProducto { get; set; }

        public virtual CatalogoProducto IdCatalogoProductoNavigation { get; set; }
    }
}
