﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class CatalogoProductoImpuesto
    {
        public Guid IdCatalogoproductoimpuesto { get; set; }
        public Guid? IdImpuesto { get; set; }
        public Guid? IdCatalogoProducto { get; set; }

        public virtual CatalogoProducto IdCatalogoProductoNavigation { get; set; }
        public virtual Impuesto IdImpuestoNavigation { get; set; }
    }
}
