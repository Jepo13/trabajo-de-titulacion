﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class EstadoProducto
    {
        public EstadoProducto()
        {
            Productos = new HashSet<Producto>();
        }

        public Guid IdEstadoProducto { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<Producto> Productos { get; set; }
    }
}
