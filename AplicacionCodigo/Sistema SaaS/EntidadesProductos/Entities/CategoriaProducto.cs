﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class CategoriaProducto
    {
        public CategoriaProducto()
        {
            CatalogoProductos = new HashSet<CatalogoProducto>();
            InverseIdCategoriaProductoPadreNavigation = new HashSet<CategoriaProducto>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdCategoriaProducto { get; set; }
        public Guid? IdCategoriaProductoPadre { get; set; }
        public Guid IdEmpresa { get; set; }
        public string NombreCategoriaProducto { get; set; }
        public bool EstadoCategoriaProducto { get; set; }
        public string DescripcionProducto { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual CategoriaProducto IdCategoriaProductoPadreNavigation { get; set; }
        public virtual ICollection<CatalogoProducto> CatalogoProductos { get; set; }
        public virtual ICollection<CategoriaProducto> InverseIdCategoriaProductoPadreNavigation { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
