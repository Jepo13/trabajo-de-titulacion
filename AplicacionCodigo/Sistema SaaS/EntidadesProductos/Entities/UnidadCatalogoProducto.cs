﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class UnidadCatalogoProducto
    {
        public Guid IdUnidadCatalogoProducto { get; set; }
        public Guid IdCatalogoProducto { get; set; }
        public string MedidaSuperior { get; set; }
        public int CantidadSuperior { get; set; }
        public string CodigoBarrasSuperior { get; set; }
        public string MedidaInferior { get; set; }
        public decimal CantidadInferior { get; set; }
        public string CodigoBarrasInferior { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual CatalogoProducto IdCatalogoProductoNavigation { get; set; }
    }
}
