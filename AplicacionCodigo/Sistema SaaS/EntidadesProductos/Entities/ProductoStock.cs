﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class ProductoStock
    {
        public Guid ProductoStock1 { get; set; }
        public Guid IdProducto { get; set; }
        public Guid? IdLocal { get; set; }
        public int CantidadProductos { get; set; }
        public DateTime? FechaCaducidad { get; set; }
        public Guid? IdBodega { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual Producto IdProductoNavigation { get; set; }
    }
}
