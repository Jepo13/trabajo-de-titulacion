﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class GrupoProducto
    {
        public GrupoProducto()
        {
            CatalogoProductos = new HashSet<CatalogoProducto>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdGrupoProducto { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string NombreGrupoProducto { get; set; }       
        public bool EstadoGrupoProducto { get; set; }
        public string DescripcionGrupoProducto { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<CatalogoProducto> CatalogoProductos { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
