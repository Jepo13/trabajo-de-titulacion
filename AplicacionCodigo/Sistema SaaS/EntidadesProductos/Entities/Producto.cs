﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class Producto
    {
        public Producto()
        {
            PrecioTipoVentaProductos = new HashSet<PrecioTipoVentaProducto>();
            ProductoImpuestos = new HashSet<ProductoImpuesto>();
            ProductoStocks = new HashSet<ProductoStock>();
            UnidadProductos = new HashSet<UnidadProducto>();
        }

        public Guid IdProducto { get; set; }
        public Guid? IdCanjeProductoProveedor { get; set; }
        public Guid? IdEstadoProducto { get; set; }
        public Guid? IdFacturaProveedor { get; set; }
        public Guid? IdMarca { get; set; }
        public Guid? IdProcedencia { get; set; }
        public Guid? IdUnidadEntrega { get; set; }
        public Guid? IdCategoria { get; set; }
        public Guid? IdGrupoProducto { get; set; }
        public Guid? IdEstado { get; set; }
        public bool EsMedicina { get; set; }
        public string NombreProducto { get; set; }
        public string DetalleProducto { get; set; }
        public decimal CostoAdquisicion { get; set; }
        public int CantidadVendidos { get; set; }
        public int CantidadExistentes { get; set; }
        public DateTime? FechaCaducidad { get; set; }
        public string RegistroSanitario { get; set; }
        public bool TieneIva { get; set; }
        public int? CantidadInservible { get; set; }
        public string NombreCorto { get; set; }
        public string CodigoBarras { get; set; }
        public string CodigoPersonalizado { get; set; }
        public string UrlImagenProducto { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual CanjeProductoProveedor IdCanjeProductoProveedorNavigation { get; set; }
        public virtual CategoriaProducto IdCategoriaNavigation { get; set; }
        public virtual EstadoProducto IdEstadoProductoNavigation { get; set; }
        public virtual GrupoProducto IdGrupoProductoNavigation { get; set; }
        public virtual Marca IdMarcaNavigation { get; set; }
        public virtual UnidadEntrega IdUnidadEntregaNavigation { get; set; }
        public virtual ICollection<PrecioTipoVentaProducto> PrecioTipoVentaProductos { get; set; }
        public virtual ICollection<ProductoImpuesto> ProductoImpuestos { get; set; }
        public virtual ICollection<ProductoStock> ProductoStocks { get; set; }
        public virtual ICollection<UnidadProducto> UnidadProductos { get; set; }
    }
}
