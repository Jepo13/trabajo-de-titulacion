﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class UnidadEntrega
    {
        public UnidadEntrega()
        {
            CatalogoProductos = new HashSet<CatalogoProducto>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdUnidadentrega { get; set; }
        public Guid IdEmpresa { get; set; }
        public string NombreContenido { get; set; }
        public string CodigoUnidad { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public bool EstadoUnidad { get; set; }
        public string DescripcionMedida { get; set; }

        public virtual ICollection<CatalogoProducto> CatalogoProductos { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
