﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class UnidadProducto
    {
        public Guid IdUnidadProducto { get; set; }
        public Guid IdProducto { get; set; }
        public string MedidaSuperior { get; set; }
        public int CantidadSuperior { get; set; }
        public string CodigoBarrasSuperior { get; set; }
        public string MedidaInferior { get; set; }
        public decimal CantidadInferior { get; set; }
        public string CodigoBarrasInferior { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Producto IdProductoNavigation { get; set; }
    }
}
