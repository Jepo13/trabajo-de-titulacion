﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class ProductoImpuesto
    {
        public Guid IdProductoImpuesto { get; set; }
        public Guid? IdImpuesto { get; set; }
        public Guid? IdProducto { get; set; }

        public virtual Impuesto IdImpuestoNavigation { get; set; }
        public virtual Producto IdProductoNavigation { get; set; }
    }
}
