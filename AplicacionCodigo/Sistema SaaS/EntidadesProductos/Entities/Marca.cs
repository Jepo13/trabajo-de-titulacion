﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class Marca
    {
        public Marca()
        {
            CatalogoProductos = new HashSet<CatalogoProducto>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdMarca { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<CatalogoProducto> CatalogoProductos { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
