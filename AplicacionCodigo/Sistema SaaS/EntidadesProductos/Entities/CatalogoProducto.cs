﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class CatalogoProducto
    {
        public CatalogoProducto()
        {
            CatalogoProductoImpuestos = new HashSet<CatalogoProductoImpuesto>();
            PrecioTipoVentaCatalogos = new HashSet<PrecioTipoVentaCatalogo>();
            UnidadCatalogoProductos = new HashSet<UnidadCatalogoProducto>();
        }

        public Guid IdCatalogoProducto { get; set; }
        public Guid? IdPorcentajeIva { get; set; }
        public Guid? IdCategoria { get; set; }
        public Guid? IdUnidadEntrega { get; set; }
        public Guid? IdGrupoProducto { get; set; }
        public Guid? IdMarca { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdPersona { get; set; }
        public Guid? IdProveedor { get; set; }
        public string NombreProducto { get; set; }
        public string DetalleCatalogoProducto { get; set; }
        public decimal CostoUltimaAdquisicion { get; set; }
        public string ImagenProducto { get; set; }
        public bool TieneIva { get; set; }
        public DateTime? FechaCaducidad { get; set; }
        public int CantidadMaxima { get; set; }
        public int CantidadMinima { get; set; }
        public string NombreCorto { get; set; }
        public byte[] CodigoPersonalizado { get; set; }
        public string UrlImagenCatalogoProducto { get; set; }
        public bool Estado { get; set; }
        public Guid? IdUsuarioCreacion { get; set; }
        public Guid? IdUsuarioModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual CategoriaProducto IdCategoriaNavigation { get; set; }
        public virtual GrupoProducto IdGrupoProductoNavigation { get; set; }
        public virtual Marca IdMarcaNavigation { get; set; }
        public virtual UnidadEntrega IdUnidadEntregaNavigation { get; set; }
        public virtual ICollection<CatalogoProductoImpuesto> CatalogoProductoImpuestos { get; set; }
        public virtual ICollection<PrecioTipoVentaCatalogo> PrecioTipoVentaCatalogos { get; set; }
        public virtual ICollection<UnidadCatalogoProducto> UnidadCatalogoProductos { get; set; }
    }
}
