﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class PrecioTipoVentaProducto
    {
        public Guid IdPrecioTipoVentaProducto { get; set; }
        public Guid? IdProducto { get; set; }
        public Guid IdTipoClienteCompra { get; set; }
        public decimal? PorcentajeUtilidad { get; set; }
        public decimal? Valor { get; set; }
        public decimal? PorcentajeDescuento { get; set; }
        public decimal ValorCompraProducto { get; set; }

        public virtual Producto IdProductoNavigation { get; set; }
    }
}
