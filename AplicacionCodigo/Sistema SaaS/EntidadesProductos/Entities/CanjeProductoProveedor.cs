﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProductos.Entities
{
    public partial class CanjeProductoProveedor
    {
        public CanjeProductoProveedor()
        {
            Productos = new HashSet<Producto>();
        }

        public Guid IdCanjeProductoProveedor { get; set; }
        public Guid? IdProveedor { get; set; }
        public DateTime FechaCanje { get; set; }
        public string Descripcion { get; set; }
        public string IdentificadorCanjeProducto { get; set; }
        public byte[] ImagenCanje { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<Producto> Productos { get; set; }
    }
}
