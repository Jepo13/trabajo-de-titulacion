﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Detallefactura
    {
        public Guid IdDetallefactura { get; set; }
        public Guid? IdFacturaventa { get; set; }
        public Guid? IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal Preciounitarioventa { get; set; }
        public decimal Preciounitarioadquisicion { get; set; }
        public decimal Valorivaventa { get; set; }
        public decimal Valorivacompra { get; set; }
        public int Porcentajeivaventa { get; set; }

        public virtual Facturaventum IdFacturaventaNavigation { get; set; }
        public virtual Producto IdProductoNavigation { get; set; }
    }
}
