﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Detallefacturacompra
    {
        public Guid IdDetallefacturacompra { get; set; }
        public Guid? IdFacturaproveedor { get; set; }
        public int Cantidad { get; set; }
        public decimal Costounitariocompra { get; set; }
        public decimal Valorivacompra { get; set; }
        public int Porcentajeivaventa { get; set; }

        public virtual Facturaproveedor IdFacturaproveedorNavigation { get; set; }
    }
}
