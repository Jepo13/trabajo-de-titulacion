﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Menu
    {
        public Menu()
        {
            Permisos = new HashSet<Permiso>();
        }

        public Guid IdMenu { get; set; }
        public Guid? IdModulo { get; set; }
        public string Nombremenu { get; set; }
        public string Rutamenu { get; set; }

        public virtual Modulo IdModuloNavigation { get; set; }
        public virtual ICollection<Permiso> Permisos { get; set; }
    }
}
