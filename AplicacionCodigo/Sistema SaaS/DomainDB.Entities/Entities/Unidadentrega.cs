﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Unidadentrega
    {
        public Unidadentrega()
        {
            Catalogoproductos = new HashSet<Catalogoproducto>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdUnidadentrega { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombrecontenido { get; set; }
        public string Codigounidad { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual ICollection<Catalogoproducto> Catalogoproductos { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
