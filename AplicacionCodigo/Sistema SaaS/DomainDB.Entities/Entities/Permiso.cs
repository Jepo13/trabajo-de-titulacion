﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Permiso
    {
        public Guid IdPermisos { get; set; }
        public Guid? IdMenu { get; set; }
        public string Nombrepermiso { get; set; }
        public bool Concedido { get; set; }
        public string Csspermiso { get; set; }

        public virtual Menu IdMenuNavigation { get; set; }
    }
}
