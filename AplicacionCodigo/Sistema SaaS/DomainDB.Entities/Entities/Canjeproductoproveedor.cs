﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Canjeproductoproveedor
    {
        public Canjeproductoproveedor()
        {
            Productos = new HashSet<Producto>();
        }

        public Guid IdCanjeproductoproveedor { get; set; }
        public Guid? IdProveedor { get; set; }
        public DateTime Fechacanje { get; set; }
        public string Descripcion { get; set; }
        public string Identificadorcanjeproducto { get; set; }
        public byte[] Imagencanje { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Proveedor IdProveedorNavigation { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
