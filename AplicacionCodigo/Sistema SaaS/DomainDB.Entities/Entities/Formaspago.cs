﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Formaspago
    {
        public Formaspago()
        {
            Facturaformaspagos = new HashSet<Facturaformaspago>();
        }

        public Guid IdFormapago { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual ICollection<Facturaformaspago> Facturaformaspagos { get; set; }
    }
}
