﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Estadoproducto
    {
        public Estadoproducto()
        {
            Productos = new HashSet<Producto>();
        }

        public Guid IdEstadoproducto { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
