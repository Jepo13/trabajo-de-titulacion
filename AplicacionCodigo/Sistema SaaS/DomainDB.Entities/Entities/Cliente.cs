﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Cliente
    {
        public Cliente()
        {
            Facturaventa = new HashSet<Facturaventum>();
            Notacreditos = new HashSet<Notacredito>();
        }

        public Guid IdCliente { get; set; }
        public Guid? IdPersona { get; set; }
        public decimal Valorcupocreditopersonal { get; set; }
        public decimal? Valorcupodisponiblecreditopersonal { get; set; }
        public int Porcentajedescuento { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Persona IdPersonaNavigation { get; set; }
        public virtual ICollection<Facturaventum> Facturaventa { get; set; }
        public virtual ICollection<Notacredito> Notacreditos { get; set; }
    }
}
