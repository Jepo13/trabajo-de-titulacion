﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Local
    {
        public Local()
        {
            Bodegas = new HashSet<Bodega>();
            Cajas = new HashSet<Caja>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdLocal { get; set; }
        public Guid? IdUbicacion { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid IdTipolocal { get; set; }
        public string Nombrelocal { get; set; }
        public string Direccionlocal { get; set; }
        public string Telefono { get; set; }
        public string Observaciones { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual Catalogo IdTipolocalNavigation { get; set; }
        public virtual Catalogo IdUbicacionNavigation { get; set; }
        public virtual ICollection<Bodega> Bodegas { get; set; }
        public virtual ICollection<Caja> Cajas { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
