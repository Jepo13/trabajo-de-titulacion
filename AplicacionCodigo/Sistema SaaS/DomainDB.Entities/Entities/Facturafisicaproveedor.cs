﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Facturafisicaproveedor
    {
        public Facturafisicaproveedor()
        {
            Facturaproveedors = new HashSet<Facturaproveedor>();
        }

        public Guid IdFacturafisicaproveedor { get; set; }
        public byte[] Imagenfacturafisica { get; set; }

        public virtual ICollection<Facturaproveedor> Facturaproveedors { get; set; }
    }
}
