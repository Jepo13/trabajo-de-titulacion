﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Catalogo
    {
        public Catalogo()
        {
            Facturaproveedors = new HashSet<Facturaproveedor>();
            InverseIdCatalogopadreNavigation = new HashSet<Catalogo>();
            LocalIdTipolocalNavigations = new HashSet<Local>();
            LocalIdUbicacionNavigations = new HashSet<Local>();
            Nota = new HashSet<Nota>();
            PersonaIdEstadoCivilNavigations = new HashSet<Persona>();
            PersonaIdLugarnacimientoNavigations = new HashSet<Persona>();
            PersonaIdPaisorigenNavigations = new HashSet<Persona>();
            PersonaIdResidenciaNavigations = new HashSet<Persona>();
            PersonaIdTipoIdentificacionNavigations = new HashSet<Persona>();
            ProductoIdEstadoNavigations = new HashSet<Producto>();
            ProductoIdProcedenciaNavigations = new HashSet<Producto>();
            ProveedorIdEspecialidadNavigations = new HashSet<Proveedor>();
            ProveedorIdTipocontribuyenteNavigations = new HashSet<Proveedor>();
        }

        public Guid IdCatalogo { get; set; }
        public Guid? IdCatalogopadre { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombrecatalogo { get; set; }
        public string Codigocatalogo { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public string Datoadicional { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Catalogo IdCatalogopadreNavigation { get; set; }
        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual ICollection<Facturaproveedor> Facturaproveedors { get; set; }
        public virtual ICollection<Catalogo> InverseIdCatalogopadreNavigation { get; set; }
        public virtual ICollection<Local> LocalIdTipolocalNavigations { get; set; }
        public virtual ICollection<Local> LocalIdUbicacionNavigations { get; set; }
        public virtual ICollection<Nota> Nota { get; set; }
        public virtual ICollection<Persona> PersonaIdEstadoCivilNavigations { get; set; }
        public virtual ICollection<Persona> PersonaIdLugarnacimientoNavigations { get; set; }
        public virtual ICollection<Persona> PersonaIdPaisorigenNavigations { get; set; }
        public virtual ICollection<Persona> PersonaIdResidenciaNavigations { get; set; }
        public virtual ICollection<Persona> PersonaIdTipoIdentificacionNavigations { get; set; }
        public virtual ICollection<Producto> ProductoIdEstadoNavigations { get; set; }
        public virtual ICollection<Producto> ProductoIdProcedenciaNavigations { get; set; }
        public virtual ICollection<Proveedor> ProveedorIdEspecialidadNavigations { get; set; }
        public virtual ICollection<Proveedor> ProveedorIdTipocontribuyenteNavigations { get; set; }
    }
}
