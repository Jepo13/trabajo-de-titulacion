﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Nota
    {
        public Guid IdNotas { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdImportancia { get; set; }
        public string Titulonota { get; set; }
        public string Contenidonota { get; set; }
        public bool Estado { get; set; }
        public DateTime? Fechavencimiento { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual Catalogo IdImportanciaNavigation { get; set; }
    }
}
