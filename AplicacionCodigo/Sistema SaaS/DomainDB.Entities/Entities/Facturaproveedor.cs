﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Facturaproveedor
    {
        public Facturaproveedor()
        {
            Detallefacturacompras = new HashSet<Detallefacturacompra>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdFacturaproveedor { get; set; }
        public Guid? IdFacturafisicaproveedor { get; set; }
        public Guid? IdFacturaelectronicaproveedor { get; set; }
        public Guid? IdProveedor { get; set; }
        public string Numerofactura { get; set; }
        public decimal Valortotal { get; set; }
        public bool Facturapagada { get; set; }
        public string Tipofactura { get; set; }
        public DateTime Fechaingresofactura { get; set; }
        public DateTime? Fechapago { get; set; }
        public string Numeroguia { get; set; }
        public Guid? IdEstado { get; set; }
        public Guid? IdUsuariocreacion { get; set; }
        public Guid? IdUsuariomodificacion { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }

        public virtual Catalogo IdEstadoNavigation { get; set; }
        public virtual Facturaelectronicaproveedor IdFacturaelectronicaproveedorNavigation { get; set; }
        public virtual Facturafisicaproveedor IdFacturafisicaproveedorNavigation { get; set; }
        public virtual Proveedor IdProveedorNavigation { get; set; }
        public virtual Usuario IdUsuariocreacionNavigation { get; set; }
        public virtual Usuario IdUsuariomodificacionNavigation { get; set; }
        public virtual ICollection<Detallefacturacompra> Detallefacturacompras { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
