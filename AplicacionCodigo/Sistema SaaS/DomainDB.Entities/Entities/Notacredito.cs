﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Notacredito
    {
        public Notacredito()
        {
            Detallenotacreditos = new HashSet<Detallenotacredito>();
        }

        public Guid IdNotacredito { get; set; }
        public Guid? IdFacturaventa { get; set; }
        public Guid? IdUsuariocreacion { get; set; }
        public Guid? IdCliente { get; set; }
        public Guid? IdUsuariomodificacion { get; set; }
        public decimal Valorivanotacredito { get; set; }
        public int? Porcentajeiva { get; set; }
        public decimal? Subtotalnotacreditoiva { get; set; }
        public decimal? Subtotalnotacreditosiniva { get; set; }
        public decimal Totalnotacredito { get; set; }
        public decimal Valordisponible { get; set; }
        public DateTime Fechacaducidad { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual Facturaventum IdFacturaventaNavigation { get; set; }
        public virtual Usuario IdUsuariocreacionNavigation { get; set; }
        public virtual Usuario IdUsuariomodificacionNavigation { get; set; }
        public virtual ICollection<Detallenotacredito> Detallenotacreditos { get; set; }
    }
}
