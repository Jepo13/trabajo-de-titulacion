﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Bodega
    {
        public Bodega()
        {
            Productos = new HashSet<Producto>();
        }

        public Guid IdBodega { get; set; }
        public Guid? IdLocal { get; set; }
        public string Nombrebodega { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
        public string Descripcion { get; set; }

        public virtual Local IdLocalNavigation { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
