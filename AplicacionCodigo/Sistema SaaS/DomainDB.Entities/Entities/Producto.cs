﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Producto
    {
        public Producto()
        {
            Detallefacturas = new HashSet<Detallefactura>();
            Detallenotacreditos = new HashSet<Detallenotacredito>();
            Productoimpuestos = new HashSet<Productoimpuesto>();
        }

        public Guid IdProducto { get; set; }
        public Guid? IdCanjeproductoproveedor { get; set; }
        public Guid? IdEstadoproducto { get; set; }
        public Guid? IdLocal { get; set; }
        public Guid? IdFacturaproveedor { get; set; }
        public Guid? IdPersona { get; set; }
        public Guid? IdMarca { get; set; }
        public string Nombreproducto { get; set; }
        public string Detalleproducto { get; set; }
        public decimal Costoadquisicion { get; set; }
        public decimal Precioventapublico { get; set; }
        public byte[] Imagenproducto { get; set; }
        public int Cantidadvendidos { get; set; }
        public int Cantidadexistentes { get; set; }
        public string Codigoproducto { get; set; }
        public DateTime? Fechacaducidad { get; set; }
        public bool Tieneiva { get; set; }
        public int? Cantidadinservible { get; set; }
        public Guid? IdProcedencia { get; set; }
        public Guid? IdUnidadentrega { get; set; }
        public Guid? IdCategoria { get; set; }
        public Guid? IdGrupoproducto { get; set; }
        public Guid? IdBodega { get; set; }
        public short? Anaquel { get; set; }
        public string Hilera { get; set; }
        public string Casillero { get; set; }
        public Guid? IdEstado { get; set; }
        public Guid? IdUsuariocreacion { get; set; }
        public Guid? IdUsuariomodificacion { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }

        public virtual Bodega IdBodegaNavigation { get; set; }
        public virtual Canjeproductoproveedor IdCanjeproductoproveedorNavigation { get; set; }
        public virtual Categorium IdCategoriaNavigation { get; set; }
        public virtual Catalogo IdEstadoNavigation { get; set; }
        public virtual Estadoproducto IdEstadoproductoNavigation { get; set; }
        public virtual Facturaproveedor IdFacturaproveedorNavigation { get; set; }
        public virtual Grupoproducto IdGrupoproductoNavigation { get; set; }
        public virtual Local IdLocalNavigation { get; set; }
        public virtual Marca IdMarcaNavigation { get; set; }
        public virtual Persona IdPersonaNavigation { get; set; }
        public virtual Catalogo IdProcedenciaNavigation { get; set; }
        public virtual Unidadentrega IdUnidadentregaNavigation { get; set; }
        public virtual Usuario IdUsuariocreacionNavigation { get; set; }
        public virtual Usuario IdUsuariomodificacionNavigation { get; set; }
        public virtual ICollection<Detallefactura> Detallefacturas { get; set; }
        public virtual ICollection<Detallenotacredito> Detallenotacreditos { get; set; }
        public virtual ICollection<Productoimpuesto> Productoimpuestos { get; set; }
    }
}
