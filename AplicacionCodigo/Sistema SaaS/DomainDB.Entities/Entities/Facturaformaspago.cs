﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Facturaformaspago
    {
        public Guid IdFacturaformapago { get; set; }
        public Guid? IdFormapago { get; set; }
        public Guid? IdFacturaventa { get; set; }
        public decimal Valorpago { get; set; }
        public decimal? Cambio { get; set; }
        public DateTime? Fechamaximapago { get; set; }
        public bool Pagada { get; set; }
        public decimal Valorabonopagocreditopersonal { get; set; }

        public virtual Facturaventum IdFacturaventaNavigation { get; set; }
        public virtual Formaspago IdFormapagoNavigation { get; set; }
    }
}
