﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Proveedor
    {
        public Proveedor()
        {
            Canjeproductoproveedors = new HashSet<Canjeproductoproveedor>();
            Catalogoproductos = new HashSet<Catalogoproducto>();
            Facturaproveedors = new HashSet<Facturaproveedor>();
        }

        public Guid IdProveedor { get; set; }
        public Guid? IdPorcenajeiva { get; set; }
        public Guid? IdTipocontribuyente { get; set; }
        public Guid? IdEspecialidad { get; set; }
        public Guid? IdEmpresa { get; set; }
        public bool Contribuyenteespecial { get; set; }
        public string Razonsocial { get; set; }
        public string Nombrecomercial { get; set; }
        public string Ruc { get; set; }
        public string Direccion { get; set; }
        public string Telefonoproveedor { get; set; }
        public string Telefonomovil { get; set; }
        public string Correoelectronico { get; set; }
        public bool Obligadollevarcontabilidad { get; set; }
        public string Nombrerepresentate { get; set; }
        public string Nombrecontacto { get; set; }
        public string Telefonocontacto { get; set; }
        public string Codigoproveedor { get; set; }
        public string Nota { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual Catalogo IdEspecialidadNavigation { get; set; }
        public virtual Porcentajeiva IdPorcenajeivaNavigation { get; set; }
        public virtual Catalogo IdTipocontribuyenteNavigation { get; set; }
        public virtual ICollection<Canjeproductoproveedor> Canjeproductoproveedors { get; set; }
        public virtual ICollection<Catalogoproducto> Catalogoproductos { get; set; }
        public virtual ICollection<Facturaproveedor> Facturaproveedors { get; set; }
    }
}
