﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Persona
    {
        public Persona()
        {
            Catalogoproductos = new HashSet<Catalogoproducto>();
            Clientes = new HashSet<Cliente>();
            Controlcajas = new HashSet<Controlcaja>();
            Productos = new HashSet<Producto>();
            Usuarios = new HashSet<Usuario>();
        }

        public Guid IdPersona { get; set; }
        public Guid? IdLugarnacimiento { get; set; }
        public Guid? IdResidencia { get; set; }
        public Guid? IdPaisorigen { get; set; }
        public Guid? IdTipoIdentificacion { get; set; }
        public Guid? IdEstadoCivil { get; set; }
        public byte[] Imagenpersona { get; set; }
        public string Formatoarchivo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Numeroidentificacion { get; set; }
        public DateTime? Fechanacimiento { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Contactoemergencia { get; set; }
        public string Direccion { get; set; }
        public string Correopersonal { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Catalogo IdEstadoCivilNavigation { get; set; }
        public virtual Catalogo IdLugarnacimientoNavigation { get; set; }
        public virtual Catalogo IdPaisorigenNavigation { get; set; }
        public virtual Catalogo IdResidenciaNavigation { get; set; }
        public virtual Catalogo IdTipoIdentificacionNavigation { get; set; }
        public virtual ICollection<Catalogoproducto> Catalogoproductos { get; set; }
        public virtual ICollection<Cliente> Clientes { get; set; }
        public virtual ICollection<Controlcaja> Controlcajas { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
