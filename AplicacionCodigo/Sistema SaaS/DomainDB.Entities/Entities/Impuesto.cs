﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Impuesto
    {
        public Impuesto()
        {
            Catalogoproductoimpuestos = new HashSet<Catalogoproductoimpuesto>();
            Productoimpuestos = new HashSet<Productoimpuesto>();
        }

        public Guid IdImpuesto { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombreimpuesto { get; set; }
        public decimal Valor { get; set; }
        public string Tipoimpuesto { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual ICollection<Catalogoproductoimpuesto> Catalogoproductoimpuestos { get; set; }
        public virtual ICollection<Productoimpuesto> Productoimpuestos { get; set; }
    }
}
