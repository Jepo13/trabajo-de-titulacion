﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Facturaelectronicaproveedor
    {
        public Facturaelectronicaproveedor()
        {
            Facturaproveedors = new HashSet<Facturaproveedor>();
        }

        public Guid IdFacturaelectronicaproveedor { get; set; }
        public string Xmlfactura { get; set; }
        public string Estado { get; set; }
        public string Numeroautorizacion { get; set; }
        public string Ambiente { get; set; }
        public DateTime? Fechaautorizacion { get; set; }

        public virtual ICollection<Facturaproveedor> Facturaproveedors { get; set; }
    }
}
