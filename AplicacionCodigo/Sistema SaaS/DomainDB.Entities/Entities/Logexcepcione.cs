﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Logexcepcione
    {
        public Guid IdLogexcepciones { get; set; }
        public Guid? IdUsuario { get; set; }
        public string Metodo { get; set; }
        public string Entidad { get; set; }
        public string Error { get; set; }
        public DateTime Fechaerror { get; set; }
        public string Descripcion { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
