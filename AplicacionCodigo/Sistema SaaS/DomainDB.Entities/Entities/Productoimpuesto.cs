﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Productoimpuesto
    {
        public Guid IdProductoimpuesto { get; set; }
        public Guid? IdImpuesto { get; set; }
        public Guid? IdProducto { get; set; }

        public virtual Impuesto IdImpuestoNavigation { get; set; }
        public virtual Producto IdProductoNavigation { get; set; }
    }
}
