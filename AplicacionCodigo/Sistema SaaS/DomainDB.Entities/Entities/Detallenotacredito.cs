﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Detallenotacredito
    {
        public Guid IdDetallenotacredito { get; set; }
        public Guid? IdNotacredito { get; set; }
        public Guid? IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal Preciounitario { get; set; }
        public decimal Valoriva { get; set; }
        public string Descripcion { get; set; }

        public virtual Notacredito IdNotacreditoNavigation { get; set; }
        public virtual Producto IdProductoNavigation { get; set; }
    }
}
