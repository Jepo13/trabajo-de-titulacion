﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Modulo
    {
        public Modulo()
        {
            Menus = new HashSet<Menu>();
        }

        public Guid IdModulo { get; set; }
        public Guid? IdRol { get; set; }
        public string Nombre { get; set; }

        public virtual Rol IdRolNavigation { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
    }
}
