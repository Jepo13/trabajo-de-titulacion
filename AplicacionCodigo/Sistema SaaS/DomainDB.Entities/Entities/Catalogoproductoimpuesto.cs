﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Catalogoproductoimpuesto
    {
        public Guid IdCatalogoproductoimpuesto { get; set; }
        public Guid? IdImpuesto { get; set; }
        public Guid? IdCatalogoproducto { get; set; }

        public virtual Catalogoproducto IdCatalogoproductoNavigation { get; set; }
        public virtual Impuesto IdImpuestoNavigation { get; set; }
    }
}
