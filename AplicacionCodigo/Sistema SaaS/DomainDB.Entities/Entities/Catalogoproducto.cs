﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Catalogoproducto
    {
        public Catalogoproducto()
        {
            Catalogoproductoimpuestos = new HashSet<Catalogoproductoimpuesto>();
        }

        public Guid IdCatalogoproducto { get; set; }
        public Guid? IdPorcentajeiva { get; set; }
        public Guid? IdCategoria { get; set; }
        public string Nombreproducto { get; set; }
        public DateTime Fechacreacion { get; set; }
        public string Detallecatalogoproducto { get; set; }
        public decimal Costoultimaadquisicion { get; set; }
        public decimal Precioventapublico { get; set; }
        public string Imagenproducto { get; set; }
        public bool Tieneiva { get; set; }
        public DateTime? Fechacaducidad { get; set; }
        public int Cantidadmaxima { get; set; }
        public int Cantidadminima { get; set; }
        public Guid? IdUsuariocreacion { get; set; }
        public Guid? IdUnidadentrega { get; set; }
        public Guid? IdUsuariomodificacion { get; set; }
        public Guid? IdGrupoproducto { get; set; }
        public Guid? IdMarca { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdPersona { get; set; }
        public Guid? IdProveedor { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public bool Estado { get; set; }

        public virtual Categorium IdCategoriaNavigation { get; set; }
        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual Grupoproducto IdGrupoproductoNavigation { get; set; }
        public virtual Marca IdMarcaNavigation { get; set; }
        public virtual Persona IdPersonaNavigation { get; set; }
        public virtual Porcentajeiva IdPorcentajeivaNavigation { get; set; }
        public virtual Proveedor IdProveedorNavigation { get; set; }
        public virtual Unidadentrega IdUnidadentregaNavigation { get; set; }
        public virtual Usuario IdUsuariocreacionNavigation { get; set; }
        public virtual Usuario IdUsuariomodificacionNavigation { get; set; }
        public virtual ICollection<Catalogoproductoimpuesto> Catalogoproductoimpuestos { get; set; }
    }
}
