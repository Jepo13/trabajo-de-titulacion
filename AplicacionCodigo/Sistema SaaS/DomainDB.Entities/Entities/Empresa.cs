﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Empresa
    {
        public Empresa()
        {
            Catalogoproductos = new HashSet<Catalogoproducto>();
            Catalogos = new HashSet<Catalogo>();
            Categoria = new HashSet<Categorium>();
            Estadoproductos = new HashSet<Estadoproducto>();
            Grupoproductos = new HashSet<Grupoproducto>();
            Impuestos = new HashSet<Impuesto>();
            Locals = new HashSet<Local>();
            Marcas = new HashSet<Marca>();
            Nota = new HashSet<Nota>();
            Proveedors = new HashSet<Proveedor>();
            Rols = new HashSet<Rol>();
            Unidadentregas = new HashSet<Unidadentrega>();
            UsuarioEmpresas = new HashSet<UsuarioEmpresa>();
        }

        public Guid IdEmpresa { get; set; }
        public string Ruc { get; set; }
        public string Razonsocial { get; set; }
        public string Telefono { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual ICollection<Catalogoproducto> Catalogoproductos { get; set; }
        public virtual ICollection<Catalogo> Catalogos { get; set; }
        public virtual ICollection<Categorium> Categoria { get; set; }
        public virtual ICollection<Estadoproducto> Estadoproductos { get; set; }
        public virtual ICollection<Grupoproducto> Grupoproductos { get; set; }
        public virtual ICollection<Impuesto> Impuestos { get; set; }
        public virtual ICollection<Local> Locals { get; set; }
        public virtual ICollection<Marca> Marcas { get; set; }
        public virtual ICollection<Nota> Nota { get; set; }
        public virtual ICollection<Proveedor> Proveedors { get; set; }
        public virtual ICollection<Rol> Rols { get; set; }
        public virtual ICollection<Unidadentrega> Unidadentregas { get; set; }
        public virtual ICollection<UsuarioEmpresa> UsuarioEmpresas { get; set; }
    }
}
