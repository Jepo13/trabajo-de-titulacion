﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Facturaventum
    {
        public Facturaventum()
        {
            Detallefacturas = new HashSet<Detallefactura>();
            Facturaformaspagos = new HashSet<Facturaformaspago>();
            Notacreditos = new HashSet<Notacredito>();
        }

        public Guid IdFacturaventa { get; set; }
        public Guid? IdCliente { get; set; }
        public Guid? IdUsuariocreacion { get; set; }
        public Guid? IdControlcaja { get; set; }
        public Guid? IdUsuariomodificacion { get; set; }
        public Guid? IdEstadoventa { get; set; }
        public decimal? Subtotalfacturaiva { get; set; }
        public decimal? Subtotalfacturasiniva { get; set; }
        public decimal Valorivafactura { get; set; }
        public int Porcentajeiva { get; set; }
        public decimal Totalfactura { get; set; }
        public string Estadofactura { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual Controlcaja IdControlcajaNavigation { get; set; }
        public virtual Estadoventum IdEstadoventaNavigation { get; set; }
        public virtual Usuario IdUsuariocreacionNavigation { get; set; }
        public virtual Usuario IdUsuariomodificacionNavigation { get; set; }
        public virtual ICollection<Detallefactura> Detallefacturas { get; set; }
        public virtual ICollection<Facturaformaspago> Facturaformaspagos { get; set; }
        public virtual ICollection<Notacredito> Notacreditos { get; set; }
    }
}
