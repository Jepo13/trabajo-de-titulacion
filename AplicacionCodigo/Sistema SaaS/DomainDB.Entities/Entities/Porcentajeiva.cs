﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Porcentajeiva
    {
        public Porcentajeiva()
        {
            Catalogoproductos = new HashSet<Catalogoproducto>();
            Proveedors = new HashSet<Proveedor>();
        }

        public Guid IdPorcenajeiva { get; set; }
        public decimal Valor { get; set; }
        public bool Activo { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual ICollection<Catalogoproducto> Catalogoproductos { get; set; }
        public virtual ICollection<Proveedor> Proveedors { get; set; }
    }
}
