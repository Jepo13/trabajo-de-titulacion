﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Usuario
    {
        public Usuario()
        {
            CatalogoproductoIdUsuariocreacionNavigations = new HashSet<Catalogoproducto>();
            CatalogoproductoIdUsuariomodificacionNavigations = new HashSet<Catalogoproducto>();
            Controlcajas = new HashSet<Controlcaja>();
            FacturaproveedorIdUsuariocreacionNavigations = new HashSet<Facturaproveedor>();
            FacturaproveedorIdUsuariomodificacionNavigations = new HashSet<Facturaproveedor>();
            FacturaventumIdUsuariocreacionNavigations = new HashSet<Facturaventum>();
            FacturaventumIdUsuariomodificacionNavigations = new HashSet<Facturaventum>();
            Logexcepciones = new HashSet<Logexcepcione>();
            NotacreditoIdUsuariocreacionNavigations = new HashSet<Notacredito>();
            NotacreditoIdUsuariomodificacionNavigations = new HashSet<Notacredito>();
            ProductoIdUsuariocreacionNavigations = new HashSet<Producto>();
            ProductoIdUsuariomodificacionNavigations = new HashSet<Producto>();
            UsuarioEmpresas = new HashSet<UsuarioEmpresa>();
        }

        public Guid IdUsuario { get; set; }
        public Guid? IdPersona { get; set; }
        public Guid? IdRol { get; set; }
        public Guid? IdEmpresadefault { get; set; }
        public bool? Estado { get; set; }
        public string Correoinstitucional { get; set; }
        public string Contrasena { get; set; }
        public string Indiciocontrasena { get; set; }
        public DateTime? Fechaultimoingreso { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Persona IdPersonaNavigation { get; set; }
        public virtual Rol IdRolNavigation { get; set; }
        public virtual ICollection<Catalogoproducto> CatalogoproductoIdUsuariocreacionNavigations { get; set; }
        public virtual ICollection<Catalogoproducto> CatalogoproductoIdUsuariomodificacionNavigations { get; set; }
        public virtual ICollection<Controlcaja> Controlcajas { get; set; }
        public virtual ICollection<Facturaproveedor> FacturaproveedorIdUsuariocreacionNavigations { get; set; }
        public virtual ICollection<Facturaproveedor> FacturaproveedorIdUsuariomodificacionNavigations { get; set; }
        public virtual ICollection<Facturaventum> FacturaventumIdUsuariocreacionNavigations { get; set; }
        public virtual ICollection<Facturaventum> FacturaventumIdUsuariomodificacionNavigations { get; set; }
        public virtual ICollection<Logexcepcione> Logexcepciones { get; set; }
        public virtual ICollection<Notacredito> NotacreditoIdUsuariocreacionNavigations { get; set; }
        public virtual ICollection<Notacredito> NotacreditoIdUsuariomodificacionNavigations { get; set; }
        public virtual ICollection<Producto> ProductoIdUsuariocreacionNavigations { get; set; }
        public virtual ICollection<Producto> ProductoIdUsuariomodificacionNavigations { get; set; }
        public virtual ICollection<UsuarioEmpresa> UsuarioEmpresas { get; set; }
    }
}
