﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Estadoventum
    {
        public Estadoventum()
        {
            Facturaventa = new HashSet<Facturaventum>();
        }

        public Guid IdEstadoventa { get; set; }
        public string Nombre { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodificacion { get; set; }

        public virtual ICollection<Facturaventum> Facturaventa { get; set; }
    }
}
