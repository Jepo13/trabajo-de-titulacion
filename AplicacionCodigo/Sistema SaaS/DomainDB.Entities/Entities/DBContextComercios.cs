﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class DBContextComercios : DbContext
    {
        public DBContextComercios()
        {
        }

        public DBContextComercios(DbContextOptions<DBContextComercios> options)
            : base(options)
        {
        }

        public virtual DbSet<Bodega> Bodegas { get; set; }
        public virtual DbSet<Caja> Cajas { get; set; }
        public virtual DbSet<Canjeproductoproveedor> Canjeproductoproveedors { get; set; }
        public virtual DbSet<Catalogo> Catalogos { get; set; }
        public virtual DbSet<Catalogoproducto> Catalogoproductos { get; set; }
        public virtual DbSet<Catalogoproductoimpuesto> Catalogoproductoimpuestos { get; set; }
        public virtual DbSet<Categorium> Categoria { get; set; }
        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Controlcaja> Controlcajas { get; set; }
        public virtual DbSet<Detallefactura> Detallefacturas { get; set; }
        public virtual DbSet<Detallefacturacompra> Detallefacturacompras { get; set; }
        public virtual DbSet<Detallenotacredito> Detallenotacreditos { get; set; }
        public virtual DbSet<Empresa> Empresas { get; set; }
        public virtual DbSet<Estadoproducto> Estadoproductos { get; set; }
        public virtual DbSet<Estadoventum> Estadoventa { get; set; }
        public virtual DbSet<Facturaelectronicaproveedor> Facturaelectronicaproveedors { get; set; }
        public virtual DbSet<Facturafisicaproveedor> Facturafisicaproveedors { get; set; }
        public virtual DbSet<Facturaformaspago> Facturaformaspagos { get; set; }
        public virtual DbSet<Facturaproveedor> Facturaproveedors { get; set; }
        public virtual DbSet<Facturaventum> Facturaventa { get; set; }
        public virtual DbSet<Formaspago> Formaspagos { get; set; }
        public virtual DbSet<Grupoproducto> Grupoproductos { get; set; }
        public virtual DbSet<Impuesto> Impuestos { get; set; }
        public virtual DbSet<Local> Locals { get; set; }
        public virtual DbSet<Logexcepcione> Logexcepciones { get; set; }
        public virtual DbSet<Marca> Marcas { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<Modulo> Modulos { get; set; }
        public virtual DbSet<Nota> Notas { get; set; }
        public virtual DbSet<Notacredito> Notacreditos { get; set; }
        public virtual DbSet<Permiso> Permisos { get; set; }
        public virtual DbSet<Persona> Personas { get; set; }
        public virtual DbSet<Porcentajeiva> Porcentajeivas { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<Productoimpuesto> Productoimpuestos { get; set; }
        public virtual DbSet<Proveedor> Proveedors { get; set; }
        public virtual DbSet<Rol> Rols { get; set; }
        public virtual DbSet<Unidadentrega> Unidadentregas { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<UsuarioEmpresa> UsuarioEmpresas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=sql5101.site4now.net;database=DB_A49FE7_locales;persist security info=True;user id=DB_A49FE7_locales_admin;password=db.x3@.pX;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Bodega>(entity =>
            {
                entity.HasKey(e => e.IdBodega);

                entity.ToTable("BODEGA");

                entity.Property(e => e.IdBodega)
                    .HasColumnName("ID_BODEGA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdLocal).HasColumnName("ID_LOCAL");

                entity.Property(e => e.Nombrebodega)
                    .HasMaxLength(30)
                    .HasColumnName("NOMBREBODEGA");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdLocalNavigation)
                    .WithMany(p => p.Bodegas)
                    .HasForeignKey(d => d.IdLocal)
                    .HasConstraintName("FK_BODEGA_REFERENCE_LOCAL");
            });

            modelBuilder.Entity<Caja>(entity =>
            {
                entity.HasKey(e => e.IdCaja);

                entity.ToTable("CAJA");

                entity.Property(e => e.IdCaja)
                    .HasColumnName("ID_CAJA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdLocal).HasColumnName("ID_LOCAL");

                entity.Property(e => e.Nombrecaja)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("NOMBRECAJA");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION_");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdLocalNavigation)
                    .WithMany(p => p.Cajas)
                    .HasForeignKey(d => d.IdLocal)
                    .HasConstraintName("FK_CAJA_REFERENCE_LOCAL");
            });

            modelBuilder.Entity<Canjeproductoproveedor>(entity =>
            {
                entity.HasKey(e => e.IdCanjeproductoproveedor);

                entity.ToTable("CANJEPRODUCTOPROVEEDOR");

                entity.Property(e => e.IdCanjeproductoproveedor)
                    .HasColumnName("ID_CANJEPRODUCTOPROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Fechacanje)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACANJE");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_PROVEEDOR");

                entity.Property(e => e.Identificadorcanjeproducto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("IDENTIFICADORCANJEPRODUCTO");

                entity.Property(e => e.Imagencanje)
                    .HasColumnType("image")
                    .HasColumnName("IMAGENCANJE");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdProveedorNavigation)
                    .WithMany(p => p.Canjeproductoproveedors)
                    .HasForeignKey(d => d.IdProveedor)
                    .HasConstraintName("FK_CANJEPRO_REFERENCE_PROVEEDO");
            });

            modelBuilder.Entity<Catalogo>(entity =>
            {
                entity.HasKey(e => e.IdCatalogo);

                entity.ToTable("CATALOGO");

                entity.Property(e => e.IdCatalogo)
                    .HasColumnName("ID_CATALOGO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Codigocatalogo)
                    .IsRequired()
                    .HasMaxLength(8)
                    .HasColumnName("CODIGOCATALOGO");

                entity.Property(e => e.Datoadicional)
                    .HasMaxLength(80)
                    .HasColumnName("DATOADICIONAL");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdCatalogopadre).HasColumnName("ID_CATALOGOPADRE");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombrecatalogo)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("NOMBRECATALOGO");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdCatalogopadreNavigation)
                    .WithMany(p => p.InverseIdCatalogopadreNavigation)
                    .HasForeignKey(d => d.IdCatalogopadre)
                    .HasConstraintName("FK_CATALOGO_FK_CATALO_CATALOGO");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Catalogos)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_CATALOGO_FK_C_EMPR_EMPRESA");
            });

            modelBuilder.Entity<Catalogoproducto>(entity =>
            {
                entity.HasKey(e => e.IdCatalogoproducto);

                entity.ToTable("CATALOGOPRODUCTO");

                entity.Property(e => e.IdCatalogoproducto)
                    .HasColumnName("ID_CATALOGOPRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidadmaxima).HasColumnName("CANTIDADMAXIMA");

                entity.Property(e => e.Cantidadminima).HasColumnName("CANTIDADMINIMA");

                entity.Property(e => e.Costoultimaadquisicion)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("COSTOULTIMAADQUISICION");

                entity.Property(e => e.Detallecatalogoproducto).HasColumnName("DETALLECATALOGOPRODUCTO");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacaducidad)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACADUCIDAD");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdCategoria).HasColumnName("ID_CATEGORIA");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdGrupoproducto).HasColumnName("ID_GRUPOPRODUCTO");

                entity.Property(e => e.IdMarca).HasColumnName("ID_MARCA");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.IdPorcentajeiva).HasColumnName("ID_PORCENTAJEIVA");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_PROVEEDOR");

                entity.Property(e => e.IdUnidadentrega).HasColumnName("ID_UNIDADENTREGA");

                entity.Property(e => e.IdUsuariocreacion).HasColumnName("ID_USUARIOCREACION");

                entity.Property(e => e.IdUsuariomodificacion).HasColumnName("ID_USUARIOMODIFICACION");

                entity.Property(e => e.Imagenproducto).HasColumnName("IMAGENPRODUCTO");

                entity.Property(e => e.Nombreproducto)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("NOMBREPRODUCTO");

                entity.Property(e => e.Precioventapublico)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIOVENTAPUBLICO");

                entity.Property(e => e.Tieneiva).HasColumnName("TIENEIVA");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdCategoria)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_CATEGORI");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_CATALOGO_FK_CP_EMP_EMPRESA");

                entity.HasOne(d => d.IdGrupoproductoNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdGrupoproducto)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_GRUPOPRO");

                entity.HasOne(d => d.IdMarcaNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdMarca)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_MARCA");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdPersona)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_PERSONA");

                entity.HasOne(d => d.IdPorcentajeivaNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdPorcentajeiva)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_PORCENTA");

                entity.HasOne(d => d.IdProveedorNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdProveedor)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_PROVEEDO");

                entity.HasOne(d => d.IdUnidadentregaNavigation)
                    .WithMany(p => p.Catalogoproductos)
                    .HasForeignKey(d => d.IdUnidadentrega)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_UNIDADEN");

                entity.HasOne(d => d.IdUsuariocreacionNavigation)
                    .WithMany(p => p.CatalogoproductoIdUsuariocreacionNavigations)
                    .HasForeignKey(d => d.IdUsuariocreacion)
                    .HasConstraintName("FK_CATALOGO_FK_USUARI_USUARIO");

                entity.HasOne(d => d.IdUsuariomodificacionNavigation)
                    .WithMany(p => p.CatalogoproductoIdUsuariomodificacionNavigations)
                    .HasForeignKey(d => d.IdUsuariomodificacion)
                    .HasConstraintName("FK_CATALOGO_PK_USUARI_USUARIO");
            });

            modelBuilder.Entity<Catalogoproductoimpuesto>(entity =>
            {
                entity.HasKey(e => e.IdCatalogoproductoimpuesto);

                entity.ToTable("CATALOGOPRODUCTOIMPUESTO");

                entity.Property(e => e.IdCatalogoproductoimpuesto)
                    .HasColumnName("ID_CATALOGOPRODUCTOIMPUESTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdCatalogoproducto).HasColumnName("ID_CATALOGOPRODUCTO");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_IMPUESTO");

                entity.HasOne(d => d.IdCatalogoproductoNavigation)
                    .WithMany(p => p.Catalogoproductoimpuestos)
                    .HasForeignKey(d => d.IdCatalogoproducto)
                    .HasConstraintName("FK_CATALOGO_FK_C_PROD_CATALOGO");

                entity.HasOne(d => d.IdImpuestoNavigation)
                    .WithMany(p => p.Catalogoproductoimpuestos)
                    .HasForeignKey(d => d.IdImpuesto)
                    .HasConstraintName("FK_CATALOGO_REFERENCE_IMPUESTO");
            });

            modelBuilder.Entity<Categorium>(entity =>
            {
                entity.HasKey(e => e.IdCategoria);

                entity.ToTable("CATEGORIA");

                entity.Property(e => e.IdCategoria)
                    .HasColumnName("ID_CATEGORIA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdCategoriapadre).HasColumnName("ID_CATEGORIAPADRE");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombrecategoria)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("NOMBRECATEGORIA");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdCategoriapadreNavigation)
                    .WithMany(p => p.InverseIdCategoriapadreNavigation)
                    .HasForeignKey(d => d.IdCategoriapadre)
                    .HasConstraintName("FK_CATEGORI_REFERENCE_CATEGORI");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Categoria)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_CATEGORI_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.IdCliente);

                entity.ToTable("CLIENTE");

                entity.Property(e => e.IdCliente)
                    .HasColumnName("ID_CLIENTE")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.Porcentajedescuento).HasColumnName("PORCENTAJEDESCUENTO");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION_");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.Property(e => e.Valorcupocreditopersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORCUPOCREDITOPERSONAL");

                entity.Property(e => e.Valorcupodisponiblecreditopersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORCUPODISPONIBLECREDITOPERSONAL");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.IdPersona)
                    .HasConstraintName("FK_CLIENTE_REFERENCE_PERSONA");
            });

            modelBuilder.Entity<Controlcaja>(entity =>
            {
                entity.HasKey(e => e.IdControlcaja);

                entity.ToTable("CONTROLCAJA");

                entity.Property(e => e.IdControlcaja)
                    .HasColumnName("ID_CONTROLCAJA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidadventascaja).HasColumnName("CANTIDADVENTASCAJA");

                entity.Property(e => e.Fechaapertura)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAAPERTURA");

                entity.Property(e => e.Fechacierre)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACIERRE");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdCaja).HasColumnName("ID_CAJA");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_USUARIO");

                entity.Property(e => e.Observacion)
                    .HasColumnType("text")
                    .HasColumnName("OBSERVACION");

                entity.Property(e => e.Reportecorrecto).HasColumnName("REPORTECORRECTO");

                entity.Property(e => e.Usuariocreacion)
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.Property(e => e.Valorfacturaventaanulada)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORFACTURAVENTAANULADA");

                entity.Property(e => e.Valorinicial)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORINICIAL");

                entity.Property(e => e.Valornotascreditogeneradas)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORNOTASCREDITOGENERADAS");

                entity.Property(e => e.Ventascreditopersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VENTASCREDITOPERSONAL");

                entity.Property(e => e.Ventasefectivo)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VENTASEFECTIVO");

                entity.Property(e => e.Ventasnotacredito)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VENTASNOTACREDITO");

                entity.HasOne(d => d.IdCajaNavigation)
                    .WithMany(p => p.Controlcajas)
                    .HasForeignKey(d => d.IdCaja)
                    .HasConstraintName("FK_CONTROLC_REFERENCE_CAJA");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Controlcajas)
                    .HasForeignKey(d => d.IdPersona)
                    .HasConstraintName("FK_CONTROLC_REFERENCE_PERSONA");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Controlcajas)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_CONTROLC_REFERENCE_USUARIO");
            });

            modelBuilder.Entity<Detallefactura>(entity =>
            {
                entity.HasKey(e => e.IdDetallefactura);

                entity.ToTable("DETALLEFACTURA");

                entity.Property(e => e.IdDetallefactura)
                    .HasColumnName("ID_DETALLEFACTURA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidad).HasColumnName("CANTIDAD");

                entity.Property(e => e.IdFacturaventa).HasColumnName("ID_FACTURAVENTA");

                entity.Property(e => e.IdProducto).HasColumnName("ID_PRODUCTO");

                entity.Property(e => e.Porcentajeivaventa).HasColumnName("PORCENTAJEIVAVENTA");

                entity.Property(e => e.Preciounitarioadquisicion)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIOUNITARIOADQUISICION");

                entity.Property(e => e.Preciounitarioventa)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIOUNITARIOVENTA");

                entity.Property(e => e.Valorivacompra)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORIVACOMPRA");

                entity.Property(e => e.Valorivaventa)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORIVAVENTA");

                entity.HasOne(d => d.IdFacturaventaNavigation)
                    .WithMany(p => p.Detallefacturas)
                    .HasForeignKey(d => d.IdFacturaventa)
                    .HasConstraintName("FK_DETALLEF_REFERENCE_FACTURAV");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Detallefacturas)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_DETALLEF_REFERENCE_PRODUCTO");
            });

            modelBuilder.Entity<Detallefacturacompra>(entity =>
            {
                entity.HasKey(e => e.IdDetallefacturacompra);

                entity.ToTable("DETALLEFACTURACOMPRA");

                entity.Property(e => e.IdDetallefacturacompra)
                    .HasColumnName("ID_DETALLEFACTURACOMPRA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidad).HasColumnName("CANTIDAD");

                entity.Property(e => e.Costounitariocompra)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("COSTOUNITARIOCOMPRA");

                entity.Property(e => e.IdFacturaproveedor).HasColumnName("ID_FACTURAPROVEEDOR");

                entity.Property(e => e.Porcentajeivaventa).HasColumnName("PORCENTAJEIVAVENTA");

                entity.Property(e => e.Valorivacompra)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORIVACOMPRA");

                entity.HasOne(d => d.IdFacturaproveedorNavigation)
                    .WithMany(p => p.Detallefacturacompras)
                    .HasForeignKey(d => d.IdFacturaproveedor)
                    .HasConstraintName("FK_DETALLEF_REFERENCE_FACTURAP");
            });

            modelBuilder.Entity<Detallenotacredito>(entity =>
            {
                entity.HasKey(e => e.IdDetallenotacredito);

                entity.ToTable("DETALLENOTACREDITO");

                entity.Property(e => e.IdDetallenotacredito)
                    .HasColumnName("ID_DETALLENOTACREDITO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidad).HasColumnName("CANTIDAD");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.IdNotacredito).HasColumnName("ID_NOTACREDITO");

                entity.Property(e => e.IdProducto).HasColumnName("ID_PRODUCTO");

                entity.Property(e => e.Preciounitario)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIOUNITARIO");

                entity.Property(e => e.Valoriva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORIVA");

                entity.HasOne(d => d.IdNotacreditoNavigation)
                    .WithMany(p => p.Detallenotacreditos)
                    .HasForeignKey(d => d.IdNotacredito)
                    .HasConstraintName("FK_DETALLEN_REFERENCE_NOTACRED");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Detallenotacreditos)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_DETALLEN_REFERENCE_PRODUCTO");
            });

            modelBuilder.Entity<Empresa>(entity =>
            {
                entity.HasKey(e => e.IdEmpresa);

                entity.ToTable("EMPRESA");

                entity.HasIndex(e => e.Ruc, "AK_UNICORUC_EMPRESA")
                    .IsUnique();

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("ID_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Razonsocial)
                    .IsRequired()
                    .HasMaxLength(35)
                    .HasColumnName("RAZONSOCIAL");

                entity.Property(e => e.Ruc)
                    .IsRequired()
                    .HasMaxLength(13)
                    .HasColumnName("RUC");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(35)
                    .HasColumnName("TELEFONO");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");
            });

            modelBuilder.Entity<Estadoproducto>(entity =>
            {
                entity.HasKey(e => e.IdEstadoproducto);

                entity.ToTable("ESTADOPRODUCTO");

                entity.Property(e => e.IdEstadoproducto)
                    .HasColumnName("ID_ESTADOPRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(20)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Estadoproductos)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_ESTADOPR_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Estadoventum>(entity =>
            {
                entity.HasKey(e => e.IdEstadoventa);

                entity.ToTable("ESTADOVENTA");

                entity.HasComment("Sirve para no tomar en cuenta los productos que son sujetos a nota de credito.");

                entity.Property(e => e.IdEstadoventa)
                    .HasColumnName("ID_ESTADOVENTA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(20)
                    .HasColumnName("NOMBRE");
            });

            modelBuilder.Entity<Facturaelectronicaproveedor>(entity =>
            {
                entity.HasKey(e => e.IdFacturaelectronicaproveedor);

                entity.ToTable("FACTURAELECTRONICAPROVEEDOR");

                entity.Property(e => e.IdFacturaelectronicaproveedor)
                    .HasColumnName("ID_FACTURAELECTRONICAPROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Ambiente)
                    .HasMaxLength(11)
                    .HasColumnName("AMBIENTE");

                entity.Property(e => e.Estado)
                    .HasMaxLength(12)
                    .HasColumnName("ESTADO");

                entity.Property(e => e.Fechaautorizacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAAUTORIZACION");

                entity.Property(e => e.Numeroautorizacion)
                    .HasMaxLength(100)
                    .HasColumnName("NUMEROAUTORIZACION");

                entity.Property(e => e.Xmlfactura)
                    .IsRequired()
                    .HasColumnType("xml")
                    .HasColumnName("XMLFACTURA");
            });

            modelBuilder.Entity<Facturafisicaproveedor>(entity =>
            {
                entity.HasKey(e => e.IdFacturafisicaproveedor);

                entity.ToTable("FACTURAFISICAPROVEEDOR");

                entity.Property(e => e.IdFacturafisicaproveedor)
                    .HasColumnName("ID_FACTURAFISICAPROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Imagenfacturafisica)
                    .IsRequired()
                    .HasColumnType("image")
                    .HasColumnName("IMAGENFACTURAFISICA");
            });

            modelBuilder.Entity<Facturaformaspago>(entity =>
            {
                entity.HasKey(e => e.IdFacturaformapago);

                entity.ToTable("FACTURAFORMASPAGO");

                entity.Property(e => e.IdFacturaformapago)
                    .HasColumnName("ID_FACTURAFORMAPAGO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cambio)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("CAMBIO");

                entity.Property(e => e.Fechamaximapago)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMAXIMAPAGO");

                entity.Property(e => e.IdFacturaventa).HasColumnName("ID_FACTURAVENTA");

                entity.Property(e => e.IdFormapago).HasColumnName("ID_FORMAPAGO");

                entity.Property(e => e.Pagada).HasColumnName("PAGADA");

                entity.Property(e => e.Valorabonopagocreditopersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORABONOPAGOCREDITOPERSONAL");

                entity.Property(e => e.Valorpago)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORPAGO");

                entity.HasOne(d => d.IdFacturaventaNavigation)
                    .WithMany(p => p.Facturaformaspagos)
                    .HasForeignKey(d => d.IdFacturaventa)
                    .HasConstraintName("FK_FACTURAF_REFERENCE_FACTURAV");

                entity.HasOne(d => d.IdFormapagoNavigation)
                    .WithMany(p => p.Facturaformaspagos)
                    .HasForeignKey(d => d.IdFormapago)
                    .HasConstraintName("FK_FACTURAF_REFERENCE_FORMASPA");
            });

            modelBuilder.Entity<Facturaproveedor>(entity =>
            {
                entity.HasKey(e => e.IdFacturaproveedor);

                entity.ToTable("FACTURAPROVEEDOR");

                entity.Property(e => e.IdFacturaproveedor)
                    .HasColumnName("ID_FACTURAPROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Facturapagada).HasColumnName("FACTURAPAGADA");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechaingresofactura)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAINGRESOFACTURA")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Fechapago)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAPAGO");

                entity.Property(e => e.IdEstado)
                    .HasColumnName("ID_ESTADO")
                    .HasDefaultValueSql("(newid())")
                    .HasComment("En Recepcion, Ingresada, anulada");

                entity.Property(e => e.IdFacturaelectronicaproveedor).HasColumnName("ID_FACTURAELECTRONICAPROVEEDOR");

                entity.Property(e => e.IdFacturafisicaproveedor).HasColumnName("ID_FACTURAFISICAPROVEEDOR");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_PROVEEDOR");

                entity.Property(e => e.IdUsuariocreacion).HasColumnName("ID_USUARIOCREACION");

                entity.Property(e => e.IdUsuariomodificacion).HasColumnName("ID_USUARIOMODIFICACION");

                entity.Property(e => e.Numerofactura)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("NUMEROFACTURA")
                    .IsFixedLength(true);

                entity.Property(e => e.Numeroguia)
                    .HasMaxLength(50)
                    .HasColumnName("NUMEROGUIA");

                entity.Property(e => e.Tipofactura)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("TIPOFACTURA");

                entity.Property(e => e.Valortotal)
                    .HasColumnType("decimal(12, 6)")
                    .HasColumnName("VALORTOTAL");

                entity.HasOne(d => d.IdEstadoNavigation)
                    .WithMany(p => p.Facturaproveedors)
                    .HasForeignKey(d => d.IdEstado)
                    .HasConstraintName("FK_FACTURAP_REFERENCE_CATALOGO");

                entity.HasOne(d => d.IdFacturaelectronicaproveedorNavigation)
                    .WithMany(p => p.Facturaproveedors)
                    .HasForeignKey(d => d.IdFacturaelectronicaproveedor)
                    .HasConstraintName("FK_FACTURAP_REFERENCE_FACTURAE");

                entity.HasOne(d => d.IdFacturafisicaproveedorNavigation)
                    .WithMany(p => p.Facturaproveedors)
                    .HasForeignKey(d => d.IdFacturafisicaproveedor)
                    .HasConstraintName("FK_FACTURAP_REFERENCE_FACTURAF");

                entity.HasOne(d => d.IdProveedorNavigation)
                    .WithMany(p => p.Facturaproveedors)
                    .HasForeignKey(d => d.IdProveedor)
                    .HasConstraintName("FK_FACTURAP_REFERENCE_PROVEEDO");

                entity.HasOne(d => d.IdUsuariocreacionNavigation)
                    .WithMany(p => p.FacturaproveedorIdUsuariocreacionNavigations)
                    .HasForeignKey(d => d.IdUsuariocreacion)
                    .HasConstraintName("FK_FACTURAP_FK_U_CREA_USUARIO");

                entity.HasOne(d => d.IdUsuariomodificacionNavigation)
                    .WithMany(p => p.FacturaproveedorIdUsuariomodificacionNavigations)
                    .HasForeignKey(d => d.IdUsuariomodificacion)
                    .HasConstraintName("FK_FACTURAP_FK_U_MODI_USUARIO");
            });

            modelBuilder.Entity<Facturaventum>(entity =>
            {
                entity.HasKey(e => e.IdFacturaventa);

                entity.ToTable("FACTURAVENTA");

                entity.Property(e => e.IdFacturaventa)
                    .HasColumnName("ID_FACTURAVENTA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estadofactura)
                    .IsRequired()
                    .HasMaxLength(7)
                    .HasColumnName("ESTADOFACTURA")
                    .HasDefaultValueSql("('Emitida')");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdCliente).HasColumnName("ID_CLIENTE");

                entity.Property(e => e.IdControlcaja).HasColumnName("ID_CONTROLCAJA");

                entity.Property(e => e.IdEstadoventa).HasColumnName("ID_ESTADOVENTA");

                entity.Property(e => e.IdUsuariocreacion).HasColumnName("ID_USUARIOCREACION");

                entity.Property(e => e.IdUsuariomodificacion).HasColumnName("ID_USUARIOMODIFICACION");

                entity.Property(e => e.Porcentajeiva).HasColumnName("PORCENTAJEIVA");

                entity.Property(e => e.Subtotalfacturaiva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUBTOTALFACTURAIVA");

                entity.Property(e => e.Subtotalfacturasiniva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUBTOTALFACTURASINIVA");

                entity.Property(e => e.Totalfactura)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("TOTALFACTURA");

                entity.Property(e => e.Valorivafactura)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORIVAFACTURA");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Facturaventa)
                    .HasForeignKey(d => d.IdCliente)
                    .HasConstraintName("FK_FACTURAV_REFERENCE_CLIENTE");

                entity.HasOne(d => d.IdControlcajaNavigation)
                    .WithMany(p => p.Facturaventa)
                    .HasForeignKey(d => d.IdControlcaja)
                    .HasConstraintName("FK_FACTURAV_REFERENCE_CONTROLC");

                entity.HasOne(d => d.IdEstadoventaNavigation)
                    .WithMany(p => p.Facturaventa)
                    .HasForeignKey(d => d.IdEstadoventa)
                    .HasConstraintName("FK_FACTURAV_REFERENCE_ESTADOVE");

                entity.HasOne(d => d.IdUsuariocreacionNavigation)
                    .WithMany(p => p.FacturaventumIdUsuariocreacionNavigations)
                    .HasForeignKey(d => d.IdUsuariocreacion)
                    .HasConstraintName("FK_FACTURAV_FK_U_CREA_USUARIO");

                entity.HasOne(d => d.IdUsuariomodificacionNavigation)
                    .WithMany(p => p.FacturaventumIdUsuariomodificacionNavigations)
                    .HasForeignKey(d => d.IdUsuariomodificacion)
                    .HasConstraintName("FK_FACTURAV_FK_U_MODI_USUARIO");
            });

            modelBuilder.Entity<Formaspago>(entity =>
            {
                entity.HasKey(e => e.IdFormapago);

                entity.ToTable("FORMASPAGO");

                entity.Property(e => e.IdFormapago)
                    .HasColumnName("ID_FORMAPAGO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");
            });

            modelBuilder.Entity<Grupoproducto>(entity =>
            {
                entity.HasKey(e => e.IdGrupoproducto);

                entity.ToTable("GRUPOPRODUCTO");

                entity.HasComment("Datos como perecibles, insumos, etc..");

                entity.Property(e => e.IdGrupoproducto)
                    .HasColumnName("ID_GRUPOPRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombregrupoproducto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("NOMBREGRUPOPRODUCTO");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Grupoproductos)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_GRUPOPRO_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Impuesto>(entity =>
            {
                entity.HasKey(e => e.IdImpuesto);

                entity.ToTable("IMPUESTO");

                entity.HasComment("Impuestos adicionales al IVA");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_IMPUESTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombreimpuesto)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("NOMBREIMPUESTO");

                entity.Property(e => e.Tipoimpuesto)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("TIPOIMPUESTO")
                    .HasComment("Se colocara \"porcentaje\" o \"valor\" ");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.Property(e => e.Valor)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Impuestos)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_IMPUESTO_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Local>(entity =>
            {
                entity.HasKey(e => e.IdLocal);

                entity.ToTable("LOCAL");

                entity.Property(e => e.IdLocal)
                    .HasColumnName("ID_LOCAL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Direccionlocal)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("DIRECCIONLOCAL");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdTipolocal)
                    .HasColumnName("ID_TIPOLOCAL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdUbicacion).HasColumnName("ID_UBICACION");

                entity.Property(e => e.Nombrelocal)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("NOMBRELOCAL");

                entity.Property(e => e.Observaciones)
                    .HasColumnType("text")
                    .HasColumnName("OBSERVACIONES");

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("TELEFONO");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Locals)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_LOCAL_REFERENCE_EMPRESA");

                entity.HasOne(d => d.IdTipolocalNavigation)
                    .WithMany(p => p.LocalIdTipolocalNavigations)
                    .HasForeignKey(d => d.IdTipolocal)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LOCAL_REFERENCE_TIPOLOCAL");

                entity.HasOne(d => d.IdUbicacionNavigation)
                    .WithMany(p => p.LocalIdUbicacionNavigations)
                    .HasForeignKey(d => d.IdUbicacion)
                    .HasConstraintName("FK_LOCAL_REFERENCE_UBICACION");
            });

            modelBuilder.Entity<Logexcepcione>(entity =>
            {
                entity.HasKey(e => e.IdLogexcepciones);

                entity.ToTable("LOGEXCEPCIONES");

                entity.Property(e => e.IdLogexcepciones)
                    .HasColumnName("ID_LOGEXCEPCIONES")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("text")
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Entidad)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("ENTIDAD");

                entity.Property(e => e.Error)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("ERROR");

                entity.Property(e => e.Fechaerror)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAERROR");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_USUARIO");

                entity.Property(e => e.Metodo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("METODO");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Logexcepciones)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_LOGEXCEP_REFERENCE_USUARIO");
            });

            modelBuilder.Entity<Marca>(entity =>
            {
                entity.HasKey(e => e.IdMarca);

                entity.ToTable("MARCA");

                entity.Property(e => e.IdMarca)
                    .HasColumnName("ID_MARCA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Marcas)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_MARCA_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.HasKey(e => e.IdMenu);

                entity.ToTable("MENU");

                entity.Property(e => e.IdMenu)
                    .HasColumnName("ID_MENU")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdModulo)
                    .HasColumnName("ID_MODULO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Nombremenu)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("NOMBREMENU");

                entity.Property(e => e.Rutamenu)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false)
                    .HasColumnName("RUTAMENU");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.Menus)
                    .HasForeignKey(d => d.IdModulo)
                    .HasConstraintName("FK_MENU_REFERENCE_MODULO");
            });

            modelBuilder.Entity<Modulo>(entity =>
            {
                entity.HasKey(e => e.IdModulo);

                entity.ToTable("MODULO");

                entity.Property(e => e.IdModulo)
                    .HasColumnName("ID_MODULO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Modulos)
                    .HasForeignKey(d => d.IdRol)
                    .HasConstraintName("FK_MODULO_REFERENCE_ROL");
            });

            modelBuilder.Entity<Nota>(entity =>
            {
                entity.HasKey(e => e.IdNotas);

                entity.ToTable("NOTAS");

                entity.Property(e => e.IdNotas)
                    .HasColumnName("ID_NOTAS")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Contenidonota)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("CONTENIDONOTA");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Fechavencimiento)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAVENCIMIENTO");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdImportancia)
                    .HasColumnName("ID_IMPORTANCIA")
                    .HasDefaultValueSql("(newid())")
                    .HasComment("Urgente, relevancia ");

                entity.Property(e => e.Titulonota)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("TITULONOTA");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Nota)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_NOTAS_REFERENCE_EMPRESA");

                entity.HasOne(d => d.IdImportanciaNavigation)
                    .WithMany(p => p.Nota)
                    .HasForeignKey(d => d.IdImportancia)
                    .HasConstraintName("FK_NOTAS_REFERENCE_CATALOGO");
            });

            modelBuilder.Entity<Notacredito>(entity =>
            {
                entity.HasKey(e => e.IdNotacredito);

                entity.ToTable("NOTACREDITO");

                entity.Property(e => e.IdNotacredito)
                    .HasColumnName("ID_NOTACREDITO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fechacaducidad)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACADUCIDAD");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdCliente).HasColumnName("ID_CLIENTE");

                entity.Property(e => e.IdFacturaventa).HasColumnName("ID_FACTURAVENTA");

                entity.Property(e => e.IdUsuariocreacion).HasColumnName("ID_USUARIOCREACION");

                entity.Property(e => e.IdUsuariomodificacion).HasColumnName("ID_USUARIOMODIFICACION");

                entity.Property(e => e.Porcentajeiva).HasColumnName("PORCENTAJEIVA");

                entity.Property(e => e.Subtotalnotacreditoiva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUBTOTALNOTACREDITOIVA");

                entity.Property(e => e.Subtotalnotacreditosiniva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUBTOTALNOTACREDITOSINIVA");

                entity.Property(e => e.Totalnotacredito)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("TOTALNOTACREDITO");

                entity.Property(e => e.Valordisponible)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORDISPONIBLE");

                entity.Property(e => e.Valorivanotacredito)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALORIVANOTACREDITO");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Notacreditos)
                    .HasForeignKey(d => d.IdCliente)
                    .HasConstraintName("FK_NOTACRED_REFERENCE_CLIENTE");

                entity.HasOne(d => d.IdFacturaventaNavigation)
                    .WithMany(p => p.Notacreditos)
                    .HasForeignKey(d => d.IdFacturaventa)
                    .HasConstraintName("FK_NOTACRED_REFERENCE_FACTURAV");

                entity.HasOne(d => d.IdUsuariocreacionNavigation)
                    .WithMany(p => p.NotacreditoIdUsuariocreacionNavigations)
                    .HasForeignKey(d => d.IdUsuariocreacion)
                    .HasConstraintName("FK_NOTACRED_FK_U_CREA_USUARIO");

                entity.HasOne(d => d.IdUsuariomodificacionNavigation)
                    .WithMany(p => p.NotacreditoIdUsuariomodificacionNavigations)
                    .HasForeignKey(d => d.IdUsuariomodificacion)
                    .HasConstraintName("FK_NOTACRED_FK_U_MODI_USUARIO");
            });

            modelBuilder.Entity<Permiso>(entity =>
            {
                entity.HasKey(e => e.IdPermisos);

                entity.ToTable("PERMISOS");

                entity.Property(e => e.IdPermisos)
                    .HasColumnName("ID_PERMISOS")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Concedido).HasColumnName("CONCEDIDO");

                entity.Property(e => e.Csspermiso)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("CSSPERMISO");

                entity.Property(e => e.IdMenu)
                    .HasColumnName("ID_MENU")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Nombrepermiso)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NOMBREPERMISO");

                entity.HasOne(d => d.IdMenuNavigation)
                    .WithMany(p => p.Permisos)
                    .HasForeignKey(d => d.IdMenu)
                    .HasConstraintName("FK_PERMISOS_REFERENCE_MENU");
            });

            modelBuilder.Entity<Persona>(entity =>
            {
                entity.HasKey(e => e.IdPersona);

                entity.ToTable("PERSONA");

                entity.HasIndex(e => e.Numeroidentificacion, "AK_UNICOIDENTIFICACIO_PERSONA")
                    .IsUnique();

                entity.Property(e => e.IdPersona)
                    .HasColumnName("ID_PERSONA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Apellido)
                    .HasMaxLength(25)
                    .HasColumnName("APELLIDO");

                entity.Property(e => e.Contactoemergencia)
                    .HasMaxLength(11)
                    .HasColumnName("CONTACTOEMERGENCIA");

                entity.Property(e => e.Correopersonal)
                    .HasMaxLength(60)
                    .HasColumnName("CORREOPERSONAL");

                entity.Property(e => e.Direccion).HasColumnName("DIRECCION");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Fechanacimiento)
                    .HasColumnType("date")
                    .HasColumnName("FECHANACIMIENTO");

                entity.Property(e => e.Formatoarchivo)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("FORMATOARCHIVO");

                entity.Property(e => e.IdEstadoCivil)
                    .HasColumnName("ID_ESTADO_CIVIL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdLugarnacimiento).HasColumnName("ID_LUGARNACIMIENTO");

                entity.Property(e => e.IdPaisorigen).HasColumnName("ID_PAISORIGEN");

                entity.Property(e => e.IdResidencia).HasColumnName("ID_RESIDENCIA");

                entity.Property(e => e.IdTipoIdentificacion)
                    .HasColumnName("ID_TIPO_IDENTIFICACION")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Imagenpersona)
                    .HasColumnType("image")
                    .HasColumnName("IMAGENPERSONA");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(25)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.Numeroidentificacion)
                    .HasMaxLength(15)
                    .HasColumnName("NUMEROIDENTIFICACION");

                entity.Property(e => e.Telefono1)
                    .HasMaxLength(11)
                    .HasColumnName("TELEFONO1");

                entity.Property(e => e.Telefono2)
                    .HasMaxLength(11)
                    .HasColumnName("TELEFONO2");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEstadoCivilNavigation)
                    .WithMany(p => p.PersonaIdEstadoCivilNavigations)
                    .HasForeignKey(d => d.IdEstadoCivil)
                    .HasConstraintName("FK_PERSONA_EstadoCivil");

                entity.HasOne(d => d.IdLugarnacimientoNavigation)
                    .WithMany(p => p.PersonaIdLugarnacimientoNavigations)
                    .HasForeignKey(d => d.IdLugarnacimiento)
                    .HasConstraintName("FK_PERSONA_FK_LUGARN_CATALOGO");

                entity.HasOne(d => d.IdPaisorigenNavigation)
                    .WithMany(p => p.PersonaIdPaisorigenNavigations)
                    .HasForeignKey(d => d.IdPaisorigen)
                    .HasConstraintName("FK_PERSONA_FK_NACION_CATALOGO");

                entity.HasOne(d => d.IdResidenciaNavigation)
                    .WithMany(p => p.PersonaIdResidenciaNavigations)
                    .HasForeignKey(d => d.IdResidencia)
                    .HasConstraintName("FK_PERSONA_FK_LUGARR_CATALOGO");

                entity.HasOne(d => d.IdTipoIdentificacionNavigation)
                    .WithMany(p => p.PersonaIdTipoIdentificacionNavigations)
                    .HasForeignKey(d => d.IdTipoIdentificacion)
                    .HasConstraintName("FK_PERSONA_TIPOIDENTIFICACION");
            });

            modelBuilder.Entity<Porcentajeiva>(entity =>
            {
                entity.HasKey(e => e.IdPorcenajeiva);

                entity.ToTable("PORCENTAJEIVA");

                entity.Property(e => e.IdPorcenajeiva)
                    .HasColumnName("ID_PORCENAJEIVA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Activo).HasColumnName("ACTIVO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.Property(e => e.Valor)
                    .HasColumnType("decimal(2, 0)")
                    .HasColumnName("VALOR");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.IdProducto);

                entity.ToTable("PRODUCTO");

                entity.Property(e => e.IdProducto)
                    .HasColumnName("ID_PRODUCTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Anaquel).HasColumnName("ANAQUEL");

                entity.Property(e => e.Cantidadexistentes).HasColumnName("CANTIDADEXISTENTES");

                entity.Property(e => e.Cantidadinservible).HasColumnName("CANTIDADINSERVIBLE");

                entity.Property(e => e.Cantidadvendidos).HasColumnName("CANTIDADVENDIDOS");

                entity.Property(e => e.Casillero)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .HasColumnName("CASILLERO")
                    .IsFixedLength(true);

                entity.Property(e => e.Codigoproducto)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("CODIGOPRODUCTO");

                entity.Property(e => e.Costoadquisicion)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("COSTOADQUISICION");

                entity.Property(e => e.Detalleproducto).HasColumnName("DETALLEPRODUCTO");

                entity.Property(e => e.Fechacaducidad)
                    .HasColumnType("date")
                    .HasColumnName("FECHACADUCIDAD");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Hilera)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("HILERA")
                    .IsFixedLength(true);

                entity.Property(e => e.IdBodega).HasColumnName("ID_BODEGA");

                entity.Property(e => e.IdCanjeproductoproveedor).HasColumnName("ID_CANJEPRODUCTOPROVEEDOR");

                entity.Property(e => e.IdCategoria).HasColumnName("ID_CATEGORIA");

                entity.Property(e => e.IdEstado)
                    .HasColumnName("ID_ESTADO")
                    .HasComment("En Recepcion, Ingresado, Devolucion, Anulado");

                entity.Property(e => e.IdEstadoproducto).HasColumnName("ID_ESTADOPRODUCTO");

                entity.Property(e => e.IdFacturaproveedor).HasColumnName("ID_FACTURAPROVEEDOR");

                entity.Property(e => e.IdGrupoproducto).HasColumnName("ID_GRUPOPRODUCTO");

                entity.Property(e => e.IdLocal).HasColumnName("ID_LOCAL");

                entity.Property(e => e.IdMarca).HasColumnName("ID_MARCA");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.IdProcedencia)
                    .HasColumnName("ID_PROCEDENCIA")
                    .HasComment("Para registrar la procedencia del Pais \"made in ...\"");

                entity.Property(e => e.IdUnidadentrega).HasColumnName("ID_UNIDADENTREGA");

                entity.Property(e => e.IdUsuariocreacion).HasColumnName("ID_USUARIOCREACION");

                entity.Property(e => e.IdUsuariomodificacion).HasColumnName("ID_USUARIOMODIFICACION");

                entity.Property(e => e.Imagenproducto)
                    .HasColumnType("image")
                    .HasColumnName("IMAGENPRODUCTO");

                entity.Property(e => e.Nombreproducto)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("NOMBREPRODUCTO");

                entity.Property(e => e.Precioventapublico)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIOVENTAPUBLICO");

                entity.Property(e => e.Tieneiva).HasColumnName("TIENEIVA");

                entity.HasOne(d => d.IdBodegaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdBodega)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_BODEGA");

                entity.HasOne(d => d.IdCanjeproductoproveedorNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdCanjeproductoproveedor)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_CANJEPRO");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdCategoria)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_CATEGORI");

                entity.HasOne(d => d.IdEstadoNavigation)
                    .WithMany(p => p.ProductoIdEstadoNavigations)
                    .HasForeignKey(d => d.IdEstado)
                    .HasConstraintName("FK_PRODUCTO_FK_ESTADO_CATALOGO");

                entity.HasOne(d => d.IdEstadoproductoNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdEstadoproducto)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_ESTADOPR");

                entity.HasOne(d => d.IdFacturaproveedorNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdFacturaproveedor)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_FACTURAP");

                entity.HasOne(d => d.IdGrupoproductoNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdGrupoproducto)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_GRUPOPRO");

                entity.HasOne(d => d.IdLocalNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdLocal)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_LOCAL");

                entity.HasOne(d => d.IdMarcaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdMarca)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_MARCA");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdPersona)
                    .HasConstraintName("FK_PRODUCTO_REF_PERSONA");

                entity.HasOne(d => d.IdProcedenciaNavigation)
                    .WithMany(p => p.ProductoIdProcedenciaNavigations)
                    .HasForeignKey(d => d.IdProcedencia)
                    .HasConstraintName("FK_PRODUCTO_FK_PROCED_CATALOGO");

                entity.HasOne(d => d.IdUnidadentregaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdUnidadentrega)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_UNIDADEN");

                entity.HasOne(d => d.IdUsuariocreacionNavigation)
                    .WithMany(p => p.ProductoIdUsuariocreacionNavigations)
                    .HasForeignKey(d => d.IdUsuariocreacion)
                    .HasConstraintName("FK_PRODUCTO_FK_U_CREA_USUARIO");

                entity.HasOne(d => d.IdUsuariomodificacionNavigation)
                    .WithMany(p => p.ProductoIdUsuariomodificacionNavigations)
                    .HasForeignKey(d => d.IdUsuariomodificacion)
                    .HasConstraintName("FK_PRODUCTO_FK_U_MODI_USUARIO");
            });

            modelBuilder.Entity<Productoimpuesto>(entity =>
            {
                entity.HasKey(e => e.IdProductoimpuesto);

                entity.ToTable("PRODUCTOIMPUESTO");

                entity.Property(e => e.IdProductoimpuesto)
                    .HasColumnName("ID_PRODUCTOIMPUESTO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_IMPUESTO");

                entity.Property(e => e.IdProducto).HasColumnName("ID_PRODUCTO");

                entity.HasOne(d => d.IdImpuestoNavigation)
                    .WithMany(p => p.Productoimpuestos)
                    .HasForeignKey(d => d.IdImpuesto)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_IMPUESTO");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Productoimpuestos)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_PRODUCTO_REFERENCE_PRODUCTO");
            });

            modelBuilder.Entity<Proveedor>(entity =>
            {
                entity.HasKey(e => e.IdProveedor);

                entity.ToTable("PROVEEDOR");

                entity.Property(e => e.IdProveedor)
                    .HasColumnName("ID_PROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Codigoproveedor)
                    .HasMaxLength(20)
                    .HasColumnName("CODIGOPROVEEDOR");

                entity.Property(e => e.Contribuyenteespecial).HasColumnName("CONTRIBUYENTEESPECIAL");

                entity.Property(e => e.Correoelectronico)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("CORREOELECTRONICO");

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("DIRECCION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdEspecialidad)
                    .HasColumnName("ID_ESPECIALIDAD")
                    .HasComment("Distribuidor, Artesano, Intermediario, Servicio, etc");

                entity.Property(e => e.IdPorcenajeiva).HasColumnName("ID_PORCENAJEIVA");

                entity.Property(e => e.IdTipocontribuyente)
                    .HasColumnName("ID_TIPOCONTRIBUYENTE")
                    .HasComment("Sociedad Anonima, Persona Juridica, etc");

                entity.Property(e => e.Nombrecomercial)
                    .HasMaxLength(60)
                    .HasColumnName("NOMBRECOMERCIAL");

                entity.Property(e => e.Nombrecontacto)
                    .HasMaxLength(50)
                    .HasColumnName("NOMBRECONTACTO");

                entity.Property(e => e.Nombrerepresentate)
                    .HasMaxLength(50)
                    .HasColumnName("NOMBREREPRESENTATE");

                entity.Property(e => e.Nota)
                    .HasColumnType("text")
                    .HasColumnName("NOTA");

                entity.Property(e => e.Obligadollevarcontabilidad).HasColumnName("OBLIGADOLLEVARCONTABILIDAD");

                entity.Property(e => e.Razonsocial)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("RAZONSOCIAL");

                entity.Property(e => e.Ruc)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("RUC");

                entity.Property(e => e.Telefonocontacto)
                    .HasMaxLength(15)
                    .HasColumnName("TELEFONOCONTACTO");

                entity.Property(e => e.Telefonomovil)
                    .HasMaxLength(15)
                    .HasColumnName("TELEFONOMOVIL");

                entity.Property(e => e.Telefonoproveedor)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("TELEFONOPROVEEDOR");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Proveedors)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_PROVEEDO_REFERENCE_EMPRESA");

                entity.HasOne(d => d.IdEspecialidadNavigation)
                    .WithMany(p => p.ProveedorIdEspecialidadNavigations)
                    .HasForeignKey(d => d.IdEspecialidad)
                    .HasConstraintName("FK_PROVEEDO_FK_ESPECI_CATALOGO");

                entity.HasOne(d => d.IdPorcenajeivaNavigation)
                    .WithMany(p => p.Proveedors)
                    .HasForeignKey(d => d.IdPorcenajeiva)
                    .HasConstraintName("FK_PROVEEDO_REFERENCE_PORCENTA");

                entity.HasOne(d => d.IdTipocontribuyenteNavigation)
                    .WithMany(p => p.ProveedorIdTipocontribuyenteNavigations)
                    .HasForeignKey(d => d.IdTipocontribuyente)
                    .HasConstraintName("FK_PROVEEDO_FK_TIPOCO_CATALOGO");
            });

            modelBuilder.Entity<Rol>(entity =>
            {
                entity.HasKey(e => e.IdRol);

                entity.ToTable("ROL");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Fechamodficacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODFICACION");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("ID_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Nombrerol)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("NOMBREROL");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Rols)
                    .HasForeignKey(d => d.IdEmpresa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ROL_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Unidadentrega>(entity =>
            {
                entity.HasKey(e => e.IdUnidadentrega);

                entity.ToTable("UNIDADENTREGA");

                entity.Property(e => e.IdUnidadentrega)
                    .HasColumnName("ID_UNIDADENTREGA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Codigounidad)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CODIGOUNIDAD")
                    .IsFixedLength(true);

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.Nombrecontenido)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRECONTENIDO");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Unidadentregas)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_UNIDADEN_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.ToTable("USUARIO");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Contrasena)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("CONTRASENA");

                entity.Property(e => e.Correoinstitucional)
                    .HasMaxLength(60)
                    .HasColumnName("CORREOINSTITUCIONAL")
                    .IsFixedLength(true);

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Fechaultimoingreso)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAULTIMOINGRESO");

                entity.Property(e => e.IdEmpresadefault)
                    .HasColumnName("ID_EMPRESADEFAULT")
                    .HasComment("La idea es tener preseleccionada una emprea del listado que va a tener el usuario, con el fin que precarguen primero las opcioens de esta empresa; si desean de otra empresa pues cambiará dentro del aplicativo. ");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Indiciocontrasena)
                    .HasMaxLength(50)
                    .HasColumnName("INDICIOCONTRASENA")
                    .IsFixedLength(true);

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.IdPersona)
                    .HasConstraintName("FK_USUARIO_REFERENCE_PERSONA");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.IdRol)
                    .HasConstraintName("FK_USUARIO_REFERENCE_ROL");
            });

            modelBuilder.Entity<UsuarioEmpresa>(entity =>
            {
                entity.HasKey(e => e.IdUsuairoEmpresa);

                entity.ToTable("USUARIO_EMPRESA");

                entity.Property(e => e.IdUsuairoEmpresa)
                    .HasColumnName("ID_USUAIRO_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("ID_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.UsuarioEmpresas)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_USUARIO__REFERENCE_EMPRESA");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.UsuarioEmpresas)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_USUARIO__REFERENCE_USUARIO");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
