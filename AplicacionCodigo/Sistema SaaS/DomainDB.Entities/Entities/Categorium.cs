﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Categorium
    {
        public Categorium()
        {
            Catalogoproductos = new HashSet<Catalogoproducto>();
            InverseIdCategoriapadreNavigation = new HashSet<Categorium>();
            Productos = new HashSet<Producto>();
        }

        public Guid IdCategoria { get; set; }
        public Guid? IdCategoriapadre { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombrecategoria { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Categorium IdCategoriapadreNavigation { get; set; }
        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual ICollection<Catalogoproducto> Catalogoproductos { get; set; }
        public virtual ICollection<Categorium> InverseIdCategoriapadreNavigation { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
