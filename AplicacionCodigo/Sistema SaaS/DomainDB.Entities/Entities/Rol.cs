﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Rol
    {
        public Rol()
        {
            Modulos = new HashSet<Modulo>();
            Usuarios = new HashSet<Usuario>();
        }

        public Guid IdRol { get; set; }
        public Guid IdEmpresa { get; set; }
        public string Nombrerol { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodficacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual ICollection<Modulo> Modulos { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
