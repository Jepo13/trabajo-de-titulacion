﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DomainDB.Entities.Entities
{
    public partial class Controlcaja
    {
        public Controlcaja()
        {
            Facturaventa = new HashSet<Facturaventum>();
        }

        public Guid IdControlcaja { get; set; }
        public Guid? IdCaja { get; set; }
        public Guid? IdPersona { get; set; }
        public Guid? IdUsuario { get; set; }
        public DateTime Fechaapertura { get; set; }
        public DateTime? Fechacierre { get; set; }
        public decimal Valorinicial { get; set; }
        public decimal Valorfacturaventaanulada { get; set; }
        public int Cantidadventascaja { get; set; }
        public decimal Ventasefectivo { get; set; }
        public decimal Ventasnotacredito { get; set; }
        public decimal Ventascreditopersonal { get; set; }
        public decimal Valornotascreditogeneradas { get; set; }
        public bool? Reportecorrecto { get; set; }
        public string Observacion { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual Caja IdCajaNavigation { get; set; }
        public virtual Persona IdPersonaNavigation { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
        public virtual ICollection<Facturaventum> Facturaventa { get; set; }
    }
}
