﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageLogin
    {
        public string recuperarIndicioContrasena(Guid idPersona);
        public Persona ingresarSistema(string numeroIdentificacion, string contrasena);

    }
}
