﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageCatalogo
    {
       
        #region Search
        public  Task<Catalogo> GetCatalogoById(Guid idCatalogo);
        public Task<List<Catalogo>> GetAllCatalogoByCompany(Guid idEmpresa);
        public  Task<List<Catalogo>> GetCatalogoByName(string name);
        public Task<Catalogo> GetCatalogoByCodeIDEmpresa(string code, Guid idEmpresa);
        public Task<List<Catalogo>> GetChildByParentCodeIDEmpresa(string code, Guid idEmpresa);
        public Task<List<Catalogo>> GetChildByParentCodeByID_IDEmpresa(Guid idPadre, Guid idEmpresa);
        public Task<List<Catalogo>> GetChildByParentCodeByID_Nombre(string nombrePadre, Guid idEmpresa);
        public Task<List<Catalogo>> GetCatalogsUpLevel_Company(Guid idCatalogoHijo, Guid idEmpresa);
        public Task<List<Catalogo>> GetCatalogsSameLevelByID_Company(Guid idCatalogoHermano, Guid idEmpresa);
        #endregion
    }
}
