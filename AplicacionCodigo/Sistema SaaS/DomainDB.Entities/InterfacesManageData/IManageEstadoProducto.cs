﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageEstadoProducto
    {
        #region CRUD
        public void AddEstadoProducto(Estadoproducto objEstadoProducto);
        public void EditEstadoProducto(Estadoproducto objEstadoProducto);
        public void DeleteEstadoProducto(Estadoproducto objEstadoProducto);
        public  Task<bool> SaveEstadoProducto();
        #endregion

        #region Search
        public Task<Estadoproducto> GetEstadoProductoById(Guid idEstadoProducto);
        public Task<List<Estadoproducto>> GetAllEstadoProducto();
        public  Task<Estadoproducto> GetEstadoProductoByName(string name);
        #endregion
    }
}
