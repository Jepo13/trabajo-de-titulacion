﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManagePorcentajeIVA
    {


        #region CRUD
        public void AddPorcenjateIVA(Porcentajeiva objPorcenjateIVA);
        public void EditPorcentajeIVA(Porcentajeiva objPorcenjateIVA);
        public void DeletePorcentajeIVA(Porcentajeiva objPorcentajeIVA);
        public Task<bool> SavePorcentajeIVA();
        #endregion

        #region Search
        public Task<Porcentajeiva> GetPorcentajeIVAById(Guid idPorcentajeIVA);
        public  Task<List<Porcentajeiva>> GetAllPorcentajeIVA();
        #endregion

    }
}
