﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageImpuesto
    {

        #region CRUD
        public void AddImpuesto(Impuesto objImpuesto);
        public void EditImpuesto(Impuesto objImpuesto);
        public void DeleteImpuesto(Impuesto objImpuesto);
        public Task<bool> SaveImpuesto();
        #endregion

        #region Search
        public Task<Impuesto> GetImpuestoById(Guid idImpuesto);
        public Task<List<Impuesto>> GetAllImpuesto();
        public Task<Impuesto> GetImpuestoByName(string name);
        #endregion
    }
}
