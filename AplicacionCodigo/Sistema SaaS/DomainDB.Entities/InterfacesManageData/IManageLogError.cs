﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageLogError
    {
        #region CRUD
        public void AddLogError(Logexcepcione objLogError);      
        public Task<bool> saveError(Logexcepcione objLog);
        #endregion

        #region Search
        public Task<Logexcepcione> ByIDLog(Guid idLog);
        public Task<Logexcepcione> ByIDUsuario(Guid idUsuario);
        public Task<Logexcepcione> GeByMetodo(string metodo);
        public Task<List<Logexcepcione>> GetByDate(DateTime fechaInicial, DateTime fechaFinal);
        #endregion
    }
}
