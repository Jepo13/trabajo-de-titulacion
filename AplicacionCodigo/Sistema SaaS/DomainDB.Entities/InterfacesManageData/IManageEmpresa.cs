﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Empresa;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageEmpresa
    {

      /*  #region CRUD
        public void AddCompany(Empresa objCompany);
        public void EditCompany(Empresa objCompany);
        public void DeleteCompany(Empresa objCompany);
        public Task<bool> SaveCompany();
        #endregion*/

        #region Search
        public Task<Empresa> GetCompanyById(Guid idCompany);
        public Task<Empresa> GetCompanyByRUC(string  ruc);
        public Task<List<Empresa>> GetAllCompany();
        public Task<List<Empresa>> GetCompaniesAdvanced(ObjetoBusquedaEmpresas objBusqueda);
        #endregion
    }
}
