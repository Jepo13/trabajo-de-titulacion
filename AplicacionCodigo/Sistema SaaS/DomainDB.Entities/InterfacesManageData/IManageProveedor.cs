﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Proveedor;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageProveedor
    {
       /*
        public void AddProveedor(Proveedor objProveedor);
        public void EditProveedor(Proveedor objProveedor);
        public void DeleteProveedor(Proveedor objProveedor);
        public  Task<bool> SaveProveedor();*/
        #region Search
        public  Task<Proveedor> GetProveedorById(Guid idProvedor);
        public Task<Proveedor> GetProveedorByRuc(string RUC, Guid idEmpresa);
        public Task<List<Proveedor>> GetAllProveedor();
        #endregion
        public Task<Proveedor> GetProveedorByName(string name);
        public Task<List<Proveedor>> GetProveedorAdvanced(ObjetoBusquedaProveedor objBusqueda);
    }
}
