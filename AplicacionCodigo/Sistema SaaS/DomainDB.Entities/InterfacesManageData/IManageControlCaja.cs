﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageControlCaja
    {
        #region CRUD
        public void AddControlCaja(Controlcaja objControlCaja);
        public void EditControlCanja(Controlcaja objControlCaja);
        public void DeleteControlCaja(Controlcaja objControlCajas);
        public Task<bool> SaveControlCaja();
        #endregion

        #region Search
        public Task<Controlcaja> GetControlCajaById(Guid idControlCaja);
        public Task<List<Controlcaja>> GetAllControlCaja();
        #endregion

    }
}
