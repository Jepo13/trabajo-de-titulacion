﻿using System;
using System.Collections.Generic;
using DomainDB.Entities.Entities;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManagePercentageIva
    {
        #region CRUD
        public void AddPercentageIva(Porcentajeiva objPercentageIva);
        public void EditPercentageIva(Porcentajeiva objPercentageIva);
        public void DeletePercentageIva(Porcentajeiva objPercentageIva);
        public Task<bool> SavePercentageIva();
        #endregion

        #region Search
        public Task<Porcentajeiva> GetPercentageIvaById(Guid idPercentageIva);
        public Task<Porcentajeiva> GetPercentageIvaByEstate(bool state);
        public Task<List<Porcentajeiva>> GetAllPercentageIva();
        #endregion
    }
}
