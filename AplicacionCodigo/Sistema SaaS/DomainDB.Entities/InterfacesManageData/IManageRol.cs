﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageRol
    {

        #region CRUD
        public void AddRol(Rol objRol);
        public Task<List<Modulo>> EditRol(Rol objRol, List<Modulo> listaModulos);
        public void DeleteRol(Rol objRol);
        public Task<bool> saveRol();
      
        #endregion

        #region Search      
        public Task<List<Rol>> GetAllRolsByCompany(Guid idCompany);
        public Task<Rol> GetRolByID(Guid IdRol);
   
        #endregion
    }
}
