﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageCaja
    {



        #region Search
        public Task<Caja> GetCajaById(Guid idCaja);
        public Task<List<Caja>> GetAllCajas();
        public Task<Caja> GetCajaByName(string nameCaja);
        public Task<List<Caja>> GetAllCajaByIdLocal(Guid idCaja);
            #endregion

    }
}
