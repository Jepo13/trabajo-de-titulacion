﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManagePerson
    {
        public Persona cargarClienteFinal();
        public Persona recuperarPersonaPorIdentificacionFacturacion(string numeroIdentificacion);
        public IEnumerable<Persona> recuperarPersonaPorIdentificacionUsuario(string numeroIdentificacion);
        public IEnumerable<Persona> getAllPersonUsers();
        public Persona recuperarPersonaPorIdentificacionCrearPersona(string numeroIdentificacion);
        public Persona recuperarPersonaPorIID(Guid idPersona);
        public IEnumerable<Persona> buscarTodasPersonasClientes();
        /*public IEnumerable<Tipoidentificacion> recuperarListaTipoIdentificaciones();*/
        public bool guardarPersona(Persona objetoPersona);
        public bool editarPersona(Persona objetoPersona);
        public bool editarPersonaUsuario(Persona objetoPersona);

        
    }
}
