﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Local;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageLocal
    {
        #region CRUD
        public void AddLocal(Local objLocal);
        public void EditLocal(Local objLocal);
        public void DeleteLocal(Local idLocalEmpresa);
        public Task<(bool estado, string mensajeError)> saveLocal();
        #endregion

        #region Search
        public Task<Local> GetLocalById(Guid idLocal);
        public Task<Local> GetLocalByNombre(string nombreLocal);
        public Task<List<Local>> GetAllLocales();
        public Task<List<Local>> GetLocalAdvanced(ObjetoBusquedaLocal objBusqueda);
        #endregion
    }
}
