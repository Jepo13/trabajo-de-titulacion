﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageGrupoProducto
    {
        #region CRUD
        public void AddGrupoProducto(Grupoproducto objGrupoProducto);
        public void EditGrupoProducto(Grupoproducto objGrupoProducto);
        public void DeleteGrupoProducto(Grupoproducto objGrupoProducto);
        public  Task<bool> SaveGrupoProducto();
        #endregion

        #region Search
        public Task<Grupoproducto> GetGrupoProductoById(Guid idGrupoProducto);
        public Task<Grupoproducto> GetGrupoProductoByName(string name);
        public Task<List<Grupoproducto>> GetAllGrupoProducto();
        #endregion
    }
}
