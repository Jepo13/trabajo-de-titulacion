﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageBodega
    {
       

        #region Search
        public Task<List<Bodega>> GetAllBodegas();
        public Task<Bodega> GetBodegaByName(string name);
        public Task<Bodega> GetBodegaById(Guid idBodega);
        public Task<List<Bodega>> GetAllBodegasByIdLocal(Guid idLocal);
        #endregion


    }
}
