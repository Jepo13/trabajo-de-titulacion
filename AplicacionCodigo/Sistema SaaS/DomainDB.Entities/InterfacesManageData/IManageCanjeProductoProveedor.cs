﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageCanjeProductoProveedor
    {
        #region CRUD
        public void AddCanjeProductoProveedor(Canjeproductoproveedor objCanjeProductoProveedor);
        public void EditCanjeProductoProveedor(Canjeproductoproveedor objCanjeProductoProveedor);
        public void DeleteCanjeProductoProveedor(Canjeproductoproveedor objCanjeProductoProveedor);

        public  Task<bool> SaveCanjeProductoProveedor();

        public Task<Canjeproductoproveedor> GetCanjeProductoProveedorById(Guid idCanjeProductoProveedor);
        public  Task<List<Canjeproductoproveedor>> GetAllCanjeProductoProveedor();
        public  Task<Canjeproductoproveedor> GetCanjeProductoProveedorByIdentificador(string Identificador);
        #endregion

        #region Search
        #endregion
    }
}
