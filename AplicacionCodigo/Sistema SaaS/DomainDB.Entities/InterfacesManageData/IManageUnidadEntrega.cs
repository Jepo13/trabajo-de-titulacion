﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageUnidadEntrega
    {
        #region CRUD
        public void AddUnidadEntrega(Unidadentrega objUnidadEntrega);
        public void EditUnidadEntrega(Unidadentrega objUnidadEntrega);
        public void DeleteUnidadEntrega(Unidadentrega objUnidadEntrega);
        public Task<bool> SaveUnidadEntrega();
        #endregion
        #region Search
        public Task<Unidadentrega> GetUnidadEntregaById(Guid idUnidadEntrega);
        public  Task<List<Unidadentrega>> GetAllUnidadEntrega();
        public Task<Unidadentrega> GetUnidadEntregaByName(string name);
        #endregion
    }
}
