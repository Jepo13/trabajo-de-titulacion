﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageNota
    {
        #region CRUD
        public void AddNota(Nota objNota);
        public void EditNota(Nota objNota);
        public void DeleteNota(Nota objNota);
        public Task<bool> SaveNota();
        #endregion

        #region Search
        public  Task<Nota> GetNotaById(Guid idNota);
        public Task<List<Nota>> GetAllNotas();
        public  Task<Nota> GetNotaByTitulo(string titulo);

        #endregion
    }
}
