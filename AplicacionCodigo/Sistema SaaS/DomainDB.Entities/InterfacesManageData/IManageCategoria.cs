﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageCategoria
    {
        #region CRUD
        public void AddCategoria(Categorium objCategoria);
        #endregion
        public void EditCategoria(Categorium objCategoria);
        public void DeleteCategoria(Categorium objCategoria);
        public Task<bool> SaveCategoria();
        #region Search
        public Task<Categorium> GetCategoriaById(Guid idCategoria);
        public Task<List<Categorium>> GetAllCategoria();
        public Task<Categorium> GetCategoriaByName(string name);

        #endregion

    }
}
