﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageEstadoVenta
    {

        #region CRUD
        public void AddEstadoVenta(Estadoventum objEstadoVenta);
        public void EditEstadoVenta(Estadoventum objEstadoVenta);
        public void DeleteEstadoVenta(Estadoventum objEstadoVenta);
        public  Task<bool> SaveEstadoVenta();
        #endregion

        #region Search
        public Task<Estadoventum> GetEstadoVentaById(Guid idEstadoVenta);
        public Task<List<Estadoventum>> GetAllEstadoVenta();
        public Task<Estadoventum> GetEstadoVentaByName(string name);
        #endregion
    }
}
