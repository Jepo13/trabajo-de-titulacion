﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageFacturaProveedor
    {
        #region CRUD

        public void AddFacturaProveedor(Facturaproveedor objFacturaProveedor);
        public void EditFacturaProveedor(Facturaproveedor objFacturaProveedor);
        public void DeleteFacturaProveedor(Facturaproveedor objFacturaProveedor);
        public Task<bool> SaveFacturaProveedor();
        #endregion

        #region Search
        public Task<Facturaproveedor> GetFacturaProveedorById(Guid idFacturaProvedor);
        public Task<Facturaproveedor> GetFacturaProveedorByInvoiceNumber(string invoiceNumber);
        public Task<List<Facturaproveedor>> GetAllFacturaProveedor();
        public Task<List<Facturaproveedor>> GetAllFacturaProveedorByIdProveedor(Guid idProveedor);
        #endregion
    }
}
