﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Usuario;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageUser
    {
        /*#region CRUD
        //public void AddUser(Usuario objUser);
        //public void EditUser(Usuario objUser);
        //public void DeleteUser(Usuario objUser);
        //public Task<bool> saveUser();
        //public Task<bool> updateUser(Usuario objUser);
        #endregion*/
        #region Search
        public Task<Usuario> getUserById(Guid idUser);
        public Task<Usuario> getUserByCorreo(string correoUser);
        public Task<List<Usuario>> getAllUsers();

        public Task<Usuario> getLoginUser(string correoUser, string contrasena);
        #endregion

        #region Buscar
        public Task<List<Usuario>> GetUserAdvanced(ObjetoBusquedaUsuarios objBusqueda);
        #endregion


    }//class
}
