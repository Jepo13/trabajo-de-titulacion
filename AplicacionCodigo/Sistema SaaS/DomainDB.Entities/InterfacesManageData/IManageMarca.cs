﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageMarca
    {
        #region CRUD
        public void AddMarca(Marca objMarca);
        public void EditMarca(Marca objMarca);
        public void DeleteMarca(Marca objMarca);
        public Task<bool> SaveMarca();
        #endregion
        #region Search
        public Task<Marca> GetMarcaById(Guid idMarca);
        public Task<List<Marca>> GetAllMarcas();
        public Task<Marca> GetMarcaByName(string name);
        #endregion

    }
}
