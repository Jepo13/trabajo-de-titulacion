﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.InterfacesManageData
{
    public interface IManageCRUD<T> where T : class
    {
        #region CRUD
        public void Add(T obj);
        public void Edit(T obj);
        public void Delete(T obj);
        public Task<(bool estado, string mensajeError)> save();

        public void DeleteEmpresa(Empresa objEmpresa);
        public void DeleteBodega(Bodega objBodega);
        public void DeleteCaja(Caja objCaja);
        #endregion
    }
}
