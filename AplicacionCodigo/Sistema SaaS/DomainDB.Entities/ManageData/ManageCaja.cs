﻿using System;
using System.Collections.Generic;
using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DomainDB.Entities.InterfacesManageData;

namespace DomainDB.Entities.ManageData
{
    public class ManageCaja : IManageCaja
    {
        public readonly DBContextComercios _contex;
        public ManageCaja(DBContextComercios context)
        {
            this._contex = context ?? throw new ArgumentException(nameof(context));
        }

     

        #region Search

        public async Task<Caja> GetCajaById(Guid idCaja)
        {
            Caja objCaja = await _contex.Cajas.Where(C => C.IdCaja == idCaja).FirstOrDefaultAsync();
            return objCaja;
        }
        public async Task<List<Caja>> GetAllCajas()
        {
            List<Caja> listCaja = new ();
            try
            {
                listCaja = await _contex.Cajas.Select(p => p).ToListAsync();
            }catch (Exception ex)
            {

            }
            return listCaja;
        }
        public async Task<Caja> GetCajaByName(string nameCaja)
        {

            Caja objCaja = await _contex.Cajas.Where(c => c.Nombrecaja.Trim().ToUpper().Contains(nameCaja.Trim().ToUpper())).FirstOrDefaultAsync();

            return objCaja;
        }
        public async Task<List<Caja>> GetAllCajaByIdLocal(Guid idLocal)
        {
            List<Caja> ListCajaByLocal = new List<Caja>();
            try
            {
                ListCajaByLocal = await _contex.Cajas.Where(c => c.IdLocal == idLocal).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return ListCajaByLocal;
        }

        #endregion
    }
}
