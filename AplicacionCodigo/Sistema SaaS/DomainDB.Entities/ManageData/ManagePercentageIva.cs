﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace DomainDB.Entities.ManageData
{
    public class ManagePercentageIva
    {
        public readonly DBContextComercios _contex;
        public ManagePercentageIva(DBContextComercios context)
        {
            this._contex = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        public void AddPercentageIva(Porcentajeiva objPercentageIva)
        {
            try
            {
                objPercentageIva.Fechacreacion = DateTime.Now;
                _contex.Porcentajeivas.Add(objPercentageIva);
            }
            catch (Exception ex)
            {

            }
        }
        public void EditPercentageIva(Porcentajeiva objPercentageIva) 
        {
            try
            {
                objPercentageIva.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        public void DeletePercentageIva(Porcentajeiva objPercentageIva) 
        {
            try
            {
                _contex.Porcentajeivas.Remove(objPercentageIva);
            }
            catch (Exception ex)
            {

            }
        }
        public async Task<bool> SavePercentageIva()
        {
            try
            {
                var created = await _contex.SaveChangesAsync();
                return created > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        #endregion

        #region Search
        public async Task<Porcentajeiva> GetPercentageIvaById(Guid idPercentageIva) 
        {
            Porcentajeiva objPercentageIva = await _contex.Porcentajeivas.Where(c => c.IdPorcenajeiva == idPercentageIva).FirstOrDefaultAsync();
            return objPercentageIva;
        }

        public async Task<Porcentajeiva> GetPercentageIvaByEstate(bool state) 
        {
            Porcentajeiva objPercentageIva = await _contex.Porcentajeivas.Where(c => c.Activo == state).FirstOrDefaultAsync();
            return objPercentageIva;
        }
        public async Task<List<Porcentajeiva>> GetAllPercentageIva()
        {
            List<Porcentajeiva> listObjPercentageIva = await _contex.Porcentajeivas.Select(p => p).ToListAsync();
            return listObjPercentageIva;
        }
        #endregion
    }
}
