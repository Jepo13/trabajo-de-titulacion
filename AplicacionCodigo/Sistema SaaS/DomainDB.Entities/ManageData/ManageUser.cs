﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.InterfacesManageData;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Usuario;

namespace DomainDB.Entities.ManageData
{
    public class ManageUser : IManageUser
    {
        public readonly DBContextComercios _context;
        public ManageUser(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
       /* #region CRUD
        public void AddUser(Usuario objUser)
        {
            try
            {
                objUser.Fechacreacion = DateTime.Now;
                objUser.Usuariomodificacion = objUser.Usuariocreacion;
                _context.Usuarios.Add(objUser);
            }
            catch (Exception exValidation)
            {

            }
        } // End AddLocal
        public void EditUser(Usuario objUser)
        {
            try
            {
                objUser.Fechamodificacion = DateTime.Now;
            }
            catch (Exception exValidation)
            {

            }
        } // End EditLocal
        public void DeleteUser(Usuario objUser)
        {
            try
            {
                _context.Usuarios.Remove(objUser);
            }
            catch (Exception exValidation)
            {

            }
        }// end Delet User
        public async Task<bool> saveUser()
        {
            try
            {
                try
                {
                    var created1 = await _context.SaveChangesAsync();

                    return created1 > 0;
                }
                catch (Exception ex)
                {
                }

                var created = await _context.SaveChangesAsync();

                return created > 0;
            }
            catch (Exception exValidation)
            {

            }
            return false;
        } // Enda Save User


        public async Task<bool> updateUser(Usuario objUser)
        {
            try
            {
                //_context.Entry(objUser).State = EntityState.Modified;
                var created = await _context.SaveChangesAsync();
                return created > 0;
            }
            catch (Exception exValidation)
            {

            }
            return false;
        } // Enda Save User



        #endregion */

        #region Search
        public async Task<Usuario> getUserById(Guid idUser)
        {
            try
            {
                Usuario objUser = await _context.Usuarios.Where(c => c.IdUsuario == idUser).Include(x => x.UsuarioEmpresas).ThenInclude(x => x.IdEmpresaNavigation).Include(x => x.IdPersonaNavigation).FirstOrDefaultAsync();

                return objUser;
            }
            catch (Exception)
            {
            }

            return null;
        } // End get user by id
        public async Task<Usuario> getUserByCorreo(string correoUser)
        {
            Usuario objUser = await _context.Usuarios.Where(c => c.Correoinstitucional == correoUser).FirstOrDefaultAsync();
            return objUser;
        }// get user by name

        public async Task<Usuario> getLoginUser(string correoUser, string contrasena)
        {
            try
            {
                Usuario objUser = await _context.Usuarios.
                    Where(c => c.Correoinstitucional == correoUser && 
                    c.Contrasena == contrasena).Include(x => x.UsuarioEmpresas).ThenInclude(x => x.IdEmpresaNavigation).
                    Include(x => x.IdPersonaNavigation).Include(x => x.IdRolNavigation).ThenInclude(x => x.Modulos).ThenInclude(x => x.Menus).ThenInclude(x => x.Permisos).AsNoTracking()
                    .FirstOrDefaultAsync();
                return objUser;
            }
            catch (Exception ex)
            {

                throw;
            }

        }// get user by name


        public async Task<List<Usuario>> getAllUsers()
        {
            List<Usuario> listUseres = new List<Usuario>();
            try
            {
                listUseres = await _context.Usuarios.Select(p => p).ToListAsync();
            }
            catch (Exception exValidation)
            {
            }

            return listUseres;
        } //  end get all users 

        public async Task<List<Usuario>> GetUserAdvanced(ObjetoBusquedaUsuarios objBusqueda)
        {
            List<Usuario> listUser = new List<Usuario>();
            try
            {
                if (objBusqueda.IdRol == null)
                {
                    listUser = await _context.Usuarios.Select(x => x).Include(x => x.IdPersonaNavigation).Include(x => x.IdRolNavigation).Include(x => x.UsuarioEmpresas).ThenInclude(x => x.IdEmpresaNavigation).ToListAsync();
                }
                else
                {
                    listUser = await _context.Usuarios.Where(x => x.IdRol == objBusqueda.IdRol).Include(x => x.IdPersonaNavigation).Include(x => x.IdRolNavigation).Include(x => x.UsuarioEmpresas).ThenInclude(x => x.IdEmpresaNavigation).ToListAsync();
                }

                if (string.IsNullOrEmpty(objBusqueda.textoBusqueda))
                {
                    listUser = listUser.Where(x => 
                    x.IdPersonaNavigation.Numeroidentificacion.Contains(objBusqueda.textoBusqueda) || x.IdPersonaNavigation.Nombre.Contains(objBusqueda.textoBusqueda) ||
                    x.IdPersonaNavigation.Apellido.Contains(objBusqueda.textoBusqueda) ||
                     x.IdPersonaNavigation.Correopersonal.Contains(objBusqueda.textoBusqueda)
                    ).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return listUser;
        }
        #endregion

        #region Comentado
        /*
           public async Task<Usuario> GetUsuriForIDPerson(Guid idPerson)
        {
            Usuario objUsuari = new Usuario();
            using (DBContextComercios _context = new DBContextComercios())
            {
                objUsuari =  await _context.Usuarios.Where(c => c.IdPersona == idPerson).FirstOrDefaultAsync();
            }
            return objUsuari;
        }

        public bool editarUsuario(Usuario objetoUsuario)
        {
            bool banderaEditar = true;
            try
            {
                using (DBContextComercios contextoConexion = new DBContextComercios())
                {
                    var objetoUsuarioDB = GetUsuriForIDPerson(objetoUsuario.IdPersona);
                    contextoConexion.Entry(objetoUsuarioDB).CurrentValues.SetValues(objetoUsuario);
                    contextoConexion.Entry(objetoUsuarioDB).State = EntityState.Modified;
                    contextoConexion.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                banderaEditar = false;
            }

            return banderaEditar;
        }//editarPersona

        public bool guardarUsuario(Usuario objetoUsuario)
        {
            bool banderaGuardado = true;
            using (DBContextComercios contextoConexion = new DBContextComercios())
            {
                try
                {
                    contextoConexion.Usuarios.Add(objetoUsuario);
                    contextoConexion.SaveChanges();
                }              
                catch (Exception ex)
                {
                    banderaGuardado = false;
                }
            }
            return banderaGuardado;
        }

        public bool eliminarUsuario(Guid idPersonaUsuario)
        {

            bool banderaEliminarUsuario = true;
            using (DBContextComercios contextoConexionEliminarUsuario = new DBContextComercios())
            {
                try
                {
                    Usuario objetoUsuario = contextoConexionEliminarUsuario.Usuarios.Find(idPersonaUsuario);
                    contextoConexionEliminarUsuario.Usuarios.Remove(objetoUsuario);
                    contextoConexionEliminarUsuario.SaveChanges();
                }
                catch (Exception ex)
                {
                    banderaEliminarUsuario = false;
                }
            }

            return banderaEliminarUsuario;
        }//eliminarUsuario

        public string recuperarIndicioContrasena(Guid idPersona)
        {
            try
            {
                using (DBContextComercios contextoConexionBuscarIndicio = new DBContextComercios())
                {

                    var objetoUsuario = contextoConexionBuscarIndicio.Usuarios.Select(user => user).Where(user => user.IdPersona == idPersona).First();


                    return objetoUsuario.Indiciocontrasena;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
         
         */
        #endregion


    }//class
}
