﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageEstadoProducto : IManageEstadoProducto
    {
        public readonly DBContextComercios _context;
        public ManageEstadoProducto(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        #region Add
        public void AddEstadoProducto(Estadoproducto objEstadoProducto)
        {
            try
            {
                objEstadoProducto.Fechacreacion = DateTime.Now;
                objEstadoProducto.Fechamodificacion = DateTime.Now;
                _context.Estadoproductos.AddAsync(objEstadoProducto);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditEstadoProducto(Estadoproducto objEstadoProducto)
        {
            try
            {
                objEstadoProducto.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteEstadoProducto(Estadoproducto objEstadoProducto)
        {
            try
            {
                _context.Estadoproductos.Remove(objEstadoProducto);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveEstadoProducto()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion
        #region Get Bodega by Id
        public async Task<Estadoproducto> GetEstadoProductoById(Guid idEstadoProducto )
        {
            Estadoproducto objEstadoProducto = await _context.Estadoproductos.Where(c => c.IdEstadoproducto == idEstadoProducto).FirstOrDefaultAsync();
            return objEstadoProducto;
        }
        #endregion
        #region Get All 
        public async Task<List<Estadoproducto>> GetAllEstadoProducto()
        {
            List<Estadoproducto> listEstadoProducto = new List<Estadoproducto>();
            try
            {
                listEstadoProducto = await _context.Estadoproductos.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listEstadoProducto;
        }
        #endregion
        #region Get By Name
        public async Task<Estadoproducto> GetEstadoProductoByName(string name)
        {
            Estadoproducto objEstadoproducto = await _context.Estadoproductos.Where(c => c.Nombre.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objEstadoproducto;
        }
        #endregion
        #region Search

        #endregion
    }
}
