﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageCRUD<T> : IManageCRUD<T> where T : class
    {
        public readonly DBContextComercios _context;

        public ManageCRUD(DBContextComercios context)
        {
            _context = context;
        }

        public void Add(T obj)
        {
            try
            {                
                obj.GetType().GetProperty("Fechacreacion").SetValue(obj, DateTime.Now);
                obj.GetType().GetProperty("Fechamodificacion").SetValue(obj, DateTime.Now);
                obj.GetType().GetProperty("Usuariomodificacion").SetValue(obj, obj.GetType().GetProperty("Usuariocreacion").GetValue(obj, null));
                //obj.Fechacreacion = DateTime.Now;
                //obj.Fechamodificacion = DateTime.Now;
                //obj.Usuariomodificacion = T.Usuariocreacion;

                //_context.Set<T>().Add(obj);
                _context.AddAsync(obj);
            }
            catch (Exception exValidation)
            {

            }
        }

        public void Edit(T obj)
        {
            obj.GetType().GetProperty("Fechamodificacion").SetValue(obj, DateTime.Now);
        }

        public void Delete(T obj)
        {

            _context.Remove(obj);
        }

        public void DeleteEmpresa (Empresa objEmpresa)
        {
            objEmpresa.Catalogoproductos = _context.Catalogoproductos.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Catalogos = _context.Catalogos.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Categoria = _context.Categoria.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Estadoproductos = _context.Estadoproductos.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Grupoproductos = _context.Grupoproductos.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Impuestos = _context.Impuestos.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Locals = _context.Locals.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Marcas = _context.Marcas.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Nota = _context.Notas.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Proveedors = _context.Proveedors.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Rols = _context.Rols.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.Unidadentregas = _context.Unidadentregas.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            objEmpresa.UsuarioEmpresas = _context.UsuarioEmpresas.Where(x => x.IdEmpresa == objEmpresa.IdEmpresa).ToList();
            _context.Remove(objEmpresa);
        }
        public void DeleteBodega(Bodega objBodega)
        {
            objBodega.Productos = _context.Productos.Where(x => x.IdBodega == objBodega.IdBodega).ToList();
            _context.Remove(objBodega);
        }
        public void DeleteCaja(Caja objCaja)
        {
            objCaja.Controlcajas = _context.Controlcajas.Where(x => x.IdCaja == objCaja.IdCaja).ToList();
            _context.Remove(objCaja);
        }

        public async Task<(bool estado, string mensajeError)> save()
        {
            try
            {
                var created = await _context.SaveChangesAsync();
                return (created > 0, string.Empty);
            }
            catch (Exception ex)
            {
                try
                {
                    var created = await _context.SaveChangesAsync();
                    return (created > 0, string.Empty);
                }
                catch (Exception exx)
                {
                    string mensajeError = "";
                    if (exx.InnerException != null)
                        mensajeError = ex.InnerException.Message;

                    return (false, mensajeError);
                }
            }
          
        }
    }
}
