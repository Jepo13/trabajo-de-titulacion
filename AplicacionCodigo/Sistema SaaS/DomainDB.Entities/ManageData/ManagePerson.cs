﻿using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DomainDB.Entities.InterfacesManageData;

namespace DomainDB.Entities.ManageData
{
    public class ManagePerson : IManagePerson
    {


        public Persona cargarClienteFinal()
        {
            Persona objetoPersona = new Persona();

            objetoPersona.Nombre = "Cliente";
            objetoPersona.Apellido = "Final";
            objetoPersona.Numeroidentificacion = "9999999999";
            objetoPersona.Telefono1 = "0999999999";
            objetoPersona.Direccion = "Quito";
            objetoPersona.Correopersonal = "correo@prueba.com";

            /*Tipoidentificacion objetoTipoIdentificacion = new Tipoidentificacion();*/
            /*objetoTipoIdentificacion.Nombretipoidentificacion = "Cédula";*/

            
            return objetoPersona;
        }

        public Persona recuperarPersonaPorIdentificacionFacturacion(string numeroIdentificacion)
        {
            try
            {
                using (DBContextComercios _context = new DBContextComercios())
                {
                  /*  var objetoPersona = _context.Personas.Select(p => p).Where(p => p.Numeroidentificacion == numeroIdentificacion && p.Cliente.Fecharegistro != null).First();

                    return objetoPersona;*/
                }
            }
            catch
            {
                return null;
            }
            return null;
        }

        public IEnumerable<Persona> recuperarPersonaPorIdentificacionUsuario(string numeroIdentificacion)
        {
            try
            {
                using (DBContextComercios _context = new DBContextComercios())
                {
                    var objetoPersona = _context.Personas.Select(p => p).Where(p => p.Numeroidentificacion == numeroIdentificacion).ToList();

                    return objetoPersona;
                }
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Persona> getAllPersonUsers()
        {
            IEnumerable<Persona> listPeople = new List<Persona>();
            try
            {
                using (DBContextComercios contextoConexion = new DBContextComercios())
                {
                    listPeople = contextoConexion.Personas.ToList();
                }

            }
            catch
            {

            }

            return listPeople;
        }

        public Persona recuperarPersonaPorIdentificacionCrearPersona(string numeroIdentificacion)
        {
            try
            {
                using (DBContextComercios contextoConexion = new DBContextComercios())
                {
                    var objetoPersona = contextoConexion.Personas.Select(p => p).Where(p => p.Numeroidentificacion == numeroIdentificacion).First();

                    return objetoPersona;
                }
            }
            catch
            {
                return null;
            }
        }


        public Persona recuperarPersonaPorIID(Guid idPersona)
        {
            Persona objetoPersona = new Persona();
            using (DBContextComercios contextoConexion = new DBContextComercios())
            {
                objetoPersona = contextoConexion.Personas.Where(x => x.IdPersona == idPersona).FirstOrDefault();
            }


            return objetoPersona;
        }

        public IEnumerable<Persona> buscarTodasPersonasClientes()
        {
            IEnumerable<Persona> listaPersonas = new List<Persona>();

            using (DBContextComercios contextoConexionBuscar = new DBContextComercios())
            {
             //   listaPersonas = contextoConexionBuscar.Personas.Select(x => x).Where(x => x.Cliente.Fecharegistro != null).ToList();
            }
            if (listaPersonas == null)
            {
               
            }

            return null;
        }

      /*  public IEnumerable<Tipoidentificacion> recuperarListaTipoIdentificaciones()
        {
            IEnumerable<Tipoidentificacion> listaTipoIdentificacion = new List<Tipoidentificacion>();

            using (DBContextComercios contextoConexion = new DBContextComercios())
            {
                //listaTipoIdentificacion = contextoConexion.Tipoidentificacions.Select(tipoIdentificacion => tipoIdentificacion);

            }
            return listaTipoIdentificacion;

        }*/

        public bool guardarPersona(Persona objetoPersona)
        {
            bool banderaGuardado = true;

            using (DBContextComercios contextoConexion = new DBContextComercios())
            {
                try
                {
                    contextoConexion.Personas.Add(objetoPersona);
                    contextoConexion.SaveChanges();
                }              
                catch (Exception ex)
                {
                    banderaGuardado = false;
                }
            }
            return banderaGuardado;
        }//guardarSeccion

        public bool editarPersona(Persona objetoPersona)
        {
            bool banderaEditar = true;
            using (DBContextComercios contextoConexionBuscar = new DBContextComercios())
            {
                try
                {
                    var objetoPersonaDB = contextoConexionBuscar.Personas.Find(objetoPersona.IdPersona);

                    contextoConexionBuscar.Entry(objetoPersonaDB).CurrentValues.SetValues(objetoPersona);
                    contextoConexionBuscar.Entry(objetoPersonaDB).State = EntityState.Modified;
                    contextoConexionBuscar.SaveChanges();
                }
                catch (Exception ex)
                {
                    banderaEditar = false;
                }
            }
            return banderaEditar;
        }//editarPersona



        public bool editarPersonaUsuario(Persona objetoPersona)
        {


            using (DBContextComercios contextoConexionBuscar = new DBContextComercios())
            {
                try
                {
                    var objetoPersonaDB = contextoConexionBuscar.Personas.Find(objetoPersona.IdPersona);

                  
                }
                catch (Exception ex)
                {
                  
                }

            }
            return true;
        }//editarPersonaUsuario

      
    }//class
}
