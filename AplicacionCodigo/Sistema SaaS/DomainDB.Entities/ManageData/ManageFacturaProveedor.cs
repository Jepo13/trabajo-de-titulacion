﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageFacturaProveedor : IManageFacturaProveedor
    {
        public readonly DBContextComercios _context;
        public ManageFacturaProveedor(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region CRUD
        #region Add
        public void AddFacturaProveedor(Facturaproveedor objFacturaProveedor)
        {
            try
            {
                objFacturaProveedor.Fechacreacion = DateTime.Now;
                objFacturaProveedor.Fechamodificacion = DateTime.Now;
                _context.Facturaproveedors.AddAsync(objFacturaProveedor);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion

        #region Edit
        public void EditFacturaProveedor(Facturaproveedor objFacturaProveedor)
        {
            try
            {
                objFacturaProveedor.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region Delete
        public void DeleteFacturaProveedor(Facturaproveedor objFacturaProveedor)
        {
            try
            {
                _context.Facturaproveedors.Remove(objFacturaProveedor);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region  Save
        public async Task<bool> SaveFacturaProveedor()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion

        #endregion

        #region Search
        #region Get Bodega by Id
        public async Task<Facturaproveedor> GetFacturaProveedorById(Guid idFacturaProvedor)
        {
            Facturaproveedor objFacturaProveedor = await _context.Facturaproveedors.Where(c => c.IdFacturaproveedor == idFacturaProvedor).FirstOrDefaultAsync();
            return objFacturaProveedor;
        }
        #endregion

        #region Get All 
        public async Task<List<Facturaproveedor>> GetAllFacturaProveedor()
        {
            List<Facturaproveedor> listFacturaProveedor = new();
            try
            {
                listFacturaProveedor = await _context.Facturaproveedors.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listFacturaProveedor;
        }

        public async Task<List<Facturaproveedor>> GetAllFacturaProveedorByIdProveedor(Guid idProveedor)
        {
            List<Facturaproveedor> listFacturaProveedor = new();
            try
            {
                listFacturaProveedor = await _context.Facturaproveedors.Where(p => p.IdProveedor == idProveedor).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listFacturaProveedor;
        }
        #endregion

        #region Get by Invoice
        public async Task<Facturaproveedor> GetFacturaProveedorByInvoiceNumber(string invoiceNumber)
        {
            Facturaproveedor objFacturaProveedor = await _context.Facturaproveedors.Where(c => c.Numerofactura.Trim().ToUpper() == invoiceNumber).FirstOrDefaultAsync();
            return objFacturaProveedor;
        }
        #endregion

        #endregion


    }
}
