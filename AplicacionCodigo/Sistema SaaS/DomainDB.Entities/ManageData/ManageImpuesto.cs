﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageImpuesto : IManageImpuesto
    {
        public readonly DBContextComercios _context;
        public ManageImpuesto (DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        #region Add
        public void AddImpuesto(Impuesto objImpuesto)
        {
            try
            {
                objImpuesto.Fechacreacion = DateTime.Now;
                objImpuesto.Fechamodificacion = DateTime.Now;
                _context.Impuestos.AddAsync(objImpuesto);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditImpuesto(Impuesto objImpuesto)
        {
            try
            {
                objImpuesto.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteImpuesto(Impuesto objImpuesto)
        {
            try
            {
                _context.Impuestos.Remove(objImpuesto);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveImpuesto()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion

        #region Search
        #region Get Impuesto By Id
        public async Task<Impuesto> GetImpuestoById(Guid idImpuesto)
        {
            Impuesto objImpuesto = await _context.Impuestos.Where(c => c.IdImpuesto == idImpuesto).FirstOrDefaultAsync();
            return objImpuesto;
        }
        #endregion
        #region Get All 
        public async Task<List<Impuesto>> GetAllImpuesto()
        {
            List<Impuesto> listImpuesto = new List<Impuesto>();
            try
            {
                listImpuesto = await _context.Impuestos.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listImpuesto;
        }
        #endregion
        #region Get by Name
        public async Task<Impuesto> GetImpuestoByName(string name)
        {
            Impuesto objImpuesto = await _context.Impuestos.Where(c => c.Nombreimpuesto.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objImpuesto;
        }
        #endregion
        #endregion

    }
}
