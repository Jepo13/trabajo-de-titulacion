﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageCanjeProductoProveedor : IManageCanjeProductoProveedor
    {
        public readonly DBContextComercios _context;
        public ManageCanjeProductoProveedor(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }


        #region CRUD
        #region Add
        public void AddCanjeProductoProveedor(Canjeproductoproveedor objCanjeProductoProveedor)
        {
            try
            {
                objCanjeProductoProveedor.Fechacreacion = DateTime.Now;
                objCanjeProductoProveedor.Fechamodificacion = DateTime.Now;
                _context.Canjeproductoproveedors.AddAsync(objCanjeProductoProveedor);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditCanjeProductoProveedor(Canjeproductoproveedor objCanjeProductoProveedor)
        {
            try
            {
                objCanjeProductoProveedor.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteCanjeProductoProveedor(Canjeproductoproveedor objCanjeProductoProveedor)
        {
            try
            {
                _context.Canjeproductoproveedors.Remove(objCanjeProductoProveedor);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveCanjeProductoProveedor()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion

        #region Search
        #region Get Bodega by Id
        public async Task<Canjeproductoproveedor> GetCanjeProductoProveedorById(Guid idCanjeProductoProveedor)
        {
            Canjeproductoproveedor objCanjeProductoProveedor = await _context.Canjeproductoproveedors.Where(c => c.IdCanjeproductoproveedor == idCanjeProductoProveedor).FirstOrDefaultAsync();
            return objCanjeProductoProveedor;
        }
        #endregion
        #region Get All 
        public async Task<List<Canjeproductoproveedor>> GetAllCanjeProductoProveedor()
        {
            List<Canjeproductoproveedor> listBCanjeProductoProveedor = new();
            try
            {
                listBCanjeProductoProveedor = await _context.Canjeproductoproveedors.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listBCanjeProductoProveedor;
        }
        #endregion
        #region Get by Identificador Canje Proveedor
        public async Task<Canjeproductoproveedor> GetCanjeProductoProveedorByIdentificador(string Identificador)
        {
            Canjeproductoproveedor objCanjeProductoProveedor = await _context.Canjeproductoproveedors.Where(c => c.Identificadorcanjeproducto.Trim().ToUpper().Contains(Identificador.Trim().ToUpper())).FirstOrDefaultAsync();
            return objCanjeProductoProveedor;
        }
        #endregion
        #endregion
    }
}
