﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageRol : IManageRol
    {
        public readonly DBContextComercios _context;

        public ManageRol(DBContextComercios context)
        {
            this._context = context ??
                throw new ArgumentException(nameof(context));
        }

        public void AddRol(Rol objRol)
        {
            objRol.Fechacreacion = DateTime.Now;
            objRol.Fechamodficacion = objRol.Fechacreacion;
            objRol.Usuariomodificacion = objRol.Usuariocreacion;
            _context.Rols.Add(objRol);
        }

        public async Task<List<Modulo>> EditRol(Rol objRol, List<Modulo> listaModulos)
        {
            try
            {
                objRol.Fechamodficacion = DateTime.Now;
                _context.Modulos.RemoveRange(objRol.Modulos);
                _context.Modulos.AddRange(listaModulos);

                var created1 = await _context.SaveChangesAsync();
                return listaModulos;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async Task<bool> saveRol()
        {
            try
            {
                try
                {
                    var created1 = await _context.SaveChangesAsync();

                    return created1 >= 0;
                }
                catch (Exception ex)
                {
                }

                var created = await _context.SaveChangesAsync();

                return created >= 0;
            }
            catch (Exception exValidation)
            {

            }
            return false;
        }
      

        public void DeleteRol(Rol objRol)
        {
            try
            {
                _context.Rols.Remove(objRol);

              
            }
            catch (Exception ex)
            {
               
            }
        }

        public async Task<List<Rol>> GetAllRolsByCompany(Guid idEmpresa)
        {
            List<Rol> listRols = new List<Rol>();
            try
            {
                listRols = await _context.Rols.Where(x => x.IdEmpresa==idEmpresa).Include(x => x.Modulos).ToListAsync();

            }
            catch (Exception ex)
            {

            }
            return listRols;
        }

        public async Task<Rol> GetRolByID(Guid IdRol)
        {
            try
            {
                Rol objRol = await _context.Rols.Where(c => c.IdRol == IdRol).Include(x => x.Modulos)
                    .ThenInclude(x => x.Menus).ThenInclude(x => x.Permisos).FirstOrDefaultAsync();

                return objRol;
            }
            catch (Exception ex)
            {
            }

            return null;
        }
    }
}
