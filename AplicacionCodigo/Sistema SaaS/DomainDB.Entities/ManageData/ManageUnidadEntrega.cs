﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageUnidadEntrega : IManageUnidadEntrega
    {
        public readonly DBContextComercios _context;
        public ManageUnidadEntrega(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        #region Add
        public void AddUnidadEntrega(Unidadentrega objUnidadEntrega)
        {
            try
            {
                objUnidadEntrega.Fechacreacion = DateTime.Now;
                objUnidadEntrega.Fechamodificacion = DateTime.Now;
                _context.Unidadentregas.AddAsync(objUnidadEntrega);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditUnidadEntrega(Unidadentrega objUnidadEntrega)
        {
            try
            {
                objUnidadEntrega.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteUnidadEntrega(Unidadentrega objUnidadEntrega)
        {
            try
            {
                _context.Unidadentregas.Remove(objUnidadEntrega);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveUnidadEntrega()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion
        #region Search
        #region Ge UnidadEntrega By Id
        public async Task<Unidadentrega> GetUnidadEntregaById(Guid idUnidadEntrega)
        {
            Unidadentrega objUnidadEntrega = await _context.Unidadentregas.Where(c => c.IdUnidadentrega == idUnidadEntrega).FirstOrDefaultAsync();
            return objUnidadEntrega;
        }
        #endregion
        #region Get All 
        public async Task<List<Unidadentrega>> GetAllUnidadEntrega()
        {
            List<Unidadentrega> listUnidadentrega = new List<Unidadentrega>();
            try
            {
                listUnidadentrega = await _context.Unidadentregas.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listUnidadentrega;
        }
        #endregion
        #region Get by Name
        public async Task<Unidadentrega> GetUnidadEntregaByName(string name)
        {
            Unidadentrega objUnidadentrega = await _context.Unidadentregas.Where(c => c.Nombrecontenido.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objUnidadentrega;
        }
        #endregion
        #endregion


    }
}
