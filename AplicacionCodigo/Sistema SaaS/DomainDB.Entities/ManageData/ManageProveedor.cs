﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Utilitarios.Busquedas.Proveedor;

namespace DomainDB.Entities.ManageData
{
    public class ManageProveedor : IManageProveedor
    {
        public readonly DBContextComercios _context;
        public ManageProveedor(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        /*
        #region CRUD
        #region Add
        public void AddProveedor(Proveedor objProveedor)
        {
            try
            {
                objProveedor.Fechacreacion = DateTime.Now;
                objProveedor.Fechamodificacion = DateTime.Now;
                _context.Proveedors.AddAsync(objProveedor);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditProveedor(Proveedor objProveedor)
        {
            try
            {
                objProveedor.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteProveedor(Proveedor objProveedor)
        {
            try
            {
                _context.Proveedors.Remove(objProveedor);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveProveedor()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion*/

        #region Search
        #region Get Bodega by Id
        public async Task<Proveedor> GetProveedorById(Guid idProvedor)
        {
            Proveedor objProveedor = await _context.Proveedors.Where(c => c.IdProveedor == idProvedor).FirstOrDefaultAsync();
            return objProveedor;
        }
        #endregion
        #region Get All 
        public async Task<List<Proveedor>> GetAllProveedor()
        {
            List<Proveedor> listProveedor = new();
            try
            {
                listProveedor = await _context.Proveedors.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listProveedor;
        }
        #endregion
        #region Get by RUC
        public async Task<Proveedor> GetProveedorByRuc(string RUC, Guid idEmpresa)
        {
            Proveedor objProveedor = await _context.Proveedors.Where(c => c.Ruc.Trim().ToUpper().Contains(RUC.Trim().ToUpper())&& c.IdEmpresa == idEmpresa).FirstOrDefaultAsync();
            return objProveedor;
        }
        #endregion

        #region Get by Name
        public async Task<Proveedor> GetProveedorByName(string name)
        {
            Proveedor objProveedor = await _context.Proveedors.Where(c => c.Nombrerepresentate.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objProveedor;
        }
        #endregion
        #region
        public async Task<List<Proveedor>> GetProveedorAdvanced(ObjetoBusquedaProveedor objBusqueda)
        {
            List<Proveedor> listProveedores = new List<Proveedor>();
            try
            {
                if (objBusqueda.Estado != null)
                {
                    listProveedores = _context.Proveedors.Where(x => x.Estado == (bool)objBusqueda.Estado).Include(x=> x.IdEspecialidadNavigation).ToList();
                }
                else
                {
                    listProveedores = await _context.Proveedors.Include(x=>x.IdEspecialidadNavigation).ToListAsync();
                }


                if (!string.IsNullOrEmpty(objBusqueda.Ruc))
                {
                    listProveedores = listProveedores.Where(x => x.Ruc.Contains(objBusqueda.Ruc.Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(objBusqueda.Razonsocial))
                {
                    listProveedores = listProveedores.Where(x => x.Razonsocial.ToUpper().Trim().Contains(objBusqueda.Razonsocial.ToUpper().Trim())).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return listProveedores;
        }
        #endregion
        #endregion


    }
}
