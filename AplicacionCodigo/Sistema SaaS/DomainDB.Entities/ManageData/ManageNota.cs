﻿using System;
using System.Collections.Generic;
using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DomainDB.Entities.InterfacesManageData;

namespace DomainDB.Entities.ManageData
{
    public class ManageNota : IManageNota
    {
        public readonly DBContextComercios _context;
        public ManageNota(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        #region Add
        public void AddNota(Nota objNota)
        {
            try
            {
                objNota.Fechacreacion = DateTime.Now;
                objNota.Fechamodificacion = DateTime.Now;
                _context.Notas.AddAsync(objNota);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditNota(Nota objNota)
        {
            try
            {
                objNota.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteNota(Nota objNota)
        {
            try
            {
                _context.Notas.Remove(objNota);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveNota()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion

        #endregion

        #region Search
        #region Get Bodega by Id
        public async Task<Nota> GetNotaById(Guid idNota)
        {
            Nota objNota = await _context.Notas.Where(c => c.IdNotas == idNota).FirstOrDefaultAsync();
            return objNota;
        }
        #endregion
        #region Get All 
        public async Task<List<Nota>> GetAllNotas()
        {
            List<Nota> listNota = new();
            try
            {
                listNota = await _context.Notas.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listNota;
        }
        #endregion

        #region Get by Name
        public async Task<Nota> GetNotaByTitulo(string titulo)
        {
            try
            {
                Nota objNota = await _context.Notas.Where(c => c.Titulonota.Trim().ToUpper().Contains(titulo.Trim().ToUpper())).FirstOrDefaultAsync();

                return objNota;
            }
            catch (Exception ex)
            {
                return null;
            }
          
        }
        #endregion

        #endregion
    }
}
