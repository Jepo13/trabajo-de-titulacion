﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageControlCaja : IManageControlCaja
    {
        public readonly DBContextComercios _contex;
        public ManageControlCaja(DBContextComercios context)
        {
            this._contex = context ?? throw new ArgumentException(nameof(context));
        }

        #region CRUD
        #region AddCaja
        public void AddControlCaja(Controlcaja objControlCaja)
        {
            try
            {
                objControlCaja.Fechacreacion = DateTime.Now;
                objControlCaja.Fechamodificacion = DateTime.Now;
                _contex.Controlcajas.AddAsync(objControlCaja);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Edit
        public void EditControlCanja(Controlcaja objControlCaja)
        {
            try
            {
                objControlCaja.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteControlCaja(Controlcaja objControlCajas)
        {
            try
            {
                _contex.Controlcajas.Remove(objControlCajas);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Save ControlCaja
        public async Task<bool> SaveControlCaja()
        {
            try
            {
                var create = await _contex.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        #endregion
        #endregion

        #region Search
        public async Task<Controlcaja> GetControlCajaById(Guid idControlCaja)
        {
            Controlcaja objControlCaja = await _contex.Controlcajas.Where(C => C.IdControlcaja == idControlCaja).FirstOrDefaultAsync();
            return objControlCaja;
        }

        public async Task<List<Controlcaja>> GetAllControlCaja()
        {
            List<Controlcaja> listControlCajas = new List<Controlcaja>();
            try
            {
                listControlCajas = await _contex.Controlcajas.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listControlCajas ;
        }

        #endregion
    }
}
