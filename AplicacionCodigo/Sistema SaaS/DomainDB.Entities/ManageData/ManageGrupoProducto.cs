﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageGrupoProducto : IManageGrupoProducto
    {
        public readonly DBContextComercios _context;
        public ManageGrupoProducto(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        #region Add
        public void AddGrupoProducto(Grupoproducto objGrupoProducto)
        {
            try
            {
                objGrupoProducto.Fechacreacion = DateTime.Now;
                objGrupoProducto.Fechamodificacion = DateTime.Now;
                _context.Grupoproductos.AddAsync(objGrupoProducto);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditGrupoProducto(Grupoproducto objGrupoProducto)
        {
            try
            {
                objGrupoProducto.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteGrupoProducto(Grupoproducto objGrupoProducto)
        {
            try
            {
                _context.Grupoproductos.Remove(objGrupoProducto);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveGrupoProducto()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion

        #region Search
        #region Get GrupoProductos By Id
        public async Task<Grupoproducto> GetGrupoProductoById(Guid idGrupoProducto)
        {
            Grupoproducto objGrupoProducto = await _context.Grupoproductos.Where(c => c.IdGrupoproducto == idGrupoProducto).FirstOrDefaultAsync();
            return objGrupoProducto;
        }
        #endregion
        #region Get by Name
        public async Task<Grupoproducto> GetGrupoProductoByName(string name)
        {
            Grupoproducto objGrupoProducto = await _context.Grupoproductos.Where(c => c.Nombregrupoproducto.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();

            return objGrupoProducto;
        }
        #endregion
        #region Get All 
        public async Task<List<Grupoproducto>> GetAllGrupoProducto()
        {
            List<Grupoproducto> listGrupoProducto = new List<Grupoproducto>();
            try
            {
                listGrupoProducto = await _context.Grupoproductos.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listGrupoProducto;
        }
        #endregion
        #endregion
    }
}
