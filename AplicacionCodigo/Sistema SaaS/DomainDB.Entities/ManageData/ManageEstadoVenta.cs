﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageEstadoVenta : IManageEstadoVenta
    {
        public readonly DBContextComercios _context;
        public ManageEstadoVenta(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        #region Add
        public void AddEstadoVenta(Estadoventum objEstadoVenta)
        {
            try
            {
                objEstadoVenta.Fechacreacion = DateTime.Now;
                objEstadoVenta.Fechamodificacion = DateTime.Now;
                _context.Estadoventa.AddAsync(objEstadoVenta);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditEstadoVenta(Estadoventum objEstadoVenta)
        {
            try
            {
                objEstadoVenta.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteEstadoVenta(Estadoventum objEstadoVenta)
        {
            try
            {
                _context.Estadoventa.Remove(objEstadoVenta);
            }
            catch (Exception ex)
            {

            }
        }
        #region  Save
        public async Task<bool> SaveEstadoVenta()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        #endregion
        #endregion

        #endregion

        #region Search
        #region Get Bodega by Id
        public async Task<Estadoventum> GetEstadoVentaById(Guid idEstadoVenta)
        {
            Estadoventum objEstadoVenta = await _context.Estadoventa.Where(c => c.IdEstadoventa == idEstadoVenta).FirstOrDefaultAsync();
            return objEstadoVenta;
        }
        #endregion
        #region Get All 
        public async Task<List<Estadoventum>> GetAllEstadoVenta()
        {
            List<Estadoventum> listEstadoVenta = new();
            try
            {
                listEstadoVenta = await _context.Estadoventa.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listEstadoVenta;
        }
        #endregion
        #region Get by Name
        public async Task<Estadoventum> GetEstadoVentaByName(string name)
        {
            Estadoventum objEstadoVenta = await _context.Estadoventa.Where(c => c.Nombre.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objEstadoVenta;
        }
        #endregion
        #endregion

    }
}
