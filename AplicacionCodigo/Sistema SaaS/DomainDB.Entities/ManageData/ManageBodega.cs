﻿using System;
using System.Collections.Generic;
using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DomainDB.Entities.InterfacesManageData;

namespace DomainDB.Entities.ManageData
{
    public class ManageBodega : IManageBodega
    {
        public readonly DBContextComercios _context;
        public ManageBodega(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region Search
        #region Get Bodega by Id
        public async Task<Bodega> GetBodegaById (Guid idBodega)
        {
            Bodega objBodega = await _context.Bodegas.Where(c => c.IdBodega == idBodega).FirstOrDefaultAsync();
            return objBodega;
        }
        #endregion
        #region Get All 
        public async Task<List<Bodega>> GetAllBodegas()
        {
            List<Bodega> listBodega = new ();
            try
            {
                listBodega = await _context.Bodegas.Select(p => p).ToListAsync();
            }
            catch(Exception ex)
            {

            }
            return listBodega;
        }
        #endregion
        #region Get by Name
        public async Task<Bodega> GetBodegaByName(string name)
        {
            Bodega objBodega = await _context.Bodegas.Where(c => c.Nombrebodega.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objBodega;
        }
        #endregion
        #region Get all by Local
        public async Task<List<Bodega>> GetAllBodegasByIdLocal(Guid idLocal)
        {
            List<Bodega> ListBodegasByLocal = new List<Bodega>();
            try
            {
                ListBodegasByLocal = await _context.Bodegas.Where(c => c.IdLocal == idLocal).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return ListBodegasByLocal;
        }
        #endregion
        #endregion


    }
}
