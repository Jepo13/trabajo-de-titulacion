﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManagePorcentajeIVA : IManagePorcentajeIVA
    {
        public readonly DBContextComercios _context;
        public ManagePorcentajeIVA(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region CRUD
        #region Add
        public void AddPorcenjateIVA(Porcentajeiva objPorcenjateIVA)
        {
            try
            {
                objPorcenjateIVA.Fechacreacion = DateTime.Now;
                objPorcenjateIVA.Fechamodificacion = DateTime.Now;
                _context.Porcentajeivas.AddAsync(objPorcenjateIVA);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditPorcentajeIVA(Porcentajeiva objPorcenjateIVA)
        {
            try
            {
                objPorcenjateIVA.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeletePorcentajeIVA(Porcentajeiva objPorcentajeIVA)
        {
            try
            {
                _context.Porcentajeivas.Remove(objPorcentajeIVA);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SavePorcentajeIVA()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion

        #region Search
        #region Get PorcentajeIVA by Id
        public async Task<Porcentajeiva> GetPorcentajeIVAById(Guid idPorcentajeIVA)
        {
            Porcentajeiva objPorcentajeIVA = await _context.Porcentajeivas.Where(c => c.IdPorcenajeiva == idPorcentajeIVA).FirstOrDefaultAsync();
            return objPorcentajeIVA;
        }
        #endregion
        #region Get All 
        public async Task<List<Porcentajeiva>> GetAllPorcentajeIVA()
        {
            List<Porcentajeiva> listPorcentajeIVA = new();
            try
            {
                listPorcentajeIVA = await _context.Porcentajeivas.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listPorcentajeIVA;
        }
        #endregion
  
        #endregion

    }
}
