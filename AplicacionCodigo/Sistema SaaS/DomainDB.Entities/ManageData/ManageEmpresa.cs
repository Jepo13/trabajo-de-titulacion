﻿using System;
using System.Collections.Generic;
using DomainDB.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DomainDB.Entities.InterfacesManageData;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Empresa;

namespace DomainDB.Entities.ManageData
{
    public class ManageEmpresa : IManageEmpresa
    {
        public readonly DBContextComercios _context;

        public ManageEmpresa(DBContextComercios context)
        {
            this._context = context ??
                throw new ArgumentException(nameof(context));
        }

        //#region CRUD
        //public void AddCompany(Empresa objCompany)
        //{
        //    try
        //    {
        //        objCompany.Fechacreacion = DateTime.Now;
        //        objCompany.Fechamodificacion = DateTime.Now;
        //        objCompany.Usuariomodificacion = objCompany.Usuariocreacion;
        //        _context.Empresas.AddAsync(objCompany);
        //    }
        //    catch (Exception exValidation)
        //    {

        //    }

        //}// END addCompany


        //public void EditCompany(Empresa objCompany)
        //{
        //    try
        //    {
        //        objCompany.Fechamodificacion = DateTime.Now;
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //} // END EditCompany

        //public void DeleteCompany(Empresa objCompany)
        //{
        //    try
        //    {
        //        _context.Empresas.Remove(objCompany);
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //}// END deleteCompany

        //public async Task<bool> SaveCompany()
        //{
        //    try
        //    {
        //        var create = await _context.SaveChangesAsync();
        //        return create > 0;
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return false;
        //}

        //#endregion

        #region Search

        public async Task<Empresa> GetCompanyById(Guid idCompany)
        {

            Empresa objCompany = await _context.Empresas.Where(c => c.IdEmpresa== idCompany).Include(c => c.UsuarioEmpresas).FirstOrDefaultAsync();

            return objCompany;
        }//END GetCompanyById


        public async Task<Empresa> GetCompanyByRUC(string ruc)
        {

            Empresa objCompany = await _context.Empresas.Where(c => c.Ruc.Trim().ToUpper().Contains(ruc.Trim().ToUpper())).FirstOrDefaultAsync();

            return objCompany;
        }//END GetCompanyById

        public async Task<List<Empresa>> GetAllCompany()
        {
            List<Empresa> listCompany = new List<Empresa>();
            try
            {

                listCompany = await _context.Empresas.Select(p => p).ToListAsync();
                
            }
            catch (Exception ex)
            {

            }
            return listCompany;
        }

        public async Task<List<Empresa>> GetCompaniesAdvanced(ObjetoBusquedaEmpresas objBusqueda)
        {
            List<Empresa> listCompany = new List<Empresa>();
            try
            {
                if (objBusqueda.estado != null)
                {
                    listCompany = _context.Empresas.Where(x => x.Estado == (bool)objBusqueda.estado).ToList();
                }
                else
                {
                    listCompany = await _context.Empresas.ToListAsync();
                }

               
                if (!string.IsNullOrEmpty(objBusqueda.Ruc))
                {
                    listCompany = listCompany.Where(x => x.Ruc.Contains(objBusqueda.Ruc.Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(objBusqueda.RazonSocial))
                {
                    listCompany = listCompany.Where(x => x.Razonsocial.ToUpper().Trim().Contains(objBusqueda.RazonSocial.ToUpper().Trim())).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return listCompany;
        }
        #endregion
    }

}
