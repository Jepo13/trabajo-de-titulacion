﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageLogError : IManageLogError
    {
        public DBContextComercios _context;
        //public readonly DBContextComercios _context;
        //public ManageLogError(DBContextComercios context)
        //{
        //    this._context = context ?? throw new ArgumentException(nameof(context));

        //}


        #region CRUD
        public void AddLogError(Logexcepcione objLogError)
        {
            objLogError.Fechaerror = DateTime.Now;
        }

        public async Task<bool> saveError(Logexcepcione objLog)
        {
            try
            {
                _context = new DBContextComercios();

                _context.Logexcepciones.Add(objLog);

                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                _context.Dispose();
            }
            return false;
        }

        #endregion

        #region Consultas
        public async Task<Logexcepcione> ByIDLog(Guid idLog)
        {
            throw new NotImplementedException();
        }

        public async Task<Logexcepcione> ByIDUsuario(Guid idUsuario)
        {
            throw new NotImplementedException();
        }

        public async Task<Logexcepcione> GeByMetodo(string metodo)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Logexcepcione>> GetByDate(DateTime fechaInicial, DateTime fechaFinal)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
