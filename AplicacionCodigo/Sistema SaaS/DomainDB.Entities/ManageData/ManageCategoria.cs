﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageCategoria : IManageCategoria
    {
        public readonly DBContextComercios _context;
        public ManageCategoria(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region CRUD
        #region Add
        public void AddCategoria(Categorium objCategoria)
        {
            try
            {
                objCategoria.Fechacreacion = DateTime.Now;
                objCategoria.Fechamodificacion = DateTime.Now;
                _context.Categoria.AddAsync(objCategoria);
            }
            catch (Exception exValidation)
            {

            }
        }
        #endregion
        #region Edit
        public void EditCategoria(Categorium objCategoria)
        {
            try
            {
                objCategoria.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region Delete
        public void DeleteCategoria(Categorium objCategoria)
        {
            try
            {
                _context.Categoria.Remove(objCategoria);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        #region  Save
        public async Task<bool> SaveCategoria()
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        #endregion
        #endregion

        #region Search
        #region Get Bodega by Id
        public async Task<Categorium> GetCategoriaById(Guid idCategoria)
        {
            Categorium objCategoria = await _context.Categoria.Where(c => c.IdCategoria == idCategoria).FirstOrDefaultAsync();
            return objCategoria;
        }
        #endregion
        #region Get All 
        public async Task<List<Categorium>> GetAllCategoria()
        {
            List<Categorium> listCategoria = new();
            try
            {
                listCategoria = await _context.Categoria.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listCategoria;
        }
        #endregion
        #region Get by Name
        public async Task<Categorium> GetCategoriaByName(string name)
        {
            Categorium objCategoria = await _context.Categoria.Where(c => c.Nombrecategoria.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objCategoria;
        }
        #endregion
        #endregion
    }
}
