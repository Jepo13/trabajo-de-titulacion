﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageCatalogo : IManageCatalogo

    {
        public readonly DBContextComercios _context;
        public ManageCatalogo(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

 

        #region Search
        #region Get Catalogo by Id
        public async Task<Catalogo> GetCatalogoById(Guid idCatalogo)
        {
            Catalogo catalogo = await _context.Catalogos.Where(c => c.IdCatalogo == idCatalogo).FirstOrDefaultAsync();
            return catalogo;
        }
        #endregion
        #region Get All 
        public async Task<List<Catalogo>> GetAllCatalogoByCompany(Guid idEmpresa)
        {
            List<Catalogo> listCatalogo = new();
            try
            {
                listCatalogo = await _context.Catalogos.Select(p => p).Include(x => x.IdEmpresaNavigation).Include(x => x.IdCatalogopadreNavigation).Where(x => x.IdEmpresa==idEmpresa).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listCatalogo;
        }
        #endregion
        #region Get by Name
        public async Task<List<Catalogo>> GetCatalogoByName(string name)
        {
            List<Catalogo> listCatalogo = await _context.Catalogos.Where(c => c.Nombrecatalogo.Trim().ToUpper().Contains(name.Trim().ToUpper())).ToListAsync();
            return listCatalogo;
        }
        #endregion
        
        #region Get by Code
        public async Task<Catalogo> GetCatalogoByCodeIDEmpresa(string code, Guid idEmpresa)
        {
            Catalogo objCatalogo = await _context.Catalogos.Where(c => c.Codigocatalogo.Trim().ToUpper().Contains(code.Trim().ToUpper()) && c.IdEmpresa==idEmpresa).FirstOrDefaultAsync();
            return objCatalogo;
        }
        #endregion
        
        #region Get Child by Parent Code 
        public async Task<List<Catalogo>> GetChildByParentCodeIDEmpresa(string code, Guid idEmpresa)
        {
            var listaCatalogos = await _context.Catalogos.Where(x => x.Codigocatalogo.Trim().ToUpper() == code.Trim().ToUpper() && x.IdEmpresa==idEmpresa)
                .Include(x => x.InverseIdCatalogopadreNavigation)
                .SelectMany(y => y.InverseIdCatalogopadreNavigation).ToListAsync();
         
            return listaCatalogos;
        }
        #endregion

        #region Get Child by Parent Code by ID
        public async Task<List<Catalogo>> GetChildByParentCodeByID_IDEmpresa(Guid idPadre, Guid idEmpresa)
        {
            var listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogopadre==idPadre && x.IdEmpresa==idEmpresa).ToListAsync();
         
            return listaCatalogos;
        }
        #endregion

        #region Get Child by Parent Code by Name
        public async Task<List<Catalogo>> GetChildByParentCodeByID_Nombre(string nombrePadre, Guid idEmpresa)
        {
            var catalogoPadre = await _context.Catalogos.Where(x => x.Nombrecatalogo.ToLower().Trim() == nombrePadre.ToLower().Trim() && x.IdEmpresa == idEmpresa).Include(x => x.InverseIdCatalogopadreNavigation).FirstOrDefaultAsync();

            var listaCatalogos = catalogoPadre.InverseIdCatalogopadreNavigation.ToList();


            return listaCatalogos;
        }
        #endregion


        #region Get Parent by Child
        public async Task<List<Catalogo>> GetCatalogsUpLevel_Company(Guid idCatalogoHijo, Guid idEmpresa)
        {
            var listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogo== idCatalogoHijo && x.IdEmpresa==idEmpresa).
                Include(x => x.IdCatalogopadreNavigation)
                .SelectMany(y => y.IdCatalogopadreNavigation.IdCatalogopadreNavigation.InverseIdCatalogopadreNavigation).ToListAsync();

            return listaCatalogos;
        }
        #endregion

        #region Get Catalogs Same Label
        public async Task<List<Catalogo>> GetCatalogsSameLevelByID_Company(Guid idCatalogoHermano, Guid idEmpresa)
        {
            try
            {
                var listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHermano && x.IdEmpresa == idEmpresa).Include(x => x.IdCatalogopadreNavigation)
                       .SelectMany(y => y.IdCatalogopadreNavigation.InverseIdCatalogopadreNavigation).ToListAsync();


                return listaCatalogos;
            }
            catch (Exception)
            {

                
            }

            return null;
        }
        #endregion

        #endregion
    }
}
