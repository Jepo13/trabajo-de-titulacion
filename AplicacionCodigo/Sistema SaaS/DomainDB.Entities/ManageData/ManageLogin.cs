﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainDB.Entities.InterfacesManageData;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace DomainDB.Entities.ManageData
{
    public class ManageLogin : IManageLogin
    {
        private readonly DBContextComercios _context;

        public ManageLogin(DBContextComercios context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public string recuperarIndicioContrasena(Guid idPersona)
        {
            try
            {
                var objetoUsuario = _context.Usuarios.Select(user => user).Where(user => user.IdPersona == idPersona).First();

                return objetoUsuario.Indiciocontrasena;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//recuperarIndicioContrasena

        public Persona ingresarSistema(string numeroIdentificacion, string contrasena)
        {
            try
            {
                //var objetoPersona = _context.Personas.Select(p => p).Where(p => p.Numeroidentificacion == numeroIdentificacion && p.Usuario.Contrasena == contrasena).First();

                //return objetoPersona;

            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }//ingresarSistema

    }//class
}
