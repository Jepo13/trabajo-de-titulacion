﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainDB.Entities.InterfacesManageData;
using DomainDB.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Local;

namespace DomainDB.Entities.ManageData
{
   public class ManageLocal : IManageLocal
    {
        public readonly DBContextComercios _context;
        public ManageLocal(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region CRUD
        public void AddLocal(Local objLocal)
        {
            try
            {
                objLocal.Fechacreacion = DateTime.Now;
                objLocal.Fechamodificacion = DateTime.Now;
                objLocal.Usuariomodificacion = objLocal.Usuariocreacion;

                _context.Locals.AddAsync(objLocal);
            }
            catch (Exception exValidation)
            {

            }
        } // End AddLocal
        public void EditLocal(Local objLocal)
        {
            try
            {
                objLocal.Fechamodificacion = DateTime.Now;
            }
            catch (Exception ExValidation)
            {

            }
        } // End EditLocal
        public void DeleteLocal(Local objLocal)
        {
            try
            {
                _context.Locals.Remove(objLocal);
            }
            catch (Exception ExValidation)
            {

            }
        } // End EditLocal
        public async Task<(bool estado, string mensajeError)> saveLocal()
        {
            try
            {
                var created = await _context.SaveChangesAsync();
                return (created > 0,string.Empty);
            }
            catch (Exception ex)
            {
                string mensajeError = "";
                if (ex.InnerException != null)
                    mensajeError = ex.InnerException.Message;
                
                return (false,mensajeError);
            }
            
        } // End SaveLocal
        #endregion

        #region Search
        public async Task<Local> GetLocalById(Guid idLocal)
        {
            Local objLocal = await _context.Locals.Where(c => c.IdLocal == idLocal).Include(x => x.IdUbicacionNavigation).FirstOrDefaultAsync();
            return objLocal;
        }// End GetLocalById

        public async Task<Local> GetLocalByNombre(string nombreLocal)
        {
            Local objLocal = await _context.Locals.Where(c => c.Nombrelocal.Trim().ToUpper().Contains(nombreLocal.Trim().ToUpper())).FirstOrDefaultAsync();
            return objLocal;
        }
        public async Task<List<Local>> GetAllLocales()
        {
            List<Local> listLocal = new List<Local>();
            try
            {
                listLocal = await _context.Locals.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listLocal;
        }

        public async Task<List<Local>> GetLocalAdvanced(ObjetoBusquedaLocal objBusqueda)
        {
            List<Local> listLocal = new List<Local>();
            try
            {
                if(objBusqueda.estado!= null)
                {
                    listLocal = _context.Locals.Where(x => x.Estado == (bool)objBusqueda.estado).Include(x => x.IdTipolocalNavigation).Include(x => x.IdUbicacionNavigation).ToList();
                }
                else
                {
                    listLocal = await _context.Locals.Include(x => x.IdTipolocalNavigation).Include(x => x.IdUbicacionNavigation).ToListAsync();
                }

                if (!string.IsNullOrEmpty(objBusqueda.Nombre))
                {
                    listLocal = listLocal.Where(x => x.Nombrelocal.ToUpper().Trim().Contains(objBusqueda.Nombre.ToUpper().Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(objBusqueda.NomreEmpresa))
                {
                    listLocal = listLocal.Where(x => x.IdEmpresaNavigation.Razonsocial.ToUpper().Trim().Contains(objBusqueda.NomreEmpresa.ToUpper().Trim())).ToList();
                }

                //if (!string.IsNullOrEmpty(objBusqueda.IdTipoLocal))
                //{
                //    listLocal = listLocal.Where(x => x.IdTipolocalNavigation.Nombrecatalogo.ToUpper().Trim().Contains(objBusqueda.TipoLocal.ToUpper().Trim())).ToList();
                //}

            }
            catch
            {

            }
           
            return listLocal;
        }
        #endregion

    }
}
