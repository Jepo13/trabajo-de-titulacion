﻿using DomainDB.Entities.Entities;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainDB.Entities.ManageData
{
    public class ManageMarca : IManageMarca
    {
        public readonly DBContextComercios _context;
        public ManageMarca(DBContextComercios context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region CRUD
        public void AddMarca(Marca objMarca)
        {
            try
            {
                objMarca.Fechacreacion = DateTime.Now;
                objMarca.Fechamodificacion = DateTime.Now;
                _context.Marcas.AddAsync(objMarca);
            }
            catch (Exception ex)
            {

            }
        }
        public void EditMarca(Marca objMarca)
        {
            try
            {
                objMarca.Fechacreacion = DateTime.Now;
            }
            catch (Exception ex)
            {

            }
        }
        public void DeleteMarca(Marca objMarca)
        {
            try
            {
                _context.Marcas.Remove(objMarca);
            }
            catch (Exception ex)
            {

            }
        }
        public async Task<bool> SaveMarca() 
        {
            try
            {
                var create = await _context.SaveChangesAsync();
                return create > 0;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        #endregion
        #region Search
        public async Task <Marca> GetMarcaById (Guid idMarca)
        {
            Marca objMarca = await _context.Marcas.Where(c => c.IdMarca == idMarca).FirstOrDefaultAsync();
            return objMarca;
        }
        public async Task <List<Marca>> GetAllMarcas()
        {
            List<Marca> listMarca = new List<Marca>();
            try
            {
                listMarca = await _context.Marcas.Select(p => p).ToListAsync();
            }catch(Exception ex) { }
            return listMarca;
        }

        public async Task<Marca> GetMarcaByName (string name)
        {
            Marca objMarca = await _context.Marcas.Where(c => c.Nombre.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objMarca;
        }
        #endregion
    }
}
