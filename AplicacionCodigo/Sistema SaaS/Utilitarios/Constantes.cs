﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios
{
    public class Constantes
    {
        //DESARROLLO
        public const string pathConsola = "";
        public const string pathAPI = "";
        public const string urlConsola = "http://localhost:2646";
        public const string mensajeSesionCaducada = "?sesionCaducada=Tu sesión ha caducado";

        #region Sesion
        public const string nombreSesion = "datosSesion";
        #endregion

        #region Empresa
        public const string getGetCompaniesAdvance = "/api/Empresa/GetCompaniesAdvanced";
        public const string getGetCompanyByID = "/api/Empresa/";
        public const string getGetCompanyByRUC = "/api/Empresa/GetCompanyByRUC?rucCompany=";
        public const string getGetCompanyEditar = "/api/Empresa/Edit?idCompany=";
        public const string getGetCompanyEliminar = "/api/Empresa/DeleteCompany?idCompany=";
        public const string getGetCompanyCreate = "/api/Empresa/Create";
        public const string getGetCompanyaAll = "/api/Empresa/GetAllCompany";







        #endregion

        #region ROL
        public const string GetAllRolsByCompany = "/api/Rol/GetAllRolsByCompany?idEmpresa=";
        public const string getGetRolCreate = "/api/Rol/Create";
        public const string getGetRolEditar = "/api/Rol/Edit?idRol=";
        public const string getGetRolEliminar = "/api/Rol/Delete?idRol=";
        public const string endPointRolByID = "/api/Rol/";
        #endregion

        #region Local
        public const string endPointCrearLocal = "/api/Local/Create";
        public const string endPointLocalByID = "/api/Local/";
        public const string getGetLocalAdvance = "/api/Local/GetLocalAdvanced";
        public const string getGetLocalEditar = "/api/Local/Edit?idLocal=";
        public const string getGetLocalEliminar = "/api/Local/DeleteLocal?idLocal=";
        public const string endPointCrearSitio = "/api/Local/CreateSitio";
        #endregion

        #region Ubicacion
        public const string endPointCrearUbicacion = "/api/Local/CreateUbicacion";
        public const string endPointEditarUbicacion = "/api/Local/EditUbicacion";
        public const string GetUbicacionById = "/api/Local/GetUbicacionById?idUbicacion=";
        public const string endPointCrearBandeja= "/api/Local/CreatBandejas";


        public const string getSitiosByIdUbicacion= "/api/Local/GetSitiosByIdUbicacion?IdUbicacion=";
        
        #endregion

        #region Proveedor
        //-----CRUD------
        public const string CreateProveedorEndPoint = "/api/Proveedor/Create";
        public const string EditProveedorEndPoint = "/api/Proveedor/Edit?idProveedor=";
        public const string DeleteProveedorEndPoint = "/api/Proveedor/DeleteProveedor?idProveedor=";
        //-----BUSQUEDA------
        public const string GetAdvanceProveedorEndPoint = "/api/Proveedor/GetProveedoresAdvanced";
        public const string GetByIdProveedorEndPoint = "/api/Proveedor/";
        public const string GetByRucProveedorEndPoint = "/api/Proveedor/GetProveedorByRUC?rucProveedor=";
        #endregion

        #region Cliente Persona
        //-----CRUD------
        public const string CreatePersonaEndPoint = "/api/Clinete/Create";
        public const string EditPersonaEndPoint = "/api/Clinete/Edit";
        public const string DeletePersonaEndPoint = "/api/Clinete/Delete";
        public const string CreateEmpresaClienteEndPoint = "/api/Clinete/CreateEmpresaCliente";
        public const string EditarEmpresaClienteEndPoint = "/api/Clinete/EditEmpresaCliente";
        public const string EliminarEmpresaClienteEndPoint = "/api/Clinete/EliminarEmpresaCliente";
        public const string GetEmpresaClienteByID = "/api/Clinete/GetEmpresaClienteById?idEmpresaCliente=";
        public const string GetEmpresaClienteByObjValidar = "/api/Clinete/GetEmpresaClienteValidar";
        public const string GetEmpresaClientebyIdPersona = "/api/Clinete/GetEmpresaClientebyIdPersona?idPersona=";


        //-----BUSQUEDA------
        public const string GetByIdPersonaEndPoint = "/api/Clinete/";
        public const string GetAdvanceEndPoint = "/api/Clinete/GetClienteAdvanced";
        public const string GetByIdentificacionPersonaEndPoint = "/api/Clinete/GetPersonaByNumeroIdentificacion?numeroIdentificacion=";
        public const string GetClientePersonaByIdPersona = "/api/Clinete/GetClientePersonaByIdPersona?idPersona=";

        #endregion

        #region Catalogo
        public const string getGetCatalogoCodeIDEmpresa = "/api/Catalogo/GetCatalogoByCodeIDCompany?codigoCatalgo=";
        public const string getGetCatalogoNombreIDEmpresa = "/api/Catalogo/GetCatalogoByName?nameCatalogo=";

        public const string getGetCatalogosHijosPorCodigoIDEmpresa = "/api/Catalogo/GetCatalogsChildsIDCompany?codigoCatalgo=";
        public const string getGetCatalogosHijosPorID = "/api/Catalogo/GetCatalogsChildsByID_Company?idPadre=";
        public const string getGetCatalogosHijosPorNombre = "/api/Catalogo/GetCatalogsChildsByName_Company?nombrePadre=";
        public const string getGetCatalogosPadreTios = "/api/Catalogo/GetCatalogsUpLevel_Company?idCatalogo=";
        public const string getGetCatalogosHermanosPorID = "/api/Catalogo/GetCatalogsSameLevelByID_Company?idCatalogoHermano=";
        public const string getGetCatalogosTodosPorIdEmpresa = "/api/Catalogo/GetAllCatalogosByIDCompany?idEmpresa=";
        public const string getGetCatalogosPorIdCatalogo = "/api/Catalogo/";
        public const string getGetCatalogosCreate = "/api/Catalogo/Create";
        public const string getEditCatalogo = "/api/Catalogo/Edit?idCatalogo=";
        public const string getDeleteCatalogo = "/api/Catalogo/DeleteCatalogo?idCatalogo=";
        public const string getCatalogosHijosPorCodigoPadre = "/api/Catalogo/optenerCatalogosGeneral?codigoPadreCatalogo=";
        public const string getCatalogosHermanoPorID = "/api/Catalogo/optenerCatalogosHermanoPorID?idCatalogoHermano=";
        public const string getCatalogosHermanoPadrePorIDHijo = "/api/Catalogo/optenerCatalogosHermanoPorIdHijo?idCatalogo=";

        #endregion

        #region Usuario
        public const string getLogin = "/api/User/LoginUser?";
        public const string getCreateUser = "/api/User/Create";
        public const string getEditUser = "/api/User/Edit?IdUsuario=";
        public const string getUserByID = "/api/User/";
        public const string getAdvancedSearch = "/api/User/GetUsersAdvanced";
        public const string eliminarUser = "/api/User/Delete?IdUsuario=";
        public const string buscarUsuarioValidarPersonaCreada = "/api/User/GetUsurioPorIdentificacionparaValidarCreacion?numeroIdentificacion=";

        public const string getUsuarioEmpresaByIdUser = "/api/User/GetUsuarioEmpresasByIdUsuario?IdUsuario=";


        #endregion

        #region CategoriasProductos
        public const string getGetCategoriasTodosPorIdEmpresa = "/api/CategoriaProducto/GetAllCategoriaByIDCompany?idEmpresa=";
        public const string getCrearCategoriaProductos = "/api/CategoriaProducto/Create";
        public const string getBuscarCategoriaProductosPorPadre = "/api/CategoriaProducto/GetHijosPorCategoria?idCategoriaPadre=";

        public const string getGetCategoriaProductoPorID = "/api/CategoriaProducto/";
        public const string getEditCategoria = "/api/CategoriaProducto/Edit?idCategoriaProducto=";
        public const string getEliminarCategoria = "/api/CategoriaProducto/DeleteCategoria?idCategoriaProducto=";

        public const string getBusquedaAvanzadaCategoria = "/api/Catalogo/BusquedaAvanzadaCatalogo";
        #endregion

        #region CategoriasProductos
        public const string getCrearProductos = "/api/Producto/Create";
        public const string getBuscarProductoPorNombre = "/api/Producto/GetProductoByName?nameProducto=";
        #endregion

        #region PorcentajeIva
        public const string GetByIdPorcentajeIva = "/api/PorcentajeIVA/";
        public const string GetAllPorcentajeIva = "/api/PorcentajeIVA/GetAll";
        #endregion

        #region CódigoCatalogos
        public const string padreTipoLocal = "TPL";
        public const string padreTipoUbicacion = "TIPUBIC";
        public const string padreProvincias = "ECUADR";
        //public const string codigoPaises = "Paises";
        //public const string padreProvincias = "EC";
        public const string padrePaises = "PAS";
        public const string padreTipoIdentificacion = "TID";
        public const string padreEstadoCivil = "ETCL";
        public const string padreModulos = "MDLS";
        public const string padrePermisos = "TPPMS";
        public const string padreTipoContribuyente = "TCB";
        public const string padreEspecialidad = "ESP";
        public const string padreImpuesto = "IVA";


        #endregion


        #region Unidad Entrega
        public const string endPointCrearUnidadEntrega = "/api/UnidadEntrega/Create";
        public const string endPointUnidadEntregaByID = "/api/UnidadEntrega/";
        public const string getGetUnidadEntregaAdvance = "/api/UnidadEntrega/GetLocalAdvanced";
        public const string getGetUnidadEntregaEditar = "/api/UnidadEntrega/Edit?idUnidadEntrega=";
        public const string getGetUnidadEntregaEliminar = "/api/UnidadEntrega/DeleteLocal?idUnidadEntrega=";
        public const string getGetUnidadEntregaPorNombreExacto = "/api/UnidadEntrega/GetUnidadEntregaByNameExact?nameUnidadEntrega=";
        public const string getGetUnidadEntregaPorCodigo = "/api/UnidadEntrega/GetUnidadEntregaByCode?codigo=";
        public const string getGetUnidadEntregaPorNombre = "/api/UnidadEntrega/GetUnidadEntregaByNameExact?nameUnidadEntrega=";
        public const string getGetUnidadEntregaPorEmpresa = "/api/UnidadEntrega/GetAllUnidadEntrega?idEmpresa=";
        public const string getBusquedaAvanzadaUnidadEntrega = "/api/UnidadEntrega/GetBusquedaAvanzada";
        #endregion


        #region Grupo Producto
        public const string endPointCrearGrupoProducto = "/api/GrupoProducto/Create";
        public const string endPointGrupoProductoByID = "/api/GrupoProducto/";
        public const string endPointGrupoProductoTodos = "/api/GrupoProducto/GetAllGrupoProducto/?idEmpresa=";

        public const string getGetGrupoProductoEditar = "/api/GrupoProducto/Edit?idGrupoProducto=";
        public const string getGetGrupoProductoEliminar = "/api/GrupoProducto/DeleteGrupoProducto?idGrupoProducto=";
        //public const string getGetGrupoProductoPorNombreExacto = "/api/GrupoProducto/GetGrupoProductoByNameExact?nameGrupoProducto=";      
        public const string getGetGrupoProductoPorNombre = "/api/GrupoProducto/GetGrupoProductoByName?name=";
        public const string getGetGrupoProductoPorEmpresa = "/api/GrupoProducto/GetAllGrupoProducto?idEmpresa=";
        public const string getBusquedaAvanzadaGrupoProducto = "/api/GrupoProducto/GetBusquedaAvanzada";

        public const string getGetGrupoProductoAdvance = "/api/GrupoProducto/GetBusquedaAvanzada";
        #endregion

    }
}
