﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios.Busquedas.Persona
{
    public class ObjetoBusquedaPersona
    {
        public Guid IdPersona { get; set; }
        public string NombreApellido { get; set; }
       
        public string NumeroIdentificacion { get; set; }
       
    }
}
