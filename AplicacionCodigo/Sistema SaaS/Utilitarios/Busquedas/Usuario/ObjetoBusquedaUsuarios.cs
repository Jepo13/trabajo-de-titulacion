﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios.Busquedas.Usuario
{
    public class ObjetoBusquedaUsuarios
    {
        public Guid? IdRol { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdPersona { get; set; }
        public string numeroIdentificacion { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string correo { get; set; }
    }
}
