﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios.Busquedas.Empresa
{
    public class ObjetoBusquedaEmpresas
    {
        public string Ruc { get; set; }
        public string RazonSocial { get; set;}
        public bool? estado { get; set; }
      
    }
}
