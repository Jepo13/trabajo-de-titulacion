﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios.Busquedas.Catalogo
{
    public class ObjetoBusquedaCatalogo
    {
        public string objCodigo { set; get; }
        public Guid idEmpresa { set; get; }
    }
}
