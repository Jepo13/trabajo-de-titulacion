﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios.Busquedas.Proveedor
{
    public class ObjetoBusquedaProveedor
    {
        public Guid? IdEmpresa { get; set; }
        public Guid? IdEspecialidad { get; set; }
        public string Ruc { get; set; }
        public string Razonsocial { get; set; }
        public bool? Estado { get; set; }
        public string NombreRepresentate { get; set; }

    }
}
