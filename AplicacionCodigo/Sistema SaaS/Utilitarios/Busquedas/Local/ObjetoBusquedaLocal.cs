﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios.Busquedas.Local
{
    public class ObjetoBusquedaLocal
    {
        public Guid? IdEmpresa { get; set; }
        public Guid? IdTipoLocal { get; set; }
        public string Nombre  { get; set; }
        public string NomreEmpresa { get; set; }         
        public bool? estado { get; set; }

    }
}
