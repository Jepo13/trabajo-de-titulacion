﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios
{
    public class MensajesRespuesta
    {
        private const string mensajeErrorConexion = "El servidor tardó mucho en responder, por favor revise su conexión a internet.";
        private const string mensajeGuardarOk = "Sus datos han sido guardados correctamente";
        private const string mensajeEliminadoOk = "Sus datos han sido eliminados correctamente, esta acción no se puede deshacer.";
        public const string mensajeIntetoGuardarDuplicado = "Ya existe un registro con la misma información por favor verifique.";
        public const string mensajeIntetoGuardarDuplicadoEmpresaCLiente = "Ya existe un registro con la misma empresa para este cliente por favor verifique.";

        private const string mensajeGuardarError = "Ocurrió un error inesperado al intentar guardar.";
        private const string mensajeErrorInesperado = "Ocurrió un error inesperado al intentar al procesar la información.";
        private const string mensajeObjetosNulos = "No se puede crear este elemento porque no contiene información.";
        public const string mensajeErrorObjetoBlanco = "No se acepta información en blanco nulos.";
        private const string mensajeInformacionInvalida = "Información no válida.";
        public const string mensajeBusquedaSinResultados = "No se encontraron resultados para su búsqueda.";
        public const string mensajeNoSePuedeEliminarPorHijos = "No se puede eliminar, porque tiene hijos.";
        
        public const string mensajeErrorUsuarioContrasena = "Usuario/Correo o contraseña incorrectos, por favor verifique e intente nuevamente.";
        public const string mensajeErroLogin = "Datos de usuario o contraseña no válidos: ";
        private const string mensajeEnvioCorreoOk = "Se ha enviado un correo electrónico, por favor revisar su bandeja de entrada:";
        private const string mensajeEnvioCorreoError = "No se ha podido enviar el correo electrónico:";
        public const string mensajeEstadoUsuario = "El estado del usuario no le permite realizar la acción solicitada:";

        public bool IsSuccess { get; set; }
        public Guid orderId { get; set; }
        public string message { get; set; }
        public string icon { get; set; }
        //para los iconos en sweet Alert
        public string state { get; set; }
        //Manejar el estado en la respuesta JSON
        public int status { get; set; }
        public string email { get; set; }
        public string title { get; set; }
        public string urlRetorno { get; set; }

        public MensajesRespuesta()
        {

        }

        public MensajesRespuesta(string mensaje, bool bandera, string state, string icon)
        {
            this.message = mensaje;
            IsSuccess = bandera;
            this.state = state;
            this.icon = icon;         
        }
        public MensajesRespuesta(string mensaje, bool bandera, string state, string icon, string urlRetorno)
        {
            this.message = mensaje;
            IsSuccess = bandera;
            this.state = state;
            this.icon = icon;         
            this.urlRetorno = urlRetorno;         
        }

        public MensajesRespuesta(string mensaje, string? correo = "")
        {
            message = mensaje;
            email = correo;
        }

        public static MensajesRespuesta errorConexion()
        {
            return new MensajesRespuesta(mensajeErrorConexion, false, "¡Error!", "error");
        }

        public static MensajesRespuesta guardarOK()
        {
            return new MensajesRespuesta(mensajeGuardarOk, true, "Exitoso!", "success");
        }

        public static MensajesRespuesta guardarOK(string controlador, string accion)
        {
            return new MensajesRespuesta(mensajeGuardarOk, true, "Exitoso!", "success",Constantes.urlConsola+"/"+controlador+"/"+accion);
        }

        public static MensajesRespuesta eliminadoOK()
        {
            return new MensajesRespuesta(mensajeEliminadoOk, true, "Exitoso!", "info");
        }
        public static MensajesRespuesta errorDuplicados(string mensaje)
        {
            return new MensajesRespuesta(mensaje, true, "Exitoso!", "info");
        }

        public static MensajesRespuesta eliminadoOK(string controlador, string accion)
        {
            return new MensajesRespuesta(mensajeEliminadoOk, true, "Exitoso!", "info", Constantes.urlConsola + "/" + controlador + "/" + accion);
        }

        public static MensajesRespuesta eliminadoOKsinRedireccion()
        {
            return new MensajesRespuesta(mensajeEliminadoOk, true, "Exitoso!", "info");
        }
        public static MensajesRespuesta guardarError()
        {
            return new MensajesRespuesta(mensajeGuardarError, false, "¡error!", "error");
        }

        public static MensajesRespuesta noSePermiteObjNulos()
        {
            return new MensajesRespuesta(mensajeObjetosNulos, false, "¡error!", "error");
        }



        public static MensajesRespuesta mensajeErrorLogin(string usuario)
        {
            return new MensajesRespuesta(mensajeErroLogin + " " + usuario, false, "¡error!", "error");
        }
        public static MensajesRespuesta errorPorEstadoUsuario(string usuario)
        {
            return new MensajesRespuesta(mensajeEstadoUsuario + " " + usuario, false, "¡error!", "error");
        }
        public static MensajesRespuesta errorInesperado()
        {
            return new MensajesRespuesta(mensajeErrorInesperado, false, "¡error!", "error");
        }

        public static MensajesRespuesta sinResultados()
        {
            return new MensajesRespuesta(mensajeBusquedaSinResultados, true, "Exitoso!", "info");
        }

        public static MensajesRespuesta ErrorEliminarHijos()
        {
            return new MensajesRespuesta(mensajeNoSePuedeEliminarPorHijos, true, "error!", "error");
        }



        public static MensajesRespuesta datoDuplicado()
        {
            return new MensajesRespuesta(mensajeIntetoGuardarDuplicado, false, "¡Error!", "info");
        }
        public static MensajesRespuesta datoDuplicadoEmpresaCliente()
        {
            return new MensajesRespuesta(mensajeIntetoGuardarDuplicadoEmpresaCLiente, false, "¡Error!", "info");
        }
        public static MensajesRespuesta usuarioContrasenaIncorrecta()
        {
            return new MensajesRespuesta(mensajeErrorUsuarioContrasena, true, "!error!", "error");
        }



    }
}
