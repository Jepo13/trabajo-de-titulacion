﻿using DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios
{
    public class FuncionesUtiles
    {
        public static string consturirUsuarioAuditoria(UserSessionDTO objUsuarioSesion)
        {
            try
            {
                string nombreUsuario = objUsuarioSesion.IdUsuario + ";" + objUsuarioSesion.Nombre + " " + objUsuarioSesion.Apellido;

                return nombreUsuario;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
