﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOs.User;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Utilitarios
{
    public static class Sesion
    {
        public static UserSessionDTO recuperarSesion(ISession session)
        {
            try
            {
                string json = session.GetString(Constantes.nombreSesion);
                var objSesion = JsonConvert.DeserializeObject<UserSessionDTO>(json);

                return objSesion;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
