﻿using EntidadesRolesUsuarios.Entities;
using RepositorioRolesUsuarios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioRolesUsuarios.Implementacion
{
    public class ManageCRUD_Roles_Usuarios<T> : IManageCRUD_Roles_Usuarios<T> where T : class
    {
        public readonly Context_DB_Roles_Usuarios_SAS _context;



        public ManageCRUD_Roles_Usuarios(Context_DB_Roles_Usuarios_SAS context)
        {
            _context = context;
        }

        public void Add(T obj)
        {
            try
            {
                obj.GetType().GetProperty("FechaCreacion").SetValue(obj, DateTime.Now);
                obj.GetType().GetProperty("FechaModificacion").SetValue(obj, DateTime.Now);
                obj.GetType().GetProperty("UsuarioModificacion").SetValue(obj, obj.GetType().GetProperty("UsuarioCreacion").GetValue(obj, null));
                //obj.Fechacreacion = DateTime.Now;
                //obj.Fechamodificacion = DateTime.Now;
                //obj.Usuariomodificacion = T.Usuariocreacion;

                //_context.Set<T>().Add(obj);
                _context.AddAsync(obj);
            }
            catch (Exception exValidation)
            {

            }

        }
        public void Edit(T obj)
        {
            try
            {
                obj.GetType().GetProperty("FechaModificacion").SetValue(obj, DateTime.Now);
            }
            catch (Exception ex)
            {

              
            }
        }

        public void Delete(T obj)
        {

            _context.Remove(obj);
        }
        public async Task<(bool estado, string mensajeError)> save()
        {
            try
            {
                var created = await _context.SaveChangesAsync();
                return (created > 0, string.Empty);
            }
            catch (Exception ex)
            {
                try
                {
                    var created = await _context.SaveChangesAsync();
                    return (created > 0, string.Empty);
                }
                catch (Exception exx)
                {
                    string mensajeError = "";
                    if (exx.InnerException != null)
                        mensajeError = ex.InnerException.Message;

                    return (false, mensajeError);
                }
            }

        }

    }
}
