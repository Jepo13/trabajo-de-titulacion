﻿using EntidadesRolesUsuarios.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioRolesUsuarios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioRolesUsuarios.Implementacion
{
    public class ManageRol : IManageRol
    {
        public readonly Context_DB_Roles_Usuarios_SAS _context;

        public ManageRol(Context_DB_Roles_Usuarios_SAS context)
        {
            this._context = context ??
            throw new ArgumentException(nameof(context));
        }

        public async Task<List<Rol>> GetAllRolsByCompany(Guid idEmpresa)
        {
            List<Rol> listRols = new List<Rol>();
            try
            {
                listRols = await _context.Rols.Where(x => x.IdEmpresa == idEmpresa).Include(x => x.Modulos).ToListAsync();

            }
            catch (Exception ex)
            {

            }
            return listRols;
        }

        public async Task<Rol> GetRolByID(Guid IdRol)
        {
            try
            {
                Rol objRol = await _context.Rols.Where(c => c.IdRol == IdRol).Include(x => x.Modulos)
                    .ThenInclude(x => x.Menus).ThenInclude(x => x.Permisos).FirstOrDefaultAsync();

                return objRol;
            }
            catch (Exception ex)
            {
            }

            return null;
        }
    }
}
