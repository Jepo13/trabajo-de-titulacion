﻿using EntidadesRolesUsuarios.Entities;
using RepositorioRolesUsuarios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioRolesUsuarios.Implementacion
{
    public class ManageLogin : IManageLogin
    {
        private readonly Context_DB_Roles_Usuarios_SAS _context;

        public ManageLogin(Context_DB_Roles_Usuarios_SAS context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public string recuperarIndicioContrasena(Guid idPersona)
        {
            try
            {
                var objetoUsuario = _context.Usuarios.Select(user => user).Where(user => user.IdPersona == idPersona).First();

                return objetoUsuario.IndicioContrasena;
            }
            catch (Exception ex)
            {
                return null;
            }
        }//recuperarIndicioContrasena

    }
}
