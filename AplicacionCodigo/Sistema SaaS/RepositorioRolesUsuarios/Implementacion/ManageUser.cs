﻿using EntidadesRolesUsuarios.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioRolesUsuarios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Usuario;

namespace RepositorioRolesUsuarios.Implementacion
{
    public class ManageUser : IManageUser
    {
        public readonly Context_DB_Roles_Usuarios_SAS _context;
        public ManageUser(Context_DB_Roles_Usuarios_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Search

        public async Task<Usuario> getPersonaById(Guid idPersona)
        {
            try
            {
                Usuario objUser = await _context.Usuarios.Where(c => c.IdPersona == idPersona).Include(X => X.IdRolNavigation).FirstOrDefaultAsync();

                return objUser;
            }
            catch (Exception)
            {
            }

            return null;
        }

        public async Task<Usuario> getUserById(Guid idUser)
        {
            try
            {
                Usuario objUser = await _context.Usuarios.Where(c => c.IdUsuario == idUser).Include(X => X.IdRolNavigation).FirstOrDefaultAsync();

                return objUser;
            }
            catch (Exception)
            {
            }

            return null;
        } // End get user by id
        public async Task<Usuario> getUserByCorreo(string correoUser)
        {
            Usuario objUser = await _context.Usuarios.Where(c => c.CorreoInstitucional == correoUser).FirstOrDefaultAsync();
            return objUser;
        }// get user by name

        public async Task<Usuario> getLoginUser(string correoUser, string contrasena)
        {
            try
            {
                Usuario objUser = await _context.Usuarios.
                    Where(c => c.CorreoInstitucional == correoUser &&
                    c.Contrasena == contrasena).Include(x => x.IdRolNavigation).ThenInclude(x => x.Modulos).ThenInclude(x => x.Menus).ThenInclude(x => x.Permisos).AsNoTracking().FirstOrDefaultAsync();
                return objUser;
            }
            catch (Exception ex)
            {

                throw;
            }

        }// get user by name


        public async Task<List<Usuario>> getAllUsers()
        {
            List<Usuario> listUseres = new List<Usuario>();
            try
            {
                listUseres = await _context.Usuarios.Select(p => p).ToListAsync();
            }
            catch (Exception exValidation)
            {
            }

            return listUseres;
        } //  end get all users 

        public async Task<List<Usuario>> GetUserAdvanced(ObjetoBusquedaUsuarios objBusqueda)
        {
            List<Usuario> listUser = new List<Usuario>();
            try
            {
                if (objBusqueda.IdRol == null)
                {
                    listUser = await _context.Usuarios.Select(x => x).Include(x => x.IdRolNavigation).ToListAsync();
                }
                else
                {
                    listUser = await _context.Usuarios.Where(x => x.IdRol == objBusqueda.IdRol).Include(x => x.IdRolNavigation).ToListAsync();
                }
                if (objBusqueda.IdEmpresa != null)
                {
                    listUser =  listUser.Where(x => x.IdEmpresaDefault == objBusqueda.IdEmpresa).ToList();
                }
                if (!string.IsNullOrEmpty(objBusqueda.correo))
                {
                    listUser = listUser.Where(x => x.CorreoInstitucional.Contains(objBusqueda.correo)).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return listUser;
        }
        #endregion

    }
}
