﻿using EntidadesRolesUsuarios.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioRolesUsuarios.Interface
{
    public interface IManageRol
    {
        public Task<List<Rol>> GetAllRolsByCompany(Guid idCompany);
        public Task<Rol> GetRolByID(Guid IdRol);
    }
}
