﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioRolesUsuarios.Interface
{
    public interface IManageLogin
    {
        public string recuperarIndicioContrasena(Guid idPersona);
    }
}
