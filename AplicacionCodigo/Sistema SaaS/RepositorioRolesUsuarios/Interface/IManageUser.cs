﻿using EntidadesRolesUsuarios.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Usuario;

namespace RepositorioRolesUsuarios.Interface
{
    public interface IManageUser
    {
        #region Search
        public Task<Usuario> getUserById(Guid idUser);
        public  Task<Usuario> getPersonaById(Guid idPersona);
        public Task<Usuario> getUserByCorreo(string correoUser);
        public Task<List<Usuario>> getAllUsers();

        public Task<Usuario> getLoginUser(string correoUser, string contrasena);
        #endregion

        #region Buscar
        public Task<List<Usuario>> GetUserAdvanced(ObjetoBusquedaUsuarios objBusqueda);
        #endregion

    }
}
