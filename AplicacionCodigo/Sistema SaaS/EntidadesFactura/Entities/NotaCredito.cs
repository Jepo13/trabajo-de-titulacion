﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class NotaCredito
    {
        public NotaCredito()
        {
            DetalleNotaCreditos = new HashSet<DetalleNotaCredito>();
        }

        public Guid IdNotaCredito { get; set; }
        public Guid? IdFacturaVenta { get; set; }
        public Guid? IdUsuarioCreacion { get; set; }
        public Guid? IdCliente { get; set; }
        public Guid? IdUsuarioModificacion { get; set; }
        public decimal ValorIvaNotaCredito { get; set; }
        public int? PorcentajeIva { get; set; }
        public decimal? SubTotalNotaCreditoIva { get; set; }
        public decimal? SubTotalNotaCreditoSinIva { get; set; }
        public decimal TotalNotaCredito { get; set; }
        public decimal ValorDisponible { get; set; }
        public DateTime FechaCaducidad { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual FacturaVentum IdFacturaVentaNavigation { get; set; }
        public virtual ICollection<DetalleNotaCredito> DetalleNotaCreditos { get; set; }
    }
}
