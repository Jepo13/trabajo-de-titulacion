﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class FacturaVentum
    {
        public FacturaVentum()
        {
            DetalleFacturas = new HashSet<DetalleFactura>();
            FacturaFormasPagos = new HashSet<FacturaFormasPago>();
            NotaCreditos = new HashSet<NotaCredito>();
        }

        public Guid IdFacturaVenta { get; set; }
        public Guid? IdCliente { get; set; }
        public Guid? IdUsuarioCreacion { get; set; }
        public Guid? IdControlCaja { get; set; }
        public Guid? IdUsuarioModificacion { get; set; }
        public Guid? IdEstadoVenta { get; set; }
        public decimal? SubTotalFacturaIva { get; set; }
        public decimal? SubTotalFacturaSinIva { get; set; }
        public decimal ValorIvaFactura { get; set; }
        public int PorcentajeIva { get; set; }
        public decimal TotalFactura { get; set; }
        public string EstadoFactura { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual EstadoVentum IdEstadoVentaNavigation { get; set; }
        public virtual ICollection<DetalleFactura> DetalleFacturas { get; set; }
        public virtual ICollection<FacturaFormasPago> FacturaFormasPagos { get; set; }
        public virtual ICollection<NotaCredito> NotaCreditos { get; set; }
    }
}
