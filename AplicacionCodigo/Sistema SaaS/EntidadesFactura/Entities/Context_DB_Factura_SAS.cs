﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class Context_DB_Factura_SAS : DbContext
    {
        public Context_DB_Factura_SAS()
        {
        }

        public Context_DB_Factura_SAS(DbContextOptions<Context_DB_Factura_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<DetalleFactura> DetalleFacturas { get; set; }
        public virtual DbSet<DetalleNotaCredito> DetalleNotaCreditos { get; set; }
        public virtual DbSet<EstadoVentum> EstadoVenta { get; set; }
        public virtual DbSet<FacturaFormasPago> FacturaFormasPagos { get; set; }
        public virtual DbSet<FacturaVentum> FacturaVenta { get; set; }
        public virtual DbSet<Formaspago> Formaspagos { get; set; }
        public virtual DbSet<NotaCredito> NotaCreditos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Factura_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<DetalleFactura>(entity =>
            {
                entity.HasKey(e => e.IdDetalleFactura);

                entity.ToTable("DETALLE_FACTURA");

                entity.Property(e => e.IdDetalleFactura)
                    .HasColumnName("ID_DETALLE_FACTURA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidad).HasColumnName("CANTIDAD");

                entity.Property(e => e.IdFacturaVenta).HasColumnName("ID_FACTURA_VENTA");

                entity.Property(e => e.IdProducto).HasColumnName("ID_PRODUCTO");

                entity.Property(e => e.PorcentajeIvaVenta).HasColumnName("PORCENTAJE_IVA_VENTA");

                entity.Property(e => e.PrecioUnitarioAdquisicion)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIO_UNITARIO_ADQUISICION");

                entity.Property(e => e.PrecioUnitarioVenta)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIO_UNITARIO_VENTA");

                entity.Property(e => e.ValorIvaCompra)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_IVA_COMPRA");

                entity.Property(e => e.ValorIvaVenta)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_IVA_VENTA");

                entity.HasOne(d => d.IdFacturaVentaNavigation)
                    .WithMany(p => p.DetalleFacturas)
                    .HasForeignKey(d => d.IdFacturaVenta)
                    .HasConstraintName("FK_DETALLE__REFERENCE_FACTURA_");
            });

            modelBuilder.Entity<DetalleNotaCredito>(entity =>
            {
                entity.HasKey(e => e.IdDetalleNotaCredito);

                entity.ToTable("DETALLE_NOTA_CREDITO");

                entity.Property(e => e.IdDetalleNotaCredito)
                    .HasColumnName("ID_DETALLE_NOTA_CREDITO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidad).HasColumnName("CANTIDAD");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.IdNotaCredito).HasColumnName("ID_NOTA_CREDITO");

                entity.Property(e => e.IdProducto).HasColumnName("ID_PRODUCTO");

                entity.Property(e => e.PrecioUnitario)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("PRECIO_UNITARIO");

                entity.Property(e => e.ValorIva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_IVA");

                entity.HasOne(d => d.IdNotaCreditoNavigation)
                    .WithMany(p => p.DetalleNotaCreditos)
                    .HasForeignKey(d => d.IdNotaCredito)
                    .HasConstraintName("FK_DETALLE__REFERENCE_NOTA_CRE");
            });

            modelBuilder.Entity<EstadoVentum>(entity =>
            {
                entity.HasKey(e => e.IdEstadoventa);

                entity.ToTable("ESTADO_VENTA");

                entity.Property(e => e.IdEstadoventa)
                    .HasColumnName("ID_ESTADOVENTA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(20)
                    .HasColumnName("NOMBRE");
            });

            modelBuilder.Entity<FacturaFormasPago>(entity =>
            {
                entity.HasKey(e => e.IdFacturaFormaPago);

                entity.ToTable("FACTURA_FORMAS_PAGO");

                entity.Property(e => e.IdFacturaFormaPago)
                    .HasColumnName("ID_FACTURA_FORMA_PAGO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cambio)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("CAMBIO");

                entity.Property(e => e.FechaMaximaPago)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MAXIMA_PAGO");

                entity.Property(e => e.IdFacturaVenta).HasColumnName("ID_FACTURA_VENTA");

                entity.Property(e => e.IdFormaPago).HasColumnName("ID_FORMA_PAGO");

                entity.Property(e => e.Pagada).HasColumnName("PAGADA");

                entity.Property(e => e.ValorAbonoPagoCreditoPersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_ABONO_PAGO_CREDITO_PERSONAL");

                entity.Property(e => e.ValorPago)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_PAGO");

                entity.HasOne(d => d.IdFacturaVentaNavigation)
                    .WithMany(p => p.FacturaFormasPagos)
                    .HasForeignKey(d => d.IdFacturaVenta)
                    .HasConstraintName("FK_FACTURA__REFERENCE_FACTURA_");

                entity.HasOne(d => d.IdFormaPagoNavigation)
                    .WithMany(p => p.FacturaFormasPagos)
                    .HasForeignKey(d => d.IdFormaPago)
                    .HasConstraintName("FK_FACTURA__REFERENCE_FORMASPA");
            });

            modelBuilder.Entity<FacturaVentum>(entity =>
            {
                entity.HasKey(e => e.IdFacturaVenta);

                entity.ToTable("FACTURA_VENTA");

                entity.Property(e => e.IdFacturaVenta)
                    .HasColumnName("ID_FACTURA_VENTA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.EstadoFactura)
                    .IsRequired()
                    .HasMaxLength(7)
                    .HasColumnName("ESTADO_FACTURA")
                    .HasDefaultValueSql("('Emitida')");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCliente).HasColumnName("ID_CLIENTE");

                entity.Property(e => e.IdControlCaja).HasColumnName("ID_CONTROL_CAJA");

                entity.Property(e => e.IdEstadoVenta).HasColumnName("ID_ESTADO_VENTA");

                entity.Property(e => e.IdUsuarioCreacion).HasColumnName("ID_USUARIO_CREACION");

                entity.Property(e => e.IdUsuarioModificacion).HasColumnName("ID_USUARIO_MODIFICACION");

                entity.Property(e => e.PorcentajeIva).HasColumnName("PORCENTAJE_IVA");

                entity.Property(e => e.SubTotalFacturaIva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUB_TOTAL_FACTURA_IVA");

                entity.Property(e => e.SubTotalFacturaSinIva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUB_TOTAL_FACTURA_SIN_IVA");

                entity.Property(e => e.TotalFactura)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("TOTAL_FACTURA");

                entity.Property(e => e.ValorIvaFactura)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_IVA_FACTURA");

                entity.HasOne(d => d.IdEstadoVentaNavigation)
                    .WithMany(p => p.FacturaVenta)
                    .HasForeignKey(d => d.IdEstadoVenta)
                    .HasConstraintName("FK_FACTURA__REFERENCE_ESTADO_V");
            });

            modelBuilder.Entity<Formaspago>(entity =>
            {
                entity.HasKey(e => e.IdFormapago);

                entity.ToTable("FORMASPAGO");

                entity.Property(e => e.IdFormapago)
                    .HasColumnName("ID_FORMAPAGO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Fechacreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHACREACION");

                entity.Property(e => e.Fechamodificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHAMODIFICACION");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOCREACION");

                entity.Property(e => e.Usuariomodificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIOMODIFICACION");
            });

            modelBuilder.Entity<NotaCredito>(entity =>
            {
                entity.HasKey(e => e.IdNotaCredito);

                entity.ToTable("NOTA_CREDITO");

                entity.Property(e => e.IdNotaCredito)
                    .HasColumnName("ID_NOTA_CREDITO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaCaducidad)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CADUCIDAD");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCliente).HasColumnName("ID_CLIENTE");

                entity.Property(e => e.IdFacturaVenta).HasColumnName("ID_FACTURA_VENTA");

                entity.Property(e => e.IdUsuarioCreacion).HasColumnName("ID_USUARIO_CREACION");

                entity.Property(e => e.IdUsuarioModificacion).HasColumnName("ID_USUARIO_MODIFICACION");

                entity.Property(e => e.PorcentajeIva).HasColumnName("PORCENTAJE_IVA");

                entity.Property(e => e.SubTotalNotaCreditoIva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUB_TOTAL_NOTA_CREDITO_IVA");

                entity.Property(e => e.SubTotalNotaCreditoSinIva)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("SUB_TOTAL_NOTA_CREDITO_SIN_IVA");

                entity.Property(e => e.TotalNotaCredito)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("TOTAL_NOTA_CREDITO");

                entity.Property(e => e.ValorDisponible)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_DISPONIBLE");

                entity.Property(e => e.ValorIvaNotaCredito)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_IVA_NOTA_CREDITO");

                entity.HasOne(d => d.IdFacturaVentaNavigation)
                    .WithMany(p => p.NotaCreditos)
                    .HasForeignKey(d => d.IdFacturaVenta)
                    .HasConstraintName("FK_NOTA_CRE_REFERENCE_FACTURA_");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
