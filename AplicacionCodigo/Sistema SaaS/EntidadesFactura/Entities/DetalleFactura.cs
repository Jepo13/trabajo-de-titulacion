﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class DetalleFactura
    {
        public Guid IdDetalleFactura { get; set; }
        public Guid? IdFacturaVenta { get; set; }
        public Guid? IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitarioVenta { get; set; }
        public decimal PrecioUnitarioAdquisicion { get; set; }
        public decimal ValorIvaVenta { get; set; }
        public decimal ValorIvaCompra { get; set; }
        public int PorcentajeIvaVenta { get; set; }

        public virtual FacturaVentum IdFacturaVentaNavigation { get; set; }
    }
}
