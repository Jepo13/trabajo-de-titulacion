﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class DetalleNotaCredito
    {
        public Guid IdDetalleNotaCredito { get; set; }
        public Guid? IdNotaCredito { get; set; }
        public Guid? IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal ValorIva { get; set; }
        public string Descripcion { get; set; }

        public virtual NotaCredito IdNotaCreditoNavigation { get; set; }
    }
}
