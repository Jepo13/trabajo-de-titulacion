﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class Formaspago
    {
        public Formaspago()
        {
            FacturaFormasPagos = new HashSet<FacturaFormasPago>();
        }

        public Guid IdFormapago { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public virtual ICollection<FacturaFormasPago> FacturaFormasPagos { get; set; }
    }
}
