﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class EstadoVentum
    {
        public EstadoVentum()
        {
            FacturaVenta = new HashSet<FacturaVentum>();
        }

        public Guid IdEstadoventa { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<FacturaVentum> FacturaVenta { get; set; }
    }
}
