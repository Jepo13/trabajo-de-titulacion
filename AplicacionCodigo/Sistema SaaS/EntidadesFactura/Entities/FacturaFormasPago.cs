﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesFactura.Entities
{
    public partial class FacturaFormasPago
    {
        public Guid IdFacturaFormaPago { get; set; }
        public Guid? IdFormaPago { get; set; }
        public Guid? IdFacturaVenta { get; set; }
        public decimal ValorPago { get; set; }
        public decimal? Cambio { get; set; }
        public DateTime? FechaMaximaPago { get; set; }
        public bool Pagada { get; set; }
        public decimal ValorAbonoPagoCreditoPersonal { get; set; }

        public virtual FacturaVentum IdFacturaVentaNavigation { get; set; }
        public virtual Formaspago IdFormaPagoNavigation { get; set; }
    }
}
