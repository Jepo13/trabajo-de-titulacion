using AutoMapper;
using Marvin.StreamExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TestAPI.DTO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;


namespace TestAPI
{
    [TestClass]
    public class CompanyTest
    {

        [TestMethod]
        public async Task createCompany()
        {
            string resposeResult = "";
            CompanyDTO objCompany = new CompanyDTO();

            objCompany.Direccion = "Calderon";
            objCompany.Razonsocial = "TicsKernel";
            objCompany.Ruc = "2200025787001";
            objCompany.Telefono = "0982119036";

            MemoryStream memoryContentStream = new MemoryStream();
            memoryContentStream.SerializeToJsonAndWrite(objCompany, new System.Text.UTF8Encoding(), 1024, true);

            //BORRAR
            var temp = JsonConvert.SerializeObject(objCompany);

            memoryContentStream.Seek(0, SeekOrigin.Begin);

            using (var request = new HttpRequestMessage(HttpMethod.Post, "/api/EmpresaAPI/Create"))
            {
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var streamContent = new StreamContent(memoryContentStream))
                {
                    try
                    {
                        request.Content = streamContent;
                        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri("http://localhost:51723/");

                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        var response = await client.SendAsync(request);
                        if (response.IsSuccessStatusCode)
                        {
                            resposeResult = await response.Content.ReadAsStringAsync();

                        }
                    }
                    catch (Exception ex)
                    {
                    }

                }
            }

            Assert.IsTrue(!string.IsNullOrEmpty(resposeResult));
        }
    }
}
