﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestAPI.DTO
{
    public class CompanyDTO
    {
        public string Ruc { get; set; }
        public string Direccion { get; set; }
        public string Razonsocial { get; set; }
        public string Telefono { get; set; }
    }
}
