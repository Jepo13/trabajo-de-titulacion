﻿using DTOs.Sitios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Ubicacion
{
    public  class UbicacionCompleteDTO
    {
        public Guid IdUbicacion { get; set; }
        public Guid? IdLocal { get; set; }
        public Guid IdTipoUbicacion { get; set; }
        public string nombreTipoUbicacion { get; set; }
        public string NombreUbicacion { get; set; }
        public bool Estado { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

      public List<SitiosCompletoDTO> listaSitiosDTO { get; set; }
       

    }
}
