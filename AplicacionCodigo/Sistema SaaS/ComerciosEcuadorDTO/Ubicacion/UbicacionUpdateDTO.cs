﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Ubicacion
{
    public  class UbicacionUpdateDTO
    {
        public Guid IdUbicacion { get; set; }
        public Guid? IdLocal { get; set; }
        public Guid IdTipoUbicacion { get; set; }
        public string nombreTipoUbicacion { get; set; }
        public string NombreUbicacion { get; set; }
        public bool Estado { get; set; }
        public string Descripcion { get; set; }
 
        public string UsuarioModificacion { get; set; }
    }
}
