﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.GrupoProducto
{
   public class GrupoProductoCreateDTO
    {
        public Guid? IdEmpresa { get; set; }
        public string NombreGrupoProducto { get; set; }
        public bool EstadoGrupoProducto { get; set; }
        public bool DescripcionGrupoProducto { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
