﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.GrupoProducto
{
    public  class GrupoProductoUpdateDTO
    {
        public Guid IdEmpresa { get; set; }
        public string NombreGrupoProducto { get; set; }
        public string DescripcionGrupoProducto { get; set; }
        public bool EstadoGrupoProducto { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
