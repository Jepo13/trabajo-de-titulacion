﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.GrupoProducto
{
    public class GrupoProductoBusquedaDTO
    {
        public Guid IdGrupoproducto { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombregrupoproducto { get; set; }
        public bool EstadoGrupoProducto { get; set; }
    }
}
