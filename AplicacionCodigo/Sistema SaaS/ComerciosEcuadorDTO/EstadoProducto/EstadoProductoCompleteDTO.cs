﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.EstadoProducto
{
    public class EstadoProductoCompleteDTO
    {
        public Guid IdEstadoproducto { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

    }
}
