﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.FacturaProveedor
{
    public class FacturaProveedorCreateDTO
    {
        public Guid? IdFacturafisicaproveedor { get; set; }
        public Guid? IdFacturaelectronicaproveedor { get; set; }
        public Guid? IdProveedor { get; set; }
        public string Numerofactura { get; set; }
        public decimal Valortotal { get; set; }
        public bool Facturapagada { get; set; }
        public string Tipofactura { get; set; }
        public DateTime Fechaingresofactura { get; set; }
        public DateTime? Fechapago { get; set; }
        public string Numeroguia { get; set; }
        public Guid? IdEstado { get; set; }
        public Guid? IdUsuariocreacion { get; set; }
        public Guid? IdUsuariomodificacion { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }

    }
}
