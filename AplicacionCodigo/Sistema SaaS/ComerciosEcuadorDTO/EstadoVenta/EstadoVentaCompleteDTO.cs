﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.EstadoVenta
{
    public class EstadoVentaCompleteDTO
    {
        public Guid IdEstadoventa { get; set; }
        public string Nombre { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodificacion { get; set; }
    }
}
