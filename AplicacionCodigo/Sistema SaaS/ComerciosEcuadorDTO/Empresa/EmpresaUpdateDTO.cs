﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.Empresa
{
    public class EmpresaUpdateDTO
    {
        public Guid? IdPersona { get; set; }
        public string Ruc { get; set; }
        public string Razonsocial { get; set; }
        public string Telefono { get; set; }
        public bool Estado { get; set; }      
        public string Usuariomodificacion { get; set; }
      

    }
}
