﻿using DTOs.Empresa;
using DTOs.Local;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.Empresa
{
    public class EmpresaResultadoBusquedaDTO
    {
        public Guid IdEmpresa { get; set; }     
        public string Ruc { get; set; }       
        public string Razonsocial { get; set; }
        public string Telefono { get; set; }
        public bool Estado { get; set; }
        public DateTime Fechacreacion { get; set; }

    }
}
