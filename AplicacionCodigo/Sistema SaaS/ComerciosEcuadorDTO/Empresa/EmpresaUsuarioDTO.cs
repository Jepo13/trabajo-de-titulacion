﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Empresa
{
    public class EmpresaUsuarioDTO
    {
        public Guid? IdEmpresa { get; set; }
        public string Razonsocial { get; set; }

        public Guid? IdUsuario { get; set; }
    }
}
