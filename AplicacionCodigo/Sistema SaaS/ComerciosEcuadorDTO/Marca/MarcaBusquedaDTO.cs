﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Marca
{
    public class MarcaBusquedaDTO
    {
        public Guid IdMarca { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }

    }
}
