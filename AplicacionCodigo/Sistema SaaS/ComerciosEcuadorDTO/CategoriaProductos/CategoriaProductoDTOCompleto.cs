﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DTOs.CategoriaProductos
{
    public class CategoriaProductoDTOCompleto
    {
        public Guid IdCategoriaProducto { get; set; }
        public Guid? IdCategoriaProductoPadre { get; set; }
        public Guid IdEmpresa { get; set; }
        public string NombreCategoriaProducto { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreCategoriaPadre { get; set; }
        public bool EstadoCategoriaProducto { get; set; }
        public string DescripcionProducto { get; set; }

        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        [JsonIgnore]
        public CategoriaProductoDTOCompleto IdCategoriaProductoPadreNavigation { get; set; }

        [JsonIgnore]
        public List<CategoriaProductoDTOCompleto> InverseIdCategoriaProductoPadreNavigation { get; set; }
    }
}
