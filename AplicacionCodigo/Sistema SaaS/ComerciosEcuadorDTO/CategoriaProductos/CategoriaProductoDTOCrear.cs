﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.CategoriaProductos
{
    public class CategoriaProductoDTOCrear
    {
        public Guid? IdCategoriaProductoPadre { get; set; }
        public Guid IdEmpresa { get; set; }
        public string NombreCategoriaProducto { get; set; }
        public bool EstadoCategoriaProducto { get; set; }
        public string DescripcionProducto { get; set; }
        public string UsuarioCreacion { get; set; }
    }
}
