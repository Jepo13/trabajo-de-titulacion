﻿using DTOs.Empresa;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Select
{
    public class CustomSelectEmpresas
    {        
        public string DataGroupField { set; get; }
        public string DataTextField { set; get; }
        public string DataValueField { set; get; }
        public List<EmpresaResultadoBusquedaDTO> Items { set; get; }
        public string SelectedValues { set; get; }
    }
}
