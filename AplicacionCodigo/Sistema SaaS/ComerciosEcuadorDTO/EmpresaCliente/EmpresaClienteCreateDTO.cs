﻿using DTOs.Cliente;
using DTOs.Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.EmpresaCliente
{
    public class EmpresaClienteCreateDTO
    {

        public Guid? IdPersona { get; set; }
        public int? PorcentajeDescuento { get; set; }
        public decimal? ValorCupoCreditoPersonal { get; set; }
        public decimal? ValorCupoDisponibleCreditoPersonal { get; set; }
        public int? DiasCredito { get; set; }
        public Guid IdEmpresa { get; set; }
        public string UsuarioCreacion { get; set; }

    }
}
