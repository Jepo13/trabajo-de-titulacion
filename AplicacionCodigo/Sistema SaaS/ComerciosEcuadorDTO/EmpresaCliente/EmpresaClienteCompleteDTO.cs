﻿using DTOs.Cliente;
using DTOs.Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.EmpresaCliente
{
   public  class EmpresaClienteCompleteDTO
    {
        public Guid IdEmpresaCliente { get; set; }
        public Guid? IdPersona { get; set; }
        public string EmpresaNombre { get; set; } 
        public int? PorcentajeDescuento { get; set; }
        public decimal? ValorCupoCreditoPersonal { get; set; }
        public decimal? ValorCupoDisponibleCreditoPersonal { get; set; }
        public int? DiasCredito { get; set; }
        public Guid IdEmpresa { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

     
    }
}
