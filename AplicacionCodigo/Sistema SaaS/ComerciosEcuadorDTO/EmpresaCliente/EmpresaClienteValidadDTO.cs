﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.EmpresaCliente
{
    public class EmpresaClienteValidadDTO
    {
        public Guid? IdPersona { get; set; }
        public Guid IdEmpresa { get; set; }

    }
}
