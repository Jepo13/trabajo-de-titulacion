﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.EmpresaCliente
{
    public class EmpresaClienteUpdateDTO
    {
        public Guid IdEmpresaCliente_Update { get; set; }
        public Guid? IdPersona_Update { get; set; }
        public string EmpresaNombre_Update { get; set; }
        public int? PorcentajeDescuento_Update { get; set; }
        public decimal? ValorCupoCreditoPersonal_Update { get; set; }
        public decimal? ValorCupoDisponibleCreditoPersonal_Update { get; set; }
        public int? DiasCredito_Update { get; set; }
        public Guid IdEmpresa_Update { get; set; }
        public string UsuarioModificacion_Update { get; set; }


    }
}
