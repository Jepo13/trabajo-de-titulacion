﻿using System.Collections.Generic;

namespace DTOs.Rol
{
    public class MenuDTO
    {        
        public string NombreMenu { get; set; }
        public string RutaMenu { get; set; }

        public List<PermisosDTO> listaPermisosDTO { get; set; }
    }
}