﻿using System;
using System.Collections.Generic;

namespace DTOs.Rol
{
    public class ModuloDTO
    {
        public Guid IdModulo { get; set; }
        public string Nombre { get; set; }
        public List<MenuDTO> listaMenuDTO { get; set; }
    }
}