﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Rol
{
    public class RolCompletoDTO
    {
        public Guid IdRol { get; set; }
        public bool Estado { get; set; }
        public List<ModuloEditarnDTO> listaModulos { get; set; }
        public Guid IdEmpresa { get; set; }
        public string Nombrerol { get; set; }  
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

        public DateTime Fechacreacion { get; set; }
        public DateTime? Fechamodficacion { get; set; }

       

    }//class

}
