﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Rol
{
    public class RolBusquedaDTO
    {
        
        public Guid IdRol { get; set; }
        public string Nombrerol { get; set; }
        public bool Estado { get; set; }

        public List<ModuloBusquedaDTO> Modulos { get; set; }
    }

    public class ModuloBusquedaDTO
    {
        public string nombreModulo { get; set; }

    }

}
