﻿namespace DTOs.Rol
{
    public class PermisosDTO
    {
        public string Nombrepermiso { get; set; }
        public bool Concedido { get; set; }
    }
}