﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Rol
{
    public class RolCreacionDTO
    {
        public List<ModuloCreacionDTO> listaModulos { get; set; }
        public Guid IdEmpresa { get; set; }
        public string Nombrerol { get; set; }        
        public bool Estado { get; set; }
        public string Usuariocreacion { get; set; }
       
        
        
    }//class
   

    public class ModuloCreacionDTO
    {
        public string nombreModulo { get; set; }
        public List<RolesMenu> rolesMenu { get; set; }
    }
    public class RolesMenu
    {
        public List<PermisosRol> permisosRoles { get; set; }
        public string nombreMenu { get; set; }
        public string Rutamenu { get; set; }
    }
    public class PermisosRol
    {
        public string nombrePermiso { get; set; }
        public string CSSPermiso { get; set; }
        public bool Concedido { get; set; }
    }

   

   

}
