﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Rol
{
    public class RolEditarDTO
    {
        //public Guid IdRol { get; set; }
        public List<ModuloEditarnDTO> listaModulos { get; set; }
        public Guid IdEmpresa { get; set; }
        public string Nombrerol { get; set; }
        
        public bool Estado { get; set; }
        public string Usuariomodificacion { get; set; }
    }//class
   

    public class ModuloEditarnDTO
    {
        public Guid IdModulo { get; set; }
        public Guid IdRol { get; set; }
        public string nombreModulo { get; set; }
        public List<RolesMenuEditarDTO> rolesMenu { get; set; }
    }
    public class RolesMenuEditarDTO
    {
        public Guid IdMenu { get; set; }
        public List<PermisosRolEditarDTO> permisosRoles { get; set; }
        public string nombreMenu { get; set; }
        public string Rutamenu { get; set; }
    }
    public class PermisosRolEditarDTO
    {
        public Guid IdPermisos { get; set; }
        public string nombrePermiso { get; set; }
        public string CSSPermiso { get; set; }
        public bool Concedido { get; set; }
    }

   

   

}
