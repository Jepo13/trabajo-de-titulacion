﻿using DTOs.EmpresaCliente;
using DTOs.Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Cliente
{
    public class ClienteCompleteDTO
    {
        public Guid IdCliente { get; set; }
        public Guid? IdPersona { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public Guid? IdTipoContribuyente { get; set; }


        public PersonaCompleteDTO PersonaCompletaDTO { get; set; }
        public  List<EmpresaClienteCompleteDTO> ListEmpresaClientesDTO { get; set; }
    }
}
