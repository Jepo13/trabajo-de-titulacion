﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Cliente
{
    public  class ClienteResultadoBusqueda
    {
        public Guid IdPersona { get; set; }
        public Guid IdCliente { get; set; }
        public Guid? IdTipoIdentificacion { get; set; }
        public string TipoIdentificacion { get; set; }
        public Guid? IdTipoContribuyente { get; set; }
        public string TipoContribuyente { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NumeroIdentificacion { get; set; }

    }
}
