﻿using DTOs.EmpresaCliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Cliente
{
    public  class PersonaClienteCreateDTO
    {

        // datos de persona 
        public Guid IdPersona { get; set; }
        public Guid? IdResidencia { get; set; }
        public Guid? IdTipoIdentificacion { get; set; }
        public Guid? IdLugarnacimiento { get; set; }
        //public Guid? IdEstadoCivil { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NumeroIdentificacion { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }

        public string Direccion { get; set; }
        public string CorreoPersonal { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        // Cliente 
        public Guid? IdTipoContribuyente { get; set; }
        public List<EmpresaClienteCompleteDTO> ListEmpresaClientesDTO { get; set; }

        // empresaCliente 
        public Guid IdEmpresa { get; set; }
        public int? PorcentajeDescuento { get; set; }
        public decimal? ValorCupoCreditoPersonal { get; set; }
        public decimal? ValorCupoDisponibleCreditoPersonal { get; set; }
        public int? DiasCredito { get; set; }

    }
}
