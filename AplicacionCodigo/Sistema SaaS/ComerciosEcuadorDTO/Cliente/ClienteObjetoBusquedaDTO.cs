﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Cliente
{
    public  class ClienteObjetoBusquedaDTO
    {
        public string nombreCliente { get; set; }
        public string apellido { get; set; }
        public Guid? idTipoIdentificacion { get; set; }
        public Guid? idEmpresaCliente { get; set; }
        public Guid? idTipoContribullente { get; set; }
        public string numeroIdentificacion { get; set; }

    }
}
