﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Notas
{
    public class NotasCompleteDTO
    {
        public Guid IdNotas { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdImportancia { get; set; }
        public string Titulonota { get; set; }
        public string Contenidonota { get; set; }
        public bool Estado { get; set; }
        public DateTime? Fechavencimiento { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
