﻿using DTOs.Empresa;
using DTOs.Rol;
using System;
using System.Collections.Generic;

namespace DTOs.User
{
    public class UserCompleteDTO
    {
        // Datos Usuario
        public Guid? IdUsuario { get; set; }
        public Guid? IdRol { get; set; }
        public Guid IdEmpresaDefault { get; set; }
        public bool EstadoUsuario { get; set; }
        public string CorreoInstitucional { get; set; }
        public string Contrasena { get; set; }
        public string IndicioContrasena { get; set; }
        public DateTime? FechaUltimoIngreso { get; set; }
        public List<EmpresaUsuarioDTO> EmpresaUsuarioDTO { get; set; }
        public virtual RolCompletoDTO IdRolNavigation { get; set; }

        // Datpos Persona 
        public Guid IdPersona { get; set; }
        public Guid? IdLugarNacimiento { get; set; }
        public Guid? IdResidencia { get; set; }
        public Guid? IdPaisOrigen { get; set; }
        public Guid IdTipoIdentificacion { get; set; }
        public Guid? IdEstadoCivil { get; set; }
        public byte[] ImagenPersona { get; set; }
        public string FormatoArchivo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NumeroIdentificacion { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string ContactoEmergencia { get; set; }
        public string Direccion { get; set; }
        public string CorreoPersonal { get; set; }     
        public DateTime? Fechamodificacion { get; set; }
        public string Usuariomodificacion { get; set; }
        public DateTime Fechacreacion { get; set; }
        public string Usuariocreacion { get; set; }


        public Guid? IdProvinciaNacimiento { get; set; }
        public Guid? IdCantonNacimiento { get; set; }
        public Guid? IdProvinciaRecidencia { get; set; }
        public Guid? IdCantonRecidencia { get; set; }
    }
}
