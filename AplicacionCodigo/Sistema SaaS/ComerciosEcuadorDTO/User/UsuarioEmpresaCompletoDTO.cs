﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.User
{
    public  class UsuarioEmpresaCompletoDTO
    {
        public Guid IdUsuairoEmpresa { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdUsuario { get; set; }
    }
}
