﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using DTOs.Select;
using DTOs.Empresa;
using DTOs.Rol;

namespace DTOs.User
{
    public class UserSessionDTO
    {
        public Guid IdUsuario { get; set; }
        public Guid IdEmpresadefault { get; set; }
        public Guid IdPersona { get; set; }
        public Guid? IdTipousuario { get; set; }
        public DateTime? Fechaultimoingreso { get; set; }
        public bool? Estado { get; set; }
        public string Nombre { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreRol { get; set; }
        public string Apellido { get; set; }
        public CustomSelectEmpresas ListaEmpresas { get; set; }
        public List<ModuloDTO> Modulos { get; set; }
        public SelectList ListaProvincias { get; set; }

        //Listado de empresas a las que tiene acceso el Usuario
        public List<EmpresaResultadoBusquedaDTO> ListaEmpresasAcceso { get; set; }

        public virtual SelectList EmpresasAccesoSelect
        {
            get
            {
                //SelectList objSelectList = new SelectList(listaCatalogos, "IdCatalogo", "Nombrecatalogo", valorSeleccionado);
               return new SelectList(ListaEmpresasAcceso, "IdEmpresa", "Razonsocial", IdEmpresadefault);

                //return JsonConvert.SerializeObject(ListaEmpresasAcceso, Formatting.Indented);
            }
        }


    }
}
