﻿using DTOs.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.User
{
    public class UserCreateDTO
    {
        public string Activado { get; set; }
        public string Contrasena { get; set; }
        public Guid IdTipoIdentificacion { get; set; }
        public Guid? IdLugarnacimiento { get; set; }
        public Guid? IdEstadoCivil { get; set; }
        public Guid? IdResidencia { get; set; }
        public Guid IdEmpresadefault { get; set; }
        public Guid? IdPaisorigen { get; set; }
        public List<EmpresaUsuarioDTO> EmpresaUsuarioDTO { get; set; }
        public Guid IdRol { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public byte[] Imagenpersona { get; set; }
        public string FormatoArchivo { get; set; }
        public string Numeroidentificacion { get; set; }
        public string Estadocivil { get; set; }
        public DateTime? Fechanacimiento { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Contactoemergencia { get; set; }
        public string Direccion { get; set; }
        public string Correopersonal { get; set; }
        public string Correoinstitucional { get; set; }
        public DateTime Fechacreacion { get; set; }
        public string Usuariocreacion { get; set; }

        public virtual bool Estado
        {
            get
            {
                return Activado == "on" ? true : false;  
            }
        }
    }
}
