﻿using DTOs.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.User
{
    public class UserResultadoBusquedaDTO
    {
        public Guid IdUsuario { get; set; }//
        public Guid IdPersona { get; set; } //
        public Guid IdEmpresaDefault { get; set; }//
        public bool? Estado { get; set; }//
        public string Empresa { get; set; }//
        public string Perfil { get; set; } //
        public string Correoinstitucional { get; set; }//
        public string Nombre { get; set; } //
        public string Apellido { get; set; } //
        public List<EmpresaUsuarioDTO> EmpresaUsuarioDTO { get; set; } //
    }
}
