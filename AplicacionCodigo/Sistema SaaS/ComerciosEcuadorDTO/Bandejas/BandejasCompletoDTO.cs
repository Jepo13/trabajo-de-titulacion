﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Bandejas
{
    public  class BandejasCompletoDTO
    {
        public Guid IdBandeja { get; set; }
        public Guid IdSitio { get; set; }
        public string NombreBandeja { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

    }
}
