﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Bandejas
{
    public  class BandejaCreateDTO
    {
        public BandejaCreateDTO()
        {
        }

        public BandejaCreateDTO(Guid IdUbicacion, Guid IdSitio)
        {
            this.IdSitio = IdSitio;
            idUbicacionBandeja = IdUbicacion;
        }

        public Guid IdBandeja { get; set; }
        public Guid IdSitio { get; set; }
        public string NombreBandeja { get; set; }
        public Guid idUbicacionBandeja { get; set; }    
        public string UsuarioCreacion { get; set; }
       
    }
}
