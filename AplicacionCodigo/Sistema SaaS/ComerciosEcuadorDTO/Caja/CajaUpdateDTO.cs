﻿using DTOs.Empresa;
using DTOs.Local;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.Caja
{
    public class CajaUpdateDTO
    {
        public Guid? IdLocal { get; set; }
        public string Nombrecaja { get; set; }
        public bool Estado { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
