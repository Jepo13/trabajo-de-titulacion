﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.UnidadEntrega
{
    public class UnidadEntregaUpdateDTO
    {
        public Guid? IdEmpresa { get; set; }
        public string Nombrecontenido { get; set; }
        public string Codigounidad { get; set; }
        public string DescripcionMedida { get; set; }
        public bool EstadoUnidad { get; set; }
        public string UsuarioModificacion { get; set; }
    }
}
