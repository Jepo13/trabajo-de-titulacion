﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.UnidadEntrega
{
    public class UnidadEntregaCreateDTO
    {
        public Guid? IdEmpresa { get; set; }
        public string NombreContenido { get; set; }
        public string CodigoUnidad { get; set; }
        public bool EstadoUnidad { get; set; }
        public string DescripcionMedida { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
    }
}
