﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.UnidadEntrega
{
    public class UnidadEntregaCompleteDTO
    {
        public Guid IdUnidadentrega { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreContenido { get; set; }
        public string CodigoUnidad { get; set; }
        public string DescripcionMedida { get; set; }
        public bool EstadoUnidad { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
