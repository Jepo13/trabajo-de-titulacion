﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.UnidadEntrega
{
    public class UnidadEntregaBusquedaDTO
    {
        public Guid IdUnidadentrega { get; set; }
        public Guid IdEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreContenido { get; set; }
        public string Codigounidad { get; set; }
        public bool EstadoUnidad { get; set; }
    }
}
