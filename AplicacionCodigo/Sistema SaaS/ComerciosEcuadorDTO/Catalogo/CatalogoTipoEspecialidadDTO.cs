﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Catalogo
{
    public class CatalogoTipoEspecialidadDTO
    {
        public Guid IdCatalogo { get; set; }
        public string Nombrecatalogo { get; set; }
    }
}
