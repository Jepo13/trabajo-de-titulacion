﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DTOs.Catalogo
{
    public class CatalogoBusquedaDTO
    {      
        public Guid IdCatalogopadre { get; set; }
        public Guid IdEmpresa { get; set; }
        public string Nombrecatalogo { get; set; }       
        public string NombreCatalogoPadre { get; set; }
        public bool Estado { get; set; }       
    }
}
