﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Catalogo
{
    public class CatalogoUpdateDTO
    {
        public Guid? IdCatalogopadre { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombrecatalogo { get; set; }
        public string Codigocatalogo { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public string Datoadicional { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
