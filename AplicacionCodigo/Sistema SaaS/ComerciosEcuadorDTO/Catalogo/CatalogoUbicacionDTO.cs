﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.Catalogo
{
    public class CatalogoUbicacionDTO
    {
        public Guid IdCatalogo { get; set; }
        public string Nombrecatalogo { get; set; }
        public string Codigocatalogo { get; set; }
        public Guid? IdCatalogopadre { get; set; }
    }
}
