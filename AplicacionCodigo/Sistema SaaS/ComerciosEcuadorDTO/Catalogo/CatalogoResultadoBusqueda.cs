﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DTOs.Catalogo
{
    public class CatalogoResultadoBusqueda
    {
        public Guid IdCatalogo { get; set; }
        public Guid? IdCatalogopadre { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombrecatalogo { get; set; }
        public string Empresa { get; set; }
        public string Codigocatalogo { get; set; }
        public string CodigoCatalogoPadre { get; set; }
        public string NombreCatalogoPadre { get; set; }
        public string Datoadicional { get; set; }
        public bool Estado { get; set; }
        
        public List<CatalogoResultadoBusqueda> InverseIdCatalogopadreNavigation { get; set; }
        //public CatalogoCompleteDTO IdCatalogopadreNavigation { get; set; }
    }
}
