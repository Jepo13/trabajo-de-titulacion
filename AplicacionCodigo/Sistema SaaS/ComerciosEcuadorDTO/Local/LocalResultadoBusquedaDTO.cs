﻿using DTOs.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.Local
{
    public class LocalResultadoBusquedaDTO
    {
        public Guid IdLocal { get; set; }
        public Guid? IdUbicacion { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string nombreEmpresa { get; set; }
        public string ubicacion { get; set; }
        public string Nombrelocal { get; set; }
        public Guid? IdTipolocal { get; set; }
        public string tipolocal { get; set; }
        public bool Estado { get; set; }


        //
        public CatalogoTipoLocalDTO IdTipolocalNavigation { get; set; }
        public CatalogoUbicacionDTO IdUbicacionNavigation { get; set; }

    }
}
