﻿using DTOs.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.Local   
{
    public class LocalUpdateDTO
    {
        public Guid? IdUbicacion { get; set; }
        public Guid? IdEmpresa { get; set; }
        public string Nombrelocal { get; set; }
        public Guid IdTipolocal { get; set; }
        public string Direccionlocal { get; set; }
        public string Telefono { get; set; }
        public string Observaciones { get; set; }
        public bool Estado { get; set; }       
        public string Usuariomodificacion { get; set; }

    }
}
