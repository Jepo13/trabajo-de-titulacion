﻿
using DTOs.Caja;
using DTOs.Catalogo;
using DTOs.Empresa;
using DTOs.Ubicacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DTOs.Local
{
    public class LocalCompleteDTO
    {
        public Guid IdLocal { get; set; }
        public Guid? IdUbicacion { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid IdTipoLocal { get; set; }
        public string NombreLocal { get; set; }
        public string DireccionLocal { get; set; }
        public string Telefono { get; set; }
        public string Observaciones { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public EmpresaCompletoDTO empresa { get; set; }


        public List<UbicacionCompleteDTO> listaUbicacionesDTO { get; set; }

    }
}
