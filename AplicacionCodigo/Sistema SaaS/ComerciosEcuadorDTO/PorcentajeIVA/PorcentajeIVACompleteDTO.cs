﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.PorcentajeIVA
{
    public class PorcentajeIVACompleteDTO
    {
        public Guid IdPorcenajeiva { get; set; }
        public int Valor { get; set; }
        public bool Activo { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

    }
}
