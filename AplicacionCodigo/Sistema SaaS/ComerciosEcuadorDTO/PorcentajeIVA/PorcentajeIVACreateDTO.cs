﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.PorcentajeIVA
{
    public class PorcentajeIVACreateDTO
    {
        public decimal Valor { get; set; }
        public bool Activo { get; set; }

        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
