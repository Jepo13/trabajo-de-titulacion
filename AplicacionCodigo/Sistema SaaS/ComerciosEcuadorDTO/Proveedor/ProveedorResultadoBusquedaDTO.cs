﻿using DTOs.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Proveedor
{
    public class ProveedorResultadoBusquedaDTO
    {

        public Guid IdProveedor { get; set; }
        public Guid? IdEspecialidad { get; set; }
        public string Especialidad { get; set; }
        public string NombreComercial { get; set; }
        public string Ruc { get; set; }
        public string TelefonoProveedor { get; set; }      
        public bool Estado { get; set; }
    }
}
