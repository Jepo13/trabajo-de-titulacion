﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Proveedor
{
    public class ProveedorCreateDTO
    {
        public Guid? IdPorcentajeIva { get; set; }
        public Guid? IdTipocontribuyente { get; set; }
        public Guid? IdEspecialidad { get; set; }
        public Guid? IdEmpresa { get; set; }
        public bool Contribuyenteespecial { get; set; }
        public string Razonsocial { get; set; }
        public string Nombrecomercial { get; set; }
        public string Ruc { get; set; }
        public string Direccion { get; set; }
        public string Telefonoproveedor { get; set; }
        public string Telefonomovil { get; set; }
        public string Correoelectronico { get; set; }
        public bool Obligadollevarcontabilidad { get; set; }
        public string Nombrerepresentate { get; set; }
        public string Nombrecontacto { get; set; }
        public string Telefonocontacto { get; set; }
        public string Codigoproveedor { get; set; }
        public string Nota { get; set; }
        public bool Estado { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
        
    }
}
