﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.CanjeProductoProveedor
{
    public class CanjeProductoProveedorCompleteDTO
    {
        public Guid IdCanjeproductoproveedor { get; set; }
        public Guid? IdProveedor { get; set; }
        public DateTime Fechacanje { get; set; }
        public string Descripcion { get; set; }
        public string Identificadorcanjeproducto { get; set; }
        public byte[] Imagencanje { get; set; }
        public DateTime Fechacreacion { get; set; }
        public DateTime Fechamodificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }

    }
}
