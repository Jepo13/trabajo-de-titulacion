﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Impuesto
{
    public class ImpuestoUpdateDTO
    {
        public Guid? IdEmpresa { get; set; }
        public string Nombreimpuesto { get; set; }
        public decimal Valor { get; set; }
        public string Tipoimpuesto { get; set; }
        public string Usuariocreacion { get; set; }
        public string Usuariomodificacion { get; set; }
    }
}
