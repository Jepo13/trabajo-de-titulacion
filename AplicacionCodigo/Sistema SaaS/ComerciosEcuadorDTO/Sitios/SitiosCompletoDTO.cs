﻿using DTOs.Bandejas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Sitios
{
    public  class SitiosCompletoDTO
    {
        public Guid IdSitio { get; set; }
        public Guid? IdUbicacion { get; set; }
        public string NombreSitio { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public List<BandejasCompletoDTO> Bandejas { get; set; }
    }
}
