﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs.Sitios
{
    public class SitioCreateDTO
    {
        public Guid IdSitio { get; set; }
        public Guid? IdUbicacion { get; set; }
        public string NombreSitio { get; set; }
        public string UsuarioCreacion { get; set; }

    }
}
