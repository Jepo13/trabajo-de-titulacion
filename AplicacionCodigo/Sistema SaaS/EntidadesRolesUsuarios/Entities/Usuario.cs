﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesRolesUsuarios.Entities
{
    public partial class Usuario
    {
        public Guid IdUsuario { get; set; }
        public Guid? IdPersona { get; set; }
        public Guid? IdRol { get; set; }
        public Guid IdEmpresaDefault { get; set; }
        public bool? Estado { get; set; }
        public string CorreoInstitucional { get; set; }
        public string Contrasena { get; set; }
        public string IndicioContrasena { get; set; }
        public DateTime? FechaUltimoIngreso { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Rol IdRolNavigation { get; set; }
    }
}
