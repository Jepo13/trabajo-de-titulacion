﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesRolesUsuarios.Entities
{
    public partial class Rol
    {
        public Rol()
        {
            Modulos = new HashSet<Modulo>();
            Usuarios = new HashSet<Usuario>();
        }

        public Guid IdRol { get; set; }
        public Guid IdEmpresa { get; set; }
        public string NombreRol { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<Modulo> Modulos { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
