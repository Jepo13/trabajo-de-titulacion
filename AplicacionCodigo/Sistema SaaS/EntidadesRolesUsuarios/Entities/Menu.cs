﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesRolesUsuarios.Entities
{
    public partial class Menu
    {
        public Menu()
        {
            Permisos = new HashSet<Permiso>();
        }

        public Guid IdMenu { get; set; }
        public Guid? IdModulo { get; set; }
        public string NombreMenu { get; set; }
        public string RutaMenu { get; set; }

        public virtual Modulo IdModuloNavigation { get; set; }
        public virtual ICollection<Permiso> Permisos { get; set; }
    }
}
