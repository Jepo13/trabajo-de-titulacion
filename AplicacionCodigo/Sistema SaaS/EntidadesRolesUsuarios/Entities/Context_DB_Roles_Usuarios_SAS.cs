﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesRolesUsuarios.Entities
{
    public partial class Context_DB_Roles_Usuarios_SAS : DbContext
    {
        public Context_DB_Roles_Usuarios_SAS()
        {
        }

        public Context_DB_Roles_Usuarios_SAS(DbContextOptions<Context_DB_Roles_Usuarios_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<Modulo> Modulos { get; set; }
        public virtual DbSet<Permiso> Permisos { get; set; }
        public virtual DbSet<Rol> Rols { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Roles_Usuarios_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.HasKey(e => e.IdMenu);

                entity.ToTable("MENU");

                entity.Property(e => e.IdMenu)
                    .HasColumnName("ID_MENU")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdModulo)
                    .HasColumnName("ID_MODULO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.NombreMenu)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE_MENU");

                entity.Property(e => e.RutaMenu)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false)
                    .HasColumnName("RUTA_MENU");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.Menus)
                    .HasForeignKey(d => d.IdModulo)
                    .HasConstraintName("FK_MENU_REFERENCE_MODULO");
            });

            modelBuilder.Entity<Modulo>(entity =>
            {
                entity.HasKey(e => e.IdModulo);

                entity.ToTable("MODULO");

                entity.Property(e => e.IdModulo)
                    .HasColumnName("ID_MODULO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Modulos)
                    .HasForeignKey(d => d.IdRol)
                    .HasConstraintName("FK_MODULO_REFERENCE_ROL");
            });

            modelBuilder.Entity<Permiso>(entity =>
            {
                entity.HasKey(e => e.IdPermisos);

                entity.ToTable("PERMISOS");

                entity.Property(e => e.IdPermisos)
                    .HasColumnName("ID_PERMISOS")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Concedido).HasColumnName("CONCEDIDO");

                entity.Property(e => e.CssPermiso)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("CSS_PERMISO");

                entity.Property(e => e.IdMenu)
                    .HasColumnName("ID_MENU")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.NombrePermiso)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE_PERMISO");

                entity.HasOne(d => d.IdMenuNavigation)
                    .WithMany(p => p.Permisos)
                    .HasForeignKey(d => d.IdMenu)
                    .HasConstraintName("FK_PERMISOS_REFERENCE_MENU");
            });

            modelBuilder.Entity<Rol>(entity =>
            {
                entity.HasKey(e => e.IdRol);

                entity.ToTable("ROL");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("ID_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.NombreRol)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("NOMBRE_ROL");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.ToTable("USUARIO");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Contrasena)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("CONTRASENA");

                entity.Property(e => e.CorreoInstitucional)
                    .HasMaxLength(60)
                    .HasColumnName("CORREO_INSTITUCIONAL")
                    .IsFixedLength(true);

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.FechaUltimoIngreso)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_ULTIMO_INGRESO");

                entity.Property(e => e.IdEmpresaDefault).HasColumnName("ID_EMPRESA_DEFAULT");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.IdRol)
                    .HasColumnName("ID_ROL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IndicioContrasena)
                    .HasMaxLength(50)
                    .HasColumnName("INDICIO_CONTRASENA")
                    .IsFixedLength(true);

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.IdRol)
                    .HasConstraintName("FK_USUARIO_REFERENCE_ROL");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
