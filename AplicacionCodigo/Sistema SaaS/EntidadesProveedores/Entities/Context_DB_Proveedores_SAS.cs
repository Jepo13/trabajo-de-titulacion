﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesProveedores.Entities
{
    public partial class Context_DB_Proveedores_SAS : DbContext
    {
        public Context_DB_Proveedores_SAS()
        {
        }

        public Context_DB_Proveedores_SAS(DbContextOptions<Context_DB_Proveedores_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<DetalleFacturaCompra> DetalleFacturaCompras { get; set; }
        public virtual DbSet<FacturaElectronicaProveedor> FacturaElectronicaProveedors { get; set; }
        public virtual DbSet<FacturaFisicaProveedor> FacturaFisicaProveedors { get; set; }
        public virtual DbSet<FacturaProveedor> FacturaProveedors { get; set; }
        public virtual DbSet<Proveedor> Proveedors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Proveedores_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<DetalleFacturaCompra>(entity =>
            {
                entity.HasKey(e => e.IdDetalleFacturaCompra);

                entity.ToTable("DETALLE_FACTURA_COMPRA");

                entity.Property(e => e.IdDetalleFacturaCompra)
                    .HasColumnName("ID_DETALLE_FACTURA_COMPRA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cantidad).HasColumnName("CANTIDAD");

                entity.Property(e => e.CostoUnitariocompra)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("COSTO_UNITARIOCOMPRA");

                entity.Property(e => e.IdFacturaProveedor).HasColumnName("ID_FACTURA_PROVEEDOR");

                entity.Property(e => e.PorcentajeIvaVenta).HasColumnName("PORCENTAJE_IVA_VENTA");

                entity.Property(e => e.ValorIvaCompra)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_IVA_COMPRA");

                entity.HasOne(d => d.IdFacturaProveedorNavigation)
                    .WithMany(p => p.DetalleFacturaCompras)
                    .HasForeignKey(d => d.IdFacturaProveedor)
                    .HasConstraintName("FK_DETALLE__REFERENCE_FACTURA_");
            });

            modelBuilder.Entity<FacturaElectronicaProveedor>(entity =>
            {
                entity.HasKey(e => e.IdFacturaElectronicaProveedor)
                    .HasName("PK_FACTURA_ELECTRONICA_PROVEED");

                entity.ToTable("FACTURA_ELECTRONICA_PROVEEDOR");

                entity.Property(e => e.IdFacturaElectronicaProveedor)
                    .HasColumnName("ID_FACTURA_ELECTRONICA_PROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Ambiente)
                    .HasMaxLength(11)
                    .HasColumnName("AMBIENTE");

                entity.Property(e => e.Estado)
                    .HasMaxLength(12)
                    .HasColumnName("ESTADO");

                entity.Property(e => e.FechaAutorizacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_AUTORIZACION");

                entity.Property(e => e.NumeroAutorizacion)
                    .HasMaxLength(100)
                    .HasColumnName("NUMERO_AUTORIZACION");

                entity.Property(e => e.XmlFactura)
                    .IsRequired()
                    .HasColumnType("xml")
                    .HasColumnName("XML_FACTURA");
            });

            modelBuilder.Entity<FacturaFisicaProveedor>(entity =>
            {
                entity.HasKey(e => e.IdFacturaFisicaProveedor);

                entity.ToTable("FACTURA_FISICA_PROVEEDOR");

                entity.Property(e => e.IdFacturaFisicaProveedor)
                    .HasColumnName("ID_FACTURA_FISICA_PROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ImagenFacturaFisica)
                    .IsRequired()
                    .HasColumnType("image")
                    .HasColumnName("IMAGEN_FACTURA_FISICA");
            });

            modelBuilder.Entity<FacturaProveedor>(entity =>
            {
                entity.HasKey(e => e.IdFacturaProveedor);

                entity.ToTable("FACTURA_PROVEEDOR");

                entity.Property(e => e.IdFacturaProveedor)
                    .HasColumnName("ID_FACTURA_PROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FacturaPagada).HasColumnName("FACTURA_PAGADA");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaIngresoFactura)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_INGRESO_FACTURA")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.FechaPago)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_PAGO");

                entity.Property(e => e.IdEstado)
                    .HasColumnName("ID_ESTADO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdFacturaElectronicaProveedor).HasColumnName("ID_FACTURA_ELECTRONICA_PROVEEDOR");

                entity.Property(e => e.IdFacturaFisicaProveedor).HasColumnName("ID_FACTURA_FISICA_PROVEEDOR");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_PROVEEDOR");

                entity.Property(e => e.IdUsuarioCreacion).HasColumnName("ID_USUARIO_CREACION");

                entity.Property(e => e.IdUsuarioModificacion).HasColumnName("ID_USUARIO_MODIFICACION");

                entity.Property(e => e.NumeroFactura)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("NUMERO_FACTURA")
                    .IsFixedLength(true);

                entity.Property(e => e.NumeroGuia)
                    .HasMaxLength(50)
                    .HasColumnName("NUMERO_GUIA");

                entity.Property(e => e.TipoFactura)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("TIPO_FACTURA");

                entity.Property(e => e.ValorTotal)
                    .HasColumnType("decimal(12, 6)")
                    .HasColumnName("VALOR_TOTAL");

                entity.HasOne(d => d.IdFacturaElectronicaProveedorNavigation)
                    .WithMany(p => p.FacturaProveedors)
                    .HasForeignKey(d => d.IdFacturaElectronicaProveedor)
                    .HasConstraintName("FK_FACTURA__REFERENCE_FACTURA_");

                entity.HasOne(d => d.IdFacturaFisicaProveedorNavigation)
                    .WithMany(p => p.FacturaProveedors)
                    .HasForeignKey(d => d.IdFacturaFisicaProveedor)
                    .HasConstraintName("FK_FACT__PROV_FISICA");

                entity.HasOne(d => d.IdProveedorNavigation)
                    .WithMany(p => p.FacturaProveedors)
                    .HasForeignKey(d => d.IdProveedor)
                    .HasConstraintName("FK_FACTURA__REFERENCE_PROVEEDO");
            });

            modelBuilder.Entity<Proveedor>(entity =>
            {
                entity.HasKey(e => e.IdProveedor);

                entity.ToTable("PROVEEDOR");

                entity.Property(e => e.IdProveedor)
                    .HasColumnName("ID_PROVEEDOR")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CodigoProveedor)
                    .HasMaxLength(20)
                    .HasColumnName("CODIGO_PROVEEDOR");

                entity.Property(e => e.ContribuyenteEspecial).HasColumnName("CONTRIBUYENTE_ESPECIAL");

                entity.Property(e => e.CorreoElectronico)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("CORREO_ELECTRONICO");

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("DIRECCION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdEspecialidad).HasColumnName("ID_ESPECIALIDAD");

                entity.Property(e => e.IdPorcentajeIva).HasColumnName("ID_PORCENTAJE_IVA");

                entity.Property(e => e.IdTipoContribuyente).HasColumnName("ID_TIPO_CONTRIBUYENTE");

                entity.Property(e => e.NombreComercial)
                    .HasMaxLength(60)
                    .HasColumnName("NOMBRE_COMERCIAL");

                entity.Property(e => e.NombreContacto)
                    .HasMaxLength(50)
                    .HasColumnName("NOMBRE_CONTACTO");

                entity.Property(e => e.NombreRepresentate)
                    .HasMaxLength(50)
                    .HasColumnName("NOMBRE_REPRESENTATE");

                entity.Property(e => e.Nota)
                    .HasColumnType("text")
                    .HasColumnName("NOTA");

                entity.Property(e => e.ObligadoLlevarContabilidad).HasColumnName("OBLIGADO_LLEVAR_CONTABILIDAD");

                entity.Property(e => e.RazonSocial)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("RAZON_SOCIAL");

                entity.Property(e => e.Ruc)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("RUC");

                entity.Property(e => e.TelefonoContacto)
                    .HasMaxLength(15)
                    .HasColumnName("TELEFONO_CONTACTO");

                entity.Property(e => e.TelefonoMovil)
                    .HasMaxLength(15)
                    .HasColumnName("TELEFONO_MOVIL");

                entity.Property(e => e.TelefonoProveedor)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("TELEFONO_PROVEEDOR");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
