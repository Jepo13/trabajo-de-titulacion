﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProveedores.Entities
{
    public partial class FacturaElectronicaProveedor
    {
        public FacturaElectronicaProveedor()
        {
            FacturaProveedors = new HashSet<FacturaProveedor>();
        }

        public Guid IdFacturaElectronicaProveedor { get; set; }
        public string XmlFactura { get; set; }
        public string Estado { get; set; }
        public string NumeroAutorizacion { get; set; }
        public string Ambiente { get; set; }
        public DateTime? FechaAutorizacion { get; set; }

        public virtual ICollection<FacturaProveedor> FacturaProveedors { get; set; }
    }
}
