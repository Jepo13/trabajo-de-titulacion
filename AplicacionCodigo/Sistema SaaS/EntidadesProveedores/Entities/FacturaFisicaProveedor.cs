﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProveedores.Entities
{
    public partial class FacturaFisicaProveedor
    {
        public FacturaFisicaProveedor()
        {
            FacturaProveedors = new HashSet<FacturaProveedor>();
        }

        public Guid IdFacturaFisicaProveedor { get; set; }
        public byte[] ImagenFacturaFisica { get; set; }

        public virtual ICollection<FacturaProveedor> FacturaProveedors { get; set; }
    }
}
