﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProveedores.Entities
{
    public partial class FacturaProveedor
    {
        public FacturaProveedor()
        {
            DetalleFacturaCompras = new HashSet<DetalleFacturaCompra>();
        }

        public Guid IdFacturaProveedor { get; set; }
        public Guid? IdFacturaFisicaProveedor { get; set; }
        public Guid? IdFacturaElectronicaProveedor { get; set; }
        public Guid? IdProveedor { get; set; }
        public string NumeroFactura { get; set; }
        public decimal ValorTotal { get; set; }
        public bool FacturaPagada { get; set; }
        public string TipoFactura { get; set; }
        public DateTime FechaIngresoFactura { get; set; }
        public DateTime? FechaPago { get; set; }
        public string NumeroGuia { get; set; }
        public Guid? IdEstado { get; set; }
        public Guid? IdUsuarioCreacion { get; set; }
        public Guid? IdUsuarioModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual FacturaElectronicaProveedor IdFacturaElectronicaProveedorNavigation { get; set; }
        public virtual FacturaFisicaProveedor IdFacturaFisicaProveedorNavigation { get; set; }
        public virtual Proveedor IdProveedorNavigation { get; set; }
        public virtual ICollection<DetalleFacturaCompra> DetalleFacturaCompras { get; set; }
    }
}
