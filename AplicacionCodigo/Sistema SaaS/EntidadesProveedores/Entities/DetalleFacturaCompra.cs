﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProveedores.Entities
{
    public partial class DetalleFacturaCompra
    {
        public Guid IdDetalleFacturaCompra { get; set; }
        public Guid? IdFacturaProveedor { get; set; }
        public int Cantidad { get; set; }
        public decimal CostoUnitariocompra { get; set; }
        public decimal ValorIvaCompra { get; set; }
        public int PorcentajeIvaVenta { get; set; }

        public virtual FacturaProveedor IdFacturaProveedorNavigation { get; set; }
    }
}
