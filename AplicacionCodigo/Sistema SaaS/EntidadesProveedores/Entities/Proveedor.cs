﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesProveedores.Entities
{
    public partial class Proveedor
    {
        public Proveedor()
        {
            FacturaProveedors = new HashSet<FacturaProveedor>();
        }

        public Guid IdProveedor { get; set; }
        public Guid? IdTipoContribuyente { get; set; }
        public Guid? IdEspecialidad { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdPorcentajeIva { get; set; }
        public bool ContribuyenteEspecial { get; set; }
        public string RazonSocial { get; set; }
        public string NombreComercial { get; set; }
        public string Ruc { get; set; }
        public string Direccion { get; set; }
        public string TelefonoProveedor { get; set; }
        public string TelefonoMovil { get; set; }
        public string CorreoElectronico { get; set; }
        public bool ObligadoLlevarContabilidad { get; set; }
        public string NombreRepresentate { get; set; }
        public string NombreContacto { get; set; }
        public string TelefonoContacto { get; set; }
        public string CodigoProveedor { get; set; }
        public string Nota { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<FacturaProveedor> FacturaProveedors { get; set; }
    }
}
