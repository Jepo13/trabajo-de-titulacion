using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainDB.Entities.InterfacesManageData;
using DomainDB.Entities.ManageData;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection.Extensions;
using EntidadesCatalogosNotas.Entities;
using RepositorioCatalogoNotas.Interface;
using RepositorioCatalogoNotas.Implementacion;
using RepositorioLogExcepcion.Interface;
using RepositorioLogExcepcion.Implementacion;
using EntidadesLogExcepcion.Entities;
using EntidadesEmpresa.Entities;
using RepositorioEmpresa.Interface;
using RepositorioEmpresa.Implementacion;
using EntidadesPersonas.Entities;
using RepositorioPersona.Interface;
using RepositorioPersona.Implementacion;
using EntidadesProveedores.Entities;
using RepositorioProveedores.Interface;
using RepositorioProveedores.Implementacion;
using RepositorioRolesUsuarios.Interface;
using RepositorioRolesUsuarios.Implementacion;
using EntidadesRolesUsuarios.Entities;
using RepositorioProductos.Interface;
using RepositorioProductos.Implementacion;
using EntidadesProductos.Entities;

namespace API_Login
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
            });

            services.AddDbContext<Context_DB_Catalogos_Nota_SAS>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString_Catalogos_Notas")));
            services.AddDbContext<Context_DB_Logs_Excepcion_SAS>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString_Logs_Exepcion")));
            services.AddDbContext<Context_DB_Empresa_SAS>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString_Empresa")));
            services.AddDbContext<Context_DB_Personas_SAS>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString_Personas")));
            services.AddDbContext<Context_DB_Proveedores_SAS>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString_Proveedores")));
            services.AddDbContext<Context_DB_Roles_Usuarios_SAS>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString_Roles_Usuarios")));
            services.AddDbContext<Context_DB_Productos_SAS>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString_Productos")));


            services.AddAutoMapper(typeof(Startup));
            services.AddMvc();
            services.AddMvcCore();

     
            services.AddScoped<IManageCatalogos, ManageCatalogos>();
            services.AddScoped<IManageNota, ManageNota>();

           
            services.AddScoped<IManageCaja, ManageCaja>();
            services.AddScoped<IManageControlCaja, ManageControlCaja>();
            services.AddScoped<IManageEmpresa, ManageEmpresa>();
            services.AddScoped<IManageLocal, ManageLocal>();
            services.AddScoped<IManageUbicacion, ManageUbicacion>();
            services.AddScoped<IManageSitio, ManageSitio>();
            services.AddScoped<IManageBandeja, ManageBandeja>();

            services.AddScoped<IManageLogError, ManageLogError>();
            
           

            services.AddScoped<IManageProveedor, ManageProveedor>();

            //Inicio Services Interface: EntidadPersona//
            services.AddScoped<IManagePersona, ManagePersona>();
            services.AddScoped<IManageEmpresaCliente, ManageEmpresaCliente>();
         

            //Fin Services Interface: EntidadPersona//

            services.AddScoped<IManageUser, ManageUser>();
            services.AddScoped<IManageRol, ManageRol>();
            services.AddScoped<IManageLogin, ManageLogin>();
            // ESTAS

            services.AddScoped<IManageCanjeProductoProveedor, ManageCanjeProductoProveedor>();
            services.AddScoped<IManageCategoria, ManageCategoria>();
            services.AddScoped<IManageGrupoProducto, ManageGrupoProducto>();
            services.AddScoped<IManageEstadoProducto, ManageEstadoProducto>();
            services.AddScoped<IManageMarca, ManageMarca>();
            services.AddScoped<IManageImpuesto, ManageImpuesto>();
            services.AddScoped<IManageUnidadEntrega, ManageUnidadEntrega>();

            services.AddScoped<IManageProducto, ManageProducto>();


            services.TryAddScoped(typeof(IManageCRUDCatalogos<>), typeof(ManageCRUDCatalogos<>));
            services.TryAddScoped(typeof(IManageCRUDEmpresa<>), typeof(ManageCRUDEmpresa<>));
            services.TryAddScoped(typeof(IManageCRUDPersona<>), typeof(ManageCRUDPersona<>));
            services.TryAddScoped(typeof(IManageCRUDProveedor<>), typeof(ManageCRUDProveedor<>));
            services.TryAddScoped(typeof(IManageCRUD_Roles_Usuarios<>), typeof(ManageCRUD_Roles_Usuarios<>));
            services.TryAddScoped(typeof(IManageCRUD_LogError<>), typeof(ManageCRUD_LogError<>));
            services.TryAddScoped(typeof(IManageCRUDProductos<>), typeof(ManageCRUDProductos<>));

        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler();
            }
            //app.UseMvc();
            //app.UseSwagger();
            //////Desarrollo
            //app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ComerciosEciador"));


            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
