﻿
using DomainDB.Entities.InterfacesManageData;
using EntidadesLogExcepcion.Entities;
using RepositorioLogExcepcion.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComerciosEcuadorAPI.Model
{
    public class LoggerAPI
    {
        private readonly IManageLogError _logError;
        public LoggerAPI(IManageLogError logError)
        {
            _logError = logError;
        }

        public async Task guardarError(string nombreControlador, string accion, string mensaje, string jsonObjeto)
        {
            LogExcepcione objLog = new LogExcepcione();

            objLog.Entidad = nombreControlador;
            objLog.Metodo = accion;

            objLog.Error = mensaje;
            objLog.Descripcion = jsonObjeto;

            _logError.AddLogError(objLog);

           bool resultado = await _logError.saveError(objLog);
        }
    }
}
