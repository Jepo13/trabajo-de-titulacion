﻿using AutoMapper;
using DTOs.UnidadEntrega;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;
using RepositorioProductos.Interface;
using EntidadesProductos.Entities;
using Newtonsoft.Json;
using ComerciosEcuadorAPI.Model;
using RepositorioLogExcepcion.Interface;

namespace API_Login.Controllers
{
    [Route("api/UnidadEntrega")]
    [ApiController]
    public class UnidadEntregaAPIController : ControllerBase
    {
        private readonly IManageUnidadEntrega _unidadEntregaRepository;
        private readonly IManageCRUDProductos<UnidadEntrega> _CRUDRepositoryProducto;
        private readonly IMapper _mapper;
        private readonly IManageLogError _logError;

        public UnidadEntregaAPIController(IManageUnidadEntrega unidadEntregaRepository, IMapper mapper, IManageCRUDProductos<UnidadEntrega> cRUDRepositoryProducto, IManageLogError logError)
        {
            _unidadEntregaRepository = unidadEntregaRepository ?? throw new ArgumentException(nameof(unidadEntregaRepository));
            this._mapper = mapper;
            _CRUDRepositoryProducto = cRUDRepositoryProducto;
            _logError = logError;
        }
        #region CRUD
        #region Create
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] UnidadEntregaCreateDTO objUnidadEntregaDTO)
        {
            try
            {
                if (objUnidadEntregaDTO == null)
                {
                    return BadRequest();
                }
                UnidadEntrega objUnidadEntrega = _mapper.Map<UnidadEntrega>(objUnidadEntregaDTO);
                _CRUDRepositoryProducto.Add(objUnidadEntrega);
                var result = await _CRUDRepositoryProducto.save();

                if (result.estado)
                {
                    UnidadEntregaCompleteDTO objUnidadEntregaResult = _mapper.Map<UnidadEntregaCompleteDTO>(objUnidadEntrega);
                    return CreatedAtRoute("GetUnidadEntregaById", new { idUnidadEntrega = objUnidadEntrega.IdUnidadentrega }, objUnidadEntregaResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objUnidadEntregaDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objUnidadEntregaDTO), ex.ToString());
            }

            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Edit
        [HttpPost("Edit")]
        public async Task<IActionResult> Edit(Guid idUnidadEntrega, UnidadEntregaUpdateDTO objUnidadEntregaDTO)
        {
            try
            {
                if (objUnidadEntregaDTO == null)
                {
                    return BadRequest();
                }
                UnidadEntrega objUnidadEntregaRepository = await _unidadEntregaRepository.GetUnidadEntregaById(idUnidadEntrega);
                _mapper.Map(objUnidadEntregaDTO, objUnidadEntregaRepository);

                _CRUDRepositoryProducto.Edit(objUnidadEntregaRepository);
                var result = await _CRUDRepositoryProducto.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                    return NoContent();
                else
                    await guardarLogs(JsonConvert.SerializeObject(objUnidadEntregaDTO), result.mensajeError);


                return BadRequest();
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objUnidadEntregaDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Delete
        [HttpPost("{DeleteUnidadEntrega}")]
        public async Task<IActionResult> DeleteUnidadEntrega(Guid idUnidadEntrega)
        {
            UnidadEntrega objUnidadEntregaRepository = await _unidadEntregaRepository.GetUnidadEntregaById(idUnidadEntrega);
            try
            {
                if (objUnidadEntregaRepository == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                _CRUDRepositoryProducto.Delete(objUnidadEntregaRepository);
                var result = await _CRUDRepositoryProducto.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                    return NoContent();
                else
                    await guardarLogs(JsonConvert.SerializeObject(objUnidadEntregaRepository), result.mensajeError);

            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objUnidadEntregaRepository), ex.ToString());
            }
            return BadRequest();
        }
        #endregion

        #endregion

        #region Serchs

        #region Get UnidadEntrega by Id
        [HttpGet("{idUnidadEntrega}", Name = "GetUnidadEntregaById")]
        [HttpHead]
        public async Task<ActionResult<UnidadEntregaCompleteDTO>> GetUnidadEntregaById(Guid idUnidadEntrega)
        {
            try
            {
                UnidadEntrega unidadentrega = await _unidadEntregaRepository.GetUnidadEntregaById(idUnidadEntrega);
                if (unidadentrega == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                UnidadEntregaCompleteDTO objUnidadEntrega = _mapper.Map<UnidadEntregaCompleteDTO>(unidadentrega);
                return Ok(objUnidadEntrega);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get all
        [HttpGet("GetAllUnidadEntrega")]
        public async Task<ActionResult<List<UnidadEntregaCompleteDTO>>> GetAllUnidadEntrega(Guid idEmpresa)
        {
            try
            {
                List<UnidadEntrega> listBodegas = await _unidadEntregaRepository.GetAllUnidadEntrega(idEmpresa);
                if (listBodegas.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<UnidadEntregaCompleteDTO> listUnidadEntregaDTO = _mapper.Map<List<UnidadEntregaCompleteDTO>>(listBodegas);
                return Ok(listUnidadEntregaDTO);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        #endregion
        #region Get by name
        [HttpGet("GetUnidadEntregaByName")]
        public async Task<ActionResult<List<UnidadEntregaCompleteDTO>>> GetUnidadEntregaByName(string nameUnidadEntrega)
        {
            try
            {
                List<UnidadEntrega> unidadentrega = await _unidadEntregaRepository.GetUnidadEntregaByName(nameUnidadEntrega);
                if (unidadentrega == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                UnidadEntregaCompleteDTO objUnidadEntrega = _mapper.Map<UnidadEntregaCompleteDTO>(unidadentrega);
                return Ok(objUnidadEntrega);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get by Nombre Exacto
        [HttpGet("GetUnidadEntregaByNameExact")]
        public async Task<ActionResult<UnidadEntregaCompleteDTO>> GetUnidadEntregaByNameExact(string nameUnidadEntrega, Guid idEmpresa)
        {
            try
            {
                UnidadEntrega unidadentrega = await _unidadEntregaRepository.GetUnidadEntregaByNameExact(nameUnidadEntrega, idEmpresa);
                if (unidadentrega == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                UnidadEntregaCompleteDTO objUnidadEntrega = _mapper.Map<UnidadEntregaCompleteDTO>(unidadentrega);
                return Ok(objUnidadEntrega);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get by Code
        [HttpGet("GetUnidadEntregaByCode")]
        public async Task<ActionResult<UnidadEntregaCompleteDTO>> GetUnidadEntregaByCode(string codigo, Guid idEmpresa)
        {
            try
            {
                UnidadEntrega unidadentrega = await _unidadEntregaRepository.GetUnidadEntregaByNameExact(codigo, idEmpresa);
                if (unidadentrega == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                UnidadEntregaCompleteDTO objUnidadEntrega = _mapper.Map<UnidadEntregaCompleteDTO>(unidadentrega);
                return Ok(objUnidadEntrega);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion


        #region Get by GetBusquedaAvanzada
        [HttpGet("GetBusquedaAvanzada")]
        public async Task<ActionResult<UnidadEntregaCompleteDTO>> GetBusquedaAvanzada(UnidadEntregaBusquedaDTO objBusqueda)
        {
            try
            {
                var listaDTO = await _unidadEntregaRepository.GetUBusquedaAvanzada(objBusqueda);
                if (listaDTO == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                List<UnidadEntregaCompleteDTO> objUnidadEntrega = _mapper.Map<List<UnidadEntregaCompleteDTO>>(listaDTO);

                return Ok(objUnidadEntrega);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion




        #endregion

        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }
}
