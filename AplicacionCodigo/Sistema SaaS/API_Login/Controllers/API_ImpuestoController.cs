﻿//using AutoMapper;
//using DTOs.Impuesto;
//using DomainDB.Entities.Entities;
//using DomainDB.Entities.InterfacesManageData;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Utilitarios;

//namespace API_Login.Controllers
//{
//    [Route("api/Impuesto")]
//    [ApiController]
//    public class ImpuestoAPIController : ControllerBase
//    {
//        private readonly IManageImpuesto _impuestoRepository;
//        private readonly IMapper _mapper;
//        public ImpuestoAPIController(IManageImpuesto impuestoRepository, IMapper mapper)
//        {
//            _impuestoRepository = impuestoRepository ?? throw new ArgumentException(nameof(impuestoRepository));
//            this._mapper = mapper;
//        }
//        // crud 
//        #region CRUD
//        #region Create
//        [HttpPost("Create")]
//        [HttpHead]

//        public async Task<IActionResult> Create([FromBody] ImpuestoCreateDTO objImpuestoDTO)
//        {
//            try
//            {
//                if (objImpuestoDTO == null)
//                {
//                    return BadRequest();
//                }
//                Impuesto objImpuesto = _mapper.Map<Impuesto>(objImpuestoDTO);
//                _impuestoRepository.AddImpuesto(objImpuesto);
//                var result = await _impuestoRepository.SaveImpuesto();
//                if (result)
//                {
//                    ImpuestoCompleteDTO objImpuestoResult = _mapper.Map<ImpuestoCompleteDTO>(objImpuesto);
//                    return CreatedAtRoute("GetImpuestoById", new { idImpuesto = objImpuesto.IdImpuesto}, objImpuestoResult);
//                }
//            }
//            catch (Exception ex)
//            {

//            }

//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Edit
//        [HttpPut("Edit")]
//        public async Task<IActionResult> Edit (Guid idImpuesto, ImpuestoCreateDTO objImpuetoDTO)
//        {
//            try
//            {
//                if (objImpuetoDTO == null)
//                {
//                    return BadRequest();
//                }
//                Impuesto objImpuetoRepository = await _impuestoRepository.GetImpuestoById(idImpuesto);
//                _mapper.Map(objImpuetoDTO, objImpuetoRepository);
//                _impuestoRepository.EditImpuesto(objImpuetoRepository);
//                var result = await _impuestoRepository.SaveImpuesto();
//                if (result)
//                {
//                    return NoContent();
//                }
//                return BadRequest();
//            }
//            catch(Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Delete
//        [HttpDelete("{DeleteImpuesto}")]
//        public async Task<IActionResult> DeleteImpuesto(Guid idImpuesto)
//        {
//            Impuesto objImpuestoRepository = await _impuestoRepository.GetImpuestoById(idImpuesto);
//            if (objImpuestoRepository == null)
//            {
//                return NotFound(MensajesRespuesta.sinResultados());
//            }
//            _impuestoRepository.DeleteImpuesto(objImpuestoRepository);
//            var result = await _impuestoRepository.SaveImpuesto();
//            //Se comprueba que se actualizó correctamente
//            if (result)
//            {
//                return NoContent();
//            }
//            return BadRequest();
//        }
//        #endregion
//        #endregion

//        #region Search
//        #region Get Bodega by Id
//        [HttpGet("{idImpuesto}", Name = "GetImpuestoById")]
//        [HttpHead]
//        public async Task<ActionResult<ImpuestoCompleteDTO>> GetBodegaById(Guid idImpuesto)
//        {
//            try
//            {
//                Impuesto impuesto = await _impuestoRepository.GetImpuestoById(idImpuesto);
//                if (impuesto == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                ImpuestoCompleteDTO objImpuesto = _mapper.Map<ImpuestoCompleteDTO>(impuesto);
//                return Ok(objImpuesto);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #region Get all
//        [HttpGet("GetAllImpuesto")]
//        [HttpHead]
//        public async Task<ActionResult<List<ImpuestoCompleteDTO>>> GetAllImpuesto()
//        {
//            try
//            {
//                List<Impuesto> listImpuesto = await _impuestoRepository.GetAllImpuesto();
//                if (listImpuesto.Count() < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<ImpuestoCompleteDTO> listImpuestoDTO = _mapper.Map<List<ImpuestoCompleteDTO>>(listImpuesto);
//                return Ok(listImpuestoDTO);
//            }
//            catch (Exception ex)
//            {

//            }

//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #region Get gy name
//        [HttpGet("GetImpuestoByName")]
//        [HttpHead]
//        public async Task<ActionResult<ImpuestoCompleteDTO>> GetImpuestoByName(string nameImpuesto)
//        {
//            try
//            {
//                Impuesto impuesto = await _impuestoRepository.GetImpuestoByName(nameImpuesto);
//                if (impuesto == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                ImpuestoCompleteDTO objImpuesto = _mapper.Map<ImpuestoCompleteDTO>(impuesto);
//                return Ok(objImpuesto);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion

//        #endregion
//        #endregion
//    }


//}
