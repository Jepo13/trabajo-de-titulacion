﻿using AutoMapper;
using ComerciosEcuadorAPI.Model;

using DomainDB.Entities.InterfacesManageData;
using EntidadesProveedores.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RepositorioLogExcepcion.Interface;
using RepositorioProveedores.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;
using RepositorioCatalogoNotas.Interface;
using EntidadesCatalogosNotas.Entities;
using RepositorioProductos.Interface;
using EntidadesProductos.Entities;
using DTOs.Productos;
using DTOs.EstadoProducto;

namespace API_Login.Controllers
{
    [Route("api/Producto")]
    [ApiController]
    public class API_ProductoController : ControllerBase
    {
        private readonly IManageCatalogos _catalogoRepository;
        private readonly IManageProducto _productoRepository;
        private readonly IMapper _mapper;
        private readonly IManageCRUDProductos<Producto> _CRUDRepositoryProducto;
        private readonly IManageLogError _logError;
        public API_ProductoController(IManageProducto proveedorRepository, IMapper mapper, IManageCRUDProductos<Producto> cRUDRepository, IManageLogError logError, IManageCatalogos catalogoRepository)
        {
            _productoRepository = proveedorRepository ?? throw new ArgumentException(nameof(proveedorRepository));
            this._mapper = mapper;
            _CRUDRepositoryProducto = cRUDRepository;
            _logError = logError;
            _catalogoRepository = catalogoRepository;
        }
        // crud
        #region CRUD

        #region Create
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] ProductoDTOCrear objDTO)
        {
            try
            {
                if (objDTO == null)
                {
                    return BadRequest();
                }
                Producto objRepositorio = _mapper.Map<Producto>(objDTO);
                _CRUDRepositoryProducto.Add(objRepositorio);
                var result = await _CRUDRepositoryProducto.save();
                if (result.estado)
                {
                    ProductoDTOCompleto objProvedorResult = _mapper.Map<ProductoDTOCompleto>(objRepositorio);
                    return CreatedAtRoute("GetProductoById", new { idProducto = objRepositorio.IdProducto }, objProvedorResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #region Edit
        [HttpPut("Edit")]
        public async Task<IActionResult> Edit(Guid idProducto, ProductoDTOEditar objDTO)
        {
            try
            {
                if (objDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }
                Producto objRepositorio = await _productoRepository.GetProductoById(idProducto);
                _mapper.Map(objDTO, objRepositorio);
                objRepositorio.IdProducto = idProducto;
                _CRUDRepositoryProducto.Edit(objRepositorio);
                var result = await _CRUDRepositoryProducto.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objDTO), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objDTO), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #region Delete
        [HttpDelete("{DeleteProveedor}")]
        public async Task<IActionResult> DeleteProveedor(Guid idProducto)
        {
            
            try
            {
                Producto objRepositorio = await _productoRepository.GetProductoById(idProducto);


                _CRUDRepositoryProducto.Delete(objRepositorio);
                var result = await _CRUDRepositoryProducto.save();
                // se comprueba que se actualizo correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objRepositorio), result.mensajeError);
                }

                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(idProducto.ToString(), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #endregion

        #region Search
        #region Get Producto By Id
        [HttpGet("{idProducto}", Name = "GetProductoById")]
       
        public async Task<ActionResult<ProductoDTOCompleto>> GetProductoById(Guid idProducto)
        {
            try
            {
                Producto proveedor = await _productoRepository.GetProductoById(idProducto);
                if (proveedor == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                ProductoDTOCompleto objDTO = _mapper.Map<ProductoDTOCompleto>(proveedor);
                return Ok(objDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
       
        #region Get by name
        [HttpGet("GetProductoByName")]
        [HttpHead]
        public async Task<ActionResult<List<ProductoDTOCompleto>>> GetProductoByName(string nameProducto)
        {
            try
            {
                var lista = await _productoRepository.GetProductoByName(nameProducto);
                if (lista == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<ProductoDTOCompleto> listaDTO = _mapper.Map<List<ProductoDTOCompleto>>(lista);
                return Ok(listaDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
    
        #region Get By Advanced
       
        //[HttpGet("GetProveedoresAdvanced")]
        //public async Task<ActionResult<List<ProveedorResultadoBusquedaDTO>>> GetProveedorAdvanced(ObjetoBusquedaProveedor objBusqueda)
        //{
        //    try
        //    {
        //        List<Producto> listProveedores = await _productoRepository.GetProveedorAdvanced(objBusqueda);

        //        if (listProveedores.Count < 1)
        //        {
        //            return NotFound(MensajesRespuesta.sinResultados());
        //        }

        //        List<ProveedorResultadoBusquedaDTO> listProveedoresDTO = _mapper.Map<List<ProveedorResultadoBusquedaDTO>>(listProveedores);
        //        foreach (var itemProveedor  in listProveedoresDTO)
        //        {
        //            if (itemProveedor.IdEspecialidad != null) 
        //            {
        //                Catalogo catalogo = await _catalogoRepository.GetCatalogoById((Guid)itemProveedor.IdEspecialidad);
        //                itemProveedor.Especialidad = catalogo.NombreCatalogo;
        //            }
        //            else 
        //            {
        //                itemProveedor.Especialidad = "!Sin Catalogar¡";
        //            }
                    
        //        }
        //        return Ok(listProveedoresDTO);
        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //    return StatusCode(StatusCodes.Status500InternalServerError);
        //}
        #endregion
        // falta hacer busdquera por razon social 
        #endregion
        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }
}
