﻿//using AutoMapper;
//using DTOs.CanjeProductoProveedor;
//using DomainDB.Entities.InterfacesManageData;
//using EntidadesProductos.Entities;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using RepositorioProductos.Interface;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Utilitarios;

//namespace API_Login.Controllers
//{
//    [Route("api/CanjeProductoProveedor")]
//    [ApiController]
//    public class CanjeProductoProveedorAPIController : Controller
//    {
//        private readonly IManageCanjeProductoProveedor _canjeProductoProveedorRepository;
//        private readonly IMapper _mapper;

//        public CanjeProductoProveedorAPIController(IManageCanjeProductoProveedor canjeProductoProveedorRepository, IMapper mapper)
//        {
//            _canjeProductoProveedorRepository = canjeProductoProveedorRepository ?? throw new ArgumentException(nameof(canjeProductoProveedorRepository));
//            this._mapper = mapper;
//        }


//        #region CRUD
//        #region Create
//        [HttpPost("Create")]
//        [HttpHead]
//        public async Task<IActionResult> Create([FromBody] CanjeProductoProveedorCreateDTO objCanjeProductoProveedorDTO)
//        {
//            try
//            {
//                if (objCanjeProductoProveedorDTO == null)
//                {
//                    return BadRequest();
//                }
//                CanjeProductoProveedor objCanjeProductoProveedor = _mapper.Map<CanjeProductoProveedor>(objCanjeProductoProveedorDTO);
//                _canjeProductoProveedorRepository.AddCanjeProductoProveedor(objCanjeProductoProveedor);
//                var result = await _canjeProductoProveedorRepository.SaveCanjeProductoProveedor();
//                if (result)
//                {
//                    CanjeProductoProveedorCompleteDTO objCanjeProductoProveedorResult = _mapper.Map<CanjeProductoProveedorCompleteDTO>(objCanjeProductoProveedor);
//                    return CreatedAtRoute("GetCanjeProductoProveedorById", new { idCanjeProductoProveedor = objCanjeProductoProveedorResult.IdCanjeproductoproveedor }, objCanjeProductoProveedorResult);
//                }

//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Edit 
//        [HttpPut("Edit")]
//        public async Task<IActionResult> Edit(Guid idCanjeProductoProveedor, CanjeProductoProveedorCreateDTO objCanjeProductoProveedorDTO)
//        {
//            try
//            {
//                if (objCanjeProductoProveedorDTO == null)
//                {
//                    return BadRequest();
//                }
//                Canjeproductoproveedor CanjeProductoProvedorRepository = await _canjeProductoProveedorRepository.GetCanjeProductoProveedorById(idCanjeProductoProveedor);
//                _mapper.Map(objCanjeProductoProveedorDTO, CanjeProductoProvedorRepository);
//                CanjeProductoProvedorRepository.IdCanjeproductoproveedor = idCanjeProductoProveedor;
//                _canjeProductoProveedorRepository.EditCanjeProductoProveedor(CanjeProductoProvedorRepository);
//                var result = await _canjeProductoProveedorRepository.SaveCanjeProductoProveedor();

//                if (result)
//                {
//                    return NoContent();
//                }
//                return BadRequest();
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Delete
//        [HttpDelete("{Delete}")]
//        public async Task<IActionResult> Delete(Guid idCanjeProductoProveedor)
//        {
//            Canjeproductoproveedor objCanjeProductoProveedorepository = await _canjeProductoProveedorRepository.GetCanjeProductoProveedorById(idCanjeProductoProveedor);
//            if (objCanjeProductoProveedorepository == null)
//            {
//                return NotFound(MensajesRespuesta.sinResultados());
//            }
//            _canjeProductoProveedorRepository.DeleteCanjeProductoProveedor(objCanjeProductoProveedorepository);
//            var result = await _canjeProductoProveedorRepository.SaveCanjeProductoProveedor();
//            //Se comprueba que se actualizó correctamente
//            if (result)
//            {
//                return NoContent();
//            }
//            return BadRequest();
//        }
//        #endregion
//        #endregion

//        #region Search
//        #region Get CanjeProductoProveedor By Id
//        [HttpGet("{idCanjeProductoProveedor}", Name = "GetCanjeProductoProveedorById")]
//        [HttpHead]
//        public async Task<ActionResult<CanjeProductoProveedorCompleteDTO>> GetCanjeProductoProveedorById(Guid idCanjeProductoProveedor)
//        {
//            try
//            {
//                Canjeproductoproveedor CanjeProductoProveedor = await _canjeProductoProveedorRepository.GetCanjeProductoProveedorById(idCanjeProductoProveedor);
//                if (CanjeProductoProveedor == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                Canjeproductoproveedor objCanjeProductoProveedor = _mapper.Map<Canjeproductoproveedor>(CanjeProductoProveedor);
//                return Ok(CanjeProductoProveedor);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #region Get all
//        [HttpGet("GetAll")]
//        [HttpHead]
//        public async Task<ActionResult<List<CanjeProductoProveedorCompleteDTO>>> GetAll()
//        {
//            try
//            {
//                List<Canjeproductoproveedor> listCanjeProductoProveedor = await _canjeProductoProveedorRepository.GetAllCanjeProductoProveedor();
//                if (listCanjeProductoProveedor.Count() < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<CanjeProductoProveedorCompleteDTO> listCanjeProductoProveedorDTO = _mapper.Map<List<CanjeProductoProveedorCompleteDTO>>(listCanjeProductoProveedor);
//                return Ok(listCanjeProductoProveedorDTO);
//            }
//            catch (Exception ex)
//            {

//            }

//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }

//        #endregion
//        #region Get by Identificador
//        [HttpGet("GetCanjeProductoProveedorByIdentificador")]
//        [HttpHead]
//        public async Task<ActionResult<CanjeProductoProveedorCompleteDTO>> GetCanjeProductoProveedorByIdentificador(string Identificador)
//        {
//            try
//            {
//                Canjeproductoproveedor canjeProductoProveedor = await _canjeProductoProveedorRepository.GetCanjeProductoProveedorByIdentificador(Identificador);
//                if (canjeProductoProveedor == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                CanjeProductoProveedorCompleteDTO objCanjeProductoProveedor = _mapper.Map<CanjeProductoProveedorCompleteDTO>(canjeProductoProveedor);
//                return Ok(objCanjeProductoProveedor);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion

//        #endregion
//    }
//}
