﻿using AutoMapper;
using ComerciosEcuadorAPI.Model;
using DTOs.Marca;
using EntidadesEmpresa.Entities;
using EntidadesProductos.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RepositorioLogExcepcion.Interface;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;

namespace ComerciosEcuadorAPI.Controllers
{
    public class API_MarcaController : Controller
    {
        private readonly IManageMarca _marcaRepository;
        private readonly IManageCRUDProductos<Marca> _CRUDRepositoryProducto;
        private readonly IMapper _mapper;
        private readonly IManageLogError _logError;

        public API_MarcaController(IManageMarca MarcaRepository, IManageCRUDProductos<Marca> cRUDRepositoryProducto = null, IMapper mapper = null, IManageLogError logError = null)
        {
            _marcaRepository = MarcaRepository;
            _CRUDRepositoryProducto = cRUDRepositoryProducto;
            _mapper = mapper;
            _logError = logError;
        }
        #region CRUD

        #region Create
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] MarcaUpdateDTO objMarcaDTO)
        {
            try
            {
                if (objMarcaDTO == null)
                {
                    return BadRequest();
                }
                Marca objRepositorio = _mapper.Map<Marca>(objMarcaDTO);
                _CRUDRepositoryProducto.Add(objRepositorio);
                var result = await _CRUDRepositoryProducto.save();

                if (result.estado)
                {
                    MarcaCompleteDTO objMarcaResult = _mapper.Map<MarcaCompleteDTO>(objRepositorio);
                    return CreatedAtRoute("GetMarcaById", new { idMarca = objRepositorio.IdMarca }, objMarcaResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objMarcaDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objMarcaDTO), ex.ToString());
            }

            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Edit
        [HttpPost("Edit")]
        public async Task<IActionResult> Edit(Guid idMarca, MarcaUpdateDTO objMarcaDTO)
        {
            try
            {
                if (objMarcaDTO == null)
                {
                    return BadRequest();
                }
                Marca objRepositorio = await _marcaRepository.GetMarcaById(idMarca);
                _mapper.Map(objMarcaDTO, objRepositorio);

                _CRUDRepositoryProducto.Edit(objRepositorio);
                var result = await _CRUDRepositoryProducto.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                    return NoContent();
                else
                    await guardarLogs(JsonConvert.SerializeObject(objMarcaDTO), result.mensajeError);


                return BadRequest();
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objMarcaDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Delete
        [HttpPost("{DeleteMarca}")]
        public async Task<IActionResult> DeleteMarca(Guid idMarca)
        {
            Marca objRepositorio = await _marcaRepository.GetMarcaById(idMarca);
            try
            {
                if (objRepositorio == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                _CRUDRepositoryProducto.Delete(objRepositorio);
                var result = await _CRUDRepositoryProducto.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                    return NoContent();
                else
                    await guardarLogs(JsonConvert.SerializeObject(objRepositorio), result.mensajeError);

            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objRepositorio), ex.ToString());
            }
            return BadRequest();
        }
        #endregion

        #endregion

        #region Serchs

        #region Get Marca by Id
        [HttpGet("{idMarca}", Name = "GetMarcaById")]
        [HttpHead]
        public async Task<ActionResult<MarcaCompleteDTO>> GetMarcaById(Guid idMarca)
        {
            try
            {
                Marca Marca = await _marcaRepository.GetMarcaById(idMarca);
                if (Marca == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                MarcaCompleteDTO objRepositorio = _mapper.Map<MarcaCompleteDTO>(Marca);
                return Ok(objRepositorio);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get all
        [HttpGet("GetAllMarca")]
        public async Task<ActionResult<List<MarcaCompleteDTO>>> GetAllMarca(Guid idEmpresa)
        {
            try
            {
                List<Marca> listBodegas = await _marcaRepository.GetAllMarcas(idEmpresa);
                if (listBodegas.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<MarcaCompleteDTO> listMarcaDTO = _mapper.Map<List<MarcaCompleteDTO>>(listBodegas);
                return Ok(listMarcaDTO);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        #endregion
        #region Get by name
        [HttpGet("GetMarcaByName")]
        public async Task<ActionResult<List<MarcaCompleteDTO>>> GetMarcaByName(string nameMarca, Guid idEmpresa)
        {
            try
            {
                List<Marca> Marca = await _marcaRepository.GetMarcaByName(nameMarca, idEmpresa);
                if (Marca == null)
                    return NotFound(MensajesRespuesta.sinResultados());

                MarcaCompleteDTO objRepositorio = _mapper.Map<MarcaCompleteDTO>(Marca);
                return Ok(objRepositorio);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get by Nombre Exacto
        [HttpGet("GetMarcaByNameExact")]
        public async Task<ActionResult<MarcaCompleteDTO>> GetMarcaByNameExact(string nameMarca, Guid idEmpresa)
        {
            try
            {
                Marca Marca = await _marcaRepository.GetMarcaByNameExact(nameMarca, idEmpresa);
                if (Marca == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                MarcaCompleteDTO objRepositorio = _mapper.Map<MarcaCompleteDTO>(Marca);
                return Ok(objRepositorio);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get by Code
        [HttpGet("GetMarcaByCode")]
        public async Task<ActionResult<MarcaCompleteDTO>> GetMarcaByCode(string codigo, Guid idEmpresa)
        {
            try
            {
                Marca Marca = await _marcaRepository.GetMarcaByNameExact(codigo, idEmpresa);
                if (Marca == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                MarcaCompleteDTO objRepositorio = _mapper.Map<MarcaCompleteDTO>(Marca);
                return Ok(objRepositorio);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion


        #region Get by GetBusquedaAvanzada
        [HttpGet("GetBusquedaAvanzada")]
        public async Task<ActionResult<MarcaCompleteDTO>> GetBusquedaAvanzada(MarcaBusquedaDTO objBusqueda)
        {
            try
            {
                var listaDTO = await _marcaRepository.BusquedaAvanzadaMarca(objBusqueda);
                if (listaDTO == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                List<MarcaCompleteDTO> objRepositorio = _mapper.Map<List<MarcaCompleteDTO>>(listaDTO);

                return Ok(objRepositorio);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #endregion

        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }
}
