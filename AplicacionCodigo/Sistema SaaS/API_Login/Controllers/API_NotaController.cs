﻿//using AutoMapper;
//using DTOs.Notas;
//using DomainDB.Entities.Entities;
//using DomainDB.Entities.InterfacesManageData;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Utilitarios;

//namespace API_Login.Controllers
//{
//    [Route("api/Nota")]
//    [ApiController]
//    public class NotaAPIController :ControllerBase
//    {
//        private readonly IManageNota _notaRepository;
//        private readonly IMapper _mapper;

//        public NotaAPIController(IManageNota notaRepository, IMapper mapper)
//        {
//            _notaRepository = notaRepository ?? throw new ArgumentNullException(nameof(notaRepository));
//            this._mapper = mapper;
//        }
//        #region CRUD
//        #region Create
//        [HttpPost("Create")]
//        [HttpHead]
//        public async Task<IActionResult> Create([FromBody] NotasCreateDTO objNotaDTO)
//        {
//            try
//            {
//                if (objNotaDTO == null)
//                {
//                    return BadRequest();
//                }
//                Nota objNota = _mapper.Map<Nota>(objNotaDTO);
//                _notaRepository.AddNota(objNota);
//                var result = await _notaRepository.SaveNota();
//                if (result)
//                {
//                    NotasCompleteDTO objNotaResult = _mapper.Map<NotasCompleteDTO>(objNota);
//                    return CreatedAtRoute("GetNotaById", new { idNota = objNota.IdNotas }, objNotaResult);
//                }
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Edit
//        [HttpPut("Edit")]
//        public async Task<IActionResult> Edit(Guid idNota, NotasCreateDTO objNotaDTO)
//        {
//            try
//            {
//                if (objNotaDTO == null)
//                {
//                    return BadRequest();
//                }
//                Nota objNotaRepository = await _notaRepository.GetNotaById(idNota);
//                _mapper.Map(objNotaDTO, objNotaRepository);
//                objNotaRepository.IdNotas = idNota;
//                _notaRepository.EditNota(objNotaRepository);
//                var result = await _notaRepository.SaveNota();
//                //Se comprueba que se actualizó correctamente
//                if (result)
//                {
//                    return NoContent();
//                }
//                return BadRequest();
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Delete
//        [HttpDelete("{DeleteNota}")]
//        public async Task<IActionResult> DeleteNota(Guid idNota)
//        {
//            Nota objNotaRepository = await _notaRepository.GetNotaById(idNota);
//            if (objNotaRepository == null)
//            {
//                return NotFound(MensajesRespuesta.sinResultados());
//            }
//            _notaRepository.DeleteNota(objNotaRepository);
//            var result = await _notaRepository.SaveNota();
//            //Se comprueba que se actualizó correctamente
//            if (result)
//            {
//                return NoContent();
//            }
//            return BadRequest();
//        }
//        #endregion
//        #endregion

//        #region Search
//        #region Get Nota By Id
//        [HttpGet("{idNota}", Name = "GetNotaById")]
//        [HttpHead]
//        public async Task<ActionResult<NotasCompleteDTO>> GetNotaById(Guid idNota)
//        {
//            try
//            {
//                Nota nota = await _notaRepository.GetNotaById(idNota);
//                if (nota == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                NotasCompleteDTO objNota = _mapper.Map<NotasCompleteDTO>(nota);
//                return Ok(objNota);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #region Get all
//        [HttpGet("GetAllNotas")]
//        [HttpHead]
//        public async Task<ActionResult<List<NotasCompleteDTO>>> GetAllNotas()
//        {
//            try
//            {
//                List<Nota> listNotas = await _notaRepository.GetAllNotas();
//                if (listNotas.Count() < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<NotasCompleteDTO> listNotaDTO = _mapper.Map<List<NotasCompleteDTO>>(listNotas);
//                return Ok(listNotaDTO);
//            }
//            catch (Exception ex)
//            {

//            }

//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }

//        #endregion
//        #region Get by Titulo
//        [HttpGet("GetNotaByTitulo")]
//        [HttpHead]
//        public async Task<ActionResult<NotasCompleteDTO>> GetNotaByTitulo(string Titulo)
//        {
//            try
//            {
//                Nota nota = await _notaRepository.GetNotaByTitulo(Titulo);
//                if (nota == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                NotasCompleteDTO objNota = _mapper.Map<NotasCompleteDTO>(nota);
//                return Ok(objNota);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #endregion


//    }
//}
