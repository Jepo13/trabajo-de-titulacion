﻿using AutoMapper;
using DTOs.GrupoProducto;
using DomainDB.Entities.InterfacesManageData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;
using EntidadesProductos.Entities;
using RepositorioProductos.Interface;
using Newtonsoft.Json;
using ComerciosEcuadorAPI.Model;
using RepositorioLogExcepcion.Interface;
using DTOs.UnidadEntrega;

namespace API_Login.Controllers
{
    [Route("api/GrupoProducto")]
    [ApiController]
    public class GrupoProductoAPIControllercs : ControllerBase
    {
        private readonly IManageGrupoProducto _grupoProductoRepository;
        private readonly IManageCRUDProductos<GrupoProducto> _CRUDRepositoryProducto;
        private readonly IMapper _mapper;
        private readonly IManageLogError _logError;

        public GrupoProductoAPIControllercs(IManageGrupoProducto grupoProductoRepository, IMapper mapper, IManageCRUDProductos<GrupoProducto> cRUDRepositoryProducto, IManageLogError logError)
        {
            _grupoProductoRepository = grupoProductoRepository ?? throw new ArgumentException(nameof(grupoProductoRepository));
            this._mapper = mapper;
            _CRUDRepositoryProducto = cRUDRepositoryProducto;
            _logError = logError;
        }

        #region CRUD

        #region Create
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] GrupoProductoCreateDTO objGrupoProductoDTO)
        {
            try
            {
                if (objGrupoProductoDTO == null)
                {
                    return BadRequest();
                }
                GrupoProducto objGrupoProducto = _mapper.Map<GrupoProducto>(objGrupoProductoDTO);
                _CRUDRepositoryProducto.Add(objGrupoProducto);

                var result = await _CRUDRepositoryProducto.save();
                if (result.estado)
                {
                    GrupoProductoCompleteDTO objGrupoProductoResult = _mapper.Map<GrupoProductoCompleteDTO>(objGrupoProducto);
                    return CreatedAtRoute("GetGrupoProductoById", new { idGrupoProducto = objGrupoProducto.IdGrupoProducto }, objGrupoProductoResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objGrupoProductoDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Edit
        [HttpPost("Edit")]
        public async Task<IActionResult> Edit(Guid idGrupoProducto, GrupoProductoUpdateDTO objGrupoProductoDTO)
        {
            try
            {
                if (objGrupoProductoDTO == null)
                {
                    return BadRequest();
                }
                GrupoProducto objGrupoProductoRepository = await _grupoProductoRepository.GetGrupoProductoById(idGrupoProducto);
                _mapper.Map(objGrupoProductoDTO, objGrupoProductoRepository);

                _CRUDRepositoryProducto.Edit(objGrupoProductoRepository);
                var result = await _CRUDRepositoryProducto.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objGrupoProductoDTO), result.mensajeError);
                }

                return BadRequest();
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Delete
        [HttpPost("{DeleteGrupoProducto}")]
        public async Task<IActionResult> DeleteGrupoProducto(Guid idGrupoProducto)
        {
            GrupoProducto objGrupoProductoRepository = await _grupoProductoRepository.GetGrupoProductoById(idGrupoProducto);
            if (objGrupoProductoRepository == null)
            {
                return NotFound(MensajesRespuesta.sinResultados());
            }
            _CRUDRepositoryProducto.Delete(objGrupoProductoRepository);
            var result = await _CRUDRepositoryProducto.save();
            //Se comprueba que se actualizó correctamente
            if (result.estado)
            {
                return NoContent();
            }
            else
            {
                await guardarLogs(idGrupoProducto.ToString(), result.mensajeError);
            }
            return BadRequest();
        }
        #endregion

        #endregion

        #region Search

        #region Get GrupoProducto by Id
        [HttpGet("{idGrupoProducto}", Name = "GetGrupoProductoById")]
        public async Task<ActionResult<GrupoProductoCompleteDTO>> GetGrupoProductoById(Guid idGrupoProducto)
        {
            try
            {
                GrupoProducto grupoProducto = await _grupoProductoRepository.GetGrupoProductoById(idGrupoProducto);
                if (grupoProducto == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                GrupoProductoCompleteDTO objGrupoProducto = _mapper.Map<GrupoProductoCompleteDTO>(grupoProducto);
                return Ok(objGrupoProducto);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion


        #region Get all
        [HttpGet("GetAllGrupoProducto")]
        public async Task<ActionResult<List<GrupoProductoCompleteDTO>>> GetAllGrupoProducto(Guid idEmpresa)
        {
            try
            {
                List<GrupoProducto> listGrupoProducto = await _grupoProductoRepository.GetAllGrupoProducto(idEmpresa);

                if (listGrupoProducto.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<GrupoProductoCompleteDTO> listBodegaDTO = _mapper.Map<List<GrupoProductoCompleteDTO>>(listGrupoProducto);
                return Ok(listBodegaDTO);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        #endregion

        #region Get By name
        [HttpGet("GetGrupoProductoByName")]
        public async Task<ActionResult<GrupoProductoCompleteDTO>> GetGrupoProductoByName(string name, Guid idEmpresa)
        {
            try
            {
                GrupoProducto grupoproducto = await _grupoProductoRepository.GetGrupoProductoByName(name, idEmpresa);
                if (grupoproducto == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                GrupoProductoCompleteDTO objGrupoProducto = _mapper.Map<GrupoProductoCompleteDTO>(grupoproducto);
                return Ok(objGrupoProducto);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion


        #region Get by GetBusquedaAvanzada
        [HttpGet("GetBusquedaAvanzada")]
        public async Task<ActionResult<UnidadEntregaCompleteDTO>> GetBusquedaAvanzada(GrupoProductoBusquedaDTO objBusqueda)
        {
            try
            {
                var listaDTO = await _grupoProductoRepository.GetUBusquedaAvanzada(objBusqueda);
                if (listaDTO == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                List<GrupoProductoCompleteDTO> objUnidadEntrega = _mapper.Map<List<GrupoProductoCompleteDTO>>(listaDTO);

                return Ok(objUnidadEntrega);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion


        #endregion

        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }
}
