﻿using AutoMapper;
using ComerciosEcuadorAPI.Model;
using DTOs.Proveedor;

using DomainDB.Entities.InterfacesManageData;
using EntidadesProveedores.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RepositorioLogExcepcion.Interface;
using RepositorioProveedores.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas.Proveedor;
using RepositorioCatalogoNotas.Interface;
using EntidadesCatalogosNotas.Entities;

namespace API_Login.Controllers
{
    [Route("api/Proveedor")]
    [ApiController]
    public class API_ProveedorController : ControllerBase
    {
        private readonly IManageCatalogos _catalogoRepository;
        private readonly IManageProveedor _proveedorRepository;
        private readonly IMapper _mapper;
        private readonly IManageCRUDProveedor<Proveedor> _CRUDRepositoryProvedor;
        private readonly IManageLogError _logError;
        public API_ProveedorController(IManageProveedor proveedorRepository, IMapper mapper, IManageCRUDProveedor<Proveedor> cRUDRepository, IManageLogError logError, IManageCatalogos catalogoRepository)
        {
            _proveedorRepository = proveedorRepository ?? throw new ArgumentException(nameof(proveedorRepository));
            this._mapper = mapper;
            _CRUDRepositoryProvedor = cRUDRepository;
            _logError = logError;
            _catalogoRepository = catalogoRepository;
        }
        // crud
        #region CRUD
        #region Create
        [HttpPost("Create")]        
        public async Task<IActionResult> Create([FromBody] ProveedorCreateDTO objProvedorDTO)
        {
            try
            {
                if (objProvedorDTO == null)
                {
                    return BadRequest();
                }
                Proveedor objProveedor = _mapper.Map<Proveedor>(objProvedorDTO);
                _CRUDRepositoryProvedor.Add(objProveedor);
                var result = await _CRUDRepositoryProvedor.save();
                if (result.estado)
                {
                    ProveedorCompleteDTO objProvedorResult = _mapper.Map<ProveedorCompleteDTO>(objProveedor);
                    return CreatedAtRoute("GetProveedorById", new { idProveedor = objProveedor.IdProveedor }, objProvedorResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objProvedorDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objProvedorDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #region Edit
        [HttpPost("Edit")]
        public async Task<IActionResult> Edit(Guid idProveedor, ProveedorUpdateDTO objProveedorDTO)
        {
            try
            {
                if (objProveedorDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }
                Proveedor objProveedorRepository = await _proveedorRepository.GetProveedorById(idProveedor);
                _mapper.Map(objProveedorDTO, objProveedorRepository);
                objProveedorRepository.IdProveedor = idProveedor;
                _CRUDRepositoryProvedor.Edit(objProveedorRepository);
                var result = await _CRUDRepositoryProvedor.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objProveedorDTO), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objProveedorDTO), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #region Delete
        [HttpDelete("{DeleteProveedor}")]
        public async Task<IActionResult> DeleteProveedor(Guid idProveedor)
        {
            
            try
            {
                Proveedor objProveedorRepository = await _proveedorRepository.GetProveedorById(idProveedor);


                _CRUDRepositoryProvedor.Delete(objProveedorRepository);
                var result = await _CRUDRepositoryProvedor.save();
                // se comprueba que se actualizo correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objProveedorRepository), result.mensajeError);
                }

                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(idProveedor.ToString(), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #endregion

        #region Search
        #region Get Proveedor By Id
        [HttpGet("{idProveedor}", Name = "GetProveedorById")]
       
        public async Task<ActionResult<ProveedorCompleteDTO>> GetProveedorById(Guid idProveedor)
        {
            try
            {
                Proveedor proveedor = await _proveedorRepository.GetProveedorById(idProveedor);
                if (proveedor == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                ProveedorCompleteDTO objProveedorDTO = _mapper.Map<ProveedorCompleteDTO>(proveedor);
                return Ok(objProveedorDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
        #region Get all
        [HttpGet("GetAllProveedor")]
        [HttpHead]
        public async Task<ActionResult<List<ProveedorCompleteDTO>>> GetAllProveedor()
        {
            try
            {
                List<Proveedor> listProveedor = await _proveedorRepository.GetAllProveedor();
                if (listProveedor.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<ProveedorCompleteDTO> listProveedorDTO = _mapper.Map<List<ProveedorCompleteDTO>>(listProveedor);
                return Ok(listProveedorDTO);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        #endregion
        #region Get by name
        [HttpGet("GetProveedorByName")]
        [HttpHead]
        public async Task<ActionResult<ProveedorCompleteDTO>> GetProveedorByName(string nameProveedor)
        {
            try
            {
                Proveedor proveedor = await _proveedorRepository.GetProveedorByName(nameProveedor);
                if (proveedor == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                ProveedorCompleteDTO objProveedor = _mapper.Map<ProveedorCompleteDTO>(proveedor);
                return Ok(objProveedor);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
        #region Get by Ruc
        [HttpGet("GetProveedorByRUC")]
        [HttpHead]
        public async Task<ActionResult<ProveedorCompleteDTO>> GetProveedorByRUC(string rucProveedor, Guid idEmpresa)
        {
            try
            {
                Proveedor proveedor = await _proveedorRepository.GetProveedorByRuc(rucProveedor, idEmpresa);
                if (proveedor == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                ProveedorCompleteDTO objProveedor = _mapper.Map<ProveedorCompleteDTO>(proveedor);
                return Ok(objProveedor);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get By Advanced
        [HttpGet("GetProveedoresAdvanced")]
        [HttpHead]
        public async Task<ActionResult<List<ProveedorResultadoBusquedaDTO>>> GetProveedorAdvanced(ObjetoBusquedaProveedor objBusqueda)
        {
            try
            {
                List<Proveedor> listProveedores = await _proveedorRepository.GetProveedorAdvanced(objBusqueda);

                if (listProveedores.Count < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                List<ProveedorResultadoBusquedaDTO> listProveedoresDTO = _mapper.Map<List<ProveedorResultadoBusquedaDTO>>(listProveedores);
                foreach (var itemProveedor  in listProveedoresDTO)
                {
                    if (itemProveedor.IdEspecialidad != null) 
                    {
                        Catalogo catalogo = await _catalogoRepository.GetCatalogoById((Guid)itemProveedor.IdEspecialidad);
                        itemProveedor.Especialidad = catalogo.NombreCatalogo;
                    }
                    else 
                    {
                        itemProveedor.Especialidad = "!Sin Catalogar¡";
                    }
                    
                }
                return Ok(listProveedoresDTO);
            }
            catch (Exception ex)
            {
            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
        // falta hacer busdquera por razon social 
        #endregion
        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }
}
