﻿using AutoMapper;
using ComerciosEcuadorAPI.Model;
using DTOs.CategoriaProductos;

using DomainDB.Entities.InterfacesManageData;
using EntidadesProductos.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RepositorioLogExcepcion.Interface;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;
using DTOs.Catalogo;
using EntidadesCatalogosNotas.Entities;
using EntidadesEmpresa.Entities;
using RepositorioEmpresa.Interface;

namespace API_Login.Controllers
{
    [Route("api/CategoriaProducto")]
    [ApiController]
    public class API_CategoriaProductoController : ControllerBase
    {
        private readonly IManageCategoria _categoriaRepository;
        private readonly IManageEmpresa _empresaRepository;
        private readonly IManageCRUDProductos<CategoriaProducto> _CRUDRepository;

        private readonly IMapper _mapper;
        private readonly IManageLogError _logError;
        public API_CategoriaProductoController(IManageCategoria categoriaRepository, IMapper mapper, IManageCRUDProductos<CategoriaProducto> cRUDRepository, IManageLogError logError, IManageEmpresa empresaRepository)
        {
            _categoriaRepository = categoriaRepository ?? throw new ArgumentException(nameof(categoriaRepository));
            this._mapper = mapper;
            _CRUDRepository = cRUDRepository;
            _logError = logError;
            _empresaRepository = empresaRepository;
        }

        #region CRUD

        #region Create
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] CategoriaProductoDTOCrear objCategoriaDTO)
        {
            try
            {
                if (objCategoriaDTO == null)
                {
                    return BadRequest();
                }
                CategoriaProducto objCategoria = _mapper.Map<CategoriaProducto>(objCategoriaDTO);
                _CRUDRepository.Add(objCategoria);
                var result = await _CRUDRepository.save();
                if (result.estado)
                {
                    CategoriaProductoDTOCompleto objCategoriaResult = _mapper.Map<CategoriaProductoDTOCompleto>(objCategoria);
                    return CreatedAtRoute("GetCategoriaById", new { idCategoriaProducto = objCategoria.IdCategoriaProducto }, objCategoriaResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCategoriaDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objCategoriaDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Edit
        [HttpPost("Edit")]
        public async Task<IActionResult> Edit(Guid idCategoriaProducto, CategoriaProductoDTOActualizar objCategoriaDTO)
        {
            try
            {
                if (objCategoriaDTO == null)
                {
                    return BadRequest();
                }
                CategoriaProducto objCategoriaRepository = await _categoriaRepository.GetCategoriaById(idCategoriaProducto);
                _mapper.Map(objCategoriaDTO, objCategoriaRepository);
              
                _CRUDRepository.Edit(objCategoriaRepository);
                var result = await _CRUDRepository.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCategoriaDTO), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception exException)
            {
                await guardarLogs(JsonConvert.SerializeObject(objCategoriaDTO), exException.ToString());
            }

            
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Delete
        [HttpPost("{DeleteCategoria}")]
        public async Task<IActionResult> DeleteCategoria(Guid idCategoriaProducto)
        {
            try
            {
                CategoriaProducto objCategoriaRepository = await _categoriaRepository.GetCategoriaById(idCategoriaProducto);
                _CRUDRepository.Delete(objCategoriaRepository);
                var result = await _CRUDRepository.save();
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCategoriaRepository), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(idCategoriaProducto.ToString(), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #endregion

        #region Search

        #region Get By Id
        [HttpGet("{idCategoriaProducto}", Name = "GetCategoriaById")]
        [HttpHead]
        public async Task<ActionResult<CategoriaProductoDTOCompleto>> GetCategoriaById(Guid idCategoriaProducto)
        {
            try
            {
                CategoriaProducto categorium = await _categoriaRepository.GetCategoriaById(idCategoriaProducto);
                if (categorium == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                CategoriaProductoDTOCompleto objCategoria = _mapper.Map<CategoriaProductoDTOCompleto>(categorium);
                return Ok(objCategoria);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get all
        [HttpGet("GetAllCategoria")]
        [HttpHead]
        public async Task<ActionResult<List<CategoriaProductoDTOCompleto>>> GetAllCategoria()
        {
            try
            {
                List<CategoriaProducto> listCategorias = await _categoriaRepository.GetAllCategoria();
                if (listCategorias.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CategoriaProductoDTOCompleto> listBodegaDTO = _mapper.Map<List<CategoriaProductoDTOCompleto>>(listCategorias);
                return Ok(listBodegaDTO);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet("GetAllCategoriaByIDCompany")]
        public async Task<ActionResult<List<CategoriaProductoDTOCompleto>>> GetAllCategoriaByIDCompany(Guid idEmpresa)
        {
            try
            {
                List<CategoriaProducto> listCatalogo = await _categoriaRepository.GetCategoriaByIdEmpresa(idEmpresa);

                if (listCatalogo.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CategoriaProductoDTOCompleto> listCatalogoDTO = _mapper.Map<List<CategoriaProductoDTOCompleto>>(listCatalogo);
                Empresa empresaBusqueda = await _empresaRepository.GetCompanyById(idEmpresa);

                foreach (var item in listCatalogoDTO)
                {
                    item.NombreEmpresa = empresaBusqueda.RazonSocial;
                }


                //var jsonSerializerSettings = new JsonSerializerSettings
                //{
                //    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                //};

                //var json = JsonConvert.SerializeObject(listCatalogoDTO, jsonSerializerSettings);

                //listCatalogoDTO = JsonConvert.DeserializeObject<List<CategoriaProductoDTOCompleto>>(json);

                return Ok(listCatalogoDTO);
            }
            catch (Exception Ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }


        
        [HttpGet("GetHijosPorCategoria")]
        public async Task<ActionResult<List<CategoriaProductoDTOCompleto>>> GetHijosPorCategoria(Guid idCategoriaPadre)
        {
            try
            {
                List<CategoriaProducto> listCatalogo = await _categoriaRepository.GetCategoriaByIDPadre(idCategoriaPadre);

                if (listCatalogo.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CategoriaProductoDTOCompleto> listCatalogoDTO = _mapper.Map<List<CategoriaProductoDTOCompleto>>(listCatalogo);

                //var jsonSerializerSettings = new JsonSerializerSettings
                //{
                //    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                //};

                //var json = JsonConvert.SerializeObject(listCatalogoDTO, jsonSerializerSettings);

                //listCatalogoDTO = JsonConvert.DeserializeObject<List<CategoriaProductoDTOCompleto>>(json);

                return Ok(listCatalogoDTO);
            }
            catch (Exception Ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }



        #endregion


        #endregion

        #region Get by name
        [HttpGet("GetCategoriaByName")]
        [HttpHead]
        public async Task<ActionResult<CategoriaProductoDTOCompleto>> GetCategoriaByName(string nameCategoria)
        {
            try
            {
                CategoriaProducto categoria = await _categoriaRepository.GetCategoriaByName(nameCategoria);
                if (categoria == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                CategoriaProductoDTOCompleto objCategoria = _mapper.Map<CategoriaProductoDTOCompleto>(categoria);
                return Ok(objCategoria);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
        
        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }
}
