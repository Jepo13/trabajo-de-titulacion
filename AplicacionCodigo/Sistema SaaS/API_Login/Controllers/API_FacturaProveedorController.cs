﻿//using AutoMapper;
//using DTOs.FacturaProveedor;
//using DomainDB.Entities.Entities;
//using DomainDB.Entities.InterfacesManageData;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Utilitarios;

//namespace API_Login.Controllers
//{
//    [Route("api/FacturaProveedor")]
//    [ApiController]
//    public class FacturaProveedorAPIController : ControllerBase
//    {
//        private readonly IManageFacturaProveedor _FacturaProveedorRepository;
//        private readonly IMapper _mapper;
//        public FacturaProveedorAPIController(IManageFacturaProveedor FacturaProveedorRepository, IMapper mapper)
//        {
//            _FacturaProveedorRepository = FacturaProveedorRepository ?? throw new ArgumentException(nameof(FacturaProveedorRepository));
//            this._mapper = mapper;
//        }
//        #region CRUD
//        #region Create
//        [HttpPost("Create")]
//        [HttpHead]
//        public async Task<IActionResult> Create([FromBody] FacturaProveedorCreateDTO objFacturaProvedorDTO)
//        {
//            try
//            {
//                if (objFacturaProvedorDTO == null)
//                {
//                    return BadRequest();
//                }
//                Facturaproveedor objFacturaProveedor = _mapper.Map<Facturaproveedor>(objFacturaProvedorDTO);
//                _FacturaProveedorRepository.AddFacturaProveedor(objFacturaProveedor);
//                var result = await _FacturaProveedorRepository.SaveFacturaProveedor();
//                if (result)
//                {
//                    FacturaProveedorCompleteDTO objFacturaProvedorResult = _mapper.Map<FacturaProveedorCompleteDTO>(objFacturaProveedor);
//                    return CreatedAtRoute("GetFacturaProveedorById", new { idFacturaProveedor = objFacturaProveedor.IdFacturaproveedor }, objFacturaProvedorResult);
//                }
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion

//        #region Edit
//        [HttpPut("Edit")]
//        public async Task<IActionResult> Edit(Guid idFacturaProveedor, FacturaProveedorCreateDTO objFacturaProveedorDTO)
//        {
//            try
//            {
//                if (objFacturaProveedorDTO == null)
//                {
//                    return BadRequest();
//                }
//                Facturaproveedor objFacturaProveedorRepository = await _FacturaProveedorRepository.GetFacturaProveedorById(idFacturaProveedor);
//                _mapper.Map(objFacturaProveedorDTO, objFacturaProveedorRepository);
//                objFacturaProveedorRepository.IdFacturaproveedor = idFacturaProveedor;
//                _FacturaProveedorRepository.EditFacturaProveedor(objFacturaProveedorRepository);
//                var result = await _FacturaProveedorRepository.SaveFacturaProveedor();
//                //Se comprueba que se actualizó correctamente
//                if (result)
//                {
//                    return NoContent();
//                }
//                return BadRequest();
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion

//        #region Delete
//        [HttpDelete("{DeleteFacturaProveedor}")]
//        public async Task<IActionResult> DeleteFacturaProveedor(Guid idFacturaProveedor)
//        {
//            Facturaproveedor objFacturaProveedorRepository = await _FacturaProveedorRepository.GetFacturaProveedorById(idFacturaProveedor);
//            if (objFacturaProveedorRepository == null)
//            {
//                return NotFound(MensajesRespuesta.sinResultados());
//            }
//            _FacturaProveedorRepository.DeleteFacturaProveedor(objFacturaProveedorRepository);
//            var result = await _FacturaProveedorRepository.SaveFacturaProveedor();
//            //Se comprueba que se actualizó correctamente
//            if (result)
//            {
//                return NoContent();
//            }
//            return BadRequest();
//        }
//        #endregion

//        #endregion

//        #region Search

//        #region Get FacturaProveedor By Id
//        [HttpGet("{idFacturaProveedor}", Name = "GetFacturaProveedorById")]
//        [HttpHead]
//        public async Task<ActionResult<FacturaProveedorCompleteDTO>> GetFacturaProveedorById(Guid idFacturaProveedor)
//        {
//            try
//            {
//                Facturaproveedor FacturaProveedor = await _FacturaProveedorRepository.GetFacturaProveedorById(idFacturaProveedor);
//                if (FacturaProveedor == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                FacturaProveedorCompleteDTO objFacturaProveedorDTO = _mapper.Map<FacturaProveedorCompleteDTO>(FacturaProveedor);
//                return Ok(objFacturaProveedorDTO);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion

//        #region Get all
//        [HttpGet("GetAllFacturaProveedor")]
//        [HttpHead]
//        public async Task<ActionResult<List<FacturaProveedorCompleteDTO>>> GetAllFacturaProveedor()
//        {
//            try
//            {
//                List<Facturaproveedor> listFacturaProveedor = await _FacturaProveedorRepository.GetAllFacturaProveedor();
//                if (listFacturaProveedor.Count < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<FacturaProveedorCompleteDTO> listFacturaProveedorDTO = _mapper.Map<List<FacturaProveedorCompleteDTO>>(listFacturaProveedor);
//                return Ok(listFacturaProveedorDTO);
//            }
//            catch (Exception ex)
//            {

//            }

//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }

//        [HttpGet("GetAllFacturaProveedorByIdProveedor")]
//        [HttpHead]
//        public async Task<ActionResult<List<FacturaProveedorCompleteDTO>>> GetAllFacturaProveedorByIdProveedor(Guid idProveedor)
//        {
//            try
//            {
//                List<Facturaproveedor> listFacturaProveedor = await _FacturaProveedorRepository.GetAllFacturaProveedorByIdProveedor(idProveedor);
//                if (listFacturaProveedor.Count < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<FacturaProveedorCompleteDTO> listFacturaProveedorDTO = _mapper.Map<List<FacturaProveedorCompleteDTO>>(listFacturaProveedor);
//                return Ok(listFacturaProveedorDTO);
//            }
//            catch (Exception ex)
//            {

//            }

//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion

//        #region Get by invoice
//        [HttpGet("GetFacturaProveedorByInvoiceNumber")]
//        [HttpHead]
//        public async Task<ActionResult<FacturaProveedorCompleteDTO>> GetFacturaProveedorByInvoiceNumber(string invoiceNumber)
//        {
//            try
//            {
//                Facturaproveedor FacturaProveedor = await _FacturaProveedorRepository.GetFacturaProveedorByInvoiceNumber(invoiceNumber);
//                if (FacturaProveedor == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                FacturaProveedorCompleteDTO objFacturaProveedor = _mapper.Map<FacturaProveedorCompleteDTO>(FacturaProveedor);
//                return Ok(objFacturaProveedor);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion

//        #endregion


//    }
//}
