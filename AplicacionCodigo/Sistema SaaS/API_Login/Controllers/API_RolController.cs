﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ComerciosEcuadorAPI.Model;
using DTOs.Rol;

using DomainDB.Entities.InterfacesManageData;
using EntidadesRolesUsuarios.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RepositorioLogExcepcion.Interface;
using RepositorioRolesUsuarios.Interface;
using Utilitarios;

namespace API_Login.Controllers
{
    [Route("api/Rol")]
    [ApiController]
    public class RolAPIController : ControllerBase
    {
        private readonly IManageRol _manageRol;
        private readonly IMapper _mapper;
        private readonly IManageCRUD_Roles_Usuarios<Rol> _CRUDRepository;
        private readonly IManageLogError _logError;

        public RolAPIController(IManageRol manageRol, IMapper mapper, IManageCRUD_Roles_Usuarios<Rol> cRUDRepository, IManageLogError logError)
        {
            _manageRol = manageRol ?? throw new ArgumentException(nameof(manageRol));
            this._mapper = mapper;
            _CRUDRepository = cRUDRepository;
            _logError = logError;
        }

        #region CRUD
        #region Create
        [HttpPost("Create")]
        [HttpHead]
        public async Task<IActionResult> Create([FromBody] RolCreacionDTO objRolCreadorDTO)
        {
            try
            {
                if (objRolCreadorDTO == null)
                {
                    return BadRequest();
                }
                Rol objetoDB = _mapper.Map<Rol>(objRolCreadorDTO);
                _CRUDRepository.Add(objetoDB);
                var result = await _CRUDRepository.save();
                if (result.estado)
                {
                    RolCreacionDTO objCatalogoResult = _mapper.Map<RolCreacionDTO>(objetoDB);
                    return CreatedAtRoute("GetRolByID", new { IdRol = objetoDB.IdRol }, objCatalogoResult);
                }
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Edit
        [HttpPut("Edit")]
        public async Task<IActionResult> Edit(Guid idRol, RolEditarDTO objDTO)
        {
            try
            {
                if (objDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }
                Rol objDB = await _manageRol.GetRolByID(idRol);
                _mapper.Map(objDTO, objDB);
                _CRUDRepository.Edit(objDB);
                var result = await _CRUDRepository.save();

                
                // se comprueba que se actualizo correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objDTO), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objDTO), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);

        }
        #endregion
        
        #region Delete
        [HttpDelete("Delete")]
        public async Task<IActionResult> Delete(Guid idRol)
        {
            try
            {
               
                Rol objDB = await _manageRol.GetRolByID(idRol);

                _CRUDRepository.Delete(objDB);
                var result = await _CRUDRepository.save();
                // se comprueba que se actualizo correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objDB), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(idRol.ToString(), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);

        }
        #endregion

        #endregion

        #region Get all Rols
        [HttpGet("GetAllRolsByCompany")]       
        public async Task<ActionResult<RolBusquedaDTO>> GetAllRolsByCompany(Guid idEmpresa)
        {
            try
            {
                List<Rol> listRols = await _manageRol.GetAllRolsByCompany(idEmpresa);

                if (listRols.Count<1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                List<RolBusquedaDTO> listRolsDTO = _mapper.Map<List<RolBusquedaDTO>>(listRols);

                return Ok(listRolsDTO);
            }
            catch (Exception ex)
            {
            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }


        [HttpGet("{IdRol}", Name = "GetRolByID")]
        public async Task<IActionResult> GetRolByID(Guid IdRol)
        {
            try
            {
                Rol objDB = await _manageRol.GetRolByID(IdRol);

                if (objDB == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                RolCompletoDTO obj = _mapper.Map<RolCompletoDTO>(objDB);

                return Ok(obj);
            }
            catch (Exception ex)
            {
            }

            return BadRequest(MensajesRespuesta.errorInesperado());
        }
        #endregion

        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }//class
}
