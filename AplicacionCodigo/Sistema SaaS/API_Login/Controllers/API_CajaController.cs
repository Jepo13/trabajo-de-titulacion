﻿//using AutoMapper;
//using ComerciosEcuadorAPI.Model;
//using DTOs.Caja;
//using DomainDB.Entities.Entities;
//using DomainDB.Entities.InterfacesManageData;
//using EntidadesEmpresa.Entities;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Newtonsoft.Json;
//using RepositorioEmpresa.Interface;
//using RepositorioLogExcepcion.Interface;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Utilitarios;

//namespace API_Login.Controllers
//{
//    [Route("api/Caja")]
//    [ApiController]
//    public class CajaAPIController : ControllerBase
//    {
//        private readonly IManageCaja _CajaRepository;
//        private readonly IMapper _mapper;

//        private readonly IManageCRUDCatalogos<Caja> _CRUDRepository;
//        private readonly IManageLogError _logError;

//        public CajaAPIController(IManageCaja cajaRepository, IMapper mapper, IManageCRUDCatalogos<Caja> cRUDRepository, IManageLogError logError)
//        {
//            _CajaRepository = cajaRepository ?? throw new ArgumentException(nameof(cajaRepository));
//            this._mapper = mapper;
//            _CRUDRepository = cRUDRepository;
//            _logError = logError;
//        }
//        #region CRUD
//        #region Create
//        [HttpPost("Create")]
//        [HttpHead]
//        public async Task<IActionResult> Create ([FromBody] CajaCreateDTO objCajaDTO)
//        {
//            try
//            {
//                if(objCajaDTO == null)
//                {
//                    return BadRequest();
//                }
//                Caja objCaja = _mapper.Map<Caja>(objCajaDTO);
//                _CRUDRepository.Add(objCaja);
//                var result = await _CRUDRepository.save();
//                if (result.estado)
//                {
//                    CajaCompleteDTO objCajaResult = _mapper.Map<CajaCompleteDTO>(objCaja);
//                    return CreatedAtRoute("GetCajaById", new { idCaja = objCaja.IdCaja }, objCajaResult);
//                }
//                else
//                {
//                    await guardarLogs(JsonConvert.SerializeObject(objCajaDTO), result.mensajeError);
//                }
//            }
//            catch (Exception ex)
//            {
//                await guardarLogs(JsonConvert.SerializeObject(objCajaDTO), ex.ToString());
//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Edit 
//        [HttpPut ("Edit")]
//        public async Task<IActionResult> Edit (Guid idCaja, CajaCreateDTO objCajaDTO)
//        {
//            try
//            {
//                if(objCajaDTO == null)
//                {
//                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
//                }
//                Caja objcajaRepository = await _CajaRepository.GetCajaById(idCaja);

//                _mapper.Map(objCajaDTO, objcajaRepository);

//                //objcajaRepository.IdCaja = idCaja; segun yo es degana pero si falla aquí es  

//                _CRUDRepository.Edit(objcajaRepository);
//                var result = await _CRUDRepository.save();

//                if (result.estado)
//                {
//                    return NoContent();
//                }
//                else
//                {
//                    await guardarLogs(JsonConvert.SerializeObject(objCajaDTO), result.mensajeError);
//                }

//                return BadRequest(MensajesRespuesta.guardarError());
//            }
//            catch (Exception ExValidation)
//            {
//                await guardarLogs(JsonConvert.SerializeObject(objCajaDTO), ExValidation.ToString());
//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Delete
//        [HttpDelete("{DeleteCaja}")]
//        public async Task<IActionResult> DeleteCaja (Guid idCaja)
//        {
//            try
//            {
//                Caja objCajaRepository = await _CajaRepository.GetCajaById(idCaja);
//                _CRUDRepository.DeleteCaja(objCajaRepository);
//                var result = await _CRUDRepository.save();
//                if (result.estado)
//                {
//                    return NoContent();
//                }
//                else
//                {
//                    await guardarLogs(JsonConvert.SerializeObject(objCajaRepository),result.mensajeError);
//                }
//                return BadRequest(MensajesRespuesta.guardarError());
//            }
//            catch(Exception ExValidation)
//            {
//                await guardarLogs(idCaja.ToString(), ExValidation.ToString());
//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #endregion

//        #region Search
//        #region Get Caja By Id
//        [HttpGet("{idCaja}", Name = "GetCajaById")]
//        [HttpHead]
//        public async Task<ActionResult<CajaCompleteDTO>> GetCajaById (Guid idCaja)
//        {
//            try
//            {
//                Caja caja = await _CajaRepository.GetCajaById(idCaja);
//                if(caja == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                CajaCompleteDTO objCaja = _mapper.Map<CajaCompleteDTO>(caja);
//                return Ok(objCaja);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #region Get All
//        [HttpGet("GetAllCajas")]
//        [HttpHead]
//        public async Task<ActionResult<List<CajaCompleteDTO>>> GetAllCajas()
//        {
//            try
//            {
//                List<Caja> listCajas = await _CajaRepository.GetAllCajas();
//                if (listCajas.Count() < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<CajaCompleteDTO> listCajaDTO = _mapper.Map<List<CajaCompleteDTO>>(listCajas);
//                return Ok(listCajaDTO);
//            }
//            catch(Exception Ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #region Get By Name
//        [HttpGet ("GetCajaByName")]
//        [HttpHead]
//        public async Task<ActionResult<CajaCompleteDTO>> GetCajaByName(string nameCaja)
//        {
//            try
//            {
//                Caja caja = await _CajaRepository.GetCajaByName(nameCaja);
//                if(caja == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                CajaCompleteDTO objCajaDTO = _mapper.Map<CajaCompleteDTO>(caja);
//                return Ok(objCajaDTO);
//            }
//            catch(Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #region Get All by Local
//        [HttpGet("GetAllCajaByIdLocal")]
//        [HttpHead]
//        public async Task<ActionResult<List<CajaCompleteDTO>>> GetAllCajaByIdLocal(Guid idLocal)
//        {
//            try
//            {
//                List<Caja> listaCajaByIdLocal = await _CajaRepository.GetAllCajaByIdLocal(idLocal);
//                if (listaCajaByIdLocal.Count() < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<CajaCompleteDTO> listaCajaDTO = _mapper.Map<List<CajaCompleteDTO>>(listaCajaByIdLocal);
//                return Ok(listaCajaDTO);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #endregion

//        #region Varios
//        private async Task guardarLogs(string objetoJSON, string mensajeError)
//        {
//            LoggerAPI objLooger = new LoggerAPI(_logError);

//            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

//        }
//        #endregion
//    }
//}
