﻿//using AutoMapper;
//using ComerciosEcuadorAPI.Model;
//using DTOs.PorcentajeIVA;
//using DomainDB.Entities.Entities;
//using DomainDB.Entities.InterfacesManageData;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Utilitarios;

//namespace API_Login.Controllers
//{
//    [Route("api/PorcentajeIVA")]
//    [ApiController]
//    public class PorcentajeIvaAPIController : ControllerBase
//    {
//        private readonly IManagePorcentajeIVA _proveedorRepository;
//        private readonly IMapper _mapper;
//        private readonly IManageCRUDCatalogos<Porcentajeiva> _CRUDRepository;
//        private readonly IManageLogError _logError;

//        public PorcentajeIvaAPIController(IManagePorcentajeIVA porcentajeIVARepository, IMapper mapper, IManageCRUDCatalogos<Porcentajeiva> cRUDRepository, IManageLogError logError = null)
//        {
//            _proveedorRepository = porcentajeIVARepository ?? throw new ArgumentException(nameof(porcentajeIVARepository));
//            this._mapper = mapper;
//            _CRUDRepository = cRUDRepository;
//            _logError = logError;
//        }

//        #region CRUD
//        #region Create
//        [HttpPost("Create")]
//        [HttpHead]
//        public async Task<IActionResult> Create([FromBody] PorcentajeIVACreateDTO objPorcentajeIVADTO)
//        {
//            try
//            {
//                if (objPorcentajeIVADTO == null)
//                {
//                    return BadRequest();
//                }
//                Porcentajeiva objPorcentajeIVA = _mapper.Map<Porcentajeiva>(objPorcentajeIVADTO);
//                _CRUDRepository.Add(objPorcentajeIVA);
//                var result = await _CRUDRepository.save();
//                if (result.estado)
//                {
//                    PorcentajeIVACompleteDTO objPorcentajeIVAResult = _mapper.Map<PorcentajeIVACompleteDTO>(objPorcentajeIVA);
//                    return CreatedAtRoute("GetPorcentajeIVAById", new { idPorcentajeIVA = objPorcentajeIVAResult.IdPorcenajeiva }, objPorcentajeIVAResult);
//                }
//                else
//                {
//                    await guardarLogs(JsonConvert.SerializeObject(objPorcentajeIVADTO), result.mensajeError);
//                }

//            }
//            catch (Exception ex)
//            {
//                await guardarLogs(JsonConvert.SerializeObject(objPorcentajeIVADTO), ex.ToString());
//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Edit 
//        [HttpPut("Edit")]
//        public async Task<IActionResult> Edit(Guid idPorcentajeIVA, PorcentajeIVACreateDTO objPorcentajeIVADTO)
//        {
//            try
//            {
//                if (objPorcentajeIVADTO == null)
//                {
//                    return BadRequest();
//                }
//                Porcentajeiva porcentajeIVARepository = await _proveedorRepository.GetPorcentajeIVAById(idPorcentajeIVA);
//                _mapper.Map(objPorcentajeIVADTO, porcentajeIVARepository);
//                porcentajeIVARepository.IdPorcenajeiva = idPorcentajeIVA;
//                _CRUDRepository.Edit(porcentajeIVARepository);
//                var result = await _CRUDRepository.save();

//                if (result.estado)
//                {
//                    return NoContent();
//                }
//                else
//                {
//                    await guardarLogs(JsonConvert.SerializeObject(objPorcentajeIVADTO), result.mensajeError);
//                }
//                return BadRequest(MensajesRespuesta.guardarError());
//            }
//            catch (Exception ExValidation)
//            {
//                await guardarLogs(JsonConvert.SerializeObject(objPorcentajeIVADTO), ExValidation.ToString());
//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);
//        }
//        #endregion
//        #region Delete
//        [HttpDelete("{Delete}")]
//        public async Task<IActionResult> Delete(Guid idPorcentajeIVA)
//        {
//            try 
//            {
//                Porcentajeiva objPorcentajeIVARepository = await _proveedorRepository.GetPorcentajeIVAById(idPorcentajeIVA);
//                _CRUDRepository.Delete(objPorcentajeIVARepository);

//                var result = await _CRUDRepository.save();
//                if (result.estado)
//                {
//                    return NoContent();
//                }
//                else
//                {
//                    await guardarLogs(JsonConvert.SerializeObject(objPorcentajeIVARepository), result.mensajeError);
//                }
//                return BadRequest(MensajesRespuesta.guardarError());
//            }
//            catch (Exception ExValidation)
//            {
//                await guardarLogs(idPorcentajeIVA.ToString(), ExValidation.ToString());
//            }
//            return StatusCode(StatusCodes.Status406NotAcceptable);



//        }
//        #endregion

//        #endregion

//        #region Search
//        #region Get By Id
//        [HttpGet("{idPorcentajeIVA}", Name = "GetPorcentajeIVAById")]
//        [HttpHead]
//        public async Task<ActionResult<PorcentajeIVACompleteDTO>> GetPorcentajeIVAById(Guid idPorcentajeIVA)
//        {
//            try
//            {
//                Porcentajeiva porcentajeIVA = await _proveedorRepository.GetPorcentajeIVAById(idPorcentajeIVA);
//                if (porcentajeIVA == null)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                PorcentajeIVACompleteDTO objPorcentajeIVA = _mapper.Map<PorcentajeIVACompleteDTO>(porcentajeIVA);
//                return Ok(objPorcentajeIVA);
//            }
//            catch (Exception ex)
//            {

//            }
//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }
//        #endregion
//        #region Get all
//        [HttpGet("GetAll")]
//        [HttpHead]
//        public async Task<ActionResult<List<PorcentajeIVACompleteDTO>>> GetAll()
//        {
//            try
//            {
//                List<Porcentajeiva> listPorcentajeIVA = await _proveedorRepository.GetAllPorcentajeIVA();
//                if (listPorcentajeIVA.Count() < 1)
//                {
//                    return NotFound(MensajesRespuesta.sinResultados());
//                }
//                List<PorcentajeIVACompleteDTO> listPorcentajeIVADTO = _mapper.Map<List<PorcentajeIVACompleteDTO>>(listPorcentajeIVA);
//                return Ok(listPorcentajeIVADTO);
//            }
//            catch (Exception ex)
//            {

//            }

//            return StatusCode(StatusCodes.Status500InternalServerError);
//        }

//        #endregion
//        #endregion
//        #region Varios
//        private async Task guardarLogs(string objetoJSON, string mensajeError)
//        {
//            LoggerAPI objLooger = new LoggerAPI(_logError);

//            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

//        }
//        #endregion
//    }
//}
