﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DomainDB.Entities.InterfacesManageData;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DTOs.Empresa;
using Microsoft.AspNetCore.JsonPatch;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Empresa;
using Newtonsoft.Json;
using ComerciosEcuadorAPI.Model;
using RepositorioEmpresa.Interface;
using RepositorioLogExcepcion.Interface;
using EntidadesEmpresa.Entities;

namespace API_Login.Controllers
{
    [Route("api/Empresa")]
    [ApiController]
    public class API_EmpresaController : ControllerBase
    {
        private readonly IManageEmpresa _empresaRepository;
        private readonly IMapper _mapper;
        private readonly IManageCRUDEmpresa<Empresa> _CRUDRepository;
        private readonly IManageLogError _logError;

        public API_EmpresaController(IManageEmpresa empresaRepository, IMapper mapper, IManageCRUDEmpresa<Empresa> cRUDRepository, IManageLogError logError)
        {
            _empresaRepository = empresaRepository ?? throw new ArgumentNullException(nameof(empresaRepository));
            this._mapper = mapper;
            _CRUDRepository = cRUDRepository;
            _logError = logError;
        }
        #region CRUD

        #region Create
        [HttpPost("Create")]
        [HttpHead]
        public async Task<IActionResult> Create([FromBody] EmpresaCreateDTO objCompanyDTO)
        {
            try
            {
                if (objCompanyDTO == null)
                {
                    return BadRequest();
                }

                Empresa objCompany = _mapper.Map<Empresa>(objCompanyDTO);

                _CRUDRepository.Add(objCompany);
                var result = await _CRUDRepository.save();

                if (result.estado)
                {
                    EmpresaCompletoDTO objCompanyResult = _mapper.Map<EmpresaCompletoDTO>(objCompany);

                    return CreatedAtRoute("GetCompanyById", new { idCompany = objCompany.IdEmpresa }, objCompanyResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCompanyDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objCompanyDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }// END crear empresa
        #endregion

        #region Edit
        [HttpPut("Edit")]
        public async Task<IActionResult> Edit(Guid idCompany, EmpresaUpdateDTO objectCompanyDTO)
        {
            try
            {
                if (objectCompanyDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }

                Empresa objCompanyRepository = await _empresaRepository.GetCompanyById(idCompany);
                _mapper.Map(objectCompanyDTO, objCompanyRepository);

                _CRUDRepository.Edit(objCompanyRepository);
                var result = await _CRUDRepository.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objectCompanyDTO), result.mensajeError);
                }

                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objectCompanyDTO), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Delete
        [HttpDelete("{DeleteCompany}")]

        public async Task<IActionResult> DeleteCompany(Guid idCompany)
        {
            try
            {
                 Empresa objCompanyRepository = await _empresaRepository.GetCompanyById(idCompany);
                _CRUDRepository.Delete(objCompanyRepository);

                var result = await _CRUDRepository.save();
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCompanyRepository), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(idCompany.ToString(), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion
        #endregion

        #region Searchs
        #region get by id
        [HttpGet("{idCompany}", Name = "GetCompanyById")]
        [HttpHead]
        public async Task<ActionResult<EmpresaCompletoDTO>> GetCompanyById(Guid idCompany)
        {
            try
            {
                Empresa company = await _empresaRepository.GetCompanyById(idCompany);

                if (company == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                EmpresaCompletoDTO objCompany = _mapper.Map<EmpresaCompletoDTO>(company);

                return Ok(objCompany);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get by Ruc
        [HttpGet("GetCompanyByRUC")]
        [HttpHead]
        public async Task<ActionResult<EmpresaCompletoDTO>> GetCompanyByRUC(string rucCompany)
        {
            try
            {
                Empresa company = await _empresaRepository.GetCompanyByRUC(rucCompany);

                if (company == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                EmpresaCompletoDTO objCompany = _mapper.Map<EmpresaCompletoDTO>(company);

                return Ok(objCompany);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        #endregion
      
        #region Get all
        [HttpGet("GetAllCompany")]
        [HttpHead]
        public async Task<ActionResult<List<EmpresaResultadoBusquedaDTO>>> GetAllCompany()
        {
            try
            {
                List<Empresa> listaCompany = await _empresaRepository.GetAllCompany();

                if (listaCompany.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                List<EmpresaCompletoDTO> listaCompanyDTO = _mapper.Map<List<EmpresaCompletoDTO>>(listaCompany);

                return Ok(listaCompanyDTO);
            }
            catch (Exception ex)
            {
            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get by Advanced
        [HttpGet("GetCompaniesAdvanced")]
        [HttpHead]
        public async Task<ActionResult<List<EmpresaResultadoBusquedaDTO>>> GetCompaniesAdvanced(ObjetoBusquedaEmpresas objBusqueda)
        {
            try
            {
                List<Empresa> listCompanies = await _empresaRepository.GetCompaniesAdvanced(objBusqueda);

                if (listCompanies.Count < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }

                List<EmpresaResultadoBusquedaDTO> listCompaniesDTO = _mapper.Map<List<EmpresaResultadoBusquedaDTO>>(listCompanies);

                return Ok(listCompaniesDTO);
            }
            catch (Exception ex)
            {
            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #endregion


        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion

    }
}
