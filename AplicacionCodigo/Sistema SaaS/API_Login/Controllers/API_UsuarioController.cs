﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainDB.Entities.InterfacesManageData;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DTOs.User;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Usuario;
using ComerciosEcuadorAPI.Model;
using Newtonsoft.Json;
using RepositorioRolesUsuarios.Interface;
using EntidadesRolesUsuarios.Entities;
using RepositorioLogExcepcion.Interface;
using RepositorioEmpresa.Interface;
using EntidadesEmpresa.Entities;
using DTOs.Empresa;
using RepositorioPersona.Interface;
using EntidadesPersonas.Entities;
using RepositorioPersona.Implementacion;
using DTOs.Persona;
using DTOs.Catalogo;
using RepositorioCatalogoNotas.Interface;
using EntidadesCatalogosNotas.Entities;

namespace API_Login.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class UsuarioApiControllers : ControllerBase
    {
        private readonly IManageUser _UserRepository;
        private readonly IMapper _mapper;

        private readonly IManageCRUD_Roles_Usuarios<Usuario> _CRUDUserRepository;
        private readonly IManageCRUDPersona<Persona> _CRUDUPersonaRepository;
        private readonly IManageLogError _logError;
        private readonly IManageRol _RolRepository;
        private readonly IManageEmpresa _empresaRepository;
        private readonly IManagePersona _personaRepository;
        private readonly IManageCatalogos _repositoryCatalogos;

        public UsuarioApiControllers(IManageUser UserRepository, IMapper mapper, IManageLogError logError, IManageCRUD_Roles_Usuarios<Usuario> cRUDRepository, IManageEmpresa empresaRepository, IManagePersona personaRepository, IManageRol rolRepository, IManageCRUDPersona<Persona> cRUDUPersonaRepository, IManageCatalogos repositoryCatalogos)
        {
            _UserRepository = UserRepository ?? throw new ArgumentException(nameof(UserRepository));
            this._mapper = mapper;
            _logError = logError;
            _CRUDUserRepository = cRUDRepository;
            _empresaRepository = empresaRepository;
            _personaRepository = personaRepository;
            _RolRepository = rolRepository;
            _CRUDUPersonaRepository = cRUDUPersonaRepository;
            _repositoryCatalogos = repositoryCatalogos;
        }

        #region CRUD
        #region Create
        [HttpPost("Create")]
        [HttpHead]
        public async Task<IActionResult> Create([FromBody] UserCreateDTO objUserDTO)
        {
            try
            {
                if (objUserDTO == null)
                {
                    return BadRequest();
                }

                Persona objPersona = _mapper.Map<Persona>(objUserDTO);
                _CRUDUPersonaRepository.Add(objPersona);
                var resultPersona = await _CRUDUPersonaRepository.save();
                if (resultPersona.estado)
                {
                    Usuario objUser = _mapper.Map<Usuario>(objUserDTO);
                    objUser.IdPersona = objPersona.IdPersona;
                    _CRUDUserRepository.Add(objUser);
                    var result = await _CRUDUserRepository.save();

                    if (result.estado)
                    {
                        UserCompleteDTO objUserResult = _mapper.Map<UserCompleteDTO>(objUser);
                        return CreatedAtRoute("GetUserById", new { idUser = objUser.IdUsuario }, objUserResult);
                    }
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objUserDTO), resultPersona.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objUserDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        } // end Create User
        #endregion

        #region Edit
        [HttpPut("Edit")]
        public async Task<IActionResult> Edit(Guid IdUsuario, UserUpdateDTO objUsuarioDTO)
        {
            try
            {

                if (objUsuarioDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }
                Usuario objUsuaioRepository = await _UserRepository.getUserById(IdUsuario);

                _mapper.Map(objUsuarioDTO, objUsuaioRepository);

                _CRUDUserRepository.Edit(objUsuaioRepository);
                var result = await _CRUDUserRepository.save();
                // se comprueba que se actualizo correctamente
                if (result.estado)
                {

                    Persona objPersonaRepository = await _personaRepository.recuperarPersonaPorID(objUsuarioDTO.IdPersona);

                    _mapper.Map(objUsuarioDTO, objPersonaRepository);

                    _CRUDUPersonaRepository.Edit(objPersonaRepository);
                    var resultPersona = await _CRUDUPersonaRepository.save();

                    if (resultPersona.estado)
                    {
                        return NoContent();
                    }
                    else
                    {
                        await guardarLogs(JsonConvert.SerializeObject(objUsuarioDTO), resultPersona.mensajeError);
                        return BadRequest(MensajesRespuesta.guardarError());
                    }
                }

                await guardarLogs(JsonConvert.SerializeObject(objUsuarioDTO), result.mensajeError);
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objUsuarioDTO), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);

        }
        #endregion

        #region Delete
        [HttpDelete("Delete")]
        public async Task<IActionResult> Delete(Guid IdUsuario)
        {
            try
            {
                Usuario objUsuaioRepository = await _UserRepository.getUserById(IdUsuario);


                _CRUDUserRepository.Delete(objUsuaioRepository);
                var result = await _CRUDUserRepository.save();
                // se comprueba que se actualizo correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objUsuaioRepository), result.mensajeError);
                }

                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(IdUsuario.ToString(), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);

        }
        #endregion

        #endregion

        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion

        #region Login
        [HttpGet("LoginUser")]
        public async Task<IActionResult> LoginUser(string user, string contrasena)
        {
            try
            {
                if (string.IsNullOrEmpty(user) && string.IsNullOrEmpty(contrasena))
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }

                Usuario objUsuario = await _UserRepository.getLoginUser(user, contrasena);

                if (objUsuario == null)
                {
                    return NotFound(MensajesRespuesta.usuarioContrasenaIncorrecta());
                }

                UserSessionDTO usuarioDTO = _mapper.Map<UserSessionDTO>(objUsuario);

                Persona objPersonaRepositorio = await _personaRepository.recuperarPersonaPorID((Guid)objUsuario.IdPersona);

                if (objPersonaRepositorio != null)
                {
                    usuarioDTO.Nombre = objPersonaRepositorio.Nombre;
                    usuarioDTO.Apellido = objPersonaRepositorio.Apellido;

                    List<UsuarioEmpresa> listaEmpresasUsuarioRepositorio = await _empresaRepository.GetUsuarioEmpresaByIdUsuario(usuarioDTO.IdUsuario);

                    usuarioDTO.ListaEmpresasAcceso = _mapper.Map<List<EmpresaResultadoBusquedaDTO>>(listaEmpresasUsuarioRepositorio);

                    Empresa objEmpresaDefault = await _empresaRepository.GetCompanyById(usuarioDTO.IdEmpresadefault);

                    usuarioDTO.NombreEmpresa = objEmpresaDefault.RazonSocial;

                    return Ok(usuarioDTO);
                }

                return NotFound(MensajesRespuesta.usuarioContrasenaIncorrecta());
            }
            catch (Exception ex)
            {
            }

            return BadRequest(MensajesRespuesta.errorInesperado());
        } // end Create User
        #endregion

        [HttpGet("GetUsuarioEmpresasByIdUsuario")]
        [HttpHead]
        public async Task<ActionResult<List<UsuarioEmpresaCompletoDTO>>> GetUsuarioEmpresasByIdUsuario(Guid IdUsuario)
        {
            try
            {
                List<UsuarioEmpresa> listaEmpresasUsuarioRepositorio = await _empresaRepository.GetUsuarioEmpresaByIdUsuario(IdUsuario);

                List<UsuarioEmpresaCompletoDTO> listaEmpesasUsuaruiDTO = _mapper.Map<List<UsuarioEmpresaCompletoDTO>>(listaEmpresasUsuarioRepositorio);
                return listaEmpesasUsuaruiDTO;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Api GetUsuarioEmpresasByIdUsuario: " + ex.Message);
               
            }
            return BadRequest(MensajesRespuesta.errorInesperado());

        }

        #region Busquedas
        [HttpGet("{idUser}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(Guid idUser)
        {
            try
            {
                Usuario objUsuario = await _UserRepository.getUserById(idUser);

                if (objUsuario == null)
                {
                    return NotFound(MensajesRespuesta.usuarioContrasenaIncorrecta());
                }
                UserCompleteDTO objUsuarioDTO = _mapper.Map<UserCompleteDTO>(objUsuario);

                Persona objPersonaRepositorio = await _personaRepository.recuperarPersonaPorID((Guid)objUsuario.IdPersona);
                if (objPersonaRepositorio != null)
                {
                    objUsuarioDTO.Apellido = objPersonaRepositorio.Apellido;
                    objUsuarioDTO.Nombre = objPersonaRepositorio.Nombre;
                    objUsuarioDTO.CorreoPersonal = objPersonaRepositorio.CorreoPersonal;
                    objUsuarioDTO.Direccion = objPersonaRepositorio.Direccion;
                    objUsuarioDTO.IdEstadoCivil = (Guid)objPersonaRepositorio.IdEstadoCivil;
                    objUsuarioDTO.IdLugarNacimiento = (Guid)objPersonaRepositorio.IdLugarNacimiento;

                    objUsuarioDTO.IdResidencia = (Guid)objPersonaRepositorio.IdResidencia;
                    objUsuarioDTO.IdTipoIdentificacion = (Guid)objPersonaRepositorio.IdTipoIdentificacion;
                    objUsuarioDTO.ImagenPersona = objPersonaRepositorio.ImagenPersona;
                    objUsuarioDTO.NumeroIdentificacion = objPersonaRepositorio.NumeroIdentificacion;
                    objUsuarioDTO.Telefono1 = objPersonaRepositorio.Telefono1;
                    objUsuarioDTO.Telefono2 = objPersonaRepositorio.Telefono2;
                    objUsuarioDTO.FechaNacimiento = objPersonaRepositorio.FechaNacimiento;

                }
                else
                {
                    return NotFound(MensajesRespuesta.usuarioContrasenaIncorrecta());
                }

                return Ok(objUsuarioDTO);
            }
            catch (Exception ex)
            {
            }

            return BadRequest(MensajesRespuesta.errorInesperado());


        }

        [HttpGet("GetUsersAdvanced")]
        [HttpHead]
        public async Task<ActionResult<List<UserResultadoBusquedaDTO>>> GetUsersAdvanced(ObjetoBusquedaUsuarios objBusqueda)
        {
            List<UserResultadoBusquedaDTO> listaResultadoDTO = new();
            if (!string.IsNullOrEmpty(objBusqueda.nombres) || !string.IsNullOrEmpty(objBusqueda.numeroIdentificacion) || !string.IsNullOrEmpty(objBusqueda.apellidos))
            {
                List<Persona> listaPersonaUsuario = await _personaRepository.BusquedaAvanzadaParaUsuario(objBusqueda);
                try
                {
                    foreach (var persona in listaPersonaUsuario)
                    {
                        UserResultadoBusquedaDTO objResultadoBusqueda = new();
                        objResultadoBusqueda.IdPersona = persona.IdPersona;
                        objResultadoBusqueda.Apellido = persona.Apellido;
                        objResultadoBusqueda.Nombre = persona.Nombre;
                        Usuario usuario = await _UserRepository.getPersonaById(objResultadoBusqueda.IdPersona);
                        if (usuario != null)
                        {
                            Empresa objEmpresa = await _empresaRepository.GetCompanyById((Guid)usuario.IdEmpresaDefault);
                            objResultadoBusqueda.IdUsuario = usuario.IdUsuario;
                            objResultadoBusqueda.IdEmpresaDefault = usuario.IdEmpresaDefault;
                            objResultadoBusqueda.Correoinstitucional = usuario.CorreoInstitucional;
                            objResultadoBusqueda.Estado = usuario.Estado;
                            objResultadoBusqueda.Perfil = usuario.IdRolNavigation.NombreRol;
                            objResultadoBusqueda.Empresa = objEmpresa.RazonSocial;
                            objResultadoBusqueda.EmpresaUsuarioDTO = _mapper.Map<List<EmpresaUsuarioDTO>>(objEmpresa.UsuarioEmpresas);
                            listaResultadoDTO.Add(objResultadoBusqueda);
                        }
                    }
                }
                catch (Exception exp)
                {
                    Console.WriteLine("Error: " + exp.Message);
                }
                return listaResultadoDTO;
            }
            else
            {
                List<Usuario> listaUsuarios = await _UserRepository.GetUserAdvanced(objBusqueda);
                if (listaUsuarios.Count < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                listaResultadoDTO = _mapper.Map<List<UserResultadoBusquedaDTO>>(listaUsuarios);

                foreach (var itemListaUsuario in listaResultadoDTO)
                {
                    try
                    {
                        Persona objPersonaUsuario = await _personaRepository.recuperarPersonaPorID((Guid)itemListaUsuario.IdPersona);
                        Empresa objEmpresa = await _empresaRepository.GetCompanyById((Guid)itemListaUsuario.IdEmpresaDefault);
                        itemListaUsuario.Empresa = objEmpresa.RazonSocial;
                        itemListaUsuario.Nombre = objPersonaUsuario.Nombre;
                        itemListaUsuario.Apellido = objPersonaUsuario.Apellido;
                        itemListaUsuario.EmpresaUsuarioDTO = _mapper.Map<List<EmpresaUsuarioDTO>>(objEmpresa.UsuarioEmpresas);
                    }
                    catch (Exception ex)
                    {

                    }

                }

                return Ok(listaResultadoDTO);

            }

        }

        [HttpGet("GetUsurioPorIdentificacionparaValidarCreacion")]
        [HttpHead]
        public async Task<ActionResult<UserCompleteDTO>> GetUsurioPorIdentificacionparaValidarCreacion(string numeroIdentificacion)
        {
            try
            {
                Persona persona = await _personaRepository.recuperarPersonaPorIdentificacion(numeroIdentificacion.Trim().Replace("-", ""));
                if (persona == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                UserCompleteDTO objUsuarioCompletoDTO = _mapper.Map<UserCompleteDTO>(persona);
                if (objUsuarioCompletoDTO.IdLugarNacimiento != null)
                {
                    Catalogo objCatalogoCantonDTO = await _repositoryCatalogos.obtenerCatalogoPadrePorIdHijo((Guid)objUsuarioCompletoDTO.IdLugarNacimiento);

                    Catalogo objCatalogoProvinciaDTO = await _repositoryCatalogos.obtenerCatalogoPadrePorIdHijo((Guid)objCatalogoCantonDTO.IdCatalogo);

                    objUsuarioCompletoDTO.IdCantonNacimiento = objCatalogoCantonDTO.IdCatalogo;
                    objUsuarioCompletoDTO.IdProvinciaNacimiento = objCatalogoProvinciaDTO.IdCatalogo;
                }
                if (objUsuarioCompletoDTO.IdResidencia != null)
                {
                    Catalogo objCatalogoCantonDTO = await _repositoryCatalogos.obtenerCatalogoPadrePorIdHijo((Guid)objUsuarioCompletoDTO.IdResidencia);
                    Catalogo objCatalogoProvinciaDTO = await _repositoryCatalogos.obtenerCatalogoPadrePorIdHijo((Guid)objCatalogoCantonDTO.IdCatalogo);

                    objUsuarioCompletoDTO.IdCantonRecidencia = objCatalogoCantonDTO.IdCatalogo;
                    objUsuarioCompletoDTO.IdProvinciaRecidencia = objCatalogoProvinciaDTO.IdCatalogo;
                }


                Usuario usuario = await _UserRepository.getPersonaById(persona.IdPersona);
                if (usuario != null)
                {
                    objUsuarioCompletoDTO.IdUsuario = usuario.IdUsuario;
                    objUsuarioCompletoDTO.IdRol = usuario.IdRol;
                    objUsuarioCompletoDTO.IdEmpresaDefault = usuario.IdEmpresaDefault;
                    objUsuarioCompletoDTO.EstadoUsuario = (bool)usuario.Estado;
                    objUsuarioCompletoDTO.CorreoInstitucional = usuario.CorreoInstitucional;
                    objUsuarioCompletoDTO.Contrasena = usuario.Contrasena;
                    objUsuarioCompletoDTO.IndicioContrasena = usuario.IndicioContrasena;
                    objUsuarioCompletoDTO.FechaUltimoIngreso = usuario.FechaUltimoIngreso;
                }
                return Ok(objUsuarioCompletoDTO);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            return BadRequest(MensajesRespuesta.errorInesperado());
        }


        #endregion Busquedas

    }



}

