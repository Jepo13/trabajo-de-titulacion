﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DomainDB.Entities.InterfacesManageData;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DTOs.Local;
using Microsoft.AspNetCore.JsonPatch;
using ComerciosEcuadorAPI.Model;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Empresa;
using Newtonsoft.Json;
using Utilitarios.Busquedas.Local;
using RepositorioEmpresa.Interface;
using RepositorioLogExcepcion.Interface;
using EntidadesEmpresa.Entities;
using RepositorioCatalogoNotas.Interface;
using EntidadesCatalogosNotas.Entities;
using DTOs.Ubicacion;
using DTOs.Sitios;
using DTOs.Bandejas;

namespace API_Login.Controllers
{
    [Route("api/Local")]
    [ApiController]
    public class API_LocalControllers : ControllerBase
    {
        private readonly IManageLocal _localRepository;
        private readonly IManageUbicacion _UbicacionRepository;
        private readonly IManageSitio _sitioRepository;
        private readonly IMapper _mapper;
        private readonly IManageCatalogos _catalogosRepository;
        private readonly IManageLogError _logError;
        private readonly IManageCRUDEmpresa<Local> _CRUDRepository;
        private readonly IManageCRUDEmpresa<Ubicacion> _CRUDRepositoryUbicacion;
        private readonly IManageCRUDEmpresa<Sitio> _CRUDRepositorySitio;
        private readonly IManageCRUDEmpresa<Bandeja> _CRUDRepositoryBandeja;


        public API_LocalControllers(IManageLocal localRepository, IMapper mapper, IManageLogError logError, IManageCRUDEmpresa<Local> cRUDRepository, IManageCatalogos catalogosRepository, IManageCRUDEmpresa<Ubicacion> cRUDRepositoryUbicacion, IManageUbicacion ubicacionRepository, IManageCRUDEmpresa<Sitio> cRUDRepositorySitio, IManageCRUDEmpresa<Bandeja> cRUDRepositoryBandeja, IManageSitio sitioRepository)
        {
            _localRepository = localRepository ?? throw new ArgumentNullException(nameof(localRepository));
            this._mapper = mapper;
            _logError = logError;
            _CRUDRepository = cRUDRepository;
            _catalogosRepository = catalogosRepository;
            _CRUDRepositoryUbicacion = cRUDRepositoryUbicacion;
            _UbicacionRepository = ubicacionRepository;
            _CRUDRepositorySitio = cRUDRepositorySitio;
            _CRUDRepositoryBandeja = cRUDRepositoryBandeja;
            _sitioRepository = sitioRepository;
        }

        #region CRUD Locales
        #region Create
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] LocalCreateDTO objLocalDTO)
        {
            try
            {
                if (objLocalDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }

                Local objLocal = _mapper.Map<Local>(objLocalDTO);
                _CRUDRepository.Add(objLocal);

                var result = await _CRUDRepository.save();

                if (result.estado)
                {
                    LocalCompleteDTO objLocalResult =
                        _mapper.Map<LocalCompleteDTO>(objLocal);
                    return CreatedAtRoute("GetLocalById", new { idLocal = objLocal.IdLocal }, objLocalResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objLocalDTO), result.mensajeError);
                }
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objLocalDTO), ExValidation.ToString());
            }
            return BadRequest(MensajesRespuesta.guardarError());
        }
        #endregion

        #region Edit
        [HttpPut("Edit")]
        public async Task<IActionResult> Edit(Guid idLocal, LocalUpdateDTO objLocalDTO)
        {
            try
            {
                if (objLocalDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }
                Local objLocalRepository = await _localRepository.GetLocalById(idLocal);
                _mapper.Map(objLocalDTO, objLocalRepository);

                _CRUDRepository.Edit(objLocalRepository);
                var result = await _CRUDRepository.save();
                // se comprueba que se actualizo correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objLocalDTO), result.mensajeError);
                }

                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objLocalDTO), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);

        }
        #endregion        

        #region Delete
        [HttpDelete("{DeleteLocal}")]
        public async Task<IActionResult> DeleteLocal(Guid idLocal)
        {
            try
            {
                Local objLocalRepository = await _localRepository.GetLocalById(idLocal);
                if (objLocalRepository == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                _CRUDRepository.Delete(objLocalRepository);
                var result = await _CRUDRepository.save();

                //Se comprueba que se actualizó correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objLocalRepository), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(idLocal.ToString(), ex.ToString());
            }

            return BadRequest();
        }
        #endregion

        #endregion
        #region CRUD Ubicaciones

        #region Create Ubicaciones

        [HttpPost("CreateUbicacion")] 
        public async Task<IActionResult> CreateUbicacion([FromBody]  UbicacionCreateDTO objUbicacionDTO)
        {
            try
            {
                if (objUbicacionDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }

                Ubicacion objUbicacion = _mapper.Map<Ubicacion>(objUbicacionDTO);
                _CRUDRepositoryUbicacion.Add(objUbicacion);

                var result = await _CRUDRepository.save();

                if (result.estado)
                {
                    return Ok();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objUbicacionDTO), result.mensajeError);
                }
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objUbicacionDTO), ExValidation.ToString());
            }
            return BadRequest(MensajesRespuesta.guardarError());
        }
        #endregion

        #region Editar Ubicaciones

        [HttpPost("EditUbicacion")]
        public async Task<IActionResult> EditUbicacion([FromBody] UbicacionUpdateDTO objUbicacionDTO)
        {
            try
            {
                if (objUbicacionDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }

                Ubicacion objUbicacionRepository = await _UbicacionRepository.GetUbicacionById(objUbicacionDTO.IdUbicacion);
                _mapper.Map(objUbicacionDTO, objUbicacionRepository);

                _CRUDRepositoryUbicacion.Edit(objUbicacionRepository);
        
                var result = await _CRUDRepository.save();

                if (result.estado)
                {
                    return Ok();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objUbicacionDTO), result.mensajeError);
                }
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objUbicacionDTO), ExValidation.ToString());
            }
            return BadRequest(MensajesRespuesta.guardarError());
        }
        #endregion

        #endregion
        #region CRUD Sitios

        #region Create 

        [HttpPost("CreateSitio")]
        public async Task<IActionResult> CreateSitio([FromBody] SitioCreateDTO objSitioDTO)
        {
            try
            {
                if (objSitioDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }

                Sitio objSitio = _mapper.Map<Sitio>(objSitioDTO);
                _CRUDRepositorySitio.Add(objSitio);

                var result = await _CRUDRepository.save();

                if (result.estado)
                {
                    return Ok();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objSitioDTO), result.mensajeError);
                }
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objSitioDTO), ExValidation.ToString());
            }
            return BadRequest(MensajesRespuesta.guardarError());
        }
        #endregion

        #endregion


        #region CRUD Bandejas

        #region Create 

        [HttpPost("CreatBandejas")]
        public async Task<IActionResult> CreatBandejas([FromBody] BandejaCreateDTO objBandejaCreateDTO)
        {
            try
            {
                if (objBandejaCreateDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }

                Bandeja obj = _mapper.Map<Bandeja>(objBandejaCreateDTO);
                _CRUDRepositoryBandeja.Add(obj);

                var result = await _CRUDRepository.save();

                if (result.estado)
                {
                    return Ok();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objBandejaCreateDTO), result.mensajeError);
                }
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objBandejaCreateDTO), ExValidation.ToString());
            }
            return BadRequest(MensajesRespuesta.guardarError());
        }
        #endregion

        #endregion


        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion

        #region Searchs Ubicacion
        [HttpGet("GetUbicacionById")]
        public async Task<ActionResult<UbicacionCompleteDTO>> GetUbicacionById(Guid idUbicacion)
        {
            try
            {
                Ubicacion objUbicacion = await _UbicacionRepository.GetUbicacionById(idUbicacion);
                if (objUbicacion == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                UbicacionCompleteDTO objUbicacionDTO = _mapper.Map<UbicacionCompleteDTO>(objUbicacion);             

                return Ok(objUbicacionDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion



        #region Searchs local 

        [HttpGet("{idLocal}", Name = "GetLocalById")]      
        public async Task<ActionResult<LocalCompleteDTO>> GetLocalById(Guid idLocal)
        {
            try
            {
                Local objlocal = await _localRepository.GetLocalById(idLocal);
                if (objlocal == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                LocalCompleteDTO objLocalDTO = _mapper.Map<LocalCompleteDTO>(objlocal);
                objLocalDTO.listaUbicacionesDTO = new();
                if (objlocal.Ubicacions!= null) 
                {
                   
                    if (objlocal.Ubicacions.Count() > 0) 
                    {
                        foreach (var item in objlocal.Ubicacions)
                        {
                            UbicacionCompleteDTO Ubicaciones = _mapper.Map<UbicacionCompleteDTO>(item);


                            Catalogo objCatalogo = await _catalogosRepository.GetCatalogoById(item.IdTipoUbicacion);
                            if(objCatalogo != null) 
                            {
                                Ubicaciones.nombreTipoUbicacion = objCatalogo.NombreCatalogo;
                            }

                            objLocalDTO.listaUbicacionesDTO.Add(Ubicaciones);
                        }
                        
                    }
                }

                return Ok(objLocalDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }


        [HttpGet("GetLocalAdvanced")]
        public async Task<ActionResult<List<LocalCompleteDTO>>> GetLocalAdvanced (ObjetoBusquedaLocal objBusqueda)
        {
            try
            {
                List<Local> lista = await _localRepository.GetLocalAdvanced(objBusqueda);

                if (lista == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<LocalResultadoBusquedaDTO> listLocalesDTO = _mapper.Map<List<LocalResultadoBusquedaDTO>>(lista);
                foreach (var item in listLocalesDTO)
                {
                    if (item.IdUbicacion != null)
                    {
                        Catalogo CatalogoRespository = await _catalogosRepository.GetCatalogoById((Guid)item.IdUbicacion);
                        item.ubicacion = CatalogoRespository.NombreCatalogo;
                    }
                    if (item.IdTipolocal != null)
                    {
                        Catalogo CatalogoRespository = await _catalogosRepository.GetCatalogoById((Guid)item.IdTipolocal);
                        item.tipolocal = CatalogoRespository.NombreCatalogo;
                    }

                }

                return Ok(listLocalesDTO);
            }
            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet("GetLocalByNombre")]       
        public async Task<ActionResult<LocalCompleteDTO>> GetLocalByNombre(string nombreLocal)
        {
            try
            {
                Local local = await _localRepository.GetLocalByNombre(nombreLocal);
                if (local == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                LocalCompleteDTO objLocalDTO = _mapper.Map<LocalCompleteDTO>(local);
                return Ok(objLocalDTO);
            }

            catch (Exception ex)
            {

            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet("GetAllLocales")]    
        public async Task<ActionResult<List<LocalCompleteDTO>>> GetAllLocales()
        {
            try
            {
                List<Local> listLocal = await _localRepository.GetAllLocales();
                if (listLocal.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<LocalCompleteDTO> listaLocaCompletelDTO = _mapper.Map<List<LocalCompleteDTO>>(listLocal);
                return Ok(listaLocaCompletelDTO);
            }
            catch (Exception ex)
            {

            }
            return BadRequest(MensajesRespuesta.errorInesperado());
        }
        #endregion


        #region Searchs Ubicacion

        [HttpGet("GetSitiosByIdUbicacion")]
        public async Task<ActionResult<List<SitiosCompletoDTO>>> GetSitiosByIdUbicacion(Guid IdUbicacion)
        {
            try
            {
               

                List<Sitio> listSitio = await _sitioRepository.GetSitiosByIdUbicacionl(IdUbicacion);
                if (listSitio.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<SitiosCompletoDTO> listaSitiosCompletoDTO  = _mapper.Map<List<SitiosCompletoDTO>>(listSitio);
                return Ok(listaSitiosCompletoDTO);
            }
            catch (Exception ex)
            {

            }
            return BadRequest(MensajesRespuesta.errorInesperado());
        }
        #endregion
    }
}
