﻿using AutoMapper;
using ComerciosEcuadorAPI.Model;
using DTOs.Cliente;
using DTOs.EmpresaCliente;
using DTOs.Persona;
using DTOs.User;
using EntidadesCatalogosNotas.Entities;
using EntidadesEmpresa.Entities;
using EntidadesPersonas.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RepositorioCatalogoNotas.Interface;
using RepositorioEmpresa.Interface;
using RepositorioLogExcepcion.Interface;
using RepositorioPersona.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas.Usuario;

namespace API_Login.Controllers
{
    [Route("api/Clinete")]
    [ApiController]
    public class API_ClienteController : ControllerBase
    {
        private readonly IManageCRUDPersona<Persona> _CRUDRepositoryPersona;
        private readonly IManageCRUDPersona<EmpresaCliente> _CRUDRepositoryEmpresaCliente;
        private readonly IManageEmpresaCliente _empresaClienteRepository;
        private readonly IManagePersona _personaRepository;
        private readonly IManageEmpresa _empresaRepository;
        private readonly IManageCatalogos _catalogosRepository;
        private readonly IMapper _mapper;
        private IManageLogError _logError;

        public API_ClienteController(IManageCRUDPersona<Persona> cRUDRepository, IMapper mapper, IManageLogError logError, IManagePersona personaRepository, IManageCatalogos catalogosRepository, IManageEmpresa empresaRepository = null, IManageCRUDPersona<EmpresaCliente> cRUDRepositoryEmpresaCliente = null, IManageEmpresaCliente empresaClienteRepository = null)
        {
            _CRUDRepositoryPersona = cRUDRepository;
            _personaRepository = personaRepository;
            this._mapper = mapper;
            _logError = logError;
            _catalogosRepository = catalogosRepository;
            _empresaRepository = empresaRepository;
            _CRUDRepositoryEmpresaCliente = cRUDRepositoryEmpresaCliente;
            _empresaClienteRepository = empresaClienteRepository;
        }



        #region CRUD

        #region CLIENTE CRUD 
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] PersonaClienteCreateDTO objPersonaCreateDTO)
        {
            try
            {
                if (objPersonaCreateDTO == null)
                {
                    return BadRequest();
                }
                #region validar null por 0 en descuentos y creditos 
                if (objPersonaCreateDTO.PorcentajeDescuento == null)
                {
                    objPersonaCreateDTO.PorcentajeDescuento = 0;
                }
                if (objPersonaCreateDTO.ValorCupoCreditoPersonal == null)
                {
                    objPersonaCreateDTO.ValorCupoCreditoPersonal = 0;
                }
                if (objPersonaCreateDTO.ValorCupoDisponibleCreditoPersonal == null)
                {
                    objPersonaCreateDTO.ValorCupoDisponibleCreditoPersonal = 0;
                }
                if (objPersonaCreateDTO.DiasCredito == null)
                {
                    objPersonaCreateDTO.DiasCredito = 0;
                }
                #endregion


                Persona objValidarPersona = await _personaRepository.recuperarPersonaPorIdentificacion(objPersonaCreateDTO.NumeroIdentificacion);
                if (objValidarPersona == null)
                {
                    Persona objPersona = _mapper.Map<Persona>(objPersonaCreateDTO);

                    EmpresaCliente empresaCliente = new EmpresaCliente();
                    empresaCliente.PorcentajeDescuento = objPersonaCreateDTO.PorcentajeDescuento;
                    empresaCliente.ValorCupoCreditoPersonal = objPersonaCreateDTO.ValorCupoCreditoPersonal;
                    empresaCliente.ValorCupoDisponibleCreditoPersonal = objPersonaCreateDTO.ValorCupoDisponibleCreditoPersonal;
                    empresaCliente.DiasCredito = objPersonaCreateDTO.DiasCredito;
                    empresaCliente.IdEmpresa = objPersonaCreateDTO.IdEmpresa;
                    empresaCliente.UsuarioCreacion = objPersona.UsuarioCreacion;
                    empresaCliente.UsuarioModificacion = objPersona.UsuarioCreacion;
                    empresaCliente.FechaModificacion = DateTime.Now;
                    empresaCliente.FechaCreacion = DateTime.Now;
                    objPersona.EmpresaClientes.Add(empresaCliente);

                    _CRUDRepositoryPersona.Add(objPersona);
                    var result = await _CRUDRepositoryPersona.save();
                    if (result.estado)
                    {
                        return Ok();
                    }
                    else
                    {
                        await guardarLogs(JsonConvert.SerializeObject(objPersonaCreateDTO), result.mensajeError);
                    }
                }

            }
            catch (Exception ex)
            {

                await guardarLogs(JsonConvert.SerializeObject(objPersonaCreateDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }

        [HttpPost("Edit")]
        public async Task<IActionResult> Edit(PersonaClienteUpdateDTO objPersonaClienteUpdateDTO) 
        {
            try
            {
                if (objPersonaClienteUpdateDTO == null)
                {
                    return BadRequest();
                }
                Persona objtPersonaRepository = await _personaRepository.recuperarPersonaPorID(objPersonaClienteUpdateDTO.IdPersona);
                _mapper.Map(objPersonaClienteUpdateDTO, objtPersonaRepository);
                _CRUDRepositoryPersona.Edit(objtPersonaRepository);
                var result = await _CRUDRepositoryPersona.save();
                if (result.estado) 
                {
                    return NoContent();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objPersonaClienteUpdateDTO), ex.ToString());

            }
            return BadRequest(MensajesRespuesta.guardarError());
        }
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(PersonaClienteUpdateDTO objPersonaClienteUpdateDTO)
        {
            try
            {
                if (objPersonaClienteUpdateDTO == null)
                {
                    return BadRequest();
                }
                Persona objtPersonaRepository = await _personaRepository.recuperarPersonaPorID(objPersonaClienteUpdateDTO.IdPersona);
              
                _CRUDRepositoryPersona.Delete(objtPersonaRepository);
                var result = await _CRUDRepositoryPersona.save();
                if (result.estado)
                {
                    return NoContent();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objPersonaClienteUpdateDTO), ex.ToString());

            }
            return BadRequest(MensajesRespuesta.guardarError());
        }

        #endregion

        #region  EMPRESA CLIENTE CRUD 
        [HttpPost("CreateEmpresaCliente")]
        public async Task<IActionResult> CreateEmpresaCliente(EmpresaClienteCreateDTO objEmpresaClienteCreateDTO)
        {
            try
            {
                if (objEmpresaClienteCreateDTO == null)
                {
                    return BadRequest();
                }


                EmpresaCliente empresaCliente = _mapper.Map<EmpresaCliente>(objEmpresaClienteCreateDTO);


                _CRUDRepositoryEmpresaCliente.Add(empresaCliente);
                var result = await _CRUDRepositoryPersona.save();
                if (result.estado)
                {
                    return Ok();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objEmpresaClienteCreateDTO), result.mensajeError);
                }

            }
            catch (Exception ex)
            {

                await guardarLogs(JsonConvert.SerializeObject(objEmpresaClienteCreateDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }

        [HttpPost("EditEmpresaCliente")]
        public async Task<IActionResult> EditEmpresaCliente(EmpresaClienteUpdateDTO objEmpresaClienteUpdateDTO)
        {
            try
            {
                if (objEmpresaClienteUpdateDTO == null)
                {
                    return BadRequest();
                }
                EmpresaCliente objtEmpresaClieteRepository = await _empresaClienteRepository.obtenerEmpresaClientePorID(objEmpresaClienteUpdateDTO.IdEmpresaCliente_Update);

                _mapper.Map(objEmpresaClienteUpdateDTO, objtEmpresaClieteRepository);


                _CRUDRepositoryEmpresaCliente.Edit(objtEmpresaClieteRepository);
                var result = await _CRUDRepositoryEmpresaCliente.save();
                if (result.estado)
                {
                    return NoContent();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objEmpresaClienteUpdateDTO), ex.ToString());

            }
            return BadRequest(MensajesRespuesta.guardarError());
        }

        [HttpPost("EliminarEmpresaCliente")]
        public async Task<IActionResult> EliminarEmpresaCliente(EmpresaClienteUpdateDTO objEmpresaClienteUpdateDTO)
        {
            try
            {
                if (objEmpresaClienteUpdateDTO == null)
                {
                    return BadRequest();
                }
                EmpresaCliente objtEmpresaClieteRepository = await _empresaClienteRepository.obtenerEmpresaClientePorID(objEmpresaClienteUpdateDTO.IdEmpresaCliente_Update);

             
                _CRUDRepositoryEmpresaCliente.Delete(objtEmpresaClieteRepository);
                var result = await _CRUDRepositoryEmpresaCliente.save();
                if (result.estado)
                {
                    return NoContent();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objEmpresaClienteUpdateDTO), ex.ToString());

            }
            return BadRequest(MensajesRespuesta.guardarError());
        }

        [HttpGet("GetEmpresaClienteById")]
        [HttpHead]
        public async Task<ActionResult<EmpresaClienteCompleteDTO>> GetEmpresaClienteById(Guid idEmpresaCliente)
        {
            try
            {
                EmpresaCliente empresaClienteRepositorio = await _empresaClienteRepository.obtenerEmpresaClientePorID(idEmpresaCliente);
                if (empresaClienteRepositorio == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                EmpresaClienteCompleteDTO objEmpresClienteDTO = _mapper.Map<EmpresaClienteCompleteDTO>(empresaClienteRepositorio);
                return Ok(objEmpresClienteDTO);
            }
            catch (Exception ex)
            {


            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion


        #endregion

        #region Search

        #region Personas
        //------ INICIO: Buscar Personas por Id de persona----//
        [HttpGet("GetClientePersonaByIdPersona")]
        [HttpHead]
        public async Task<ActionResult<PersonaClienteCompletoDTO>> GetClientePersonaByIdPersona(Guid idPersona)
        {
            try
            {
                Persona persona = await _personaRepository.recuperarClientePersonaPorID(idPersona);
                if (persona == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                PersonaClienteCompletoDTO objPersonaDTO = _mapper.Map<PersonaClienteCompletoDTO>(persona);
                if (objPersonaDTO.ListEmpresaClientesDTO != null)
                {
                    if (objPersonaDTO.ListEmpresaClientesDTO.Count() > 0)
                    {
                        foreach (var item in objPersonaDTO.ListEmpresaClientesDTO)
                        {
                            Empresa empresa = await _empresaRepository.GetCompanyById(item.IdEmpresa);
                            item.EmpresaNombre = empresa.RazonSocial;
                        }

                    }
                }


                return Ok(objPersonaDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        //------ FIN: Buscar Personas por Id de persona----//


        //--INICIO: Buscar Persona por numero de Identificación---//
        [HttpGet("GetPersonaByNumeroIdentificacion")]
        [HttpHead]
        public async Task<ActionResult<PersonaCompleteDTO>> GetPersonaByNumeroIdentificacion(string numeroIdentificacion)
        {
            try
            {
                Persona persona = await _personaRepository.recuperarPersonaPorIdentificacion(numeroIdentificacion.Trim().Replace("-", ""));

                if (persona == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                PersonaCompleteDTO objPersonaDTO = _mapper.Map<PersonaCompleteDTO>(persona);
                return Ok(objPersonaDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        //--FIN: Buscar Persona por numero de Identificación--//

        #endregion

        #region Cliente
        [HttpGet("GetClienteAdvanced")]
        [HttpHead]
        public async Task<ActionResult<List<ClienteResultadoBusqueda>>> GetClienteAdvanced(ClienteObjetoBusquedaDTO objBusqueda)
        {

            List<Persona> listaPersonaCliente = await _personaRepository.BusquedaAvanzadaParaCliente(objBusqueda);
            List<ClienteResultadoBusqueda> listaClienteResultadoBusqueda = _mapper.Map<List<ClienteResultadoBusqueda>>(listaPersonaCliente);
            try
            {
                foreach (var clienteResultado in listaClienteResultadoBusqueda)
                {
                    if (clienteResultado.IdTipoContribuyente != null)
                    {
                        Catalogo catalogo = await _catalogosRepository.GetCatalogoById((Guid)clienteResultado.IdTipoContribuyente);
                        clienteResultado.TipoContribuyente = catalogo.NombreCatalogo;
                    }
                    if (clienteResultado.IdTipoIdentificacion != null)
                    {
                        Catalogo catalogo = await _catalogosRepository.GetCatalogoById((Guid)clienteResultado.IdTipoIdentificacion);
                        clienteResultado.TipoIdentificacion = catalogo.NombreCatalogo;
                    }

                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error: " + ex.Message);
            }

            return listaClienteResultadoBusqueda;
        }


        #endregion

        #region EmpresaCLiente


        [HttpGet("GetEmpresaClientebyIdPersona")]
        [HttpHead]
        public async Task<ActionResult<List<EmpresaClienteCompleteDTO>>> GetEmpresaClientebyIdPersona(Guid idPersona)
        {
            List<EmpresaClienteCompleteDTO> listaEmpresasClieteDTO = new();
            try
            {
                List<EmpresaCliente> listaEmpresaClienteRepository = await _empresaClienteRepository.obtenerEmpresaClientePorIdPersona(idPersona);
                listaEmpresasClieteDTO = _mapper.Map<List<EmpresaClienteCompleteDTO>>(listaEmpresaClienteRepository);
                if (listaEmpresasClieteDTO != null)
                {
                    if (listaEmpresasClieteDTO.Count() > 0)
                    {
                        foreach (var item in listaEmpresasClieteDTO)
                        {
                            Empresa empresa = await _empresaRepository.GetCompanyById(item.IdEmpresa);
                            item.EmpresaNombre = empresa.RazonSocial;
                        }

                    }
                }

               
            }
            catch (Exception)
            {

            }
            return listaEmpresasClieteDTO;
        }

        [HttpGet("GetEmpresaClienteValidar")]
        [HttpHead]
        public async Task<ActionResult<EmpresaClienteCompleteDTO>> GetEmpresaClienteValidar(EmpresaClienteValidadDTO objBusqueda)
        {
            EmpresaClienteCompleteDTO objDTO = new();
            try
            {
                EmpresaCliente empresaClienteRepository = await _empresaClienteRepository.obtenerEmpresaClienteBusquedaValidar(objBusqueda);
                objDTO = _mapper.Map<EmpresaClienteCompleteDTO>(empresaClienteRepository);

            }
            catch (Exception EX)
            {

                Console.WriteLine("Error: " + EX.Message);
            }

            return objDTO;
        }
        #endregion

        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }

        #endregion

        #endregion
    }
}
