﻿using AutoMapper;
using ComerciosEcuadorAPI.Model;
using DTOs.Catalogo;

using DomainDB.Entities.InterfacesManageData;
using EntidadesCatalogosNotas.Entities;
using EntidadesEmpresa.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RepositorioCatalogoNotas.Interface;
using RepositorioEmpresa.Interface;
using RepositorioLogExcepcion.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;

namespace API_Login.Controllers
{
    [Route("api/Catalogo")]
    [ApiController]

    public class API_CatalogoController : ControllerBase
    {
        private readonly IManageCatalogos _catalogoRepository;
        private readonly IManageEmpresa _empresaRepository;
        private readonly IMapper _mapper;
        private readonly IManageCRUDCatalogos<Catalogo> _CRUDRepository;
        private readonly IManageLogError _logError;
        public API_CatalogoController(IManageCatalogos catalogoRepository, IMapper mapper, IManageCRUDCatalogos<Catalogo> cRUDRepository, IManageLogError logError, IManageEmpresa empresaRepository)
        {
            _catalogoRepository = catalogoRepository ?? throw new ArgumentException(nameof(catalogoRepository));
            this._mapper = mapper;
            _CRUDRepository = cRUDRepository;
            _logError = logError;
            _empresaRepository = empresaRepository;
        }

        #region CRUD
        // crud
        #region Create
        [HttpPost("Create")]
        [HttpHead]
        public async Task<IActionResult> Create([FromBody] CatalogoCreateDTO objCatalogoDTO)
        {
            try
            {
                if (objCatalogoDTO == null)
                {
                    return BadRequest();
                }
                Catalogo objCatalogo = _mapper.Map<Catalogo>(objCatalogoDTO);
                _CRUDRepository.Add(objCatalogo);
                var result = await _CRUDRepository.save();
                if (result.estado)
                {
                    CatalogoCompleteDTO objCatalogoResult = _mapper.Map<CatalogoCompleteDTO>(objCatalogo);
                    return CreatedAtRoute("GetCatalogoById", new { idCatalogo = objCatalogo.IdCatalogo }, objCatalogoResult);
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCatalogoDTO), result.mensajeError);
                }
            }
            catch (Exception ex)
            {
                await guardarLogs(JsonConvert.SerializeObject(objCatalogoDTO), ex.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Edit
        [HttpPut("Edit")]
        public async Task<IActionResult> Edit(Guid idCatalogo, CatalogoEditarDTO objCatalogoDTO)
        {
            try
            {
                if (objCatalogoDTO == null)
                {
                    return BadRequest(MensajesRespuesta.noSePermiteObjNulos());
                }
                Catalogo objCatalogoRepository = await _catalogoRepository.GetCatalogoById(idCatalogo);
                _mapper.Map(objCatalogoDTO, objCatalogoRepository);
                objCatalogoRepository.IdCatalogo = idCatalogo;
                _CRUDRepository.Edit(objCatalogoRepository);
                var result = await _CRUDRepository.save();
                //Se comprueba que se actualizó correctamente
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCatalogoDTO), result.mensajeError);
                }
                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(JsonConvert.SerializeObject(objCatalogoDTO), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);
        }
        #endregion

        #region Delete
        [HttpDelete("{DeleteCatalogo}")]
        public async Task<IActionResult> DeleteCatalogo(Guid idCatalogo)
        {
            try
            {
                Catalogo objCatalogoRepository = await _catalogoRepository.GetCatalogoById(idCatalogo);
                _CRUDRepository.Delete(objCatalogoRepository);
                var result = await _CRUDRepository.save();
                if (result.estado)
                {
                    return NoContent();
                }
                else
                {
                    await guardarLogs(JsonConvert.SerializeObject(objCatalogoRepository), result.mensajeError);    
                }

                return BadRequest(MensajesRespuesta.guardarError());
            }
            catch (Exception ExValidation)
            {
                await guardarLogs(idCatalogo.ToString(), ExValidation.ToString());
            }
            return StatusCode(StatusCodes.Status406NotAcceptable);

        }
        #endregion
        #endregion

        #region Search
        #region Get Catalogo by ID
        [HttpGet("{idCatalogo}", Name = "GetCatalogoById")]
        [HttpHead]
        public async Task<ActionResult<CatalogoCompleteDTO>> GetCatalogoById(Guid idCatalogo)
        {
            try
            {
                Catalogo Catalogo = await _catalogoRepository.GetCatalogoById(idCatalogo);
                if (Catalogo == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                CatalogoCompleteDTO objCatalogoDTO = _mapper.Map<CatalogoCompleteDTO>(Catalogo);
                return Ok(objCatalogoDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
        
        #region Get All
        [HttpGet("GetAllCatalogosByIDCompany")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoResultadoBusqueda>>> GetAllCatalogosByIDCompany(Guid idEmpresa)
        {
            try
            {
                List<Catalogo> listCatalogo = await _catalogoRepository.GetAllCatalogoByCompany(idEmpresa);

                if (listCatalogo.Count() < 1)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoResultadoBusqueda> listCatalogoDTO = _mapper.Map<List<CatalogoResultadoBusqueda>>(listCatalogo);
                Empresa empresaBusqueda = await _empresaRepository.GetCompanyById(idEmpresa);
                foreach (var item in listCatalogoDTO)
                {
                    item.Empresa = empresaBusqueda.RazonSocial;
                }
                

                 //llenar empresa de 

                return Ok(listCatalogoDTO);
            }
            catch (Exception Ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region Get By Name
        [HttpGet("GetCatalogoByName")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoCompleteDTO>>> GetCatalogoByName(string nameCatalogo)
        {
            try
            {
                List<Catalogo> listCatalago = await _catalogoRepository.GetCatalogoByName(nameCatalogo);
                if (listCatalago == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoCompleteDTO> listCatalogoDTO = _mapper.Map<List<CatalogoCompleteDTO>>(listCatalago);
                return Ok(listCatalogoDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region get by Codigo
        [HttpGet("GetCatalogoByCodeIDCompany")]
        [HttpHead]
        public async Task<ActionResult<CatalogoResultadoBusqueda>> GetCatalogoByCodeIDEmpresa(string codigoCatalgo, Guid idEmpresa)
        {
            try
            {
                Catalogo catalago = await _catalogoRepository.GetCatalogoByCodeIDEmpresa(codigoCatalgo, idEmpresa);
                if (catalago == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                CatalogoResultadoBusqueda objCatalogoDTO = _mapper.Map<CatalogoResultadoBusqueda>(catalago);

                return Ok(objCatalogoDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region get Childs
        [HttpGet("GetCatalogsChildsIDCompany")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> GetCatalogsChildsIDCompany(string codigoCatalgo, Guid? idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.GetChildByParentCodeIDEmpresa(codigoCatalgo, idEmpresa);
               
                List<CatalogoResultadoBusqueda> listaCatalogosDTO = _mapper.Map<List<CatalogoResultadoBusqueda>>(listaCatalogos);
                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region get Childs by ID Fathers
        [HttpGet("GetCatalogsChildsByID_Company")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> GetCatalogsChildsByID_Company(Guid idPadre, Guid? idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.GetChildByParentCodeByID_IDEmpresa(idPadre, idEmpresa);
               
                List<CatalogoDropDownDTO> listaCatalogosDTO = _mapper.Map<List<CatalogoDropDownDTO>>(listaCatalogos);

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        #endregion

        #region get Childs by Name Fathers
        [HttpGet("GetCatalogsChildsByName_Company")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> GetCatalogsChildsByName_Company(string nombrePadre, Guid idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.GetChildByParentCodeByID_Nombre(nombrePadre, idEmpresa);
                if (listaCatalogos == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoDropDownDTO> listaCatalogosDTO = _mapper.Map<List<CatalogoDropDownDTO>>(listaCatalogos);

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        #endregion

        #region get Childs by ID Fathers
        [HttpGet("GetCatalogsUpLevel_Company")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> GetCatalogsUpLevel_Company(Guid idCatalogo, Guid idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.GetCatalogsUpLevel_Company(idCatalogo, idEmpresa);
                if (listaCatalogos == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoDropDownDTO> listaCatalogosDTO = _mapper.Map<List<CatalogoDropDownDTO>>(listaCatalogos);

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region get Childs by ID Fathers
        [HttpGet("GetCatalogsSameLevelByID_Company")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> GetCatalogsSameLevelByID_Company(Guid idCatalogoHermano, Guid idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.GetCatalogsSameLevelByID_Company(idCatalogoHermano, idEmpresa);
                if (listaCatalogos == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoDropDownDTO> listaCatalogosDTO = _mapper.Map<List<CatalogoDropDownDTO>>(listaCatalogos);

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion

        #region optener catalogos hijos por codigo y empresa opcional
        [HttpGet("optenerCatalogosGeneral")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> optenerCatalogosGeneral(string codigoPadreCatalogo, Guid? idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.GetChildsByParentCode(codigoPadreCatalogo, idEmpresa);
                if (listaCatalogos == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoDropDownDTO> listaCatalogosDTO = _mapper.Map<List<CatalogoDropDownDTO>>(listaCatalogos);

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        #endregion
       
        [HttpGet("optenerCatalogosHermanoPorID")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> optenerCatalogosHermanoPorID(Guid idCatalogoHermano, Guid? idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.obtenerCatalogosHermanoPorIdCatalogo(idCatalogoHermano, idEmpresa);
                if (listaCatalogos == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoDropDownDTO> listaCatalogosDTO = _mapper.Map<List<CatalogoDropDownDTO>>(listaCatalogos);

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    

        [HttpGet("optenerCatalogosHermanoPorIdHijo")]
        [HttpHead]
        public async Task<ActionResult<List<CatalogoDropDownDTO>>> optenerCatalogosHermanoPorIdHijo (Guid idCatalogo, Guid? idEmpresa)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.obtenerCatalogosPadrePorIdHijo(idCatalogo, idEmpresa);
                if (listaCatalogos == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoDropDownDTO> listaCatalogosDTO = _mapper.Map<List<CatalogoDropDownDTO>>(listaCatalogos);

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet("BusquedaAvanzadaCatalogo")]
        public async Task<ActionResult<List<CatalogoResultadoBusqueda>>> BusquedaAvanzadaCatalogo(CatalogoBusquedaDTO objBusqueda)
        {
            try
            {
                List<Catalogo> listaCatalogos = await _catalogoRepository.BusquedaAvanzadaCatalogo(objBusqueda);
                if (listaCatalogos == null)
                {
                    return NotFound(MensajesRespuesta.sinResultados());
                }
                List<CatalogoResultadoBusqueda> listaCatalogosDTO = _mapper.Map<List<CatalogoResultadoBusqueda>>(listaCatalogos);

                Empresa objEmpresa = await _empresaRepository.GetCompanyById(objBusqueda.IdEmpresa);

                foreach (var resultado in listaCatalogosDTO)
                {
                    resultado.Empresa = objEmpresa.RazonSocial;
                }

                return Ok(listaCatalogosDTO);
            }
            catch (Exception ex)
            {

            }
            return StatusCode(StatusCodes.Status500InternalServerError);
        }




        #endregion

        #region Varios
        private async Task guardarLogs(string objetoJSON, string mensajeError)
        {
            LoggerAPI objLooger = new LoggerAPI(_logError);

            await objLooger.guardarError(this.ControllerContext.RouteData.Values["controller"].ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), mensajeError, objetoJSON);

        }
        #endregion
    }
}


