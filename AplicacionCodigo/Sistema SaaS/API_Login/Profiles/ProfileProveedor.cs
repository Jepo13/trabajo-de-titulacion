﻿using DTOs.Proveedor;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DTOs.Catalogo;
using EntidadesProveedores.Entities;
using EntidadesCatalogosNotas.Entities;

namespace API_Login.Profiles
{
    public class ProfileProveedor : Profile
    {
        public ProfileProveedor ()
        {
            CreateMap<ProveedorCompleteDTO, Proveedor>();
            CreateMap<Proveedor, ProveedorCompleteDTO>();

            CreateMap<ProveedorResultadoBusquedaDTO, Proveedor>();
            CreateMap<Proveedor, ProveedorResultadoBusquedaDTO>();

            CreateMap<CatalogoTipoEspecialidadDTO, Catalogo>();
            CreateMap<Catalogo, CatalogoTipoEspecialidadDTO>();

            CreateMap<ProveedorCreateDTO, Proveedor>();
            CreateMap<Proveedor, ProveedorCreateDTO>();

            CreateMap<ProveedorUpdateDTO, Proveedor>();
            CreateMap<Proveedor, ProveedorUpdateDTO>();
        }
    }
}
