﻿
using EntidadesPersonas.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTOs.User;
using EntidadesRolesUsuarios.Entities;
using DTOs.Persona;
using DTOs.Cliente;
using DTOs.EmpresaCliente;

namespace ComerciosEcuadorAPI.Profiles
{
    public class ProfilePersona : Profile
    {

        public ProfilePersona() 
        {
            #region Persona 
       
            CreateMap<PersonaCompleteDTO, Persona>();

            CreateMap<Persona, PersonaCreateDTO>();
            CreateMap<PersonaCreateDTO, Persona>();


            CreateMap<PersonaClienteUpdateDTO, Persona>();
            CreateMap<Persona, PersonaClienteUpdateDTO>();


            #endregion

            #region Cliente 
            CreateMap<Persona, PersonaClienteCreateDTO>();
            CreateMap<PersonaClienteCreateDTO, Persona>()
                .ForMember(x=>x.EmpresaClientes,y=> y.MapFrom(fuente=> fuente.ListEmpresaClientesDTO));

            CreateMap<Persona, PersonaClienteCompletoDTO>()
                .ForMember(x=>x.ListEmpresaClientesDTO, y=> y.MapFrom(fuente => fuente.EmpresaClientes));
            CreateMap<PersonaClienteCompletoDTO, Persona>();



 

            CreateMap<Persona, ClienteResultadoBusqueda>();
            CreateMap<ClienteResultadoBusqueda, Persona>();


            #endregion

            #region EmpresaCliente 
            CreateMap<EmpresaCliente, EmpresaClienteCompleteDTO>();
            CreateMap<EmpresaClienteCompleteDTO, EmpresaCliente>();


            CreateMap<EmpresaClienteUpdateDTO, EmpresaCliente>()
                .ForMember(x=>x.IdEmpresaCliente,y=>y.MapFrom(fuente=>fuente.IdEmpresaCliente_Update))
                .ForMember(x=>x.IdEmpresa,y=>y.MapFrom(fuente=>fuente.IdEmpresa_Update))
                .ForMember(x=>x.IdPersona,y=>y.MapFrom(fuente=>fuente.IdPersona_Update))
                .ForMember(x=>x.PorcentajeDescuento,y=>y.MapFrom(fuente=>fuente.PorcentajeDescuento_Update))
                .ForMember(x=>x.ValorCupoCreditoPersonal,y=>y.MapFrom(fuente=>fuente.ValorCupoCreditoPersonal_Update))
                .ForMember(x=>x.ValorCupoDisponibleCreditoPersonal,y=>y.MapFrom(fuente=>fuente.ValorCupoDisponibleCreditoPersonal_Update))
                .ForMember(x=>x.DiasCredito,y=>y.MapFrom(fuente=>fuente.DiasCredito_Update))
                .ForMember(x=>x.UsuarioModificacion,y=>y.MapFrom(fuente=>fuente.UsuarioModificacion_Update));

            CreateMap<EmpresaCliente, EmpresaClienteUpdateDTO>();

            CreateMap<EmpresaCliente, EmpresaClienteCreateDTO>();
            CreateMap<EmpresaClienteCreateDTO, EmpresaCliente>();

            
            #endregion





        }
    }
}
