﻿using DTOs.Local;
using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTOs.Catalogo;
using EntidadesEmpresa.Entities;
using EntidadesCatalogosNotas.Entities;
using DTOs.Ubicacion;
using DTOs.Sitios;
using DTOs.Bandejas;

namespace API_Login.Profiles
{
    public class ProfileLocal : Profile
    {
        public ProfileLocal()
        {
            CreateMap<LocalCompleteDTO, Local>()
                .ForMember(x=>x.Ubicacions, y=>y.MapFrom(fuente=>fuente.listaUbicacionesDTO != null? fuente.listaUbicacionesDTO: null));
            CreateMap<Local, LocalCompleteDTO>();

            CreateMap<LocalCreateDTO, Local>();
            CreateMap<Local, LocalCreateDTO>();

            CreateMap<LocalUpdateDTO, Local >();
            CreateMap<Local, LocalUpdateDTO>();
            
            CreateMap<LocalResultadoBusquedaDTO, Local >();

            CreateMap<Local, LocalResultadoBusquedaDTO>().ForMember(x=>x.nombreEmpresa,y=>y.MapFrom(fuente=>fuente.IdEmpresaNavigation != null ? fuente.IdEmpresaNavigation.RazonSocial:null ));
            
            CreateMap<CatalogoTipoLocalDTO, Catalogo>();
            CreateMap<Catalogo, CatalogoTipoLocalDTO>();

             CreateMap<CatalogoUbicacionDTO, Catalogo>();
            CreateMap<Catalogo, CatalogoUbicacionDTO>();


            CreateMap<Ubicacion, UbicacionCreateDTO>();
            CreateMap<UbicacionCreateDTO, Ubicacion>();
              // verificar 
            CreateMap<Ubicacion, UbicacionCompleteDTO>()
                .ForMember(x=>x.listaSitiosDTO, y=>y.MapFrom(fuente => fuente.Sitios));
            CreateMap<UbicacionCompleteDTO, Ubicacion>();

            CreateMap<Ubicacion, UbicacionUpdateDTO>();
            CreateMap<UbicacionUpdateDTO, Ubicacion>();

            CreateMap<SitioCreateDTO, Sitio>();
            CreateMap<Sitio, SitioCreateDTO>();

            CreateMap<SitiosCompletoDTO, Sitio>();
            CreateMap<Sitio, SitiosCompletoDTO>();


            CreateMap<Bandeja, BandejaCreateDTO>();
            CreateMap<BandejaCreateDTO, Bandeja>();


        }
    }
}
