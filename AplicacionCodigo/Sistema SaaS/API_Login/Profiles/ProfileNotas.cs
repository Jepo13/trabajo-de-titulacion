﻿using AutoMapper;
using DTOs.Notas;
using EntidadesCatalogosNotas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileNotas : Profile
    {
        public ProfileNotas()
        {
            CreateMap<Nota, NotasCompleteDTO >();
            CreateMap<NotasCompleteDTO, Nota>();

            CreateMap<Nota, NotasCreateDTO>();
            CreateMap<NotasCreateDTO, Nota>();

            CreateMap<Nota, NotasUpdateDTO>();
            CreateMap<NotasUpdateDTO, Nota>();
        }
    }
}
