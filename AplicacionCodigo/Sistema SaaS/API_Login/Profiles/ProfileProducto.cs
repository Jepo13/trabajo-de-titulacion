﻿using AutoMapper;
using DTOs.Productos;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileProducto : Profile
    {
        public ProfileProducto()
        {
            CreateMap<ProductoDTOCrear, Producto>();
            CreateMap<Producto, ProductoDTOCrear>();

            CreateMap<ProductoDTOCompleto, Producto>();
            CreateMap<Producto, ProductoDTOCompleto>();

            CreateMap<ProductoDTOEditar, Producto>();
            CreateMap<Producto, ProductoDTOEditar>();
        }
    }
}
