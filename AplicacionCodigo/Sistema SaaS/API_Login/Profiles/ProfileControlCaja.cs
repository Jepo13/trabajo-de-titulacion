﻿using DTOs.ControlCaja;
using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntidadesEmpresa.Entities;

namespace API_Login.Profiles
{
    public class ProfileControlCaja : Profile
    {
        public ProfileControlCaja()
        {
            CreateMap<ControlCajaCompleteDTO, ControlCaja>();
            CreateMap<ControlCaja, ControlCajaCompleteDTO>();
            
            CreateMap<ControlCajaCreateDTO, ControlCaja>();           
            CreateMap<ControlCaja, ControlCajaCreateDTO>();
            
            CreateMap<ControlCajaUpdateDTO, ControlCaja>();
            CreateMap<ControlCaja, ControlCajaUpdateDTO>();
        }
    }
}
