﻿using AutoMapper;
using DTOs.CategoriaProductos;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileCategoria : Profile
    {
        public ProfileCategoria()
        {
            CreateMap<CategoriaProducto, CategoriaProductoDTOCrear>();
            CreateMap<CategoriaProductoDTOCrear, CategoriaProducto>();

            CreateMap<CategoriaProducto, CategoriaProductoDTOCompleto>()
                .ForMember(x => x.NombreCategoriaPadre, y => y.MapFrom(fuente => fuente.IdCategoriaProductoPadreNavigation !=null ? fuente.IdCategoriaProductoPadreNavigation.NombreCategoriaProducto : ""));
            CreateMap<CategoriaProductoDTOCompleto, CategoriaProducto>();

            CreateMap<CategoriaProducto, CategoriaProductoDTOActualizar>();
            CreateMap<CategoriaProductoDTOActualizar, CategoriaProducto>();
        }
    }
}
