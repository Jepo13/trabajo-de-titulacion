﻿using AutoMapper;
using DTOs.Catalogo;
using EntidadesCatalogosNotas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileCatalogo : Profile
    {
        public ProfileCatalogo()
        {
            CreateMap<Catalogo, CatalogoCreateDTO>();
            CreateMap<CatalogoCreateDTO, Catalogo>();

            CreateMap<Catalogo, CatalogoCompleteDTO>().
                ForMember(x => x.CodigoCatalogoPadre, y => y.MapFrom(fuente => fuente.IdCatalogopadreNavigation.CodigoCatalogo)).
                ForMember(x => x.NombreCatalogoPadre, y => y.MapFrom(fuente => fuente.IdCatalogopadreNavigation.NombreCatalogo));
            CreateMap<CatalogoCompleteDTO, Catalogo>();

            CreateMap<Catalogo, CatalogoUpdateDTO>();
            CreateMap<CatalogoUpdateDTO, Catalogo>();

            CreateMap<Catalogo, CatalogoEditarDTO>();
            CreateMap<CatalogoEditarDTO, Catalogo>();

            CreateMap<Catalogo, CatalogoResultadoBusqueda>().
                ForMember(x => x.CodigoCatalogoPadre, y => y.MapFrom(fuente => fuente.IdCatalogopadreNavigation.CodigoCatalogo)).
                ForMember(x => x.NombreCatalogoPadre, y => y.MapFrom(fuente => fuente.IdCatalogopadreNavigation.NombreCatalogo));

            CreateMap<CatalogoResultadoBusqueda, Catalogo>();

            CreateMap<Catalogo, CatalogoDropDownDTO>();
            CreateMap<CatalogoDropDownDTO, Catalogo>();

        }
    }
}
