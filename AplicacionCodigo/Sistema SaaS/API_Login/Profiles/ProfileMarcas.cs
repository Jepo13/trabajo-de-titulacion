﻿using DTOs.Marca;
using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntidadesProductos.Entities;

namespace API_Login.Profiles
{
    public class ProfileMarcas : Profile
    {
        public ProfileMarcas() {

            CreateMap<MarcaCompleteDTO, Marca>();
            CreateMap<Marca, MarcaCompleteDTO>();

            CreateMap<MarcaCreateDTO, Marca>();
            CreateMap<Marca, MarcaCreateDTO>();

            CreateMap<MarcaUpdateDTO, Marca>();
            CreateMap<Marca, MarcaUpdateDTO>();
        }

    }
 }
