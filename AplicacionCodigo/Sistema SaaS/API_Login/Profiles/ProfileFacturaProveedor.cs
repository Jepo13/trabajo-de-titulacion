﻿using DTOs.Proveedor;
using AutoMapper;

using EntidadesProveedores.Entities;

namespace API_Login.Profiles
{
    public class ProfileFacturaProveedor : Profile
    {
        public ProfileFacturaProveedor()
        {
            CreateMap<ProveedorCompleteDTO, ProfileFacturaProveedor>();
            CreateMap<Proveedor, ProveedorCompleteDTO>();

            CreateMap<ProveedorCreateDTO, ProfileFacturaProveedor>();
            CreateMap<ProfileFacturaProveedor, ProveedorCreateDTO>();

            CreateMap<ProveedorUpdateDTO, ProfileFacturaProveedor>();
            CreateMap<ProfileFacturaProveedor, ProveedorUpdateDTO>();
        }
    }
}
