﻿using AutoMapper;
using DTOs.Rol;
using EntidadesRolesUsuarios.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComerciosEcuadorAPI.Profiles
{
    public class ProfileRols : Profile
    {
        public ProfileRols()
        {
            CreateMap<Rol, RolBusquedaDTO>();
            CreateMap<RolBusquedaDTO, Rol>();

            CreateMap<Rol, RolCreacionDTO>().ForMember(x => x.listaModulos, y => y.MapFrom(f => f.Modulos));
            CreateMap<RolCreacionDTO, Rol>().ForMember(x => x.Modulos, y => y.MapFrom(f => f.listaModulos));

            CreateMap<Modulo, ModuloCreacionDTO>().ForMember(x => x.nombreModulo, y => y.MapFrom(fuente => fuente.Nombre))
                .ForMember(x => x.rolesMenu, y => y.MapFrom(f => f.Menus));
            CreateMap<Modulo, ModuloEditarnDTO>().ForMember(x => x.nombreModulo, y => y.MapFrom(fuente => fuente.Nombre))
                .ForMember(x => x.rolesMenu, y => y.MapFrom(f => f.Menus));

            CreateMap<ModuloCreacionDTO, Modulo>().ForMember(x => x.Nombre, y => y.MapFrom(fuente => fuente.nombreModulo))
                .ForMember(x => x.Menus, y => y.MapFrom(f => f.rolesMenu));

            CreateMap<Modulo, ModuloBusquedaDTO>().ForMember(x => x.nombreModulo, y => y.MapFrom(fuente => fuente.Nombre));

            CreateMap<Menu, RolesMenu>().ForMember(x => x.permisosRoles, y => y.MapFrom(f => f.Permisos));
            CreateMap<Menu, RolesMenuEditarDTO>().ForMember(x => x.permisosRoles, y => y.MapFrom(f => f.Permisos));
            CreateMap<RolesMenu, Menu>().ForMember(x => x.Permisos, y => y.MapFrom(f => f.permisosRoles));
            CreateMap<RolesMenuEditarDTO, Menu>().ForMember(x => x.Permisos, y => y.MapFrom(f => f.permisosRoles));

            CreateMap<Permiso, PermisosRol>();
            CreateMap<Permiso, PermisosRolEditarDTO>();
            CreateMap<PermisosRol, Permiso>();
            CreateMap<PermisosRolEditarDTO, Permiso>();

            CreateMap<Rol, RolEditarDTO>().ForMember(x => x.listaModulos, y => y.MapFrom(f => f.Modulos));
            CreateMap<RolEditarDTO, Rol>().ForMember(x => x.Modulos, y => y.MapFrom(f => f.listaModulos));

             CreateMap<Rol, RolCompletoDTO>().ForMember(x => x.listaModulos, y => y.MapFrom(f => f.Modulos));
            CreateMap<RolCompletoDTO, Rol>().ForMember(x => x.Modulos, y => y.MapFrom(f => f.listaModulos));


            CreateMap<ModuloEditarnDTO, Modulo>().ForMember(x => x.Nombre, y => y.MapFrom(fuente => fuente.nombreModulo))
                .ForMember(x => x.Menus, y => y.MapFrom(f => f.rolesMenu));



            CreateMap<Permiso, PermisosRolEditarDTO>();
            CreateMap<PermisosRolEditarDTO, Permiso>();
        }


    }
}
