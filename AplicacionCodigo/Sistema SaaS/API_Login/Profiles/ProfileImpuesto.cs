﻿using AutoMapper;
using DTOs.Impuesto;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileImpuesto : Profile
    {
        public ProfileImpuesto() 
        {
            CreateMap<ImpuestoCreateDTO, Impuesto>();
            CreateMap<Impuesto, ImpuestoCreateDTO>();

            CreateMap<ImpuestoCompleteDTO, Impuesto>();
            CreateMap<Impuesto, ImpuestoCompleteDTO>();

            CreateMap<ImpuestoUpdateDTO, Impuesto>();
            CreateMap<Impuesto, ImpuestoUpdateDTO>();
        }
    }
}
