﻿using DTOs.Empresa;
using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntidadesEmpresa.Entities;

namespace API_Login.Profiles
{
    public class ProfileCompany : Profile
    {
        public ProfileCompany()
        {
            CreateMap<EmpresaCompletoDTO, Empresa>();
            CreateMap<Empresa, EmpresaCompletoDTO>();

            CreateMap<EmpresaCreateDTO, Empresa>().
                ForMember(x => x.UsuarioEmpresas, y => y.MapFrom(fuent =>  new List<EmpresaUsuarioDTO> { new EmpresaUsuarioDTO { IdUsuario = fuent.IdUsuario } } ));
            CreateMap<Empresa, EmpresaCreateDTO>();     
            CreateMap<EmpresaUsuarioDTO, UsuarioEmpresa>();     
            

            CreateMap<EmpresaUpdateDTO, Empresa>();
            CreateMap<Empresa, EmpresaUpdateDTO>();

            CreateMap<Empresa, EmpresaResultadoBusquedaDTO>();
            CreateMap<EmpresaResultadoBusquedaDTO, Empresa>();
        }

    }
}
