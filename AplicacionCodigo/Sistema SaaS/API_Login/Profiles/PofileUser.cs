﻿using DTOs.User;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTOs.Empresa;
using DTOs.Rol;
using EntidadesRolesUsuarios.Entities;
using EntidadesEmpresa.Entities;
using EntidadesPersonas.Entities;

namespace API_Login.Profiles
{
    public class PofileUser : Profile
    {
        public PofileUser()
        {
            CreateMap<UserCompleteDTO, Persona>();
            CreateMap<Persona, UserCompleteDTO>();

            CreateMap<UserCompleteDTO, Usuario>()
                .ForMember(x => x.Estado, y => y.MapFrom(fuente => fuente.EstadoUsuario));
            CreateMap<Usuario, UserCompleteDTO>();

            CreateMap<Usuario, UserUpdateDTO>();
            CreateMap<UserUpdateDTO, Usuario>();
            CreateMap<UserUpdateDTO, Persona>();

            CreateMap<UserSessionDTO, Usuario>();
            CreateMap<Usuario, UserSessionDTO>().
                ForMember(x => x.NombreRol, y => y.MapFrom(fuent => fuent.IdRolNavigation.NombreRol)).
               ForMember(x => x.Modulos, y => y.MapFrom(fuent => fuent.IdRolNavigation.Modulos));
     
            CreateMap<Modulo, ModuloDTO>()
                .ForMember(x => x.listaMenuDTO, y => y.MapFrom(f => f.Menus));
            CreateMap<Menu, MenuDTO>()
                .ForMember(x => x.NombreMenu, y => y.MapFrom(f => f.NombreMenu))
                .ForMember(x => x.RutaMenu, y => y.MapFrom(f => f.RutaMenu))
                .ForMember(x => x.listaPermisosDTO, y => y.MapFrom(f => f.Permisos));
            CreateMap<Permiso, PermisosDTO>();

            CreateMap<UsuarioEmpresa, EmpresaResultadoBusquedaDTO>().
                ForMember(x => x.Razonsocial, y => y.MapFrom(fuent => fuent.IdEmpresaNavigation.RazonSocial)).
                ForMember(x => x.Ruc, y => y.MapFrom(fuent => fuent.IdEmpresaNavigation.Ruc)).
                ForMember(x => x.IdEmpresa, y => y.MapFrom(fuent => fuent.IdEmpresa)).
                ForMember(x => x.Estado, y => y.MapFrom(fuent => fuent.IdEmpresaNavigation.Estado));
            
            CreateMap<UserCreateDTO, Usuario>();

            CreateMap<UserCreateDTO, Persona>();

            CreateMap<UsuarioEmpresa, EmpresaUsuarioDTO>()
                .ForMember(x => x.Razonsocial, y => y.MapFrom(fuente => fuente.IdEmpresaNavigation.RazonSocial));

            CreateMap<EmpresaUsuarioDTO, UsuarioEmpresa>()
                .ForMember(x => x.IdEmpresa, y => y.MapFrom(fuent => fuent.IdEmpresa))
                .ForMember(x => x.IdUsuario, y => y.MapFrom(fuent => fuent.IdUsuario));

            CreateMap<EmpresaUsuarioDTO, UsuarioEmpresa>()
                .ForMember(x => x.IdEmpresa, y => y.MapFrom(fuent => fuent.IdEmpresa))
                .ForMember(x => x.IdUsuario, y => y.MapFrom(fuent => fuent.IdUsuario));

            CreateMap<UsuarioEmpresaCompletoDTO, UsuarioEmpresa>();
            CreateMap<UsuarioEmpresa, UsuarioEmpresaCompletoDTO>();

            CreateMap<UserResultadoBusquedaDTO, Usuario>();
            CreateMap<Usuario, UserResultadoBusquedaDTO>()
                .ForMember(x => x.Perfil, y => y.MapFrom(fuente => fuente.IdRolNavigation.NombreRol))
                .ForMember(x => x.Estado, y => y.MapFrom(fuente => fuente.Estado));          
        }
    }
}

