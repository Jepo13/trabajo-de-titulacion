﻿using DTOs.Caja;
using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntidadesEmpresa.Entities;

namespace API_Login.Profiles
{
    public class ProfileCaja : Profile 
    {
        public ProfileCaja()
        {
            CreateMap<CajaCompleteDTO, Caja>();
            CreateMap<Caja, CajaCompleteDTO>();

            CreateMap<CajaCreateDTO, Caja>();
            CreateMap<Caja, CajaCreateDTO>();

            CreateMap<CajaUpdateDTO, Caja>();
            CreateMap<Caja, CajaUpdateDTO>();
        }
    }
}
