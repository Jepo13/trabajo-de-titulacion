﻿using AutoMapper;
using DTOs.EstadoVenta;
using EntidadesFactura.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileEstadoVenta : Profile
    {
        public ProfileEstadoVenta()
        {
            CreateMap<EstadoVentum, EstadoVentaCompleteDTO>();
            CreateMap<EstadoVentaCompleteDTO, EstadoVentum>();

            CreateMap <EstadoVentaCreateDTO, EstadoVentum>();
            CreateMap<EstadoVentum, EstadoVentaCreateDTO>();

            CreateMap<EstadoVentaUpdateDTO, EstadoVentum>();
            CreateMap<EstadoVentum, EstadoVentaUpdateDTO>();
        }
    }
}
