﻿using AutoMapper;
using DTOs.EstadoProducto;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileEstadoProducto : Profile
    {
        public ProfileEstadoProducto()
        {
            CreateMap<EstadoProductoCreateDTO, EstadoProducto>();
            CreateMap<EstadoProducto, EstadoProductoCreateDTO>();

            CreateMap<EstadoProductoCompleteDTO, EstadoProducto>();
            CreateMap<EstadoProducto, EstadoProductoCompleteDTO>();

            CreateMap<EstadoProductoUpdateDTO, EstadoProducto>();
            CreateMap<EstadoProducto, EstadoProductoUpdateDTO>();
        }
    }
}
