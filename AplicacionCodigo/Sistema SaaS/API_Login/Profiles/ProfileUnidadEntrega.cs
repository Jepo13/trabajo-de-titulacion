﻿using AutoMapper;
using DTOs.UnidadEntrega;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileUnidadEntrega : Profile
    {
        public ProfileUnidadEntrega () 
        {
            CreateMap<UnidadEntregaCreateDTO, UnidadEntrega>();
            CreateMap<UnidadEntrega, UnidadEntregaCreateDTO>();

            CreateMap<UnidadEntregaCompleteDTO, UnidadEntrega>();
            CreateMap<UnidadEntrega, UnidadEntregaCompleteDTO>();

            CreateMap<UnidadEntregaUpdateDTO, UnidadEntrega>();
            CreateMap<UnidadEntrega, UnidadEntregaUpdateDTO>();
        }
    }
}
