﻿using AutoMapper;
using DTOs.GrupoProducto;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Login.Profiles
{
    public class ProfileGrupoProducto : Profile
    {
        public ProfileGrupoProducto()
        {
            CreateMap<GrupoProductoCreateDTO, GrupoProducto>();
            CreateMap<GrupoProducto, GrupoProductoCreateDTO>();

            CreateMap<GrupoProductoCompleteDTO, GrupoProducto>();
            CreateMap<GrupoProducto, GrupoProductoCompleteDTO>();

            CreateMap<GrupoProductoUpdateDTO, GrupoProducto>();
            CreateMap<GrupoProducto, GrupoProductoUpdateDTO>();
        }
    }
}
