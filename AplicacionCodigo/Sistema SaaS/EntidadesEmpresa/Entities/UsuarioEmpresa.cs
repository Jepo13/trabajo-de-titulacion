﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class UsuarioEmpresa
    {
        public Guid IdUsuairoEmpresa { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid? IdUsuario { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
    }
}
