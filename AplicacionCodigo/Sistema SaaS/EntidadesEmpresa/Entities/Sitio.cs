﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class Sitio
    {
        public Sitio()
        {
            Bandejas = new HashSet<Bandeja>();
        }

        public Guid IdSitio { get; set; }
        public Guid? IdUbicacion { get; set; }
        public string NombreSitio { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Ubicacion IdUbicacionNavigation { get; set; }
        public virtual ICollection<Bandeja> Bandejas { get; set; }
    }
}
