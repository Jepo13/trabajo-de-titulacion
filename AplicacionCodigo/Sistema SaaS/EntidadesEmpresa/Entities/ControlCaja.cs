﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class ControlCaja
    {
        public Guid IdControlCaja { get; set; }
        public Guid? IdCaja { get; set; }
        public Guid? IdPersona { get; set; }
        public Guid? IdUsuario { get; set; }
        public DateTime FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }
        public decimal ValorInicial { get; set; }
        public decimal ValorFacturaVentaAnulada { get; set; }
        public int CantidadVentasCaja { get; set; }
        public decimal VentasEfectivo { get; set; }
        public decimal VentasNotaCredito { get; set; }
        public decimal VentasCreditoPersonal { get; set; }
        public decimal ValorNotasCreditoGeneradas { get; set; }
        public bool? ReporteCorrecto { get; set; }
        public string Observacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Caja IdCajaNavigation { get; set; }
    }
}
