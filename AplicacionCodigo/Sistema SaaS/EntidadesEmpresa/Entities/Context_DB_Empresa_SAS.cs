﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class Context_DB_Empresa_SAS : DbContext
    {
        public Context_DB_Empresa_SAS()
        {
        }

        public Context_DB_Empresa_SAS(DbContextOptions<Context_DB_Empresa_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<Bandeja> Bandejas { get; set; }
        public virtual DbSet<Caja> Cajas { get; set; }
        public virtual DbSet<ControlCaja> ControlCajas { get; set; }
        public virtual DbSet<Empresa> Empresas { get; set; }
        public virtual DbSet<Local> Locals { get; set; }
        public virtual DbSet<Sitio> Sitios { get; set; }
        public virtual DbSet<Ubicacion> Ubicacions { get; set; }
        public virtual DbSet<UsuarioEmpresa> UsuarioEmpresas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Empresas_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<Bandeja>(entity =>
            {
                entity.HasKey(e => e.IdBandeja);

                entity.ToTable("BANDEJA");

                entity.Property(e => e.IdBandeja)
                    .HasColumnName("ID_BANDEJA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdSitio)
                    .HasColumnName("ID_SITIO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.NombreBandeja)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE_BANDEJA");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdSitioNavigation)
                    .WithMany(p => p.Bandejas)
                    .HasForeignKey(d => d.IdSitio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BANDEJA_REFERENCE_SITIO");
            });

            modelBuilder.Entity<Caja>(entity =>
            {
                entity.HasKey(e => e.IdCaja);

                entity.ToTable("CAJA");

                entity.Property(e => e.IdCaja)
                    .HasColumnName("ID_CAJA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdLocal).HasColumnName("ID_LOCAL");

                entity.Property(e => e.NombreCaja)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("NOMBRE_CAJA");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.Property(e => e.Usuariocreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIOCREACION_");

                entity.HasOne(d => d.IdLocalNavigation)
                    .WithMany(p => p.Cajas)
                    .HasForeignKey(d => d.IdLocal)
                    .HasConstraintName("FK_CAJA_REFERENCE_LOCAL");
            });

            modelBuilder.Entity<ControlCaja>(entity =>
            {
                entity.HasKey(e => e.IdControlCaja);

                entity.ToTable("CONTROL_CAJA");

                entity.Property(e => e.IdControlCaja)
                    .HasColumnName("ID_CONTROL_CAJA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CantidadVentasCaja).HasColumnName("CANTIDAD_VENTAS_CAJA");

                entity.Property(e => e.FechaApertura)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_APERTURA");

                entity.Property(e => e.FechaCierre)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CIERRE");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdCaja).HasColumnName("ID_CAJA");

                entity.Property(e => e.IdPersona).HasColumnName("ID_PERSONA");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_USUARIO");

                entity.Property(e => e.Observacion)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OBSERVACION");

                entity.Property(e => e.ReporteCorrecto).HasColumnName("REPORTE_CORRECTO");

                entity.Property(e => e.UsuarioCreacion)
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.Property(e => e.ValorFacturaVentaAnulada)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_FACTURA_VENTA_ANULADA");

                entity.Property(e => e.ValorInicial)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_INICIAL");

                entity.Property(e => e.ValorNotasCreditoGeneradas)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_NOTAS_CREDITO_GENERADAS");

                entity.Property(e => e.VentasCreditoPersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VENTAS_CREDITO_PERSONAL");

                entity.Property(e => e.VentasEfectivo)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VENTAS_EFECTIVO");

                entity.Property(e => e.VentasNotaCredito)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VENTAS_NOTA_CREDITO");

                entity.HasOne(d => d.IdCajaNavigation)
                    .WithMany(p => p.ControlCajas)
                    .HasForeignKey(d => d.IdCaja)
                    .HasConstraintName("FK_CONTROL__REFERENCE_CAJA");
            });

            modelBuilder.Entity<Empresa>(entity =>
            {
                entity.HasKey(e => e.IdEmpresa);

                entity.ToTable("EMPRESA");

                entity.HasIndex(e => e.Ruc, "AK_UNICORUC_EMPRESA")
                    .IsUnique();

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("ID_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.RazonSocial)
                    .IsRequired()
                    .HasMaxLength(35)
                    .HasColumnName("RAZON_SOCIAL");

                entity.Property(e => e.Ruc)
                    .IsRequired()
                    .HasMaxLength(13)
                    .HasColumnName("RUC");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(35)
                    .HasColumnName("TELEFONO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            modelBuilder.Entity<Local>(entity =>
            {
                entity.HasKey(e => e.IdLocal);

                entity.ToTable("LOCAL");

                entity.Property(e => e.IdLocal)
                    .HasColumnName("ID_LOCAL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.DireccionLocal)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("DIRECCION_LOCAL");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdTipoLocal)
                    .HasColumnName("ID_TIPO_LOCAL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdUbicacion).HasColumnName("ID_UBICACION");

                entity.Property(e => e.NombreLocal)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("NOMBRE_LOCAL");

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("OBSERVACIONES");

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("TELEFONO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Locals)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_LOCAL_REFERENCE_EMPRESA");
            });

            modelBuilder.Entity<Sitio>(entity =>
            {
                entity.HasKey(e => e.IdSitio);

                entity.ToTable("SITIO");

                entity.Property(e => e.IdSitio)
                    .HasColumnName("ID_SITIO")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdUbicacion)
                    .HasColumnName("ID_UBICACION")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.NombreSitio)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("NOMBRE_SITIO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdUbicacionNavigation)
                    .WithMany(p => p.Sitios)
                    .HasForeignKey(d => d.IdUbicacion)
                    .HasConstraintName("FK_SITIO_REFERENCE_UBICACIO");
            });

            modelBuilder.Entity<Ubicacion>(entity =>
            {
                entity.HasKey(e => e.IdUbicacion);

                entity.ToTable("UBICACION");

                entity.Property(e => e.IdUbicacion)
                    .HasColumnName("ID_UBICACION")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdLocal).HasColumnName("ID_LOCAL");

                entity.Property(e => e.IdTipoUbicacion).HasColumnName("ID_TIPO_UBICACION");

                entity.Property(e => e.NombreUbicacion)
                    .HasMaxLength(30)
                    .HasColumnName("NOMBRE_UBICACION");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.HasOne(d => d.IdLocalNavigation)
                    .WithMany(p => p.Ubicacions)
                    .HasForeignKey(d => d.IdLocal)
                    .HasConstraintName("FK_UBICACIO_REFERENCE_LOCAL");
            });

            modelBuilder.Entity<UsuarioEmpresa>(entity =>
            {
                entity.HasKey(e => e.IdUsuairoEmpresa);

                entity.ToTable("USUARIO_EMPRESA");

                entity.Property(e => e.IdUsuairoEmpresa)
                    .HasColumnName("ID_USUAIRO_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("ID_EMPRESA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_USUARIO")
                    .HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.UsuarioEmpresas)
                    .HasForeignKey(d => d.IdEmpresa)
                    .HasConstraintName("FK_USUARIO__REFERENCE_EMPRESA");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
