﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class Empresa
    {
        public Empresa()
        {
            Locals = new HashSet<Local>();
            UsuarioEmpresas = new HashSet<UsuarioEmpresa>();
        }

        public Guid IdEmpresa { get; set; }
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public string Telefono { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<Local> Locals { get; set; }
        public virtual ICollection<UsuarioEmpresa> UsuarioEmpresas { get; set; }
    }
}
