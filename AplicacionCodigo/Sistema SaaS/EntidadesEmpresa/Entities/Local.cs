﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class Local
    {
        public Local()
        {
            Cajas = new HashSet<Caja>();
            Ubicacions = new HashSet<Ubicacion>();
        }

        public Guid IdLocal { get; set; }
        public Guid? IdUbicacion { get; set; }
        public Guid? IdEmpresa { get; set; }
        public Guid IdTipoLocal { get; set; }
        public string NombreLocal { get; set; }
        public string DireccionLocal { get; set; }
        public string Telefono { get; set; }
        public string Observaciones { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Empresa IdEmpresaNavigation { get; set; }
        public virtual ICollection<Caja> Cajas { get; set; }
        public virtual ICollection<Ubicacion> Ubicacions { get; set; }
    }
}
