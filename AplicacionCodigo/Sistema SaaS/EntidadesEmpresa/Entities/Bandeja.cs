﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class Bandeja
    {
        public Guid IdBandeja { get; set; }
        public Guid IdSitio { get; set; }
        public string NombreBandeja { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Sitio IdSitioNavigation { get; set; }
    }
}
