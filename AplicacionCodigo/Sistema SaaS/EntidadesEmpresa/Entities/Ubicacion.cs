﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class Ubicacion
    {
        public Ubicacion()
        {
            Sitios = new HashSet<Sitio>();
        }

        public Guid IdUbicacion { get; set; }
        public Guid? IdLocal { get; set; }
        public Guid IdTipoUbicacion { get; set; }
        public string NombreUbicacion { get; set; }
        public bool Estado { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Local IdLocalNavigation { get; set; }
        public virtual ICollection<Sitio> Sitios { get; set; }
    }
}
