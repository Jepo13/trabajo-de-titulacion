﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesEmpresa.Entities
{
    public partial class Caja
    {
        public Caja()
        {
            ControlCajas = new HashSet<ControlCaja>();
        }

        public Guid IdCaja { get; set; }
        public Guid? IdLocal { get; set; }
        public string NombreCaja { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string Usuariocreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Local IdLocalNavigation { get; set; }
        public virtual ICollection<ControlCaja> ControlCajas { get; set; }
    }
}
