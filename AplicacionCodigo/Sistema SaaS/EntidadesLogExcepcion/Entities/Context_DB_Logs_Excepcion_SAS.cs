﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesLogExcepcion.Entities
{
    public partial class Context_DB_Logs_Excepcion_SAS : DbContext
    {
        public Context_DB_Logs_Excepcion_SAS()
        {
        }

        public Context_DB_Logs_Excepcion_SAS(DbContextOptions<Context_DB_Logs_Excepcion_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<LogExcepcione> LogExcepciones { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Logs_Excepcion_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<LogExcepcione>(entity =>
            {
                entity.HasKey(e => e.IdLogExcepciones);

                entity.ToTable("LOG_EXCEPCIONES");

                entity.Property(e => e.IdLogExcepciones)
                    .HasColumnName("ID_LOG_EXCEPCIONES")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Entidad)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("ENTIDAD");

                entity.Property(e => e.Error)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("ERROR");

                entity.Property(e => e.FechaError)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_ERROR");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_USUARIO");

                entity.Property(e => e.Metodo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("METODO");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
