﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesLogExcepcion.Entities
{
    public partial class LogExcepcione
    {
        public Guid IdLogExcepciones { get; set; }
        public Guid? IdUsuario { get; set; }
        public string Metodo { get; set; }
        public string Entidad { get; set; }
        public string Error { get; set; }
        public DateTime FechaError { get; set; }
        public string Descripcion { get; set; }
    }
}
