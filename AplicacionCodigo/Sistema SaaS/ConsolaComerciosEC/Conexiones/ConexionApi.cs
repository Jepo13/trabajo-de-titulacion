﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsolaComerciosEC.Conexiones
{
    public class ConexionApi
    {
        public HttpClient Client { get; set; }
        public ConexionApi(HttpClient client)
        {
            Client = client;
        }
    }
}