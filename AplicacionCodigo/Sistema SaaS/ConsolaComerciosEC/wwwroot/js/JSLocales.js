﻿function recuperarLugaresProvinciasCantonesCiudades(idSelectPadre, idSelecHijo, seleccionHijo) {
    let elemento = document.getElementById(idSelectPadre);
    if (elemento != undefined)
    {
        return new Promise((resolve, reject) => {
        try
        {
            var idSeleccion = elemento.options[elemento.selectedIndex].value;
            //Elemento en donde se colocará el resultado
            let selectCiudad = document.getElementById(idSelecHijo);

            //let selectEmpresa = document.getElementById("IdEmpresadefault");
            //var idSeleccionEmpresa = selectEmpresa.options[selectEmpresa.selectedIndex].value;

            var options = document.querySelectorAll('#' + idSelecHijo + ' option');
            options.forEach(o => o.remove());
            option = document.createElement('option');
            option.text = "Seleccionar";
            selectCiudad.add(option);
            //alert(value)

            $.ajax({
                url: "/C_Local/cargarListaDropDownCiudades?idPadreCatalogoCiudad=" + idSeleccion,
                type: "get",
/*                data: formData,*/
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) 
                    { 


            //var xhttp = new XMLHttpRequest();
            //xhttp.open("GET", "/C_Local/cargarListaDropDownCiudades?idPadreCatalogoCiudad=" + idSeleccion /*+ "&idEmpresa=" + idSeleccionEmpresa*/, true);
            //xhttp.send();

            //xhttp.onreadystatechange = function () {
                /*if (this.readyState == 4 && this.status == 200) {*/
        /*            var directo = xhttp.response*/
                    let responseRead = JSON.stringify(result);
                    let listaCiudades = JSON.parse(responseRead);

                 /*   var listaCiudades = JSON.parse(directo)*/
                    if (listaCiudades.length > 0) {
                        let contadorOpciones = 0
                        for (var ciudad of listaCiudades) {
                            option = document.createElement('option');

                            option.text = ciudad.nombrecatalogo;
                            option.value = ciudad.idCatalogo;

                            selectCiudad.add(option);

                            if (seleccionHijo != undefined)
                            {
                                contadorOpciones++
                                if (seleccionHijo == option.value) {
                                    selectCiudad.selectedIndex = contadorOpciones;
                                    console.log("Se seleccionar " + seleccionHijo + " Posición: " + contadorOpciones)
                                }
                            }
                        }
                    }
                    else {
                        Swal.fire({
                            title: 'Advertencia',
                            text: "No existen ciudades para la provincia seleccionada.",
                            icon: 'info',
                        })
                    }
                },                
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        title: 'Error Inesperado',
                        text: "Ocurrió un error al recuperar las ciudades, por favor intente más tarde.",
                        icon: 'error',
                    })
                },
                complete: function (data) {
                    resolve("OK")
                }
            });

        } catch (error)
        {
            console.log("Ocurrio un error" + error)

        }

        })

    }

};

//////////////////////// Ubicaciones -> bandejas /////////////
//Cambia algunos atributos incluyendo ID del modal para crear Ubicaciones
function crearNuevaBandeja(IdSitio_atributo, IdUbicacion_atributo, IdSitioHtml,
    IdUbicacionHtml, nombreModal, Contador, btn_idCerrarModal_cabecera, btn_idCerrarModal_Foter, btn_FormularioModal)
{
    let IdUbicacion = document.getElementById(IdUbicacionHtml)
    let IdSitio = document.getElementById(IdSitioHtml)
    let elementor_Btn_FormularioModal = document.getElementById(btn_FormularioModal)


    IdUbicacion.value = IdUbicacion_atributo
    IdSitio.value = IdSitio_atributo
    let nuevoIdModal = 'ModalSitio_' + Contador

    let campoDIVModal = document.getElementsByName(nombreModal)[0]
    campoDIVModal.setAttribute("id", nuevoIdModal)


    let Id_btn_cerrarModal_Cabecera = document.getElementById(btn_idCerrarModal_cabecera)
    Id_btn_cerrarModal_Cabecera.setAttribute("onclick", "cerrarModal('" + nuevoIdModal + "')")
    let Id_btn_cerrarModal_Foter = document.getElementById(btn_idCerrarModal_Foter)
    Id_btn_cerrarModal_Foter.setAttribute("onclick", "cerrarModal('" + nuevoIdModal + "')")

    elementor_Btn_FormularioModal.setAttribute("onclick", "sendFormAjax(this, 'formularioCrearNuevoBandeja', 'resultadoSitios','/C_Local/obtenerAreasPorIdDepartamento?IdDepartamento=" + IdUbicacion_atributo + "','btn_AgregarSitio','idTablaSitiosBandejas','" + nuevoIdModal + "')")

    MostrarModal(nuevoIdModal)

}