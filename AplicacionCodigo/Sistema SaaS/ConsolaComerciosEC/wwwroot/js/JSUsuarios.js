﻿function mostrarModalEditarEmpresaCliente(idModal, idEmpresaCliente, tipoAccion, Accion)
{
    let spanTextoBotronAccion = document.getElementById("btnEditarEmpresaClienteSpan")
    let btnAccionFormulario = document.getElementById("btnEditarEmpresaClienteModal")
    let formulario = document.getElementById('formularioEditarEmpresaClienteModal')

    if (spanTextoBotronAccion != undefined)
    {
        if (tipoAccion == "Editar")
        {
            spanTextoBotronAccion.innerHTML = "Editar"
            btnAccionFormulario.classList.remove("btn-danger");
            btnAccionFormulario.classList.add("btn-primary")
            let accionBoton = "sendFormAjax(this, 'formularioEditarEmpresaClienteModal','resultadoClienteEmpersas','" + Accion + "','btnEditarEmpresaClienteModal','idTablaEmpresaCliente','modal_Gestion_EmpresaCliente')"

            btnAccionFormulario.setAttribute("onclick", accionBoton)
            formulario.action = pathConsola + "/C_Cliente/EditarEmpresaCliente";
        }
        else {
            spanTextoBotronAccion.innerHTML = "Eliminar"
            btnAccionFormulario.classList.add("btn-danger")
            btnAccionFormulario.classList.remove("btn-primary");


            formulario.action = pathConsola + "/C_Cliente/EliminarEmpresaCliente";

            let accionBotonEliminar = "sendFormAjaxEliminar(this, 'formularioEditarEmpresaClienteModal','resultadoClienteEmpersas','" + Accion + "','btnEditarEmpresaClienteModal','idTablaEmpresaCliente','modal_Gestion_EmpresaCliente')"

            btnAccionFormulario.setAttribute("onclick", accionBotonEliminar)
        }
    }

    $.ajax({
        url: pathConsola+"/C_Cliente/RecuperarEmpresaClientePorID?idEmpresaCliente=" + idEmpresaCliente,
        type: "get",
        //data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            let responseRead = JSON.stringify(result);
            let jsonObject = JSON.parse(responseRead);
            if (jsonObject != undefined)
            {
                document.getElementById("IdEmpresaCliente_Update").value = jsonObject.idEmpresaCliente
                document.getElementById("IdPersona_Update").value = jsonObject.idPersona
                $("#IdEmpresa_Update").val(jsonObject.idEmpresa).change();               
                document.getElementById("PorcentajeDescuento_Update").value = jsonObject.porcentajeDescuento
                document.getElementById("ValorCupoCreditoPersonal_Update").value = jsonObject.valorCupoCreditoPersonal
                document.getElementById("ValorCupoCreditoPersonal_Update").value = jsonObject.valorCupoCreditoPersonal
                document.getElementById("DiasCredito_Update").value = jsonObject.diasCredito

            }

        },
        error: function (xhr, textStatus, error) {
            alert(xhr.statusText);
        },
        failure: function (response) {
            alert("failure " + response.responseText);
        }


    })

    MostrarModal(idModal)
}
