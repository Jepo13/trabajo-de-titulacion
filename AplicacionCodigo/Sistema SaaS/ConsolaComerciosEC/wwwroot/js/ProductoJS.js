﻿function ConsultarCatalogoPorCodigoEmpresa(idEmpresa, CatalogoCodigPadre, idCatalogopadre) {

    let campoEmpresa = document.getElementById(idEmpresa)
    let campoCatalogoCodigPadre = document.getElementById(CatalogoCodigPadre)
    let campoIdCatalogopadre = document.getElementById(idCatalogopadre)
   
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/C_Catalogo/BusquedaCatalogoPorCodigoEmpresa?objCodigoCatalogo=" + campoCatalogoCodigPadre.value +"&idEmpresa=" + campoEmpresa.value, true);
    xhttp.send();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var directo = xhttp.response
            var respuestaPersona = JSON.parse(directo)

            if (respuestaPersona.idCatalogo != undefined) {

                campoIdCatalogopadre.value = respuestaPersona.idCatalogo
              
            } else {
                Swal.fire({
                    title: 'Advertencia',
                    text: "El usuario con el número de identificación registrado ya existe.",
                    icon: 'error',
                })
            }

            //$("#" + "IdEmpresa").val(respuestaPersona.idEmpresadefault).change();
            //$("#" + "IdRol").val(respuestaPersona.idRol).change();

            //let objetoJavaScript = document.getElementById("Nombre")
            //objetoJavaScript.value = respuestaPersona.nombre
        }
        else if (this.readyState != 4 && this.status != 200) {
            Swal.fire({
                title: 'No se encontro datos de persona',
                text: "",
                icon: 'info',
            })
        }
    }
}


async function cargarProvinciasCantonesParroquias(idProvincias, idCantonNacimiento, IdseleccionCantonNacimiento, lugarnaciomiento, idSeleccionParroquiaNacimiento) {
    recuperarLugaresProvinciasCantonesCiudades(idProvincias, idCantonNacimiento, IdseleccionCantonNacimiento).then( respu => {
        recuperarLugaresProvinciasCantonesCiudades(idCantonNacimiento, lugarnaciomiento, idSeleccionParroquiaNacimiento)
        console.log("idCantonNacimiento:-> " + idCantonNacimiento + " lugarnaciomiento:-> " + lugarnaciomiento + " idSeleccionParroquiaNacimiento: -> " + idSeleccionParroquiaNacimiento + " respu: " + respu)

    });
}
 