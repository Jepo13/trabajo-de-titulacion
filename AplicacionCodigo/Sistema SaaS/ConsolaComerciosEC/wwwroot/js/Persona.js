﻿function ConsultaPersona(idInputNumeroIdentificacion, idProvincias, idCantonNacimiento, IdProvinciaRecidencia, idCantonRecidencia) {

    let elementoNumeroIdentificacion = document.getElementById(idInputNumeroIdentificacion)
    var numeroIdentifficacion = elementoNumeroIdentificacion.value

    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "/C_Usuario/buscarPersona?numeroIdentificacion=" + numeroIdentifficacion, true);
    xhttp.send();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var directo = xhttp.response
            var respuestaPersona = JSON.parse(directo)

            if (respuestaPersona.idRol == null) {

                document.getElementById("Nombre").value = respuestaPersona.nombre
                document.getElementById("Apellido").value = respuestaPersona.apellido
                document.getElementById("Telefono1").value = respuestaPersona.telefono1
                document.getElementById("Correopersonal").value = respuestaPersona.correoPersonal
                document.getElementById("Telefono2").value = respuestaPersona.telefono2
                document.getElementById("Direccion").innerHTML = respuestaPersona.direccion
                document.getElementById("Correoinstitucional").innerHTML = respuestaPersona.direccion

                $("#IdTipoIdentificacion").val(respuestaPersona.idTipoIdentificacion).change();
                $("#IdEstadoCivil").val(respuestaPersona.idEstadoCivil).change();


                $("#idProvincias").val(respuestaPersona.idProvinciaNacimiento).change();
               
                //let campoIdProvincia = document.getElementById("idProvincias");
                //var idProvincia = campoIdProvincia.val
                //let campoIdCantonNacimiento = document.getElementById("idCantonNacimiento");
                //var idCanton = campoIdCantonNacimiento.val
               

                cargarProvinciasCantonesParroquias(idProvincias, idCantonNacimiento, respuestaPersona.idCantonNacimiento, "IdLugarnacimiento", respuestaPersona.idLugarNacimiento)

                $("#IdProvinciaRecidencia").val(respuestaPersona.idProvinciaRecidencia).change();
                $("#IdTipoIdentificacion").val(respuestaPersona.idTipoIdentificacion).change();
                $("#IdEstadoCivil").val(respuestaPersona.idEstadoCivil).change();

                cargarProvinciasCantonesParroquias(IdProvinciaRecidencia, idCantonRecidencia, respuestaPersona.idCantonRecidencia, "IdResidencia", respuestaPersona.idResidencia)

                //recuperarLugaresProvinciasCantonesCiudades(idProvincias, idCantonNacimiento, respuestaPersona.idCantonNacimiento)

                //recuperarLugaresProvinciasCantonesCiudades(idCantonNacimiento, lugarnaciomiento, respuestaPersona.idLugarNacimiento)


                /* $("#idCantonNacimiento").val(respuestaPersona.idCantonNacimiento).change();*/

                /*       $("#" + "IdLugarnacimiento").val(respuestaPersona.idLugarNacimiento).change();*/

            } else {
                Swal.fire({
                    title: 'Advertencia',
                    text: "El usuario con el número de identificación registrado ya existe.",
                    icon: 'error',
                })
            }

            //$("#" + "IdEmpresa").val(respuestaPersona.idEmpresadefault).change();
            //$("#" + "IdRol").val(respuestaPersona.idRol).change();

            //let objetoJavaScript = document.getElementById("Nombre")
            //objetoJavaScript.value = respuestaPersona.nombre
        }
        else if (this.readyState != 4 && this.status != 200) {
            Swal.fire({
                title: 'No se encontro datos de persona',
                text: "",
                icon: 'info',
            })
        }
    }
}


async function cargarProvinciasCantonesParroquias(idProvincias, idCantonNacimiento, IdseleccionCantonNacimiento, lugarnaciomiento, idSeleccionParroquiaNacimiento) {
    recuperarLugaresProvinciasCantonesCiudades(idProvincias, idCantonNacimiento, IdseleccionCantonNacimiento).then( respu => {
        recuperarLugaresProvinciasCantonesCiudades(idCantonNacimiento, lugarnaciomiento, idSeleccionParroquiaNacimiento)
        console.log("idCantonNacimiento:-> " + idCantonNacimiento + " lugarnaciomiento:-> " + lugarnaciomiento + " idSeleccionParroquiaNacimiento: -> " + idSeleccionParroquiaNacimiento + " respu: " + respu)

    });
}
 