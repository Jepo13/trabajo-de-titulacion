﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function sendFormAjaxEliminar(btnSubmit, formAjax, idDIVCargar, rutaCargarSubVista, id_BTN_Formulario, dataTable, idModal) {
    Swal.fire({
        title: '¿Esta seguro de eliminar?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, borrar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            sendFormAjax(btnSubmit, formAjax, idDIVCargar, rutaCargarSubVista, id_BTN_Formulario, dataTable, idModal)
        }
    })
}


function sendFormAjax(btnSubmit, formAjax, idDIVCargar, rutaCargarSubVista, id_BTN_Formulario, dataTable, idModal) {
    let continueSummit = true;
    let elementLoading = null

    //Se valida si recibimos el objetoHTML o el nombre del formulario
    let nombreFormularioTexto = "";
    if (formAjax.id != undefined) {
        nombreFormularioTexto = formAjax.id;
    }
    else {
        nombreFormularioTexto = formAjax
    }

    let myForm = document.getElementById(nombreFormularioTexto);


    if (idModal == undefined && idModal == null)
        elementLoading = document.getElementById("imagenCargando");

    if (myForm != undefined) {
        if (elementLoading != null) {
            elementLoading.classList.remove("invisible");
            elementLoading.style.display = "block";
        }

        if (id_BTN_Formulario != undefined) {
            let idBotonFormularioFormulario = document.getElementById(id_BTN_Formulario);
            if (idBotonFormularioFormulario != undefined) {
                idBotonFormularioFormulario.setAttribute("data-kt-indicator", "on");
            }
        }


        //console.log("Formulario guardar:", formAjax);
        //console.log("elementLoading:", elementLoading);

        $(btnSubmit).attr('disabled', true);

        //event.preventDefault();
        //event.stopImmediatePropagation();


        let formData = new FormData(myForm);

        continueSummit = validarCamposObligatorios(formAjax)

        console.log("*********************************")
        if (continueSummit) {

            if (idDIVCargar != undefined) {
                let idDIVCarga = document.getElementById(idDIVCargar);
                if (idDIVCarga != undefined) {
                    idDIVCarga.innerHTML = ""
                }
            }
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: myForm.action,
                    type: myForm.method,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {

                        let responseRead = JSON.stringify(result);
                        let jsonObject = JSON.parse(responseRead);

                        if (jsonObject != undefined) {


                            if (jsonObject.result != undefined) {
                                console.log("Error mensaje linea 70 - Site.js")

                                console.log("URL REDIRECCION" + jsonObject.result.urlVerDetalle)

                                Swal.fire(
                                    jsonObject.result.message,
                                    jsonObject.result.state,
                                    jsonObject.result.icon,
                                )
                                    .then((resulThen) => {
                                        if (jsonObject.result.urlRetorno != undefined) {
                                            window.location.href = jsonObject.result.urlRetorno
                                        }

                                        if (idDIVCargar != undefined && rutaCargarSubVista != undefined)
                                            $('#' + idDIVCargar).load(pathConsola + rutaCargarSubVista, function () {
                                                if (dataTable != undefined)
                                                    ordenarTabla(dataTable)

                                                if (elementLoading != null) {
                                                    //elementLoading.classList.add("invisible");
                                                    elementLoading.style.display = "none";
                                                }
                                                if (id_BTN_Formulario != undefined) {
                                                    let idBotonFormularioFormulario = document.getElementById(id_BTN_Formulario);
                                                    if (idBotonFormularioFormulario != undefined) {
                                                        idBotonFormularioFormulario.removeAttribute("data-kt-indicator");
                                                    }
                                                }
                                            });
                                        limpiarCamposFormulario(formAjax)
                                    })
                            }

                            else {
                                console.log("Else linea 143 - Site.js")
                                if (id_BTN_Formulario != undefined) {
                                    let idBotonFormularioFormulario = document.getElementById(id_BTN_Formulario);
                                    if (idBotonFormularioFormulario != undefined) {
                                        idBotonFormularioFormulario.removeAttribute("data-kt-indicator");
                                    }
                                }
                                if (jsonObject.state != undefined) {
                                    if (jsonObject.state == "Exitoso!") {
                                        limpiarCamposFormulario(formAjax)
                                    }
                                }
                                Swal.fire(
                                    jsonObject.message,
                                    jsonObject.state,
                                    jsonObject.icon,
                                ).then((resulThen) => {
                                    if (jsonObject.url != undefined) {
                                        window.location.href = jsonObject.url;
                                    }
                                    if (jsonObject.urlRetorno != undefined) {
                                        window.location.href = jsonObject.urlRetorno;
                                    }

                                    if (idDIVCargar != undefined && rutaCargarSubVista != undefined)
                                        $('#' + idDIVCargar).load(pathConsola + rutaCargarSubVista, function () {
                                            if (dataTable != undefined)
                                                ordenarTabla(dataTable)
                                        });
                                })
                            }
                        }
                        else {
                            console.log("Else linea 24 - Site.js")
                            if (id_BTN_Formulario != undefined) {
                                let idBotonFormularioFormulario = document.getElementById(id_BTN_Formulario);
                                if (idBotonFormularioFormulario != undefined) {
                                    idBotonFormularioFormulario.removeAttribute("data-kt-indicator");
                                }
                            }

                            Swal.fire(
                                "Ocurró un error inesperado",
                                "Error",
                                "error"
                            )
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (id_BTN_Formulario != undefined) {
                            let idBotonFormularioFormulario = document.getElementById(id_BTN_Formulario);
                            if (idBotonFormularioFormulario != undefined) {
                                idBotonFormularioFormulario.removeAttribute("data-kt-indicator");
                            }
                        }

                        Swal.fire(
                            "Ocurró un error inesperado",
                            "Error",
                            "error"
                        )
                    },
                    complete: function (data) {
                        if (elementLoading != null) {
                            //elementLoading.classList.add("invisible");
                            elementLoading.style.display = "none";
                        }

                        myForm.classList.remove("formOpacityDisabled");
                        $(btnSubmit).attr('disabled', false);

                        if (idModal != undefined) {
                            cerrarModal(idModal)
                        }
                    }
                });
            })
        }


        myForm.classList.remove("formOpacityDisabled");
    }
    else {
        Swal.fire(
            "!No se encontró el formulario¡ Contáctese con soporte técnico",
            "error",
            "error",
        )
    }

    if (elementLoading != null) {
        //elementLoading.classList.add("invisible");
        elementLoading.style.display = "none";
    }

    if (id_BTN_Formulario != undefined) {
        let idBotonFormularioFormulario = document.getElementById(id_BTN_Formulario);
        if (idBotonFormularioFormulario != undefined) {
            idBotonFormularioFormulario.removeAttribute("data-kt-indicator");
        }
    }


    $(btnSubmit).attr('disabled', false);

    if (idModal != undefined) {
        cerrarModal(idModal)
    }
}



//function sendFormAjax(btnSubmit, formAjax, formularioEliminar) {
//    let continueSummit = true;
//    let elementLoading = document.getElementById("imagenCargando");

//    if (elementLoading != null) {
//        elementLoading.classList.remove("invisible");
//    }
//    else {
//        btnSubmit.setAttribute("data-kt-indicator", "on");
//    }
    
//    //console.log("Formulario guardar:", formAjax);
//    //console.log("elementLoading:", elementLoading);

//    $(btnSubmit).attr('disabled', true);

//    event.preventDefault();
//    event.stopImmediatePropagation();

    
//    let myForm = document.getElementById(formAjax);
    
//    myForm.classList.add("formOpacityDisabled");

    

//    console.log("*********************************")
//    if (formularioEliminar == undefined) {
//        continueSummit = validarCamposObligatorios(continueSummit, elementLoading, myForm, btnSubmit)
//        console.log("else continua")
//        procesoEnvioAControlador(continueSummit, elementLoading, myForm, btnSubmit)
//    }
//    else if (formularioEliminar == "Delete") {
//        Swal.fire({
//            title: '¿Esta seguro de eliminar?',
//            text: "¡No podrás revertir esto!",
//            icon: 'warning',
//            showCancelButton: true,
//            confirmButtonText: 'Si, borrar!',
//            cancelButtonText: 'No, cancelar!',
//            reverseButtons: true
//        }).then((result) => {
//            /* Read more about isConfirmed, isDenied below */
//            if (result.isConfirmed) {
//                console.log("els if eliminar confimado")
//                procesoEnvioAControlador(continueSummit, elementLoading, myForm, btnSubmit)
//            } else {
//                console.log("els if eliminar negado")
//                ocultarCargando(elementLoading, myForm, btnSubmit);
//            }
//        })
//    }


//    console.log("pasa")

//}



function validarCamposObligatorios(continueSummit, elementLoading, myForm, btnSubmit ) {
    $(':input', 'form').each(function (i, inputForm) {
        if ($(inputForm).attr("required")) {
            if (!$(inputForm).val()) {
                let campo;

                try {
                    campo = $("label[for='" + $(inputForm).attr('id') + "']")[0].innerText
                }
                catch {
                    try {
                        campo = $(inputForm).attr('placeholder');
                    }
                    catch {
                        campo = "Por favor revisa los campos obligatorios.";
                    }
                }

                Swal.fire(
                    campo,
                    "Por favor Ingresa la información solicitada.",
                    "error"
                )
                ocultarCargando(elementLoading, myForm, btnSubmit);

                continueSummit = false;
              

                return false;
            }
        }
    })

    return continueSummit;
}

function ocultarCargando(elementLoading, myForm, btnSubmit) {
    console.log("ocultar cargando")
    if (elementLoading != null) {
        try {
            elementLoading.className = "invisible"
        }
        catch {
            elementLoading.classList.add("invisible");
        }

    }
    else {
        btnSubmit.removeAttribute("data-kt-indicator");
    }

    myForm.classList.remove("formOpacityDisabled");
    $(btnSubmit).attr('disabled', false);
}

function procesoEnvioAControlador(continueSummit, elementLoading, myForm, btnSubmit) {
    if (continueSummit) {
        let formData = new FormData(myForm);
        $.ajax({
            url: myForm.action,
            type: myForm.method,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {

                let responseRead = JSON.stringify(result);
                let jsonObject = JSON.parse(responseRead);

                if (jsonObject.result != undefined) {
                    Swal.fire(
                        jsonObject.result.message,
                        jsonObject.result.state,
                        jsonObject.result.icon,
                    ).then((result) => {
                        if (jsonObject.result.urlRetorno != undefined) {
                            window.location.href = jsonObject.result.urlRetorno
                        }
                    })
                }
                else {
                    Swal.fire(
                        jsonObject.message,
                        jsonObject.state,
                        jsonObject.icon,
                    )
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                Swal.fire(
                    "Ocurró un error inesperado",
                    "Error",
                    "error"
                )
            },
            complete: function (data) {
                ocultarCargando(elementLoading, myForm, btnSubmit);
            }
        });
    }
}



function MostrarModal(nombreModal) {
    let modalModulo = document.getElementById(nombreModal);

    if (modalModulo != undefined) {
        modalModulo.classList.add("show");
        modalModulo.setAttribute('aria-labelledby', 'modalRolCategoria');
        modalModulo.setAttribute('style', 'display: block; padding-left: 0px; z-index:1022');
        //modalModulo.setAttribute('style', 'display: block; padding-left: 0px; z-index:1');
        modalModulo.setAttribute('aria-modal', 'true;');
        modalModulo.setAttribute('role', 'dialog;');
        modalModulo.removeAttribute('aria-hidden', 'true');
    }
    else {
        let modalModuloPermidos = document.getElementById("modal_Sin_Permisos");
        modalModuloPermidos.classList.add("show");
        modalModuloPermidos.setAttribute('aria-labelledby', 'modalRolCategoria');
        //modalModuloPermidos.setAttribute('style', 'display: block; padding-left: 0px; z-index:1');
        modalModuloPermidos.setAttribute('style', 'display: block; padding-left: 0px; z-index:1022');
        modalModuloPermidos.setAttribute('aria-modal', 'true;');
        modalModuloPermidos.setAttribute('role', 'dialog;');
        modalModuloPermidos.removeAttribute('aria-hidden', 'true');
    }
}

function cerrarModal(nombreModal) {

    let modalModulo = document.getElementById(nombreModal);

    if (modalModulo != undefined) {
        modalModulo.classList.remove("show");
        modalModulo.removeAttribute('aria-labelledby', 'modalRolCategoria');
        modalModulo.removeAttribute('style', 'display: block; padding-left: 0px; z-index:1022');
        modalModulo.removeAttribute('aria-modal', 'true;');
        modalModulo.removeAttribute('role', 'dialog;');
        modalModulo.removeAttribute('aria-hidden', 'true');
    }
    else {
        console.log("Se intentó cerrar un modal que ya no existe en el contexto actual en cerrarModal() -> site.js => " + nombreModal)
    }
}

function limpiarCamposFormulario(nombreFormulario) {
    var formulario = document.getElementById(nombreFormulario);

    if (formulario != undefined) {
        var inputs = document.getElementById(nombreFormulario).elements;

        // Iterate over the form controls
        for (i = 0; i < inputs.length; i++) {
            if (inputs[i].nodeName === "INPUT" && inputs[i].type === "text" || inputs[i].nodeName === "TEXTAREA") {
                inputs[i].value = ""
            }
            else if (inputs[i].nodeName === "SELECT") {
                $("#" + inputs[i].getAttribute('id')).val('').change();
            }
            else if (inputs[i].type === "file") {
                let inputIdDIVInformacion = inputs[i]
                let idDIVInformacion = inputIdDIVInformacion.getAttribute("data-divinformacion");

                if (idDIVInformacion != undefined) {
                    let divDIVInformacion = document.getElementById(idDIVInformacion)
                    if (divDIVInformacion != undefined) {
                        divDIVInformacion.innerHTML = ""
                        divDIVInformacion.style.display = "none"
                    }
                }
            }
        }
    }
    else {
        console.log("Se intentó limpiar un formuario en limpiarCamposFormulario() -> site.js que no existe: " + nombreFormulario)
    }
    return true;
}