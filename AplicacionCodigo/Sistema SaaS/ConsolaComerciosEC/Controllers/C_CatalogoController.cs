﻿using AutoMapper;
using DTOs.Catalogo;
using DTOs.Empresa;
using DTOs.Select;
using DTOs.User;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas.Catalogo;
using Utilitarios.Busquedas.Usuario;

namespace ConsolaComerciosEC.Controllers
{
    public class C_CatalogoController : Controller
    {
        private readonly IServicioConsumoAPI<ObjetoBusquedaCatalogo> _servicioConsumoAPIBusqueda;
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;
        private readonly IServicioConsumoAPI<CatalogoCreateDTO> _servicioConsumoAPI;
        private readonly IServicioConsumoAPI<CatalogoEditarDTO> _servicioConsumoAPIUpdate;

        private readonly IMapper _mapper;
        public C_CatalogoController(IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos = null, IMapper mapper = null, IServicioConsumoAPI<ObjetoBusquedaCatalogo> servicioConsumoAPIBusqueda = null, IServicioConsumoAPI<CatalogoCreateDTO> servicioConsumoAPI = null, IServicioConsumoAPI<CatalogoEditarDTO> servicioConsumoAPIUpdate = null)
        {
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;
            _mapper = mapper;
            _servicioConsumoAPIBusqueda = servicioConsumoAPIBusqueda;
            _servicioConsumoAPI = servicioConsumoAPI;
            _servicioConsumoAPIUpdate = servicioConsumoAPIUpdate;
        }

        [HttpGet]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> GestionCatalogos()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                await cargaInicial(objUsuarioSesion);

                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #region CRUD

        #region Crear
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Escritura", concedido = true)]
        public IActionResult CrearCatalogo()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearCatalogo(CatalogoCreateDTO objCatalogo)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objCatalogo.Usuariocreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCatalogosCreate, HttpMethod.Post, objCatalogo);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Catalogo", "GestionCatalogos"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Editar

        [HttpGet]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarCatalogo(Guid idCatalogo)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                CatalogoCompleteDTO objCatalogo = await obtenerCatalogoPorID(idCatalogo, objUsuarioSesion);

                return View(objCatalogo);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarCatalogo(Guid IdCatalogo, CatalogoEditarDTO objCatalogo)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objCatalogo.Usuariomodificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuestaEditar = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getEditCatalogo + IdCatalogo, HttpMethod.Put, objCatalogo);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuestaEditar, "C_Catalogo", "GestionCatalogos"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Eliminar
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Catálogo", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarCatalogo(Guid idCatalogo)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                CatalogoCompleteDTO objCatalogo = await obtenerCatalogoPorID(idCatalogo, objUsuarioSesion);

                return View(objCatalogo);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Catálogo", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarCatalogo(Guid idCatalogo, string dato)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getDeleteCatalogo + idCatalogo, HttpMethod.Delete);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Catalogo", "GestionCatalogos", true));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion

        #region Detalle 
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> DetalleCatalogo(Guid idCatalogo)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCatalogosPorIdCatalogo + idCatalogo, HttpMethod.Get);

            CatalogoCompleteDTO objCatalogo = await LeerRespuestas<CatalogoCompleteDTO>.procesarRespuestasConsultas(respuesta);
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            await cargaInicial(objUsuarioSesion);

            return View(objCatalogo);
        }
        #endregion

        #endregion


        public async Task<List<CatalogoDropDownDTO>> obtenerIdCatalogo(string codigoCatalogoPadre)
        {

            HttpResponseMessage respuestaBusqueda = await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHijosPorCodigoPadre + codigoCatalogoPadre, HttpMethod.Get);

            List<CatalogoDropDownDTO> listaCatalogos = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(respuestaBusqueda);
            return listaCatalogos;

        }

        #region Búsquedas

        public async Task<CatalogoCompleteDTO> obtenerCatalogoPorID(Guid idCatalogo, UserSessionDTO objUsuarioSesion)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCatalogosPorIdCatalogo + idCatalogo, HttpMethod.Get);

            CatalogoCompleteDTO objResultado = await LeerRespuestas<CatalogoCompleteDTO>.procesarRespuestasConsultas(respuesta);

            await cargaInicial(objUsuarioSesion);

            return objResultado;
        }

        public async Task<IActionResult> BusquedaPorCodigo(string objCodigo, Guid idEmpresa)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getGetCatalogoCodeIDEmpresa + objCodigo + "&idEmpresa=" + idEmpresa, HttpMethod.Get);

            List<CatalogoResultadoBusqueda> listaCatalogo = await LeerRespuestas<List<CatalogoResultadoBusqueda>>.procesarRespuestasConsultas(respuesta);

            return View("_ListaCatalogo", listaCatalogo);
        }

        public async Task<CatalogoResultadoBusqueda> BusquedaCatalogoPorCodigoEmpresa(string objCodigoCatalogo, Guid idEmpresa)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getGetCatalogoCodeIDEmpresa + objCodigoCatalogo + "&idEmpresa=" + idEmpresa, HttpMethod.Get);

            CatalogoResultadoBusqueda objCatalogo = await LeerRespuestas<CatalogoResultadoBusqueda>.procesarRespuestasConsultas(respuesta);

            return objCatalogo;
        }



        public async Task<IActionResult> BusquedaTodosCatalogosPorIDEmpresa(Guid IdEmpresaBusqueda)
        {
            List<CatalogoResultadoBusqueda> listaCatalogosOrganizada = await RecuperarListaCatalogos(IdEmpresaBusqueda);

            return View("_ListaCatalogo", listaCatalogosOrganizada);
        }

        public async Task<List<CatalogoResultadoBusqueda>> RecuperarListaCatalogos(Guid IdEmpresaBusqueda)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getGetCatalogosTodosPorIdEmpresa + IdEmpresaBusqueda, HttpMethod.Get);

            List<CatalogoResultadoBusqueda> listaCatalogo = await LeerRespuestas<List<CatalogoResultadoBusqueda>>.procesarRespuestasConsultas(respuesta);

            if (listaCatalogo == null)
            {
                listaCatalogo = new List<CatalogoResultadoBusqueda>();
            }

            List<CatalogoResultadoBusqueda> listaCatalogosOrganizada = listaCatalogo.Where(x => x.IdCatalogopadre == null).ToList();

            return listaCatalogosOrganizada;
        }

        public async Task<List<CatalogoDropDownDTO>> RecuperarCatalogoHijos(Guid IdEmpresaBusqueda, Guid idCatalogoPadre)
        {
            HttpResponseMessage respuestaCatalogo = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCatalogosHijosPorID + idCatalogoPadre + "&idEmpresa=" + IdEmpresaBusqueda, HttpMethod.Get);

            List<CatalogoDropDownDTO> listaCatalogos = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(respuestaCatalogo);

            return listaCatalogos;
        }



        public async Task<List<CatalogoDropDownDTO>> RecuperarCatalogoHijosPorcodigoCatalogo(Guid IdEmpresaBusqueda, string codigoCatalogoPadre)
        {
            HttpResponseMessage respuestaCatalogo = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + codigoCatalogoPadre + "&idEmpresa=" + IdEmpresaBusqueda, HttpMethod.Get);

            List<CatalogoDropDownDTO> listaCatalogos = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(respuestaCatalogo);

            return listaCatalogos;
        }

        public async Task<List<CatalogoDropDownDTO>> RecuperarCatalogoHijosPorNombre(Guid IdEmpresaBusqueda, string nombreCatalogo)
        {
            HttpResponseMessage respuestaCatalogo = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCatalogosHijosPorNombre + nombreCatalogo + "&idEmpresa=" + IdEmpresaBusqueda, HttpMethod.Get);

            List<CatalogoDropDownDTO> listaCatalogos = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(respuestaCatalogo);

            return listaCatalogos;
        }

        #endregion

        private async Task cargaInicial(UserSessionDTO objUsuarioSesion)
        {
            if (objUsuarioSesion != null)
            {
                List<CatalogoResultadoBusqueda> listaCatalogosOrganizada = await RecuperarListaCatalogos(objUsuarioSesion.IdEmpresadefault);

                //Cargar todos los catalogos
                ViewData["listaTodosCatalogos"] = listaCatalogosOrganizada;

                if (objUsuarioSesion.ListaEmpresas == null)
                {
                    SelectList listaEmpresas = await DropDownsCatalogos<List<EmpresaResultadoBusquedaDTO>>.cargarListaDropDownGenerico(_servicioConsumoAPICatalogos, Constantes.getGetCompanyaAll, "IdEmpresa", "Razonsocial", objUsuarioSesion.IdEmpresadefault);

                    objUsuarioSesion.ListaEmpresas = _mapper.Map<CustomSelectEmpresas>(listaEmpresas);

                    ViewData["listaEmpresas"] = listaEmpresas;
                    SesionExtensions.SetObject(HttpContext.Session, Constantes.nombreSesion, objUsuarioSesion);
                }
                else
                {
                    SelectList objSelect = _mapper.Map<SelectList>(objUsuarioSesion.ListaEmpresas);
                    ViewData["listaEmpresas"] = objSelect;
                }
            }
        }
    }
}