﻿using DTOs.Catalogo;
using DTOs.Proveedor;
using DTOs.User;
using DTOs.PorcentajeIVA;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas.Proveedor;

namespace ConsolaComerciosEC.Controllers
{
    public class C_ProveedorController : Controller
    {
        private readonly IServicioConsumoAPI<ProveedorCreateDTO> _servicioConsumoAPI;
        private readonly IServicioConsumoAPI<ProveedorUpdateDTO> _servicioConsumoAPIUpdate;
        private readonly IServicioConsumoAPI<ObjetoBusquedaProveedor> _servicioConsumoAPIBusqueda;
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;
        private readonly IServicioConsumoAPI<PorcentajeIVACompleteDTO> _servicioPorcentajeIVACompleteDTO;


        public C_ProveedorController(IServicioConsumoAPI<ProveedorCreateDTO> servicioConsumoAPI, IServicioConsumoAPI<ProveedorUpdateDTO> servicioConsumoAPIUpdate, IServicioConsumoAPI<ObjetoBusquedaProveedor> servicioConsumoAPIBusqueda, IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos, IServicioConsumoAPI<PorcentajeIVACompleteDTO> servicioPorcentajeIVACompleteDTO)
        {
            _servicioConsumoAPI = servicioConsumoAPI;
            _servicioConsumoAPIUpdate = servicioConsumoAPIUpdate;
            _servicioConsumoAPIBusqueda = servicioConsumoAPIBusqueda;
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;
            _servicioPorcentajeIVACompleteDTO = servicioPorcentajeIVACompleteDTO;
        }

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> AdministrarProveedor()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                ViewData["ListaEspecialidad"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreEspecialidad + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault);

                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #region CRUD

        #region Crear Proveedor 
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearProveedor()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                await cargaInicial(objUsuarioSesion);
                return View();
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearProveedor(ProveedorCreateDTO objProveedor)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objProveedor.Usuariocreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuestaValidarDuplicado = await _servicioConsumoAPI.consumoAPI(Constantes.GetByRucProveedorEndPoint + objProveedor.Ruc + "&idEmpresa=" + objProveedor.IdEmpresa, HttpMethod.Get);

                ProveedorCreateDTO objProveedorDB = await LeerRespuestas<ProveedorCreateDTO>.procesarRespuestasConsultas(respuestaValidarDuplicado);

                if (objProveedorDB == null)
                {
                    HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.CreateProveedorEndPoint, HttpMethod.Post, objProveedor);

                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Proveedor", "AdministrarProveedor"));
                }
                else
                {
                    return new JsonResult(MensajesRespuesta.datoDuplicado());
                }
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Editar

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarProveedor(Guid idProveedor)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                var objProveedor = await recuperarProveedor(idProveedor);
                await cargaInicial(objUsuarioSesion);
                return View(objProveedor);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }


        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarProveedor(Guid idProveedor, ProveedorUpdateDTO objProveedorEditar)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objProveedorEditar.Usuariomodificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.EditProveedorEndPoint + idProveedor, HttpMethod.Post, objProveedorEditar);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Proveedor", "AdministrarProveedor"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion


        #region Detalle (READ)
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> DetalleProveedor(Guid idProveedor)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                var objProveedor = await recuperarProveedor(idProveedor);

                return View(objProveedor);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Eliminar 
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarProveedor(Guid idProveedor)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                var objProveedor = await recuperarProveedor(idProveedor);
                return View(objProveedor);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarProveedor(Guid idProveedor, bool eliminar)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.DeleteProveedorEndPoint + idProveedor, HttpMethod.Delete);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Proveedor", "AdministrarProveedor", true));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion



        #endregion

        #region Busquedas
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> BusquedaAvanzada(ObjetoBusquedaProveedor objBusqueda)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.GetAdvanceProveedorEndPoint, HttpMethod.Get, objBusqueda);
                if (respuesta.IsSuccessStatusCode) 
                {
                    var listaProveedor = await LeerRespuestas<List<ProveedorResultadoBusquedaDTO>>.procesarRespuestasConsultas(respuesta);
                    return View("_ListaProveedores", listaProveedor);
                }
              
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Utilitarios
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Escritura", concedido = true)]


        private async Task cargaInicial(UserSessionDTO objUsuarioSesion, Guid? valorSeleccionadoTipoLocal = null, Guid? valorSeleccionadoEmpresa = null)
        {

            ViewData["listaPorcentajeIva"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreImpuesto, valorSeleccionadoTipoLocal);


            ViewData["ListaTipoContribuyente"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreTipoContribuyente + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, valorSeleccionadoTipoLocal);

            ViewData["ListaEspecialidad"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreEspecialidad + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, valorSeleccionadoTipoLocal);

            ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
        }

        private async Task<ProveedorCompleteDTO> recuperarProveedor(Guid idProveedor)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.GetByIdProveedorEndPoint + idProveedor, HttpMethod.Get);

            ProveedorCompleteDTO objProveedor = await LeerRespuestas<ProveedorCompleteDTO>.procesarRespuestasConsultas(respuesta);
            await cargaInicial(objUsuarioSesion);

            return objProveedor;
        }
        #endregion

    }
}
