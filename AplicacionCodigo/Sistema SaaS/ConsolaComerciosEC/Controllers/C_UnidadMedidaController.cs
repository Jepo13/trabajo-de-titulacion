﻿using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using DTOs.Proveedor;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Proveedor;
using Utilitarios;
using DTOs.Catalogo;
using ConsolaComerciosEC.Servicios.Interface;
using ConsolaComerciosEC.Filters;
using System.Linq;
using System.Drawing;
using System;
using DTOs.User;
using DTOs.UnidadEntrega;
using DTOs.Empresa;

namespace ConsolaComerciosEC.Controllers
{
    public class C_UnidadMedidaController : Controller
    {
        private readonly IServicioConsumoAPI<UnidadEntregaBusquedaDTO> _servicioConsumoAPI;
        private readonly IServicioConsumoAPI<UnidadEntregaCreateDTO> _servicioConsumoAPICrear;
        private readonly IServicioConsumoAPI<UnidadEntregaUpdateDTO> _servicioConsumoAPIUpdate;

        public C_UnidadMedidaController(IServicioConsumoAPI<UnidadEntregaBusquedaDTO> servicioConsumoAPI, IServicioConsumoAPI<UnidadEntregaCreateDTO> servicioConsumoAPICrear, IServicioConsumoAPI<UnidadEntregaUpdateDTO> servicioConsumoAPIUpdate)
        {
            _servicioConsumoAPI = servicioConsumoAPI;
            _servicioConsumoAPICrear = servicioConsumoAPICrear;
            _servicioConsumoAPIUpdate = servicioConsumoAPIUpdate;
        }

        public async Task<ActionResult> GestionarUnidadMedida()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;

                HttpResponseMessage respuestaTodos = await _servicioConsumoAPICrear.consumoAPI(Constantes.getGetUnidadEntregaPorEmpresa + objUsuarioSesion.IdEmpresadefault, HttpMethod.Get);

                List<UnidadEntregaCompleteDTO> listaUnidades = await LeerRespuestas<List<UnidadEntregaCompleteDTO>>.procesarRespuestasConsultas(respuestaTodos);


                if (listaUnidades != null)
                {
                    List<UnidadEntregaCompleteDTO> listaMedidasFinal = listaUnidades.
                          Select(x => new UnidadEntregaCompleteDTO
                          {
                              IdUnidadentrega = x.IdUnidadentrega,
                              IdEmpresa = x.IdEmpresa,
                              NombreEmpresa = objUsuarioSesion.NombreEmpresa,
                              NombreContenido = x.NombreContenido,
                              CodigoUnidad = x.CodigoUnidad,
                              DescripcionMedida = x.DescripcionMedida,
                              EstadoUnidad = x.EstadoUnidad
                          }).ToList();

                    ViewData["listaUnidadesMedidas"] = listaMedidasFinal;

                }
                else
                {
                    ViewData["listaUnidadesMedidas"] = new List<UnidadEntregaCompleteDTO>();
                }

                return View();
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #region Crear
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Escritura", concedido = true)]
        public IActionResult CrearUnidadMedida()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;

                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearUnidadMedida(UnidadEntregaCreateDTO objDTOUnidad, string CodigoCatalogoPadre)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objDTOUnidad.UsuarioCreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                //Valida que no se tenga medias con el mismo Código
                HttpResponseMessage respuestaCodigoDuplicado = await _servicioConsumoAPICrear.consumoAPI(Constantes.getGetUnidadEntregaPorCodigo + objDTOUnidad.CodigoUnidad + "&idEmpresa=" + objDTOUnidad.IdEmpresa, HttpMethod.Get);

                UnidadEntregaCompleteDTO objCatalogoPorCodigoDuplicado = await LeerRespuestas<UnidadEntregaCompleteDTO>.procesarRespuestasConsultas(respuestaCodigoDuplicado);

                if (objCatalogoPorCodigoDuplicado == null)
                {
                    //Valida que no se tenga medias con el mismo Nombre
                    HttpResponseMessage respuestaNombreDuplicado = await _servicioConsumoAPICrear.consumoAPI(Constantes.getGetUnidadEntregaPorNombre + objDTOUnidad.NombreContenido + "&idEmpresa=" + objDTOUnidad.IdEmpresa, HttpMethod.Get);

                    UnidadEntregaCompleteDTO objCatalogoPorNombreDuplicado = await LeerRespuestas<UnidadEntregaCompleteDTO>.procesarRespuestasConsultas(respuestaNombreDuplicado);

                    if (objCatalogoPorNombreDuplicado == null)
                    {
                        HttpResponseMessage respuesta = await _servicioConsumoAPICrear.consumoAPI(Constantes.endPointCrearUnidadEntrega, HttpMethod.Post, objDTOUnidad);

                        if (respuesta.IsSuccessStatusCode)
                        {
                            return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_UnidadMedida", "GestionarUnidadMedida"));
                        }

                        MensajesRespuesta objMensajeRespuesta = await respuesta.ExceptionResponse();
                        return new JsonResult(objMensajeRespuesta);

                    }
                    else
                        return new JsonResult(MensajesRespuesta.errorDuplicados("Ya existe un registro con el mismo nombre."));
                }
                else
                    return new JsonResult(MensajesRespuesta.errorDuplicados("Ya existe un registro con el mismo código."));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Editar

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarUnidadMedida(Guid idUnidadEntrega)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
                UnidadEntregaCompleteDTO objDTOUnidad = await obtenerUnidadEntregaID(idUnidadEntrega);

                return View(objDTOUnidad);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarUnidadMedida(Guid IdUnidadentrega, UnidadEntregaUpdateDTO objDTOUnidad)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objDTOUnidad.UsuarioModificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuestaEditar = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetUnidadEntregaEditar + IdUnidadentrega, HttpMethod.Post, objDTOUnidad);

                if (respuestaEditar.IsSuccessStatusCode)
                {
                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuestaEditar, "C_UnidadMedida", "GestionarUnidadMedida")); 
                }
                else
                {
                    MensajesRespuesta objMensajeRespuesta = await respuestaEditar.ExceptionResponse();
                    return new JsonResult(objMensajeRespuesta);
                }
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Detalle

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> DetalleUnidadMedida(Guid idUnidadEntrega)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
                UnidadEntregaCompleteDTO objDTOUnidad = await obtenerUnidadEntregaID(idUnidadEntrega);

                return View(objDTOUnidad);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion

        #region Eliminar
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Catálogo", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarUnidadMedida(Guid idUnidadEntrega)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                UnidadEntregaCompleteDTO objDTOUnidad = await obtenerUnidadEntregaID(idUnidadEntrega);
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
                return View(objDTOUnidad);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Catálogo", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarUnidadMedida(Guid idUnidadEntrega, string dato)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetUnidadEntregaEliminar + idUnidadEntrega, HttpMethod.Post);

                if (respuesta.IsSuccessStatusCode)
                {
                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_UnidadMedida", "GestionarUnidadMedida", true)); 
                }
                else
                {
                    MensajesRespuesta objMensajeRespuesta = await respuesta.ExceptionResponse();
                    return new JsonResult(objMensajeRespuesta);
                }
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion


        #region Busquedas
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Proveedor", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> BusquedaAvanzada(UnidadEntregaBusquedaDTO objBusqueda)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getBusquedaAvanzadaUnidadEntrega, HttpMethod.Get, objBusqueda);

                if (respuesta.IsSuccessStatusCode)
                {
                    var listaUnidades = await LeerRespuestas<List<UnidadEntregaCompleteDTO>>.procesarRespuestasConsultas(respuesta);

                    if (listaUnidades != null)
                    {
                        HttpResponseMessage respuestaEmpresa = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCompanyByID + objUsuarioSesion.IdEmpresadefault, HttpMethod.Get);

                        EmpresaCompletoDTO objEmpresa = await LeerRespuestas<EmpresaCompletoDTO>.procesarRespuestasConsultas(respuestaEmpresa);

                        List<UnidadEntregaCompleteDTO> listaMedidasFinal = listaUnidades.
                              Select(x => new UnidadEntregaCompleteDTO
                              {
                                  IdUnidadentrega = x.IdUnidadentrega,
                                  IdEmpresa = x.IdEmpresa,
                                  NombreEmpresa = objEmpresa.Razonsocial,
                                  NombreContenido = x.NombreContenido,
                                  CodigoUnidad = x.CodigoUnidad,
                                  DescripcionMedida = x.DescripcionMedida,
                                  EstadoUnidad = x.EstadoUnidad
                              }).ToList();

                        return View("_ListaUnidadMedida", listaMedidasFinal);
                    }

                    return View("_ListaUnidadMedida", listaUnidades);
                }
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        public async Task<UnidadEntregaCompleteDTO> obtenerUnidadEntregaID(Guid idUnidadEntrega)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.endPointUnidadEntregaByID + idUnidadEntrega, HttpMethod.Get);

            UnidadEntregaCompleteDTO objResultado = await LeerRespuestas<UnidadEntregaCompleteDTO>.procesarRespuestasConsultas(respuesta);


            return objResultado;
        }
        #endregion

    }
}
