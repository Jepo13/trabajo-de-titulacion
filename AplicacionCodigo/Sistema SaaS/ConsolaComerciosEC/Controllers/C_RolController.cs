﻿using AutoMapper;
using DTOs.Catalogo;
using DTOs.Empresa;
using DTOs.Rol;
using DTOs.Select;
using DTOs.User;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;

namespace ConsolaComerciosEC.Controllers
{
    public class C_RolController : Controller
    {
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;
        private readonly IServicioConsumoAPI<RolCreacionDTO> _servicioConsumoAPI;
        private readonly IServicioConsumoAPI<RolBusquedaDTO> _servicioConsumoAPIBusqueda;
        private readonly IServicioConsumoAPI<RolEditarDTO> _servicioConsumoAPIUpdate;
        private readonly IMapper _mapper;

        public C_RolController(IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos, IMapper mapper, IServicioConsumoAPI<RolCreacionDTO> servicioConsumoAPI, IServicioConsumoAPI<RolBusquedaDTO> servicioConsumoAPIBusqueda, IServicioConsumoAPI<RolEditarDTO> servicioConsumoAPIUpdate)
        {
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;
            _mapper = mapper;
            _servicioConsumoAPI = servicioConsumoAPI;
            _servicioConsumoAPIBusqueda = servicioConsumoAPIBusqueda;
            _servicioConsumoAPIUpdate = servicioConsumoAPIUpdate;
        }


        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Administrar Rol", tipoPermiso = "Lectura", concedido = true)]
        public IActionResult AdministrarRol()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion!=null)
            {
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;


                return View(); 
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #region CRUD


        #region Crear
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Rol", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearRol()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                await cargarInicial();

                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Rol", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearRol([FromBody] RolCreacionDTO listaRoles)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                listaRoles.Usuariocreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getGetRolCreate, HttpMethod.Post, listaRoles);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Rol", "AdministrarRol"));
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion


        #region Editar
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Rol", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarRol(Guid IdRol)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                RolCompletoDTO objRol = await recuperarRolPorID(IdRol);
                await cargarInicial();

                return View(objRol);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Rol", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarRol([FromBody] RolCompletoDTO listaRoles)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                listaRoles.Usuariomodificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                listaRoles.listaModulos = listaRoles.listaModulos.Where(x => x != null).ToList();

                RolEditarDTO objEditar = new RolEditarDTO();
                _mapper.Map(listaRoles, objEditar);

                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetRolEditar + listaRoles.IdRol, HttpMethod.Put, objEditar);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Rol", "AdministrarRol"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Eliminar
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Rol", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarRol(Guid IdRol)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                RolCompletoDTO objRol = await recuperarRolPorID(IdRol);
                await cargarInicial();

                return View(objRol);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Rol", tipoPermiso = "Elminar", concedido = true)]
        public async Task<IActionResult> EliminarRol(Guid IdRol, bool eliminar)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetRolEliminar + IdRol, HttpMethod.Delete);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Rol", "AdministrarRol", true));
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Detalle
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Rol", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> DetalleRol(Guid IdRol)
        {
            RolCompletoDTO objRol = await recuperarRolPorID(IdRol);
            await cargarInicial();

            return View(objRol);
        }

        #endregion

        #endregion






        private async Task<RolCompletoDTO> recuperarRolPorID(Guid IdRol)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.endPointRolByID + IdRol, HttpMethod.Get);

            RolCompletoDTO objRol = await LeerRespuestas<RolCompletoDTO>.procesarRespuestasConsultas(respuesta);

            return objRol;
        }



        public async Task<IActionResult> BusquedaAvanzadaRol(Guid IdEmpresa)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.GetAllRolsByCompany + IdEmpresa, HttpMethod.Get);
            var lista = await LeerRespuestas<List<RolBusquedaDTO>>.procesarRespuestasConsultas(respuesta);

            return View("_ListaRoles", lista);
        }

        public async Task cargarInicial()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            ViewData["idEmpresa"] = objUsuarioSesion.IdEmpresadefault;
            ViewData["listaModulos"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreModulos + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault);

            HttpResponseMessage respuesta = await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padrePermisos + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, HttpMethod.Get);

            string listaPermisos = await LeerRespuestas<string>.procesarRespuestasJSON(respuesta);

            ViewData["listaPermisos"] = listaPermisos;

            ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
        }



    }//class
}
