﻿using AutoMapper;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Implementar;
using ConsolaComerciosEC.Servicios.Interface;
using DTOs.Catalogo;
using DTOs.CategoriaProductos;
using DTOs.Empresa;
using DTOs.Productos;
using DTOs.Select;
using DTOs.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;

namespace ConsolaComerciosEC.Controllers
{
    public class C_ProductoController : Controller
    {
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;
        private readonly IServicioConsumoAPI<ProductoDTOCrear> _servicioConsumoAPICrear;
        private readonly IServicioConsumoAPI<CatalogoBusquedaDTO> _servicioConsumoAPI;
        private readonly IMapper _mapper;

        public C_ProductoController(IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos, IMapper mapper, IServicioConsumoAPI<ProductoDTOCrear> servicioConsumoAPICrear, IServicioConsumoAPI<CatalogoBusquedaDTO> servicioConsumoAPI)
        {
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;
            _mapper = mapper;
            _servicioConsumoAPICrear = servicioConsumoAPICrear;
            _servicioConsumoAPI = servicioConsumoAPI;
        }
        public async Task<ActionResult> GestionarProducto()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {

                return View();
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #region CRUD

        #region Crear
        public async Task<ActionResult> CrearProducto()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                await CargaInicialProducto(objUsuarioSesion);

                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        public async Task<ActionResult> CrearProducto(ProductoDTOCrear objDTO)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objDTO.UsuarioCreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                await CargaInicialProducto(objUsuarioSesion);

                HttpResponseMessage respuesta = await _servicioConsumoAPICrear.consumoAPI(Constantes.getCrearProductos, HttpMethod.Post, objDTO);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Cliente", "AdministrarClientes"));

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion

        #endregion


        private async Task CargaInicialProducto(UserSessionDTO objUsuarioSesion)
        {
            CatalogoBusquedaDTO objBusqueda = new CatalogoBusquedaDTO();
            objBusqueda.IdEmpresa = objUsuarioSesion.IdEmpresadefault;

            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getBusquedaAvanzadaUnidadEntrega, HttpMethod.Get, objBusqueda);

            if (respuesta.IsSuccessStatusCode)
            {
                var listaUnidades = await LeerRespuestas<List<CatalogoResultadoBusqueda>>.procesarRespuestasConsultas(respuesta);
                ViewData["listaCategoriasProductos"] = listaUnidades;
            }
            else
            {
                ViewData["listaCategoriasProductos"] = new List<CatalogoResultadoBusqueda>();
            }
            

            if (objUsuarioSesion.ListaEmpresas == null)
            {
                SelectList listaEmpresas = await DropDownsCatalogos<List<EmpresaResultadoBusquedaDTO>>.cargarListaDropDownGenerico(_servicioConsumoAPICatalogos, Constantes.getGetCompanyaAll, "IdEmpresa", "Razonsocial", objUsuarioSesion.IdEmpresadefault);

                objUsuarioSesion.ListaEmpresas = _mapper.Map<CustomSelectEmpresas>(listaEmpresas);

                ViewData["listaEmpresas"] = listaEmpresas;
                SesionExtensions.SetObject(HttpContext.Session, Constantes.nombreSesion, objUsuarioSesion);
            }
            else
            {
                SelectList objSelect = _mapper.Map<SelectList>(objUsuarioSesion.ListaEmpresas);
                ViewData["listaEmpresas"] = objSelect;
            }

            //HttpResponseMessage respuestaCatalogoPadre = await _servicioConsumoAPICrear.consumoAPI(Constantes.getGetCatalogoCodeIDEmpresa + ConstantesAplicacion.codigoPadreMedida + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, HttpMethod.Get);

            //CatalogoResultadoBusqueda objCatalogoPadre = await LeerRespuestas<CatalogoResultadoBusqueda>.procesarRespuestasConsultas(respuestaCatalogoPadre);

            //ViewData["listaUnidadesMedidas"] = new SelectList(objCatalogoPadre.InverseIdCatalogopadreNavigation, "IdCatalogo", "Nombrecatalogo");
        }

    }
}
