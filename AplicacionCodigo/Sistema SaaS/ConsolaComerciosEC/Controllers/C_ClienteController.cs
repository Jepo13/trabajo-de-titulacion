﻿
using DTOs.Persona;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;
using DTOs.User;
using Microsoft.AspNetCore.Mvc.Rendering;
using DTOs.Catalogo;
using DTOs.Rol;
using Utilitarios.Busquedas.Usuario;
using DTOs.Cliente;
using Microsoft.AspNetCore.Identity;
using ConsolaComerciosEC.Servicios.Implementar;
using DTOs.EmpresaCliente;
using DTOs.Empresa;

namespace ConsolaComerciosEC.Controllers
{
    public class C_ClienteController : Controller
    {
        private readonly IServicioConsumoAPI<PersonaClienteCreateDTO> _servicioCreatePersonaAPI;
        private readonly IServicioConsumoAPI<PersonaClienteCompletoDTO> _servicioconsultaClientePersonaAPI;
        private readonly IServicioConsumoAPI<PersonaClienteUpdateDTO> _servicioUpdatePersonaClienteAPI;
        private readonly IServicioConsumoAPI<EmpresaClienteCreateDTO> _servicioEmpresaClieneAPI;
        private readonly IServicioConsumoAPI<EmpresaClienteValidadDTO> _servicioEmpresaClieneValidarAPI;
        private readonly IServicioConsumoAPI<EmpresaClienteUpdateDTO> _servicioEmpresaCLienteUPdateAPI;
        private readonly IServicioConsumoAPI<PersonaUpdateDTO> _servicioUpdatePersonaAPI;
        private readonly IServicioConsumoAPI<ClienteObjetoBusquedaDTO> _servicioClienteBusquedaAvanzadaAPI;
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;

        public C_ClienteController(IServicioConsumoAPI<PersonaClienteCreateDTO> servicioCreatePersonaAPI, IServicioConsumoAPI<PersonaUpdateDTO> servicioUpdatePersonaAPI, IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos, IServicioConsumoAPI<ClienteObjetoBusquedaDTO> servicioClienteBusquedaAvanzadaAPI, IServicioConsumoAPI<PersonaClienteCompletoDTO> servicioconsultaClientePersonaAPI, IServicioConsumoAPI<EmpresaClienteCreateDTO> servicioEmpresaClieneAPI, IServicioConsumoAPI<EmpresaClienteValidadDTO> servicioEmpresaClieneValidarAPI, IServicioConsumoAPI<PersonaClienteUpdateDTO> servicioUpdatePersonaClienteAPI, IServicioConsumoAPI<EmpresaClienteUpdateDTO> servicioEmpresaCLienteUPdateAPI)
        {
            _servicioCreatePersonaAPI = servicioCreatePersonaAPI;
            _servicioUpdatePersonaAPI = servicioUpdatePersonaAPI;
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;
            _servicioClienteBusquedaAvanzadaAPI = servicioClienteBusquedaAvanzadaAPI;
            _servicioconsultaClientePersonaAPI = servicioconsultaClientePersonaAPI;
            _servicioEmpresaClieneAPI = servicioEmpresaClieneAPI;
            _servicioEmpresaClieneValidarAPI = servicioEmpresaClieneValidarAPI;
            _servicioUpdatePersonaClienteAPI = servicioUpdatePersonaClienteAPI;
            _servicioEmpresaCLienteUPdateAPI = servicioEmpresaCLienteUPdateAPI;
        }


        [HttpGet]
        public IActionResult AministrarPersona()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpGet]
        public async Task<IActionResult> AdministrarClientes()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;

                ViewData["listaTipoIdentificacion"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreTipoIdentificacion);
                ViewData["listaTipoContribullente"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreTipoContribuyente);

                return View();

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));

        }

   
        public async Task<IActionResult> BusquedaAvanzadaCliente(ClienteObjetoBusquedaDTO objBusqueda)

        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioClienteBusquedaAvanzadaAPI.consumoAPI(Constantes.GetAdvanceEndPoint, HttpMethod.Get, objBusqueda);

                var lista = await LeerRespuestas<List<ClienteResultadoBusqueda>>.procesarRespuestasConsultas(respuesta);

                if (lista == null)
                {
                    lista = new List<ClienteResultadoBusqueda>();
                }


                return View("_ListaClientes", lista);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #region CRUD 

        #region CRUD  Cliente

        #region Create 
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearCliente()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                await cargarInformacionEsencial();
                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearCliente(PersonaClienteCreateDTO objPersona)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                objPersona.UsuarioCreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuesta = await _servicioCreatePersonaAPI.consumoAPI(Constantes.CreatePersonaEndPoint, HttpMethod.Post, objPersona);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Cliente", "AdministrarClientes"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion

        #region Edit
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> EditarCliente(Guid idPersona)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                PersonaClienteCompletoDTO personaCliente = await recuperarClientePersona(idPersona);
                await cargarInformacionEsencial();
                await cargaCiudades(objUsuarioSesion, personaCliente);
                Guid idUsuario = objUsuarioSesion.IdUsuario;

                HttpResponseMessage respuestaeUsuarioEmpresasValidar = await _servicioEmpresaClieneValidarAPI.consumoAPI(Constantes.getUsuarioEmpresaByIdUser + idUsuario, HttpMethod.Get);
                if (respuestaeUsuarioEmpresasValidar.IsSuccessStatusCode)
                {
                    List<UsuarioEmpresaCompletoDTO> listaUsuarioEmpresas = await LeerRespuestas<List<UsuarioEmpresaCompletoDTO>>.procesarRespuestasConsultas(respuestaeUsuarioEmpresasValidar);
                    int usuarioEmpresaContador = listaUsuarioEmpresas.Count();
                    int empresaClienteContador = personaCliente.ListEmpresaClientesDTO.Count();
                    if (usuarioEmpresaContador == empresaClienteContador)
                    {
                        personaCliente.empresasClienteCompleta = true;
                    }
                    else
                    {
                        personaCliente.empresasClienteCompleta = false;
                    }
                }

                //ViewData["listaEmpresasadicionales"] = objUsuarioSesion.EmpresasAccesoSelect;
                return View(personaCliente);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        public async Task<IActionResult> EditarCliente(PersonaClienteUpdateDTO objClienteDTO)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                objClienteDTO.UsuarioModificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuesta = await _servicioUpdatePersonaClienteAPI.consumoAPI(Constantes.EditPersonaEndPoint, HttpMethod.Post, objClienteDTO);
                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Cliente", "EditarCliente?idPersona=" + objClienteDTO.IdPersona));

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Eliminar

        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> ElimianrCliente(Guid idPersona)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                PersonaClienteCompletoDTO personaCliente = await recuperarClientePersona(idPersona);
                await cargarInformacionEsencial();
                await cargaCiudades(objUsuarioSesion, personaCliente);
                Guid idUsuario = objUsuarioSesion.IdUsuario;

                HttpResponseMessage respuestaeUsuarioEmpresasValidar = await _servicioEmpresaClieneValidarAPI.consumoAPI(Constantes.getUsuarioEmpresaByIdUser + idUsuario, HttpMethod.Get);
                if (respuestaeUsuarioEmpresasValidar.IsSuccessStatusCode)
                {
                    List<UsuarioEmpresaCompletoDTO> listaUsuarioEmpresas = await LeerRespuestas<List<UsuarioEmpresaCompletoDTO>>.procesarRespuestasConsultas(respuestaeUsuarioEmpresasValidar);
                    int usuarioEmpresaContador = listaUsuarioEmpresas.Count();
                    int empresaClienteContador = personaCliente.ListEmpresaClientesDTO.Count();
                    if (usuarioEmpresaContador == empresaClienteContador)
                    {
                        personaCliente.empresasClienteCompleta = true;
                    }
                    else
                    {
                        personaCliente.empresasClienteCompleta = false;
                    }
                }
                return View(personaCliente);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        public async Task<IActionResult> ElimianrCliente(PersonaClienteUpdateDTO objClienteDTO)
        {

            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioUpdatePersonaClienteAPI.consumoAPI(Constantes.DeletePersonaEndPoint, HttpMethod.Post, objClienteDTO);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Cliente", "AdministrarClientes"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Detalle Read

        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> DetalleCliente(Guid idPersona)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                PersonaClienteCompletoDTO personaCliente = await recuperarClientePersona(idPersona);
                await cargarInformacionEsencial();
                await cargaCiudades(objUsuarioSesion, personaCliente);
                Guid idUsuario = objUsuarioSesion.IdUsuario;

                HttpResponseMessage respuestaeUsuarioEmpresasValidar = await _servicioEmpresaClieneValidarAPI.consumoAPI(Constantes.getUsuarioEmpresaByIdUser + idUsuario, HttpMethod.Get);
                if (respuestaeUsuarioEmpresasValidar.IsSuccessStatusCode)
                {
                    List<UsuarioEmpresaCompletoDTO> listaUsuarioEmpresas = await LeerRespuestas<List<UsuarioEmpresaCompletoDTO>>.procesarRespuestasConsultas(respuestaeUsuarioEmpresasValidar);
                    int usuarioEmpresaContador = listaUsuarioEmpresas.Count();
                    int empresaClienteContador = personaCliente.ListEmpresaClientesDTO.Count();
                    if (usuarioEmpresaContador == empresaClienteContador)
                    {
                        personaCliente.empresasClienteCompleta = true;
                    }
                    else
                    {
                        personaCliente.empresasClienteCompleta = false;
                    }
                }

                //ViewData["listaEmpresasadicionales"] = objUsuarioSesion.EmpresasAccesoSelect;
                return View(personaCliente);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #endregion

        #region CRUD Empresa Cliente 
        public async Task<JsonResult> RecuperarEmpresaClientePorID(Guid idEmpresaCliente)
        {
            HttpResponseMessage respuesta = await _servicioEmpresaClieneAPI.consumoAPI(Constantes.GetEmpresaClienteByID + idEmpresaCliente, HttpMethod.Get);
            EmpresaClienteCompleteDTO objRespuesta = await LeerRespuestas<EmpresaClienteCompleteDTO>.procesarRespuestasConsultas(respuesta);
            return new JsonResult(objRespuesta);

        }
        #region Create EmpresaCliente
        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearEmpresaCliente(EmpresaClienteCreateDTO objEmpresaCliente)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                //ValidarDuplicado
                EmpresaClienteValidadDTO objValidarEmpresaClienteDuplicado = new();
                objValidarEmpresaClienteDuplicado.IdEmpresa = objEmpresaCliente.IdEmpresa;
                objValidarEmpresaClienteDuplicado.IdPersona = objEmpresaCliente.IdPersona;

                HttpResponseMessage respuestaeEmpresaCliente = await _servicioEmpresaClieneValidarAPI.consumoAPI(Constantes.GetEmpresaClienteByObjValidar, HttpMethod.Get, objValidarEmpresaClienteDuplicado);
                if (respuestaeEmpresaCliente.IsSuccessStatusCode)
                {

                    EmpresaClienteCompleteDTO objEmpresaClienteDTO = await LeerRespuestas<EmpresaClienteCompleteDTO>.procesarRespuestasConsultas(respuestaeEmpresaCliente);
                    if (objEmpresaClienteDTO == null)
                    {
                        objEmpresaCliente.UsuarioCreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                        HttpResponseMessage respuesta = await _servicioEmpresaClieneAPI.consumoAPI(Constantes.CreateEmpresaClienteEndPoint, HttpMethod.Post, objEmpresaCliente);

                        return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Cliente", "EditarCliente?idPersona=" + objEmpresaCliente.IdPersona));
                    }
                    else
                    {
                        return new JsonResult(MensajesRespuesta.datoDuplicado());
                    }

                }


            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Edit EmpresaCliente 
        [HttpGet]
        public async Task<IActionResult> EditarEmpresaCliente(Guid idEmpresaCliete)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuestaeEmpresaClienteById = await _servicioEmpresaClieneAPI.consumoAPI(Constantes.GetEmpresaClienteByID + idEmpresaCliete, HttpMethod.Get);


                if (respuestaeEmpresaClienteById.IsSuccessStatusCode)
                {
                    EmpresaClienteCompleteDTO objEmpresaClienteDTO = await LeerRespuestas<EmpresaClienteCompleteDTO>.procesarRespuestasConsultas(respuestaeEmpresaClienteById);
                    ViewData["listaEmpresasadicionales"] = objUsuarioSesion.EmpresasAccesoSelect;

                    return View(objEmpresaClienteDTO);
                }

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        [HttpPost]
        public async Task<IActionResult> EditarEmpresaCliente(EmpresaClienteUpdateDTO objEmpresClienteUpdateDTO)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                objEmpresClienteUpdateDTO.UsuarioModificacion_Update = objUsuarioSesion.Nombre;

                HttpResponseMessage respuesta = await _servicioEmpresaCLienteUPdateAPI.consumoAPI(Constantes.EditarEmpresaClienteEndPoint, HttpMethod.Post, objEmpresClienteUpdateDTO);
                //return new JsonResult(MensajesRespuesta.guardadoOksinRedireccion());
                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        public async Task<IActionResult> EliminarEmpresaCliente(EmpresaClienteUpdateDTO objEmpresClienteUpdateDTO)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {

                HttpResponseMessage respuesta = await _servicioEmpresaCLienteUPdateAPI.consumoAPI(Constantes.EliminarEmpresaClienteEndPoint, HttpMethod.Post, objEmpresClienteUpdateDTO);
                if (respuesta.IsSuccessStatusCode)
                {
                    return new JsonResult(MensajesRespuesta.eliminadoOKsinRedireccion());
                }
                else
                {
                    MensajesRespuesta objMensajeRespuesta = await respuesta.ExceptionResponse();
                    return new JsonResult(objMensajeRespuesta);
                }
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpGet]
        public async Task<IActionResult> DetalleEmpresaCliente(Guid idEmpresaCliete)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuestaeEmpresaClienteById = await _servicioEmpresaClieneAPI.consumoAPI(Constantes.GetEmpresaClienteByID + idEmpresaCliete, HttpMethod.Get);


                if (respuestaeEmpresaClienteById.IsSuccessStatusCode)
                {
                    EmpresaClienteCompleteDTO objEmpresaClienteDTO = await LeerRespuestas<EmpresaClienteCompleteDTO>.procesarRespuestasConsultas(respuestaeEmpresaClienteById);
                    ViewData["listaEmpresasadicionales"] = objUsuarioSesion.EmpresasAccesoSelect;

                    return View(objEmpresaClienteDTO);
                }

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #endregion

        #endregion



        //Métodop que devueolve una lista completa de EmpresasCliente de acuerdo al id cliente y el acceso del usuario
        public async Task<IActionResult> obtenerEmpresasClientes(Guid idPersona)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            HttpResponseMessage respuestaEmpresaClientes = await _servicioconsultaClientePersonaAPI.consumoAPI(Constantes.GetEmpresaClientebyIdPersona + idPersona, HttpMethod.Get);
            if (respuestaEmpresaClientes.IsSuccessStatusCode)
            {
                List<EmpresaClienteCompleteDTO> listEmpresaClienteCompleteDTOs = await LeerRespuestas<List<EmpresaClienteCompleteDTO>>.procesarRespuestasConsultas(respuestaEmpresaClientes);
                return View("_ListaEmpresaCliente", listEmpresaClienteCompleteDTOs);
            }

            return View("_ListaEmpresaCliente", new List<EmpresaClienteCompleteDTO>());

        }

        private async Task<PersonaClienteCompletoDTO> recuperarClientePersona(Guid idPersona)
        {
            HttpResponseMessage respuestaUsuarioEditar = await _servicioconsultaClientePersonaAPI.consumoAPI(Constantes.GetClientePersonaByIdPersona + idPersona, HttpMethod.Get);
            PersonaClienteCompletoDTO personClienteComplete = await LeerRespuestas<PersonaClienteCompletoDTO>.procesarRespuestasConsultas(respuestaUsuarioEditar);

            return personClienteComplete;
        }

        #region Cargas
        private async Task cargaCiudades(UserSessionDTO objUsuarioSesion, PersonaClienteCompletoDTO personaCliente)
        {

            if (personaCliente.IdLugarNacimiento != null)
            {
                ViewData["listaParroquias"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + personaCliente.IdLugarNacimiento, personaCliente.IdLugarNacimiento);

                HttpResponseMessage restapuestaParroquia =
                   await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + personaCliente.IdLugarNacimiento, HttpMethod.Get);

                if (restapuestaParroquia.IsSuccessStatusCode)
                {
                    List<CatalogoDropDownDTO> ListaParroquias = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaParroquia);

                    Guid idSeleccionCanton = ListaParroquias.Where(x => x.IdCatalogo == personaCliente.IdLugarNacimiento).FirstOrDefault().IdCatalogopadre;

                    ViewData["listaCantones"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + ListaParroquias.First().IdCatalogopadre, idSeleccionCanton);


                    HttpResponseMessage restapuestaCatones =
                       await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + idSeleccionCanton, HttpMethod.Get);
                    List<CatalogoDropDownDTO> ListaCantones = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaCatones);

                    Guid idSeleccionProvincia = ListaCantones.Where(x => x.IdCatalogopadre == ListaCantones.FirstOrDefault().IdCatalogopadre).FirstOrDefault().IdCatalogopadre;

                    SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias, idSeleccionProvincia);
                    ViewData["listaProvincias"] = listaProvincias;
                }
            }


            if (personaCliente.IdResidencia != null)
            {
                ViewData["listaParroquiasRecidencia"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + personaCliente.IdResidencia, personaCliente.IdResidencia);

                HttpResponseMessage restapuestaParroquia =
                   await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + personaCliente.IdResidencia, HttpMethod.Get);

                if (restapuestaParroquia.IsSuccessStatusCode)
                {
                    List<CatalogoDropDownDTO> ListaParroquias = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaParroquia);

                    Guid idSeleccionCanton = ListaParroquias.Where(x => x.IdCatalogo == personaCliente.IdResidencia).FirstOrDefault().IdCatalogopadre;

                    ViewData["listaCantonesRecidencia"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + ListaParroquias.First().IdCatalogopadre, idSeleccionCanton);


                    HttpResponseMessage restapuestaCatones =
                       await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + idSeleccionCanton, HttpMethod.Get);
                    List<CatalogoDropDownDTO> ListaCantones = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaCatones);

                    Guid idSeleccionProvincia = ListaCantones.Where(x => x.IdCatalogopadre == ListaCantones.FirstOrDefault().IdCatalogopadre).FirstOrDefault().IdCatalogopadre;

                    SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias, idSeleccionProvincia);
                    ViewData["listaProvinciasRecidencia"] = listaProvincias;
                }
            }
        }

        private async Task cargarInformacionEsencial(Guid? idEmpresa = null, Guid? idEstadoCivil = null, Guid? idPaisNacimiento = null, Guid? idTipoIdentificacion = null, Guid? idRol = null, Guid? valorSeleccionadoTipoLocal = null)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (idEmpresa == null)
            {
                idEmpresa = objUsuarioSesion.IdEmpresadefault;
            }

            await cargaInicial(objUsuarioSesion);



            ViewData["listaEstadoCivil"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreEstadoCivil);

            //ViewData["listaPaises"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogosPreselect(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padrePaises + idPaisNacimiento);

            ViewData["listaTipoIdentificacion"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreTipoIdentificacion, idTipoIdentificacion);

            //ViewData["listaRoles"] = await DropDownsCatalogos<List<RolBusquedaDTO>>.cargarListaDropDownGenerico(_servicioConsumoAPICatalogos, Constantes.GetAllRolsByCompany + idEmpresa, "IdRol", "Nombrerol", idRol);

            ViewData["ListaTipoContribuyente"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreTipoContribuyente + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, valorSeleccionadoTipoLocal);
        }


        private async Task cargaInicial(UserSessionDTO objUsuarioSesion)
        {
            ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;

            SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias);

            ViewData["listaProvincias"] = listaProvincias;

        }
        #endregion
    }
}
