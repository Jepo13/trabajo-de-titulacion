﻿using DTOs.Catalogo;
using DTOs.Empresa;
using DTOs.Local;
using DTOs.User;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Empresa;
using Utilitarios.Busquedas.Local;
using AutoMapper;
using DTOs.Ubicacion;
using DTOs.Sitios;
using DTOs.Bandejas;

namespace ConsolaComerciosEC.Controllers
{
    public class C_LocalController : Controller

    {
        private readonly IMapper _mapper;
        private readonly IServicioConsumoAPI<LocalCreateDTO> _servicioConsumoAPI;
        private readonly IServicioConsumoAPI<UbicacionCreateDTO> _servicioCrearUbicacionAPI;
        private readonly IServicioConsumoAPI<SitioCreateDTO> _servicioCrearSitioAPI;
        private readonly IServicioConsumoAPI<BandejaCreateDTO> _servicioCrearBandejaAPI;

        private readonly IServicioConsumoAPI<UbicacionUpdateDTO> _servicioActualizarUbicacionAPI;
        private readonly IServicioConsumoAPI<LocalUpdateDTO> _servicioConsumoAPIUpdate;
        private readonly IServicioConsumoAPI<ObjetoBusquedaLocal> _servicioConsumoAPIBusquedaLocal;
        private readonly IServicioConsumoAPI<ObjetoBusquedaEmpresas> _servicioConsumoAPIBusqueda;
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;

        public C_LocalController(IServicioConsumoAPI<LocalCreateDTO> servicioConsumoAPI, IServicioConsumoAPI<ObjetoBusquedaEmpresas> servicioConsumoAPIBusqueda, IServicioConsumoAPI<LocalUpdateDTO> servicioConsumoAPIUpdate, IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos, IServicioConsumoAPI<ObjetoBusquedaLocal> servicioConsumoAPIBusquedaLocal, IMapper mapper = null, IServicioConsumoAPI<UbicacionCreateDTO> servicioCrearUbicacionAPI = null, IServicioConsumoAPI<UbicacionUpdateDTO> servicioActualizarUbicacionAPI = null, IServicioConsumoAPI<SitioCreateDTO> servicioCrearSitioAPI = null, IServicioConsumoAPI<BandejaCreateDTO> servicioCrearBandejaAPI = null)
        {
            _servicioConsumoAPI = servicioConsumoAPI;
            _servicioConsumoAPIBusqueda = servicioConsumoAPIBusqueda;
            _servicioConsumoAPIUpdate = servicioConsumoAPIUpdate;
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;

            _mapper = mapper;
            _servicioCrearUbicacionAPI = servicioCrearUbicacionAPI;
            _servicioConsumoAPIBusquedaLocal = servicioConsumoAPIBusquedaLocal;
            _servicioActualizarUbicacionAPI = servicioActualizarUbicacionAPI;
            _servicioCrearSitioAPI = servicioCrearSitioAPI;
            _servicioCrearBandejaAPI = servicioCrearBandejaAPI;
        }

        #region CRUD Local

        #region Creat Local


        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearLocal()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            await cargaInicial(objUsuarioSesion);

            return View();
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearLocal(LocalCreateDTO objCrearLocal)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objCrearLocal.Usuariocreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.endPointCrearLocal, HttpMethod.Post, objCrearLocal);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Local", "AdministrarLocal"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion

        #region Editar
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarLocal(Guid idLocal)
        {
            UserSessionDTO objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                LocalCompleteDTO objLocal = await recuperarLocal(idLocal);


                return View(objLocal);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarLocal(Guid IdLocal, LocalUpdateDTO objLocalEditar)
        {
            UserSessionDTO objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objLocalEditar.Usuariomodificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetLocalEditar + IdLocal, HttpMethod.Put, objLocalEditar);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Local", "EditarLocal?idLocal="+ IdLocal));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion Editar

        #region Detalle "Read"

        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> DetalleLocal(Guid idLocal)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                var objLocal = await recuperarLocal(idLocal);

                return View(objLocal);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));

        }
        #endregion

        #region Delete
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarLocal(Guid idLocal)
        {
            var objLocal = await recuperarLocal(idLocal);

            return View(objLocal);
        }
        [HttpPost]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarLocal(Guid idLocal, bool eliminar)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetLocalEliminar + idLocal, HttpMethod.Delete);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Local", "AdministrarLocal", true));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion


        #endregion

        #region CRUD Ubicacion
        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearUbicacion(UbicacionCreateDTO objUbicacionCreate)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objUbicacionCreate.UsuarioCreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                objUbicacionCreate.Estado = true;

                HttpResponseMessage respuesta = await _servicioCrearUbicacionAPI.consumoAPI(Constantes.endPointCrearUbicacion, HttpMethod.Post, objUbicacionCreate);
                if (respuesta.IsSuccessStatusCode)
                {
                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Local", "EditarLocal?idLocal=" + objUbicacionCreate.IdLocal));
                }
                else
                {
                    return new JsonResult(MensajesRespuesta.guardarError());
                }

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpGet]
        public async Task<IActionResult> EditarUbicacion(Guid idUbicacion)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {

                HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.GetUbicacionById + idUbicacion, HttpMethod.Get);
                if (respuesta.IsSuccessStatusCode) 
                {
                    UbicacionCompleteDTO objUbicacion = await LeerRespuestas<UbicacionCompleteDTO>.procesarRespuestasConsultas(respuesta);

                    ViewData["listaTiUbicaciones"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreTipoUbicacion + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, objUbicacion.IdTipoUbicacion);

                    return View("Ubicacion/AdministrarUbicacion", objUbicacion);
                }
                

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        public async Task<IActionResult> EditarUbicacion (UbicacionUpdateDTO UbicacionUpdateDTO) 
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                UbicacionUpdateDTO.UsuarioModificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuesta = await _servicioActualizarUbicacionAPI.consumoAPI(Constantes.endPointEditarUbicacion, HttpMethod.Post, UbicacionUpdateDTO);
                if (respuesta.IsSuccessStatusCode) 
                {
                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Local", "EditarUbicacion?idUbicacion=" + UbicacionUpdateDTO.IdUbicacion));
                }
                else
                {
                    return new JsonResult(MensajesRespuesta.guardarError());
                }
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion 

        #region CRUD Sitio

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearSitio(SitioCreateDTO objCreate)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objCreate.UsuarioCreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
             

                HttpResponseMessage respuesta = await _servicioCrearSitioAPI.consumoAPI(Constantes.endPointCrearSitio, HttpMethod.Post, objCreate);

                if (respuesta.IsSuccessStatusCode)
                {
                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Local", "EditarUbicacion?idUbicacion=" + objCreate.IdUbicacion));
                }
                else
                {
                    return new JsonResult(MensajesRespuesta.guardarError());
                }

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion
        #region Bandejas
        [HttpPost]
        public async Task<IActionResult> CrearBandeja(BandejaCreateDTO objBandejaCreate) 
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objBandejaCreate.UsuarioCreacion= FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                

                         HttpResponseMessage respuesta = await _servicioCrearBandejaAPI.consumoAPI(Constantes.endPointCrearBandeja, HttpMethod.Post, objBandejaCreate);

                if (respuesta.IsSuccessStatusCode)
                {
                    return new JsonResult(MensajesRespuesta.guardarOK());

                    //return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Local", "EditarUbicacion?idUbicacion=" + objCreate.IdUbicacion));
                }
                else
                {
                    return new JsonResult(MensajesRespuesta.guardarError());
                }

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
           
        #endregion

        #region Busquedas
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Local", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> AdministrarLocal()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            await cargaInicial(objUsuarioSesion);

            return View();
        }

        public async Task<IActionResult> BusquedaAvanzadaLocal(ObjetoBusquedaLocal objBusqueda)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusquedaLocal.consumoAPI(Constantes.getGetLocalAdvance, HttpMethod.Get, objBusqueda);

            var lista = await LeerRespuestas<List<LocalResultadoBusquedaDTO>>.procesarRespuestasConsultas(respuesta);

            return View("_ListaLocales", lista);
        }

        // Método que devuelve una lista completa de sitios con sus bandejas de una ubicacion  por su ID
        public async Task<IActionResult> obtenerSitiosPorIdUbicaciones(Guid IdUbicacion)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusquedaLocal.consumoAPI(Constantes.getSitiosByIdUbicacion+ IdUbicacion, HttpMethod.Get);
           List<SitiosCompletoDTO> lista = await LeerRespuestas<List<SitiosCompletoDTO>>.procesarRespuestasConsultas(respuesta);
        
            if(lista != null) 
            {
                return View("Sitios/_ListaSitios", lista);
            }
                
            return View("Sitios/_ListaSitios", new List<UbicacionCompleteDTO>());
        }


        #endregion



        #region Utilitarios

        private async Task<LocalCompleteDTO> recuperarLocal(Guid idLocal)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.endPointLocalByID + idLocal, HttpMethod.Get);

            LocalCompleteDTO objLocal = await LeerRespuestas<LocalCompleteDTO>.procesarRespuestasConsultas(respuesta);
            await cargaInicial(objUsuarioSesion, objLocal.IdTipoLocal, objLocal.IdEmpresa);

            await cargaCiudades(objUsuarioSesion, objLocal);
            return objLocal;
        }

        private async Task cargaInicial(UserSessionDTO objUsuarioSesion, Guid? valorSeleccionadoTipoLocal = null, Guid? valorSeleccionadoEmpresa = null, Guid? idParroquia = null, Guid? valorSeleccionadoTipoUbicacion = null)
        {
            ViewData["listaTiUbicaciones"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreTipoUbicacion + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, valorSeleccionadoTipoUbicacion);

            // validar si es necesario llevar empresa a la consuilta 
            ViewData["listaTipoLocal"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreTipoLocal + "&idEmpresa=" + objUsuarioSesion.IdEmpresadefault, valorSeleccionadoTipoLocal);

            ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;

            if (idParroquia == null)
            {
                SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias);

                ViewData["listaProvincias"] = listaProvincias;
            }
        }

        private async Task cargaCiudades(UserSessionDTO objUsuarioSesion, LocalCompleteDTO objLocal)
        {


            if (objLocal.IdUbicacion != null)
            {
                ViewData["listaParroquias"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + objLocal.IdUbicacion, objLocal.IdUbicacion);

                HttpResponseMessage restapuestaParroquia =
                   await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + objLocal.IdUbicacion, HttpMethod.Get);

                if (restapuestaParroquia.IsSuccessStatusCode)
                {
                    List<CatalogoDropDownDTO> ListaParroquias = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaParroquia);

                    Guid idSeleccionCanton = ListaParroquias.Where(x => x.IdCatalogo == objLocal.IdUbicacion).FirstOrDefault().IdCatalogopadre;

                    ViewData["listaCantones"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + ListaParroquias.First().IdCatalogopadre, idSeleccionCanton);


                    HttpResponseMessage restapuestaCatones =
                       await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + idSeleccionCanton, HttpMethod.Get);
                    List<CatalogoDropDownDTO> ListaCantones = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaCatones);

                    Guid idSeleccionProvincia = ListaCantones.Where(x => x.IdCatalogopadre == ListaCantones.FirstOrDefault().IdCatalogopadre).FirstOrDefault().IdCatalogopadre;

                    SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias, idSeleccionProvincia);
                    ViewData["listaProvincias"] = listaProvincias;
                }
            }
        }



        private async Task<(List<CatalogoDropDownDTO> listaParroquia, Guid cantonSeleccionada, List<CatalogoResultadoBusqueda> listaCantones, List<CatalogoResultadoBusqueda> listaProvinciasFinal, Guid provinciaSeleccionada)> recuperarListasNacimiento(Guid? IdLugarParroquia, List<CatalogoDropDownDTO> listaProvincias)
        {
            List<CatalogoDropDownDTO> listaParroquianFinal = new List<CatalogoDropDownDTO>();
            List<CatalogoResultadoBusqueda> listaCantonesFinal = new List<CatalogoResultadoBusqueda>();
            List<CatalogoResultadoBusqueda> listaProvinciasFinal = new List<CatalogoResultadoBusqueda>();
            Guid cantonSeleccionada = Guid.Empty;
            Guid provinciaSeleccionada = Guid.Empty;

            if (IdLugarParroquia != null)
            {
                var resultadoLocalidadParroquias = await recuperarLocalidadSeleccionada((Guid)IdLugarParroquia);
                cantonSeleccionada = (Guid)resultadoLocalidadParroquias.IdSeleccionado;

                var resultadoLocalidadProvincias = await recuperarLocalidadSeleccionada((Guid)resultadoLocalidadParroquias.IdSeleccionado);
                provinciaSeleccionada = resultadoLocalidadProvincias.IdSeleccionado;

                listaParroquianFinal = await DropDownsCatalogos<CatalogoDropDownDTO>.procesarRespuestasConsultaCatlogoObjeto(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHermanosPorID + IdLugarParroquia);

                listaCantonesFinal = resultadoLocalidadParroquias.listaLocalidad;
                listaProvinciasFinal = resultadoLocalidadProvincias.listaLocalidad;

                return (listaParroquianFinal, cantonSeleccionada, listaCantonesFinal, listaProvinciasFinal, provinciaSeleccionada);
            }

            listaProvinciasFinal = _mapper.Map<List<CatalogoResultadoBusqueda>>(listaProvincias);

            return (listaParroquianFinal, cantonSeleccionada, listaCantonesFinal, listaProvinciasFinal, provinciaSeleccionada);
        }

        private async Task<(List<CatalogoResultadoBusqueda> listaLocalidad, Guid IdSeleccionado)> recuperarLocalidadSeleccionada(Guid idLugar)
        {
            HttpResponseMessage respuestaCatalogo = await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getGetCatalogosPorIdCatalogo + idLugar, HttpMethod.Get);

            var objtoCatalogo = await LeerRespuestas<CatalogoResultadoBusqueda>.procesarRespuestasConsultas(respuestaCatalogo);

            if (objtoCatalogo != null)
            {
                HttpResponseMessage respuestaCantones = await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getGetCatalogosPadreTios + idLugar, HttpMethod.Get);

                var listaCantones = await LeerRespuestas<List<CatalogoResultadoBusqueda>>.procesarRespuestasConsultas(respuestaCantones);

                return (listaCantones, (Guid)objtoCatalogo.IdCatalogopadre);
            }
            return (new List<CatalogoResultadoBusqueda>(), default);
        }




        public async Task<JsonResult> cargarListaDropDownCiudades(Guid idPadreCatalogoCiudad)
        {
            HttpResponseMessage restapuestaCatalogo = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCatalogosHijosPorID + idPadreCatalogoCiudad, HttpMethod.Get);

            string responseJSON = await LeerRespuestas<HttpResponseMessage>.procesarRespuestasJSON(restapuestaCatalogo);
            var listaCatalogos = JsonConvert.DeserializeObject<List<CatalogoDropDownDTO>>(responseJSON);

            return new JsonResult(listaCatalogos);
        }


        #endregion


    }
}
