﻿using AutoMapper;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Implementar;
using ConsolaComerciosEC.Servicios.Interface;
using DTOs.Catalogo;
using DTOs.CategoriaProductos;
using DTOs.Empresa;
using DTOs.Select;
using DTOs.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;


namespace ConsolaComerciosEC.Controllers
{
    public class C_CategoriaProductoController : Controller
    {
        private readonly IServicioConsumoAPI<CategoriaProductoDTOCompleto> _servicioConsumoAPIBusqueda;
        private readonly IServicioConsumoAPI<CategoriaProductoDTOCrear> _servicioConsumoAPICrear;
        private readonly IServicioConsumoAPI<CategoriaProductoDTOActualizar> _servicioConsumoAPIActualizar;
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;
        private readonly IMapper _mapper;

        public C_CategoriaProductoController(IServicioConsumoAPI<CategoriaProductoDTOCompleto> servicioConsumoAPIBusqueda, IMapper mapper, IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos, IServicioConsumoAPI<CategoriaProductoDTOCrear> servicioConsumoAPICrear, IServicioConsumoAPI<CategoriaProductoDTOActualizar> servicioConsumoAPIActualizar)
        {
            _servicioConsumoAPIBusqueda = servicioConsumoAPIBusqueda;
            _mapper = mapper;
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;
            _servicioConsumoAPICrear = servicioConsumoAPICrear;
            _servicioConsumoAPIActualizar = servicioConsumoAPIActualizar;
        }

        public async Task<ActionResult> GestionarCategoriaProducto()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                await cargaInicial(objUsuarioSesion);

                return View();
            }

            return View();
        }

        #region CRUD

        #region Crear
        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Escritura", concedido = true)]
        public async Task<ActionResult> CrearCategoriaProducto(CategoriaProductoDTOCrear objDTO)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objDTO.UsuarioCreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                HttpResponseMessage respuesta = await _servicioConsumoAPICrear.consumoAPI(Constantes.getCrearCategoriaProductos, HttpMethod.Post, objDTO);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta));
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Editar

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarCategoriaProducto(Guid idCategoriaProducto)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                CategoriaProductoDTOCompleto objCategoría = await obtenerCategoriaPorID(idCategoriaProducto, objUsuarioSesion);

                await cargaInicial(objUsuarioSesion);

                return View(objCategoría);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarCategoriaProducto(Guid idCategoriaProducto, CategoriaProductoDTOActualizar objCategoría)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objCategoría.UsuarioModificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuestaEditar = await _servicioConsumoAPIActualizar.consumoAPI(Constantes.getEditCategoria + idCategoriaProducto, HttpMethod.Post, objCategoría);

                if (respuestaEditar.IsSuccessStatusCode)
                {
                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuestaEditar, "C_CategoriaProducto", "GestionarCategoriaProducto"));
                }
                else
                {
                    MensajesRespuesta objMensajeRespuesta = await respuestaEditar.ExceptionResponse();
                    return new JsonResult(objMensajeRespuesta);
                }
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion


        #region Eliminar

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EliminarCategoriaProducto(Guid idCategoriaProducto)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                CategoriaProductoDTOCompleto objCategoría = await obtenerCategoriaPorID(idCategoriaProducto, objUsuarioSesion);

                await cargaInicial(objUsuarioSesion);

                return View(objCategoría);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EliminarCategoriaProducto(Guid idCategoriaProducto, CategoriaProductoDTOActualizar objCategoría)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objCategoría.UsuarioModificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                //Validar si tiene hijos
                HttpResponseMessage respuestaHijos = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getBuscarCategoriaProductosPorPadre + idCategoriaProducto, HttpMethod.Get);

                List<CategoriaProductoDTOCompleto> listaHijos = await LeerRespuestas<List<CategoriaProductoDTOCompleto>>.procesarRespuestasConsultas(respuestaHijos);

                if (listaHijos == null)
                    listaHijos = new List<CategoriaProductoDTOCompleto>();

                if (listaHijos.Count()<1)
                {
                    HttpResponseMessage respuestaEditar = await _servicioConsumoAPIActualizar.consumoAPI(Constantes.getEliminarCategoria + idCategoriaProducto, HttpMethod.Post);

                    if (respuestaEditar.IsSuccessStatusCode)
                    {
                        return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuestaEditar, "C_CategoriaProducto", "GestionarCategoriaProducto"));
                    }
                    else
                    {
                        MensajesRespuesta objMensajeRespuesta = await respuestaEditar.ExceptionResponse();
                        return new JsonResult(objMensajeRespuesta);
                    }
                }
                else
                {
          

                    return new JsonResult(MensajesRespuesta.ErrorEliminarHijos());
                }

            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion


        #region Detalle

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Configuraciones", nombreMenu = "Catálogo", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> DetalleCategoriaProducto(Guid idCategoriaProducto)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                CategoriaProductoDTOCompleto objCategoría = await obtenerCategoriaPorID(idCategoriaProducto, objUsuarioSesion);

                await cargaInicial(objUsuarioSesion);

                return View(objCategoría);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }


        #endregion


        #endregion

        #region CargaInicial

        public async Task<List<CategoriaProductoDTOCompleto>> RecuperarListaCategoria(Guid IdEmpresaBusqueda)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            List<Guid> idsEmpresas = new List<Guid>();
            List<CategoriaProductoDTOCompleto> listaCatalogo = new List<CategoriaProductoDTOCompleto>();
            if (objUsuarioSesion != null)
            {
                if (IdEmpresaBusqueda == ConstantesAplicacion.guidNulo)
                {
                    foreach (var objEmpresa in objUsuarioSesion.ListaEmpresasAcceso)
                    {
                        idsEmpresas.Add(objEmpresa.IdEmpresa);
                    }
                }
                else
                {
                    idsEmpresas.Add(IdEmpresaBusqueda);
                }

                foreach (var idEmpresa in idsEmpresas)
                {
                    try
                    {
                        HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getGetCategoriasTodosPorIdEmpresa + idEmpresa, HttpMethod.Get);

                        List<CategoriaProductoDTOCompleto> listaCatalogoTemporal = await LeerRespuestas<List<CategoriaProductoDTOCompleto>>.procesarRespuestasConsultas(respuesta);

                        listaCatalogo = listaCatalogo.Union(listaCatalogoTemporal).ToList();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                if (listaCatalogo == null)
                    listaCatalogo = new List<CategoriaProductoDTOCompleto>();


                List<CategoriaProductoDTOCompleto> listaCatalogosOrganizada = listaCatalogo.Where(x => x.IdCategoriaProducto == ConstantesAplicacion.guidNulo).ToList();

                return listaCatalogo;
            }

            return new List<CategoriaProductoDTOCompleto>();
        }

        public async Task<List<CategoriaProductoDTOCompleto>> RecuperarHijosCategoriasPorIDCategoriaPadre(Guid idCatalogoPadre)
        {
            try
            {
                HttpResponseMessage respuestaCatalogo = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getBuscarCategoriaProductosPorPadre + idCatalogoPadre, HttpMethod.Get);

                List<CategoriaProductoDTOCompleto> listaCategorias = await LeerRespuestas<List<CategoriaProductoDTOCompleto>>.procesarRespuestasConsultas(respuestaCatalogo);

                return listaCategorias;
            }
            catch (Exception ex)
            {

            }

            return default;
        }


        private async Task cargaInicial(UserSessionDTO objUsuarioSesion)
        {

            List<CategoriaProductoDTOCompleto> listaCatalogosOrganizada = await RecuperarListaCategoria(objUsuarioSesion.IdEmpresadefault);

            //Cargar todos los catalogos
            ViewData["listaTodosCategoriaProductos"] = listaCatalogosOrganizada;


            if (objUsuarioSesion.ListaEmpresas == null)
            {
                SelectList listaEmpresas = await DropDownsCatalogos<List<EmpresaResultadoBusquedaDTO>>.cargarListaDropDownGenerico(_servicioConsumoAPICatalogos, Constantes.getGetCompanyaAll, "IdEmpresa", "Razonsocial", objUsuarioSesion.IdEmpresadefault);

                objUsuarioSesion.ListaEmpresas = _mapper.Map<CustomSelectEmpresas>(listaEmpresas);

                ViewData["listaEmpresas"] = listaEmpresas;
                SesionExtensions.SetObject(HttpContext.Session, Constantes.nombreSesion, objUsuarioSesion);
            }
            else
            {
                SelectList objSelect = _mapper.Map<SelectList>(objUsuarioSesion.ListaEmpresas);
                ViewData["listaEmpresas"] = objSelect;
            }

        }
        #endregion


        #region Búsquedas

        public async Task<CategoriaProductoDTOCompleto> obtenerCategoriaPorID(Guid idCategoriaProducto, UserSessionDTO objUsuarioSesion)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getGetCategoriaProductoPorID + idCategoriaProducto, HttpMethod.Get);

            CategoriaProductoDTOCompleto objResultado = await LeerRespuestas<CategoriaProductoDTOCompleto>.procesarRespuestasConsultas(respuesta);

            await cargaInicial(objUsuarioSesion);

            return objResultado;
        }



        #endregion
    }
}
