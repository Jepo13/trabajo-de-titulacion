﻿ using DTOs.Catalogo;
using DTOs.Empresa;
using DTOs.Rol;
using DTOs.User;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Usuario;
using DTOs.Persona;

namespace ConsolaComerciosEC.Controllers
{
    public class C_UsuarioController : Controller
    {

        
        private readonly IServicioConsumoAPI<UserCreateDTO> _servicioConsumoAPI;
        private readonly IServicioConsumoAPI<UserUpdateDTO> _servicioConsumoAPIUpdate;
        private readonly IServicioConsumoAPI<ObjetoBusquedaUsuarios> _servicioConsumoAPIBusqueda;
        private readonly IServicioConsumoAPI<CatalogoDropDownDTO> _servicioConsumoAPICatalogos;
        private readonly IServicioConsumoAPI<PersonaCompleteDTO> _servicioCreatePersonaAPI;

        public C_UsuarioController(IServicioConsumoAPI<UserCreateDTO> servicioConsumoAPI, IServicioConsumoAPI<ObjetoBusquedaUsuarios> servicioConsumoAPIBusqueda, IServicioConsumoAPI<UserUpdateDTO> servicioConsumoAPIUpdate, IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPICatalogos, IServicioConsumoAPI<PersonaCompleteDTO> servicioCreatePersonaAPI)
        {
            _servicioConsumoAPI = servicioConsumoAPI;
            _servicioConsumoAPIBusqueda = servicioConsumoAPIBusqueda;
            _servicioConsumoAPIUpdate = servicioConsumoAPIUpdate;
            _servicioConsumoAPICatalogos = servicioConsumoAPICatalogos;
            _servicioCreatePersonaAPI = servicioCreatePersonaAPI;
        }


        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Administrar Usuarios", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> AdministrarUsuarios()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            ViewData["listaRoles"] = await DropDownsCatalogos<List<RolBusquedaDTO>>.cargarListaDropDownGenerico(_servicioConsumoAPICatalogos, Constantes.GetAllRolsByCompany + objUsuarioSesion.IdEmpresadefault, "IdRol", "Nombrerol");
            ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
            return View();
        }

        #region CRUD

        #region CREATE 
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Crear Usuario", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearUsuario()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                await cargarInformacionEsencial();
                await cargaInicial(objUsuarioSesion);

                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));

        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Crear Usuario", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearUsuario(UserCreateDTO objUsuario, IFormFile Imagenpersona)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objUsuario.Usuariocreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                objUsuario.Contrasena = FuncionesContrasena.encriptarContrasena(objUsuario.Contrasena);
                objUsuario.IdPaisorigen = null;
                var resultado = await readFile(Imagenpersona);
                objUsuario.Imagenpersona = resultado.Item1;
                objUsuario.FormatoArchivo = resultado.Item2;

                HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getCreateUser, HttpMethod.Post, objUsuario);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Usuario", "AdministrarUsuarios"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion

        #region Edit
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Administrar Usuarios", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarUsuario(Guid idUsuario)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                UserCompleteDTO objUserDTO = await recuperarUsuarioEditarDetalle(idUsuario);
                ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;
                await cargaCiudades(objUsuarioSesion, objUserDTO);

                return View(objUserDTO);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Administrar Usuarios", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarUsuario(Guid IdUsuario, UserUpdateDTO objUsuario)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objUsuario.Usuariomodificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);

                HttpResponseMessage respuestaUsuarioEditar = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getEditUser + IdUsuario, HttpMethod.Put, objUsuario);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuestaUsuarioEditar, "C_Usuario", "AdministrarUsuarios"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #region Eliminar 

        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Administrar Usuarios", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarUsuario(Guid idUsuario)
        {
            var objUserDTO = await recuperarUsuarioEditarDetalle(idUsuario);
            if (objUserDTO != null)
            {
                return View(objUserDTO);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Administrar Usuarios", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarUsuario(Guid idUsuario, bool eliminar)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.eliminarUser + idUsuario, HttpMethod.Delete);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Usuario", "AdministrarUsuarios", true));
            }

            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion


        #region Detalle READ
        [HttpGet]
        //[AccionesFiltro(nombreModulo = "Usuarios", nombreMenu = "Administrar Usuarios", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> DetalleUsuario(Guid idUsuario)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                var objUserDTO = await recuperarUsuarioEditarDetalle(idUsuario);

            return View(objUserDTO);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

        #endregion

        #region busquedas
        public async Task<IActionResult> BusquedaAvanzadaUsuario(ObjetoBusquedaUsuarios objBusqueda)
        
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getAdvancedSearch, HttpMethod.Get, objBusqueda);

            var lista = await LeerRespuestas<List<UserResultadoBusquedaDTO>>.procesarRespuestasConsultas(respuesta);

            if (lista == null)
            {
                lista = new List<UserResultadoBusquedaDTO>();
            }

            return View("_ListaUsuarios", lista);
        }

        #endregion

        #region Utilitarios


        private async Task<UserCompleteDTO> recuperarUsuarioEditarDetalle(Guid idUsuario)
        {
            HttpResponseMessage respuestaUsuarioEditar = await _servicioConsumoAPI.consumoAPI(Constantes.getUserByID + idUsuario, HttpMethod.Get);

            UserCompleteDTO objUserDTO = await LeerRespuestas<UserCompleteDTO>.procesarRespuestasConsultas(respuestaUsuarioEditar);

            await cargarInformacionEsencial(objUserDTO.IdEmpresaDefault, objUserDTO.IdEstadoCivil, objUserDTO.IdPaisOrigen, objUserDTO.IdTipoIdentificacion, objUserDTO.IdRol);



            return objUserDTO;
        }

        public async Task<(byte[], string)> readFile(IFormFile imagenFile)
        {
            try
            {
                if (imagenFile != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await imagenFile.CopyToAsync(memoryStream);
                        // Upload the file if less than 2 MB
                        if (memoryStream.Length < 2097152)
                        {
                            return (memoryStream.ToArray(), imagenFile.ContentType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return (null, null);
        }



        private async Task cargarInformacionEsencial(Guid? idEmpresa = null, Guid? idEstadoCivil = null, Guid? idPaisNacimiento = null, Guid? idTipoIdentificacion = null, Guid? idRol = null)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (idEmpresa == null)
            {
                idEmpresa = objUsuarioSesion.IdEmpresadefault;
            }

            await cargaInicial(objUsuarioSesion);

            ViewData["listaEstadoCivil"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getGetCatalogosHijosPorCodigoIDEmpresa + Constantes.padreEstadoCivil);
             
            ViewData["listaPaises"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogosPreselect(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padrePaises + idPaisNacimiento);

            ViewData["listaTipoIdentificacion"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreTipoIdentificacion, idTipoIdentificacion);

            ViewData["listaRoles"] = await DropDownsCatalogos<List<RolBusquedaDTO>>.cargarListaDropDownGenerico(_servicioConsumoAPICatalogos, Constantes.GetAllRolsByCompany + idEmpresa, "IdRol", "Nombrerol", idRol);
        }

        private async Task cargaInicial(UserSessionDTO objUsuarioSesion)
        {
            ViewData["listaEmpresas"] = objUsuarioSesion.EmpresasAccesoSelect;

            SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias);

            ViewData["listaProvincias"] = listaProvincias;
          
        }

        private async Task cargaCiudades(UserSessionDTO objUsuarioSesion, UserCompleteDTO objUserDTO)
        {

            if (objUserDTO.IdLugarNacimiento != null)
            {
                ViewData["listaParroquias"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + objUserDTO.IdLugarNacimiento, objUserDTO.IdLugarNacimiento);

                HttpResponseMessage restapuestaParroquia =
                   await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + objUserDTO.IdLugarNacimiento, HttpMethod.Get);

                if (restapuestaParroquia.IsSuccessStatusCode)
                {
                    List<CatalogoDropDownDTO> ListaParroquias = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaParroquia);

                    Guid idSeleccionCanton = ListaParroquias.Where(x => x.IdCatalogo == objUserDTO.IdLugarNacimiento).FirstOrDefault().IdCatalogopadre;

                    ViewData["listaCantones"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + ListaParroquias.First().IdCatalogopadre, idSeleccionCanton);


                    HttpResponseMessage restapuestaCatones =
                       await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + idSeleccionCanton, HttpMethod.Get);
                    List<CatalogoDropDownDTO> ListaCantones = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaCatones);

                    Guid idSeleccionProvincia = ListaCantones.Where(x => x.IdCatalogopadre == ListaCantones.FirstOrDefault().IdCatalogopadre).FirstOrDefault().IdCatalogopadre;

                    SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias, idSeleccionProvincia);
                    ViewData["listaProvincias"] = listaProvincias;
                }
            }


            if (objUserDTO.IdResidencia != null)
            {
                ViewData["listaParroquiasRecidencia"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + objUserDTO.IdResidencia, objUserDTO.IdResidencia);

                HttpResponseMessage restapuestaParroquia =
                   await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + objUserDTO.IdResidencia, HttpMethod.Get);

                if (restapuestaParroquia.IsSuccessStatusCode)
                {
                    List<CatalogoDropDownDTO> ListaParroquias = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaParroquia);

                    Guid idSeleccionCanton = ListaParroquias.Where(x => x.IdCatalogo == objUserDTO.IdResidencia).FirstOrDefault().IdCatalogopadre;

                    ViewData["listaCantonesRecidencia"] = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHermanoPorID + ListaParroquias.First().IdCatalogopadre, idSeleccionCanton);


                    HttpResponseMessage restapuestaCatones =
                       await _servicioConsumoAPICatalogos.consumoAPI(Constantes.getCatalogosHermanoPorID + idSeleccionCanton, HttpMethod.Get);
                    List<CatalogoDropDownDTO> ListaCantones = await LeerRespuestas<List<CatalogoDropDownDTO>>.procesarRespuestasConsultas(restapuestaCatones);

                    Guid idSeleccionProvincia = ListaCantones.Where(x => x.IdCatalogopadre == ListaCantones.FirstOrDefault().IdCatalogopadre).FirstOrDefault().IdCatalogopadre;

                    SelectList listaProvincias = await DropDownsCatalogos<List<CatalogoDropDownDTO>>.cargarListaDropDownCatalogos(_servicioConsumoAPICatalogos, Constantes.getCatalogosHijosPorCodigoPadre + Constantes.padreProvincias, idSeleccionProvincia);
                    ViewData["listaProvinciasRecidencia"] = listaProvincias;
                }
            }
        }


        public async Task<JsonResult> buscarPersona(string numeroIdentificacion) 
        {
            HttpResponseMessage respuestaValidarDuplicado = await _servicioCreatePersonaAPI.consumoAPI(Constantes.buscarUsuarioValidarPersonaCreada + numeroIdentificacion, HttpMethod.Get);

            string responseJSON = await LeerRespuestas<HttpResponseMessage>.procesarRespuestasJSON(respuestaValidarDuplicado);
            var respuesta  = JsonConvert.DeserializeObject<UserCompleteDTO>(responseJSON);

            return new JsonResult(respuesta);
          
        #endregion
        }
    }
}
