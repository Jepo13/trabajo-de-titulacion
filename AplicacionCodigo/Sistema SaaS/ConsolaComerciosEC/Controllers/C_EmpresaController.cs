﻿using DTOs.Empresa;
using DTOs.User;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Empresa;

namespace ConsolaComerciosEC.Controllers
{
    public class C_EmpresaController : Controller
    {
        private readonly IServicioConsumoAPI<EmpresaCreateDTO> _servicioConsumoAPI;
        private readonly IServicioConsumoAPI<EmpresaUpdateDTO> _servicioConsumoAPIUpdate;
        private readonly IServicioConsumoAPI<ObjetoBusquedaEmpresas> _servicioConsumoAPIBusqueda;

        public C_EmpresaController(IServicioConsumoAPI<EmpresaCreateDTO> servicioConsumoAPI, IServicioConsumoAPI<EmpresaUpdateDTO> servicioConsumoAPIUpdate, IServicioConsumoAPI<ObjetoBusquedaEmpresas> servicioConsumoAPIBusqueda)
        {
            _servicioConsumoAPI = servicioConsumoAPI;
            _servicioConsumoAPIUpdate = servicioConsumoAPIUpdate;
            _servicioConsumoAPIBusqueda = servicioConsumoAPIBusqueda;
        }

        [HttpGet]
        public IActionResult AdministrarEmpresa()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #region CRUD

         #region Create
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public IActionResult CrearEmpresa()
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                return View();
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }


        [HttpPost]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Escritura", concedido = true)]
        public async Task<IActionResult> CrearEmpresa(EmpresaCreateDTO objEmpresa)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                objEmpresa.Usuariocreacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                objEmpresa.IdUsuario = objUsuarioSesion.IdUsuario;

                HttpResponseMessage respuestaValidarDuplicado = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCompanyByRUC + objEmpresa.Ruc, HttpMethod.Get);

                EmpresaCompletoDTO objEmpresaDB = await LeerRespuestas<EmpresaCompletoDTO>.procesarRespuestasConsultas(respuestaValidarDuplicado);

                if (objEmpresaDB == null)
                {
                    HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCompanyCreate, HttpMethod.Post, objEmpresa);

                    return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Empresa", "AdministrarEmpresa"));
                }
                else
                {
                    return new JsonResult(MensajesRespuesta.datoDuplicado());
                }
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion

         #region Editar  
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarEmpresa(Guid idEmpresa)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                EmpresaCompletoDTO objEmpresa = await obtenerEmpresa(idEmpresa);

                return View(objEmpresa);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Edición", concedido = true)]
        public async Task<IActionResult> EditarEmpresa(Guid idEmpresa, EmpresaUpdateDTO objEmpresa)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);
            if (objUsuarioSesion != null)
            {
                objEmpresa.Usuariomodificacion = FuncionesUtiles.consturirUsuarioAuditoria(objUsuarioSesion);
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetCompanyEditar + idEmpresa, HttpMethod.Put, objEmpresa);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Empresa", "AdministrarEmpresa"));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion

         #region Eliminar
        [HttpGet]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarEmpresa(Guid idEmpresa)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                EmpresaCompletoDTO objEmpresa = await obtenerEmpresa(idEmpresa);

                return View(objEmpresa);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        [HttpPost]
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Eliminar", concedido = true)]
        public async Task<IActionResult> EliminarEmpresa(Guid idEmpresa, string dato)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                HttpResponseMessage respuesta = await _servicioConsumoAPIUpdate.consumoAPI(Constantes.getGetCompanyEliminar + idEmpresa, HttpMethod.Delete);

                return new JsonResult(LeerRespuestas<MensajesRespuesta>.procesarRespuestaCRUD(respuesta, "C_Empresa", "AdministrarEmpresa", true));
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }
        #endregion Detalle

         #region Detalle
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> DetalleEmpresa(Guid idEmpresa)
        {
            var objUsuarioSesion = Sesion.recuperarSesion(HttpContext.Session);

            if (objUsuarioSesion != null)
            {
                EmpresaCompletoDTO objEmpresa = await obtenerEmpresa(idEmpresa);

                return View(objEmpresa);
            }
            return new JsonResult(new ObjetoRedireccion(Constantes.urlConsola + Constantes.mensajeSesionCaducada));
        }

        #endregion 

        #endregion

        #region Búsquedas
        [AccionesFiltro(nombreModulo = "Administración", nombreMenu = "Empresa", tipoPermiso = "Lectura", concedido = true)]
        public async Task<IActionResult> BusquedaAvanzada(ObjetoBusquedaEmpresas objBusqueda)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPIBusqueda.consumoAPI(Constantes.getGetCompaniesAdvance, HttpMethod.Get, objBusqueda);

            var listaEmpresas = await LeerRespuestas<List<EmpresaResultadoBusquedaDTO>>.procesarRespuestasConsultas(respuesta);

            return View("_ListaEmpresas", listaEmpresas);
        }
        #endregion

        #region Utilitario
        public async Task<EmpresaCompletoDTO> obtenerEmpresa(Guid idEmpresa)
        {
            HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getGetCompanyByID + idEmpresa, HttpMethod.Get);

            EmpresaCompletoDTO objEmpresa = await LeerRespuestas<EmpresaCompletoDTO>.procesarRespuestasConsultas(respuesta);

            return objEmpresa;
        }
        #endregion

    }
}
