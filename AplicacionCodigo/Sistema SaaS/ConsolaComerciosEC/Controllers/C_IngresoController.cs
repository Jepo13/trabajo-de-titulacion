﻿using DTOs.User;
using ConsolaComerciosEC.Filters;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;

namespace ConsolaComerciosEC.Controllers
{
    public class C_IngresoController : Controller
    {

        private readonly IServicioConsumoAPI<UserCompleteDTO> _servicioConsumoAPI;

        public C_IngresoController(IServicioConsumoAPI<UserCompleteDTO> servicioConsumoAPI)
        {
            _servicioConsumoAPI = servicioConsumoAPI;
        }

        [AccionesFiltro(inicioSesion = "Inicio")]
        public IActionResult Ingresar()
        {
            return View();
        }

        [HttpPost]
        //[AccionesFiltro(inicioSesion = "Inicio")]
        public async Task<IActionResult> Ingresar(string email, string password, string InputToken)
        {
            GoogleRecapchaData objGooogle = new GoogleRecapchaData
            {
                secret = "6LciDPkeAAAAADhq5NxVY8TBlC7pLq0k0nMsho1E",
                response = InputToken
            };

            HttpClient cliente = new HttpClient();

            var resultado = await cliente.GetStringAsync($"https://www.google.com/recaptcha/api/siteverify?secret={objGooogle.secret}&response={objGooogle.response}");

            var objeto = JObject.Parse(resultado);
            var banderaCapchaGoogle = (bool)objeto.SelectToken("success");

            if (banderaCapchaGoogle)
            {
                string nuevaContrasena = FuncionesContrasena.encriptarContrasena(password);

                HttpResponseMessage respuesta = await _servicioConsumoAPI.consumoAPI(Constantes.getLogin + "user=" + email + "&contrasena=" + nuevaContrasena, HttpMethod.Get);

                if (respuesta.IsSuccessStatusCode)
                {
                    UserSessionDTO objUsuario = await LeerRespuestas<UserSessionDTO>.procesarRespuestasConsultas(respuesta);

                    if (objUsuario != null)
                    {
                        HttpContext.Session.Clear();
                        SesionExtensions.SetObject(HttpContext.Session, Constantes.nombreSesion, objUsuario);

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.errorLogin = "Usuario y contraseña incorrecto.";
                    }
                }
                else if(respuesta.StatusCode.ToString()== "NotFound")
                {
                    ViewBag.errorLogin = "Usuario y contraseña incorrecto.";
                }
                else
                {
                    ViewBag.errorLogin = "Ocurrió un error al intentar ingresar.";
                }
            }
            else
            {
                ViewBag.errorLogin = "Captcha erroneo intente nuevamente.";
            }
            
            return View();
        }
    }
}
