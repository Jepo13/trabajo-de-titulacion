using DTOs.Catalogo;

using DTOs.Empresa;
using DTOs.Local;
using DTOs.Persona;
using DTOs.PorcentajeIVA;
using DTOs.Proveedor;
using DTOs.Rol;
using DTOs.User;
using ConsolaComerciosEC.Conexiones;
using ConsolaComerciosEC.Filters;
//using ConsolaComerciosEC.Middleware;
using ConsolaComerciosEC.Models;
using ConsolaComerciosEC.Servicios.Implementar;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Utilitarios;
using Utilitarios.Busquedas;
using Utilitarios.Busquedas.Catalogo;
using Utilitarios.Busquedas.Empresa;
using Utilitarios.Busquedas.Proveedor;
using Utilitarios.Busquedas.Usuario;
using Utilitarios.Busquedas.Local;
using DTOs.Cliente;
using DTOs.EmpresaCliente;
using DTOs.CategoriaProductos;
using DTOs.Productos;
using DTOs.Ubicacion;
using DTOs.Sitios;
using DTOs.Bandejas;
using DTOs.UnidadEntrega;

namespace ConsolaComerciosEC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddMvc().AddSessionStateTempDataProvider();
            services.AddSession(options =>
            {
                //cambiar el idle timeout a 5 minutos el default es 20
                //options.IdleTimeout = TimeSpan.FromSeconds(60 * 5);
                options.Cookie.IsEssential = true;
            });

            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddHttpClient<ConexionApi>(client =>
            {
                client.BaseAddress = new Uri("http://localhost:51723/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });
            

            services.AddScoped(typeof(LocalCreateDTO));
            services.AddScoped(typeof(EmpresaResultadoBusquedaDTO));
            services.AddScoped(typeof(LocalResultadoBusquedaDTO));
            services.AddScoped(typeof(UbicacionUpdateDTO));
            services.AddScoped(typeof(SitioCreateDTO));
            services.AddScoped(typeof(SitiosCompletoDTO));

            services.AddScoped(typeof(ObjetoBusquedaEmpresas));
            services.AddScoped(typeof(MensajesRespuesta));
            services.AddScoped(typeof(LocalCompleteDTO));
            services.AddScoped(typeof(LocalUpdateDTO));
            services.AddScoped(typeof(UbicacionCreateDTO));
            services.AddScoped(typeof(UbicacionCompleteDTO));
            services.AddScoped(typeof(BandejaCreateDTO));
            services.AddScoped(typeof(ObjetoBusquedaEmpresas));
            services.AddScoped(typeof(EmpresaUpdateDTO));
            services.AddScoped(typeof(EmpresaCreateDTO));
            services.AddScoped(typeof(UserCompleteDTO));
            services.AddScoped(typeof(AccionesFiltro));
            services.AddScoped(typeof(CatalogoDropDownDTO));
            services.AddScoped(typeof(CatalogoResultadoBusqueda));
            services.AddScoped(typeof(UserUpdateDTO));
            services.AddScoped(typeof(RolBusquedaDTO));
            services.AddScoped(typeof(UserCreateDTO));
            services.AddScoped(typeof(ObjetoBusquedaUsuarios));
            services.AddScoped(typeof(ObjetoBusquedaCatalogo));
            services.AddScoped(typeof(CatalogoCreateDTO));
            services.AddScoped(typeof(CatalogoEditarDTO));
            services.AddScoped(typeof(RolCreacionDTO));
            services.AddScoped(typeof(RolEditarDTO));
            services.AddScoped(typeof(ProveedorCreateDTO));
            services.AddScoped(typeof(ProveedorUpdateDTO));
            services.AddScoped(typeof(ObjetoBusquedaProveedor));
            services.AddScoped(typeof(PorcentajeIVACompleteDTO));
            services.AddScoped(typeof(ObjetoBusquedaLocal));
            services.AddScoped(typeof(ClienteObjetoBusquedaDTO));
            
            services.AddScoped(typeof(PersonaCreateDTO));
            services.AddScoped(typeof(PersonaClienteCreateDTO));
            services.AddScoped(typeof(PersonaClienteCompletoDTO));
            services.AddScoped(typeof(PersonaClienteUpdateDTO));
            services.AddScoped(typeof(PersonaUpdateDTO));
            services.AddScoped(typeof(PersonaCompleteDTO));

            services.AddScoped(typeof(EmpresaClienteCreateDTO));
            services.AddScoped(typeof(EmpresaClienteCompleteDTO));
            services.AddScoped(typeof(EmpresaClienteValidadDTO));
            services.AddScoped(typeof(EmpresaClienteUpdateDTO));

            services.AddScoped(typeof(CategoriaProductoDTOCrear));
            services.AddScoped(typeof(CategoriaProductoDTOCompleto));
            services.AddScoped(typeof(CategoriaProductoDTOActualizar));

            services.AddScoped(typeof(ProductoDTOCrear));
            services.AddScoped(typeof(ProductoDTOCompleto));
            services.AddScoped(typeof(ProductoDTOEditar));

            services.AddScoped(typeof(UnidadEntregaCreateDTO));
            services.AddScoped(typeof(UnidadEntregaUpdateDTO));
            services.AddScoped(typeof(UnidadEntregaCompleteDTO));
            services.AddScoped(typeof(UnidadEntregaBusquedaDTO));

            //services.AddScoped(typeof(ClienteEmpresaCompleteDTO));
            //services.AddScoped(typeof(ClienteEmpresaCreateDTO));

            services.TryAddScoped(typeof(IServicioConsumoAPI<>), typeof(ServicioConsumoAPI<>));
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();            
            services.AddHttpContextAccessor();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();
            //app.UseMiddleware<AutenticacionMiddleware>();

            app.UseCors(x => x
            .AllowAnyMethod()
            .AllowAnyHeader()
            .SetIsOriginAllowed(origin => true)
            .AllowCredentials());
      

            app.UseAuthorization();
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=C_Ingreso}/{action=Ingresar}/{id?}");
            });
        }
    }
}
