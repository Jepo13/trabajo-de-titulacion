﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsolaComerciosEC.Models
{
    public class ObjetoRedireccion
    {
        public ObjetoRedireccion(string url, string controlador, string vista)
        {
            Controlador = controlador;
            Vista = vista;
            this.URL = url;
        }
        public ObjetoRedireccion(string url)
        {
            this.URL = url;
        }

        public string Controlador { get; set; }
        public string Vista { get; set; }
        public string URL { get; set; }
        public virtual string urlCompleta
        {
            get
            {
                return URL;
            }
        }
    }
}
