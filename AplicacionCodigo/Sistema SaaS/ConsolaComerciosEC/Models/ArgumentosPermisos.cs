﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsolaComerciosEC.Models
{
    public class ArgumentosPermisos
    {        
        public string tipoPermiso { get; set; }
        public string controlador { get; set; }
        public bool concedido { get; set; }
    }
}
