﻿using DTOs.User;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;

namespace ConsolaComerciosEC.Models
{
    public static class SesionExtensions
    {
        public static void SetObject<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value, Formatting.Indented));
        }

        public static T GetObject<T>(this ISession session, string key)
        {
            var jsonString = session.GetString(key);

            if (string.IsNullOrEmpty(jsonString))
                return default(T);
            else
                return JsonConvert.DeserializeObject<T>(jsonString);

        }

    }//class
   
}
