﻿using DTOs.Catalogo;
using DTOs.Local;
using ConsolaComerciosEC.Servicios;
using ConsolaComerciosEC.Servicios.Interface;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;

namespace ConsolaComerciosEC.Models
{
    public static class DropDownsCatalogos<T> where T : class
    {
        public static SelectList cargarListaDropDown(List<CatalogoDropDownDTO> listaDropDownDTO, Guid? valorSeleccionado = null)

        {           
            SelectList objSelectList = new SelectList(listaDropDownDTO, "IdCatalogo", "Nombrecatalogo", valorSeleccionado);

            return objSelectList;
        }

        public static async Task<SelectList> cargarListaDropDownGenerico(IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPI, string urlAPI, string id, string value, Guid? valorSeleccionado = null)

        {
            HttpResponseMessage restapuestaCatalogo = await servicioConsumoAPI.consumoAPI(urlAPI,HttpMethod.Get);

            string responseJSON = await LeerRespuestas<string>.procesarRespuestasJSON(restapuestaCatalogo);
            var listaCatalogos = JsonConvert.DeserializeObject<T>(responseJSON);

            SelectList objSelectList = new SelectList((System.Collections.IEnumerable)listaCatalogos, id, value, valorSeleccionado);

            return objSelectList;
        }
      
        public static async Task<SelectList> cargarListaDropDownCatalogos(IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPI, string urlAPI, Guid? valorSeleccionado = null)

        {
            try
            {
                HttpResponseMessage restapuestaCatalogo = await servicioConsumoAPI.consumoAPI(urlAPI, HttpMethod.Get);

                string responseJSON = await LeerRespuestas<string>.procesarRespuestasJSON(restapuestaCatalogo);
                var listaCatalogos = JsonConvert.DeserializeObject<List<CatalogoDropDownDTO>>(responseJSON);

                SelectList objSelectList = new SelectList(listaCatalogos, "IdCatalogo", "Nombrecatalogo", valorSeleccionado);

                return objSelectList;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

         public static async Task<SelectList> cargarListaDropDownCatalogosPreselect(IServicioConsumoAPI<CatalogoDropDownDTO> servicioConsumoAPI, string urlAPI, Guid? idSeleccionado=null)

        {
            HttpResponseMessage restapuestaCatalogo = await servicioConsumoAPI.consumoAPI(urlAPI,HttpMethod.Get);

            string responseJSON = await LeerRespuestas<string>.procesarRespuestasJSON(restapuestaCatalogo);
            var listaCatalogos = JsonConvert.DeserializeObject<List<CatalogoDropDownDTO>>(responseJSON);

            if (listaCatalogos.Count > 0)
            {
                if (idSeleccionado!=null)
                {
                    SelectList objSelectList = new SelectList(listaCatalogos, "IdCatalogo", "Nombrecatalogo", listaCatalogos.FirstOrDefault().IdCatalogo);
                    return objSelectList;
                }
                else
                {
                    SelectList objSelectList = new SelectList(listaCatalogos, "IdCatalogo", "Nombrecatalogo", idSeleccionado);
                    return objSelectList;
                }

                
            }

            return null;
        }


        public static async Task<List<T>> procesarRespuestasConsultaCatlogoObjeto(IServicioConsumoAPI<T> servicioConsumoAPI, string urlAPI)
        {
            HttpResponseMessage restapuestaCatalogo = await servicioConsumoAPI.consumoAPI(urlAPI, HttpMethod.Get);

            List<T> objCatalogo = await LeerRespuestas<List<T>>.procesarRespuestasConsultas(restapuestaCatalogo);

            return objCatalogo;
        }

    }
}
