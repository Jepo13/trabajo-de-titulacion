﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsolaComerciosEC.Models
{
    public class GoogleRecapchaData
    {        public string response { get; set; }
        public string secret { get; set; }
    }
}
