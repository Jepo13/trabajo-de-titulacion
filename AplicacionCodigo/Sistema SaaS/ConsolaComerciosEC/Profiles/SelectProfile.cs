﻿using AutoMapper;
using DTOs.Select;
using ConsolaComerciosEC.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTOs.Catalogo;

namespace ConsolaComerciosEC.Profiles
{
    public class SelectProfile : Profile
    {
        public SelectProfile()
        {
            CreateMap<CustomSelectEmpresas, SelectList>();
            CreateMap<SelectList, CustomSelectEmpresas>().
                ForMember(x => x.DataTextField, y => y.MapFrom(fuente => fuente.DataTextField)).
                ForMember(x => x.Items, y => y.MapFrom(fuente => fuente.Items)).
                ForMember(x => x.SelectedValues, y => y.MapFrom(fuente => fuente.SelectedValue.ToString()));
            CreateMap<CatalogoDropDownDTO, CatalogoResultadoBusqueda>();
            CreateMap<CatalogoResultadoBusqueda, CatalogoDropDownDTO>();


        }
    }
}
