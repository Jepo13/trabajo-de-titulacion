﻿using AutoMapper;
using DTOs.Rol;
using DTOs.Select;
using ConsolaComerciosEC.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsolaComerciosEC.Profiles
{
    public class RolDTOProfile : Profile
    {
        public RolDTOProfile()
        {
            CreateMap<RolCompletoDTO, RolEditarDTO>();
        }
    }
}
