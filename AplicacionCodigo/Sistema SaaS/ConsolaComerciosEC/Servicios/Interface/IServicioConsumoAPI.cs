﻿using DTOs.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsolaComerciosEC.Servicios.Interface
{
    public interface IServicioConsumoAPI<T> where T : class
    {
        #region CRUD
        public Task<HttpResponseMessage> consumoAPI(string urlEndPoint, HttpMethod tipoMetodo, T obj = null);
        
        #endregion

    }
}
