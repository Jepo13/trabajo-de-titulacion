﻿using DTOs.Catalogo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilitarios;

namespace ConsolaComerciosEC.Servicios
{
    public static class LeerRespuestas<T> where T : class
    {
        public static async Task<T> procesarRespuestasConsultas(HttpResponseMessage respuestaCatalogo)
        {
            string respuestaString = "";
            
            if (respuestaCatalogo.IsSuccessStatusCode)
            {
                MemoryStream memoryContentStream = new();
                memoryContentStream.Seek(0, SeekOrigin.Begin);

                using (var streamContent = new StreamContent(memoryContentStream))
                {
                    try
                    {
                        respuestaString = await respuestaCatalogo.Content.ReadAsStringAsync();
                        var listaRespuesta = JsonConvert.DeserializeObject<T>(respuestaString);

                        return listaRespuesta;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            return default;
        }

        public static async Task<string> procesarRespuestasJSON(HttpResponseMessage respuestaCatalogo)
        {
            string respuestaString = "";

            if (respuestaCatalogo.IsSuccessStatusCode)
            {
                MemoryStream memoryContentStream = new();
                memoryContentStream.Seek(0, SeekOrigin.Begin);

                using (var streamContent = new StreamContent(memoryContentStream))
                {
                    try
                    {
                        respuestaString = await respuestaCatalogo.Content.ReadAsStringAsync();                        

                        return respuestaString;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return default;
        }

        public static async Task<MensajesRespuesta> procesarRespuestaCRUD(HttpResponseMessage respuesta, string controlador = "", string accion = "", bool eliminar = false)
        {
            MensajesRespuesta objMensajeRespuesta = new MensajesRespuesta();
            try
            {
                if (respuesta == null)
                {
                    objMensajeRespuesta = MensajesRespuesta.errorConexion();
                    return objMensajeRespuesta;
                }


                if (respuesta.IsSuccessStatusCode && !eliminar)
                {

                    if (!string.IsNullOrEmpty(controlador) && !string.IsNullOrEmpty(accion))
                    {
                        objMensajeRespuesta = MensajesRespuesta.guardarOK(controlador, accion);
                    }
                    else
                    {
                        objMensajeRespuesta = MensajesRespuesta.guardarOK();
                    }
                    return objMensajeRespuesta;
                }
                else if (respuesta.IsSuccessStatusCode && eliminar)
                {

                    if (!string.IsNullOrEmpty(controlador) && !string.IsNullOrEmpty(accion))
                    {
                        objMensajeRespuesta = MensajesRespuesta.eliminadoOK(controlador, accion);
                    }
                    else
                    {
                        objMensajeRespuesta = MensajesRespuesta.eliminadoOK();
                    }

                    return objMensajeRespuesta;
                }
                else
                {
                    try
                    {
                        var createdContent = await respuesta.Content.ReadAsStringAsync();
                        objMensajeRespuesta = JsonConvert.DeserializeObject<MensajesRespuesta>(createdContent);

                        if (objMensajeRespuesta.title == "Not Acceptable")
                        {
                            objMensajeRespuesta = MensajesRespuesta.errorInesperado();
                        }
                    }
                    catch (Exception)
                    {
                        objMensajeRespuesta = await respuesta.ExceptionResponse();
                    }
                    return objMensajeRespuesta;
                }
            }
            catch (Exception ex)
            {
            }
            return MensajesRespuesta.errorInesperado();
        }

    }
}
