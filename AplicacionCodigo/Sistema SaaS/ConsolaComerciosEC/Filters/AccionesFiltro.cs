﻿using DTOs.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilitarios;

namespace ConsolaComerciosEC.Filters
{
    public class AccionesFiltro : ActionFilterAttribute
    {
        public string inicioSesion { get; set; }
        public string sesionHome { get; set; }
        public string nombreModulo { get; set; }
        public string nombreMenu { get; set; }
        public string tipoPermiso { get; set; }        
        public bool concedido { get; set; }
       
        public override void OnActionExecuting(ActionExecutingContext actionExecutingContext)
        {
            bool banderaContinuar = false;
            try
            {
                string json = actionExecutingContext.HttpContext.Session.GetString(Constantes.nombreSesion);
                

                if (nombreModulo != null)
                {
                    var objSesion = JsonConvert.DeserializeObject<UserSessionDTO>(json);

                    var modulo = objSesion.Modulos.Where(x => x.Nombre.ToUpper().Trim() == nombreModulo.ToUpper().Trim()).FirstOrDefault();

                    if (modulo != null)
                    {
                        var menu = modulo.listaMenuDTO.Where(x => x.NombreMenu.ToUpper().Trim() == nombreMenu.ToUpper().Trim()).FirstOrDefault();
                        if (menu != null)
                        {
                            var permiso = menu.listaPermisosDTO.Where(x => x.Nombrepermiso.ToUpper().Trim() == tipoPermiso.ToUpper().Trim()).FirstOrDefault();
                            if (permiso!=null)
                            {
                                if (permiso.Concedido == concedido)
                                {
                                    banderaContinuar = true;
                                } 
                            }
                        }
                    }
                    if (!banderaContinuar)
                    {
                        actionExecutingContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "Home",
                            action = "SinPermisos",
                            returnurl = Microsoft.AspNetCore.Http.Extensions.UriHelper.GetEncodedUrl(actionExecutingContext.HttpContext.Request)
                        }));
                    }
                }


            }
            catch (Exception ex)
            {

                actionExecutingContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "C_Ingreso",
                    action = "Ingresar",
                    returnurl = Microsoft.AspNetCore.Http.Extensions.UriHelper.GetEncodedUrl(actionExecutingContext.HttpContext.Request)
                }));
            }
           
        }
    }
}
