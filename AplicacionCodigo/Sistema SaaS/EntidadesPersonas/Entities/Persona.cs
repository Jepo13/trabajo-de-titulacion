﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesPersonas.Entities
{
    public partial class Persona
    {
        public Persona()
        {
            EmpresaClientes = new HashSet<EmpresaCliente>();
        }

        public Guid IdPersona { get; set; }
        public Guid? IdLugarNacimiento { get; set; }
        public Guid? IdResidencia { get; set; }
        public Guid? IdPaisOrigen { get; set; }
        public Guid? IdTipoIdentificacion { get; set; }
        public Guid? IdTipoContribuyente { get; set; }
        public Guid? IdEstadoCivil { get; set; }
        public byte[] ImagenPersona { get; set; }
        public string FormatoArchivo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NumeroIdentificacion { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string ContactoEmergencia { get; set; }
        public string Direccion { get; set; }
        public string CorreoPersonal { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual ICollection<EmpresaCliente> EmpresaClientes { get; set; }
    }
}
