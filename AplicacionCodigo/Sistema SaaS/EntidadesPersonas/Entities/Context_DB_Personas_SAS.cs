﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EntidadesPersonas.Entities
{
    public partial class Context_DB_Personas_SAS : DbContext
    {
        public Context_DB_Personas_SAS()
        {
        }

        public Context_DB_Personas_SAS(DbContextOptions<Context_DB_Personas_SAS> options)
            : base(options)
        {
        }

        public virtual DbSet<EmpresaCliente> EmpresaClientes { get; set; }
        public virtual DbSet<Persona> Personas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=181.39.23.39;database=SAS_Personas_DB;persist security info=True;user id=User_SAS_DB;password=*-9@xp.ds4s@-;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<EmpresaCliente>(entity =>
            {
                entity.HasKey(e => e.IdEmpresaCliente);

                entity.ToTable("EMPRESA_CLIENTE");

                entity.Property(e => e.IdEmpresaCliente)
                    .HasColumnName("ID_EMPRESA_CLIENTE")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.DiasCredito).HasColumnName("DIAS_CREDITO");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.IdEmpresa).HasColumnName("ID_EMPRESA");

                entity.Property(e => e.IdPersona)
                    .HasColumnName("ID_PERSONA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.PorcentajeDescuento).HasColumnName("PORCENTAJE_DESCUENTO");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");

                entity.Property(e => e.ValorCupoCreditoPersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_CUPO_CREDITO_PERSONAL")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ValorCupoDisponibleCreditoPersonal)
                    .HasColumnType("decimal(12, 5)")
                    .HasColumnName("VALOR_CUPO_DISPONIBLE_CREDITO_PERSONAL");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.EmpresaClientes)
                    .HasForeignKey(d => d.IdPersona)
                    .HasConstraintName("FK_EMPRESA__REFERENCE_PERSONA");
            });

            modelBuilder.Entity<Persona>(entity =>
            {
                entity.HasKey(e => e.IdPersona);

                entity.ToTable("PERSONA");

                entity.HasIndex(e => e.NumeroIdentificacion, "AK_UNICOIDENTIFICACIO_PERSONA")
                    .IsUnique();

                entity.Property(e => e.IdPersona)
                    .HasColumnName("ID_PERSONA")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Apellido)
                    .HasMaxLength(25)
                    .HasColumnName("APELLIDO");

                entity.Property(e => e.ContactoEmergencia)
                    .HasMaxLength(11)
                    .HasColumnName("CONTACTO_EMERGENCIA");

                entity.Property(e => e.CorreoPersonal)
                    .HasMaxLength(60)
                    .HasColumnName("CORREO_PERSONAL");

                entity.Property(e => e.Direccion).HasColumnName("DIRECCION");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_CREACION");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_MODIFICACION");

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnType("datetime")
                    .HasColumnName("FECHA_NACIMIENTO");

                entity.Property(e => e.FormatoArchivo)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("FORMATO_ARCHIVO");

                entity.Property(e => e.IdEstadoCivil)
                    .HasColumnName("ID_ESTADO_CIVIL")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IdLugarNacimiento).HasColumnName("ID_LUGAR_NACIMIENTO");

                entity.Property(e => e.IdPaisOrigen).HasColumnName("ID_PAIS_ORIGEN");

                entity.Property(e => e.IdResidencia).HasColumnName("ID_RESIDENCIA");

                entity.Property(e => e.IdTipoContribuyente).HasColumnName("ID_TIPO_CONTRIBUYENTE");

                entity.Property(e => e.IdTipoIdentificacion)
                    .HasColumnName("ID_TIPO_IDENTIFICACION")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ImagenPersona)
                    .HasColumnType("image")
                    .HasColumnName("IMAGEN_PERSONA");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(25)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.NumeroIdentificacion)
                    .HasMaxLength(15)
                    .HasColumnName("NUMERO_IDENTIFICACION");

                entity.Property(e => e.Telefono1)
                    .HasMaxLength(11)
                    .HasColumnName("TELEFONO_1");

                entity.Property(e => e.Telefono2)
                    .HasMaxLength(11)
                    .HasColumnName("TELEFONO_2");

                entity.Property(e => e.UsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_CREACION");

                entity.Property(e => e.UsuarioModificacion)
                    .HasMaxLength(36)
                    .HasColumnName("USUARIO_MODIFICACION");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
