﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EntidadesPersonas.Entities
{
    public partial class EmpresaCliente
    {
        public Guid IdEmpresaCliente { get; set; }
        public Guid? IdPersona { get; set; }
        public int? PorcentajeDescuento { get; set; }
        public decimal? ValorCupoCreditoPersonal { get; set; }
        public decimal? ValorCupoDisponibleCreditoPersonal { get; set; }
        public int? DiasCredito { get; set; }
        public Guid IdEmpresa { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }

        public virtual Persona IdPersonaNavigation { get; set; }
    }
}
