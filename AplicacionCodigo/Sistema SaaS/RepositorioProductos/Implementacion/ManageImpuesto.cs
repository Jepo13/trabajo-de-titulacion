﻿using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageImpuesto : IManageImpuesto
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageImpuesto(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Search
        #region Get Impuesto By Id
        public async Task<Impuesto> GetImpuestoById(Guid idImpuesto)
        {
            Impuesto objImpuesto = await _context.Impuestos.Where(c => c.IdImpuesto == idImpuesto).FirstOrDefaultAsync();
            return objImpuesto;
        }
        #endregion
        #region Get All 
        public async Task<List<Impuesto>> GetAllImpuesto()
        {
            List<Impuesto> listImpuesto = new List<Impuesto>();
            try
            {
                listImpuesto = await _context.Impuestos.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listImpuesto;
        }
        #endregion
        #region Get by Name
        public async Task<Impuesto> GetImpuestoByName(string name)
        {
            Impuesto objImpuesto = await _context.Impuestos.Where(c => c.NombreImpuesto.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objImpuesto;
        }
        #endregion
        #endregion

    }
}
