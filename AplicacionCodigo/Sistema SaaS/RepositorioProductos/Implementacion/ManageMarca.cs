﻿using DTOs.Marca;
using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RepositorioProductos.Implementacion
{
    public class ManageMarca: IManageMarca
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageMarca(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Search       
        public async Task<Marca> GetMarcaById(Guid idMarca)
        {
            Marca listaMarca = await _context.Marcas.Where(c => c.IdMarca == idMarca).FirstOrDefaultAsync();

            return listaMarca;
        }
        
        public async Task<List<Marca>> GetAllMarcas(Guid idEmpresa)
        {
            List<Marca> listaMarca = await _context.Marcas.Where(c => c.IdEmpresa == idEmpresa).ToListAsync();

            return listaMarca;
        }
        

        public async Task<List<Marca>> GetMarcaByName(string name, Guid idEmpresa)
        {
            List<Marca> listaMarca = await _context.Marcas.Where(c => c.Nombre.Trim().ToUpper().Contains(name.Trim().ToUpper()) && c.IdEmpresa == idEmpresa).ToListAsync();

            return listaMarca;
        }


        public async Task<Marca> GetMarcaByNameExact(string name, Guid idEmpresa)
        {
            Marca listaMarca = await _context.Marcas.Where(c => c.Nombre.Trim().ToUpper() == name.Trim().ToUpper() && c.IdEmpresa == idEmpresa).FirstOrDefaultAsync();

            return listaMarca;
        }

        Task<List<Marca>> IManageMarca.BusquedaAvanzadaMarca(MarcaBusquedaDTO objMarca)
        {
            throw new NotImplementedException();
        }
        #endregion



    }
}
