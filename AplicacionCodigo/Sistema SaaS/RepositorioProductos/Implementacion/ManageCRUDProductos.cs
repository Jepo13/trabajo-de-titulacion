﻿using EntidadesProductos.Entities;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageCRUDProductos<T>: IManageCRUDProductos<T> where T : class
    {
        public readonly Context_DB_Productos_SAS _context;

        public ManageCRUDProductos(Context_DB_Productos_SAS context)
        {
            _context = context;
        }

        public void Add(T obj)
        {
            try
            {
                obj.GetType().GetProperty("FechaCreacion").SetValue(obj, DateTime.Now);
                obj.GetType().GetProperty("FechaModificacion").SetValue(obj, DateTime.Now);
                obj.GetType().GetProperty("UsuarioModificacion").SetValue(obj, obj.GetType().GetProperty("UsuarioCreacion").GetValue(obj, null));
     
                _context.AddAsync(obj);
            }
            catch (Exception exValidation)
            {

            }
        }

        public void Edit(T obj)
        {
            obj.GetType().GetProperty("FechaModificacion").SetValue(obj, DateTime.Now);
        }

        public void Delete(T obj)
        {

            _context.Remove(obj);
        }
        public async Task<(bool estado, string mensajeError)> save()
        {
            try
            {
                var created = await _context.SaveChangesAsync();
                return (created > 0, string.Empty);
            }
            catch (Exception ex)
            {
                try
                {
                    var created = await _context.SaveChangesAsync();
                    return (created > 0, string.Empty);
                }
                catch (Exception exx)
                {
                    string mensajeError = "";
                    if (exx.InnerException != null)
                        mensajeError = ex.InnerException.Message;

                    return (false, mensajeError);
                }
            }

        }
    }
}
