﻿using DTOs.UnidadEntrega;
using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageUnidadEntrega : IManageUnidadEntrega
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageUnidadEntrega(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Ge UnidadEntrega By Id
        public async Task<UnidadEntrega> GetUnidadEntregaById(Guid idUnidadEntrega)
        {
            UnidadEntrega objUnidadEntrega = await _context.UnidadEntregas.Where(c => c.IdUnidadentrega == idUnidadEntrega).FirstOrDefaultAsync();
            return objUnidadEntrega;
        }
        #endregion
        #region Get All 
        public async Task<List<UnidadEntrega>> GetAllUnidadEntrega(Guid idEmpresa)
        {
            List<UnidadEntrega> listUnidadentrega = new List<UnidadEntrega>();
            try
            {
                listUnidadentrega = await _context.UnidadEntregas.Where(x => x.IdEmpresa == idEmpresa).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listUnidadentrega;
        }
        #endregion

        #region Get by Name
        public async Task<List<UnidadEntrega>> GetUnidadEntregaByName(string name)
        {
            List<UnidadEntrega> listUnidadentrega = await _context.UnidadEntregas.Where(c => c.NombreContenido.Trim().ToUpper().Contains(name.Trim().ToUpper())).ToListAsync();

            return listUnidadentrega;
        }
        #endregion

        #region Get by Name
        public async Task<UnidadEntrega> GetUnidadEntregaByNameExact(string name, Guid idEmpresa)
        {
            UnidadEntrega objUnidadentrega = await _context.UnidadEntregas.Where(c => c.NombreContenido.Trim().ToUpper() == name.Trim().ToUpper() && c.IdEmpresa == idEmpresa).FirstOrDefaultAsync();

            return objUnidadentrega;
        }
        #endregion
        #region Get by Code
        public async Task<UnidadEntrega> GetUnidadEntregaByCode(string code, Guid idEmpresa)
        {
            UnidadEntrega objUnidadentrega = await _context.UnidadEntregas.Where(c => c.CodigoUnidad.Trim().ToUpper() == code.Trim().ToUpper() && c.IdEmpresa == idEmpresa).FirstOrDefaultAsync();
            return objUnidadentrega;
        }
        #endregion

        #region GetUBusquedaAvanzada
        public async Task<List<UnidadEntrega>> GetUBusquedaAvanzada(UnidadEntregaBusquedaDTO objBusqueda)
        {
            List<UnidadEntrega> listaDTO = await _context.UnidadEntregas.Where(x => x.IdEmpresa == objBusqueda.IdEmpresa).ToListAsync();

            if (listaDTO != null)
            {
                if (!string.IsNullOrEmpty(objBusqueda.NombreContenido))
                    listaDTO = listaDTO.Where(x => x.NombreContenido.ToUpper().Trim().Contains(objBusqueda.NombreContenido.ToUpper().Trim())).ToList();

                if (!string.IsNullOrEmpty(objBusqueda.Codigounidad))
                    listaDTO = listaDTO.Where(x => x.CodigoUnidad.ToUpper().Trim().Contains(objBusqueda.Codigounidad.ToUpper().Trim())).ToList();

                listaDTO = listaDTO.Where(x => x.EstadoUnidad == objBusqueda.EstadoUnidad).ToList();
            }

            return listaDTO;
        }
        #endregion


    }
}
