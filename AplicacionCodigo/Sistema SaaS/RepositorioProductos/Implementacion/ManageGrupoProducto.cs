﻿using DTOs.GrupoProducto;
using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageGrupoProducto : IManageGrupoProducto
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageGrupoProducto(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Search
        #region Get GrupoProductos By Id
        public async Task<GrupoProducto> GetGrupoProductoById(Guid idGrupoProducto)
        {
            GrupoProducto objGrupoProducto = await _context.GrupoProductos.Where(c => c.IdGrupoProducto == idGrupoProducto).FirstOrDefaultAsync();
            return objGrupoProducto;
        }
        #endregion
        #region Get by Name
        public async Task<GrupoProducto> GetGrupoProductoByName(string name, Guid idEmpresa)
        {
            GrupoProducto objGrupoProducto =
                await _context.GrupoProductos.Where(x =>
                x.NombreGrupoProducto.Trim().ToUpper().Contains(name.Trim().ToUpper()) && x.IdEmpresa == idEmpresa).FirstOrDefaultAsync();

            return objGrupoProducto;
        }
        #endregion

        #region Get All 
        public async Task<List<GrupoProducto>> GetAllGrupoProducto(Guid idEmpresa)
        {
            List<GrupoProducto> listGrupoProducto = new List<GrupoProducto>();
            try
            {
                listGrupoProducto = await _context.GrupoProductos.Where(x => x.IdEmpresa == idEmpresa).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listGrupoProducto;
        }

        public async Task<List<GrupoProducto>> GetUBusquedaAvanzada(GrupoProductoBusquedaDTO objBusqueda)
        {
            List<GrupoProducto> listGrupoProducto = new List<GrupoProducto>();
            try
            {
                listGrupoProducto = await _context.GrupoProductos.Where(x => x.IdEmpresa == objBusqueda.IdEmpresa).ToListAsync();

                if (!string.IsNullOrEmpty(objBusqueda.Nombregrupoproducto))
                    listGrupoProducto = listGrupoProducto.Where(x => x.NombreGrupoProducto.ToUpper().Trim().Contains(objBusqueda.Nombregrupoproducto.ToUpper().Trim())).ToList();

                listGrupoProducto = listGrupoProducto.Where(x => x.EstadoGrupoProducto == objBusqueda.EstadoGrupoProducto).ToList();

            }
            catch (Exception ex)
            {

            }
            return listGrupoProducto;
        }
        #endregion

        #endregion


    }
}
