﻿using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageEstadoProducto : IManageEstadoProducto
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageEstadoProducto(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Get Bodega by Id
        public async Task<EstadoProducto> GetEstadoProductoById(Guid idEstadoProducto)
        {
            EstadoProducto objEstadoProducto = await _context.EstadoProductos.Where(c => c.IdEstadoProducto == idEstadoProducto).FirstOrDefaultAsync();
            return objEstadoProducto;
        }
        #endregion
        #region Get All 
        public async Task<List<EstadoProducto>> GetAllEstadoProducto()
        {
            List<EstadoProducto> listEstadoProducto = new List<EstadoProducto>();
            try
            {
                listEstadoProducto = await _context.EstadoProductos.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listEstadoProducto;
        }
        #endregion
        #region Get By Name
        public async Task<EstadoProducto> GetEstadoProductoByName(string name)
        {
            EstadoProducto objEstadoproducto = await _context.EstadoProductos.Where(c => c.Nombre.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objEstadoproducto;
        }
        #endregion
    }
}
