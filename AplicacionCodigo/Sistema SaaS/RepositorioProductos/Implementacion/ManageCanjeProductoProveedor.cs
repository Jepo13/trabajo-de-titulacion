﻿using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageCanjeProductoProveedor : IManageCanjeProductoProveedor
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageCanjeProductoProveedor(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region Search

        #region Get Bodega by Id
        public async Task<CanjeProductoProveedor> GetCanjeProductoProveedorById(Guid idCanjeProductoProveedor)
        {
            CanjeProductoProveedor objCanjeProductoProveedor = await _context.CanjeProductoProveedors.Where(c => c.IdCanjeProductoProveedor == idCanjeProductoProveedor).FirstOrDefaultAsync();
            return objCanjeProductoProveedor;
        }
        #endregion

        #region Get All 
        public async Task<List<CanjeProductoProveedor>> GetAllCanjeProductoProveedor()
        {
            List<CanjeProductoProveedor> listBCanjeProductoProveedor = new();
            try
            {
                listBCanjeProductoProveedor = await _context.CanjeProductoProveedors.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listBCanjeProductoProveedor;
        }
        #endregion

        #region Get by Identificador Canje Proveedor
        public async Task<CanjeProductoProveedor> GetCanjeProductoProveedorByIdentificador(string Identificador)
        {
            CanjeProductoProveedor objCanjeProductoProveedor = await _context.CanjeProductoProveedors.Where(c => c.IdentificadorCanjeProducto.Trim().ToUpper().Contains(Identificador.Trim().ToUpper())).FirstOrDefaultAsync();
            return objCanjeProductoProveedor;
        }
        #endregion

        #endregion


    }
}
