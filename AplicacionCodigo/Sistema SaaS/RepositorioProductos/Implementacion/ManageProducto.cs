﻿using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageProducto : IManageProducto
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageProducto(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        public async Task<Producto> GetProductoById(Guid idProducto)
        {
            Producto objProducto = await _context.Productos.Where(x => x.IdProducto == idProducto).FirstOrDefaultAsync();

            return objProducto;
        }

        public async Task<List<Producto>> GetProductoByName(string name)
        {
            var listaProductos = await _context.Productos.Where(x => x.NombreProducto.Trim().ToUpper().Contains(name.Trim().ToUpper()) || x.NombreCorto.Trim().ToUpper().Contains(name.Trim().ToUpper())).ToListAsync();

            return listaProductos;
        }
    }
}
