﻿using EntidadesProductos.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProductos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Implementacion
{
    public class ManageCategoria: IManageCategoria
    {
        public readonly Context_DB_Productos_SAS _context;
        public ManageCategoria(Context_DB_Productos_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Search

        #region Get Bodega by Id
        public async Task<CategoriaProducto> GetCategoriaById(Guid idCategoria)
        {
            CategoriaProducto objCategoria = await _context.CategoriaProductos.Where(c => c.IdCategoriaProducto == idCategoria).FirstOrDefaultAsync();
            return objCategoria;
        }
        #endregion
        #region Get All 
        public async Task<List<CategoriaProducto>> GetAllCategoria()
        {
            List<CategoriaProducto> listCategoria = new();
            try
            {
                listCategoria = await _context.CategoriaProductos.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listCategoria;
        }
        #endregion
        #region Get by Name
        public async Task<CategoriaProducto> GetCategoriaByName(string name)
        {
            CategoriaProducto objCategoria = await _context.CategoriaProductos.Where(c => c.NombreCategoriaProducto.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();

            return objCategoria;
        }

        #endregion

        public async Task<List<CategoriaProducto>> GetCategoriaByIdEmpresa(Guid idEmpresa)
        {
            List<CategoriaProducto> listCategoria = new();
            try
            {
                listCategoria = await _context.CategoriaProductos.Where(x => x.IdEmpresa == idEmpresa).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listCategoria;
        }

        public async Task<List<CategoriaProducto>> GetCategoriaByIDPadre(Guid idCategoriaPadre)
        {
            List<CategoriaProducto> listCategoria = new();
            try
            {
                listCategoria = await _context.CategoriaProductos.Where(x => x.IdCategoriaProductoPadre == idCategoriaPadre).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listCategoria;
        }



        #endregion


    }
}
