﻿using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageImpuesto
    {

        #region Search
        public Task<Impuesto> GetImpuestoById(Guid idImpuesto);
        public Task<List<Impuesto>> GetAllImpuesto();
        public Task<Impuesto> GetImpuestoByName(string name);
        #endregion

    }
}
