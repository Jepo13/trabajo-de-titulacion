﻿using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageCanjeProductoProveedor
    {

        public Task<CanjeProductoProveedor> GetCanjeProductoProveedorById(Guid idCanjeProductoProveedor);
        public Task<List<CanjeProductoProveedor>> GetAllCanjeProductoProveedor();
        public Task<CanjeProductoProveedor> GetCanjeProductoProveedorByIdentificador(string Identificador);
    }
}
