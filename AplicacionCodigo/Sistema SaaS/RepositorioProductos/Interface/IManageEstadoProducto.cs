﻿using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageEstadoProducto
    {
        public Task<EstadoProducto> GetEstadoProductoById(Guid idEstadoProducto);
        public Task<List<EstadoProducto>> GetAllEstadoProducto();
        public Task<EstadoProducto> GetEstadoProductoByName(string name);
    }
}
