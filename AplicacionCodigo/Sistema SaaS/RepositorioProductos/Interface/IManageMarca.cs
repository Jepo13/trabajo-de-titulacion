﻿using DTOs.Marca;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageMarca
    {
        #region Search
        public Task<Marca> GetMarcaById(Guid idMarca);
        public Task<List<Marca>> GetAllMarcas(Guid idEmpresa);
        public Task<List<Marca>> GetMarcaByName(string name, Guid idEmpresa);
        public Task<Marca> GetMarcaByNameExact(string name, Guid idEmpresa);
        public Task<List<Marca>> BusquedaAvanzadaMarca(MarcaBusquedaDTO objMarca);
        #endregion

    }
}
