﻿using DTOs.GrupoProducto;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageGrupoProducto
    {
        #region Search
        public Task<GrupoProducto> GetGrupoProductoById(Guid idGrupoProducto);
        public Task<GrupoProducto> GetGrupoProductoByName(string name, Guid idEmpresa);
        public Task<List<GrupoProducto>> GetAllGrupoProducto(Guid idEmpresa);
        public Task<List<GrupoProducto>> GetUBusquedaAvanzada(GrupoProductoBusquedaDTO objBusqueda);
        #endregion

    }
}
