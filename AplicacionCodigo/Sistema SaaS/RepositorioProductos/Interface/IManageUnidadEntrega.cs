﻿using DTOs.UnidadEntrega;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageUnidadEntrega
    {

        #region Search
        public Task<UnidadEntrega> GetUnidadEntregaById(Guid idUnidadEntrega);
        public Task<List<UnidadEntrega>> GetAllUnidadEntrega(Guid idEmpresa);
        public Task<List<UnidadEntrega>> GetUnidadEntregaByName(string name);
        public Task<UnidadEntrega> GetUnidadEntregaByNameExact(string name, Guid idEmpresa);
        public Task<UnidadEntrega> GetUnidadEntregaByCode(string codigo, Guid idEmpresa);
        public Task<List<UnidadEntrega>> GetUBusquedaAvanzada(UnidadEntregaBusquedaDTO objBusqueda);
        #endregion
    }
}
