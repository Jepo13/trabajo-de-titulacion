﻿using DTOs.GrupoProducto;
using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageProducto
    {
        #region Search
        public Task<Producto> GetProductoById(Guid idProducto);
        public Task<List<Producto>> GetProductoByName(string name);
    
        #endregion

    }
}
