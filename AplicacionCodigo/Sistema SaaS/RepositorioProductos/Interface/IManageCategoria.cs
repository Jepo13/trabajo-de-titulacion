﻿using EntidadesProductos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProductos.Interface
{
    public interface IManageCategoria
    {
        #region Search
        public Task<List<CategoriaProducto>> GetCategoriaByIdEmpresa(Guid idEmpresa);
        public Task<List<CategoriaProducto>> GetCategoriaByIDPadre(Guid idCategoriaPadre);
        public Task<CategoriaProducto> GetCategoriaById(Guid idCategoria);
        public Task<List<CategoriaProducto>> GetAllCategoria();
        public Task<CategoriaProducto> GetCategoriaByName(string name);

        #endregion


    }
}
