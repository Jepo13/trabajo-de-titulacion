﻿using EntidadesCatalogosNotas.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioCatalogoNotas.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioCatalogoNotas.Implementacion
{
    public class ManageNota : IManageNota
    {
        public readonly Context_DB_Catalogos_Nota_SAS _context;
        public ManageNota(Context_DB_Catalogos_Nota_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region Search
        #region Get Bodega by Id
        public async Task<Nota> GetNotaById(Guid idNota)
        {
            Nota objNota = await _context.Notas.Where(c => c.IdNotas == idNota).FirstOrDefaultAsync();
            return objNota;
        }
        #endregion
        #region Get All 
        public async Task<List<Nota>> GetAllNotas()
        {
            List<Nota> listNota = new();
            try
            {
                listNota = await _context.Notas.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listNota;
        }
        #endregion

        #region Get by Name
        public async Task<Nota> GetNotaByTitulo(string titulo)
        {
            try
            {
                Nota objNota = await _context.Notas.Where(c => c.TituloNota.Trim().ToUpper().Contains(titulo.Trim().ToUpper())).FirstOrDefaultAsync();

                return objNota;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        #endregion

        #endregion
    }
}
