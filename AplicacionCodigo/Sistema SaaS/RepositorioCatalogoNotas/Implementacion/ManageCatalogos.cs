﻿using EntidadesCatalogosNotas.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioCatalogoNotas.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilitarios;
using System.Text;
using System.Threading.Tasks;
using DTOs.Catalogo;

namespace RepositorioCatalogoNotas.Implementacion
{
    public class ManageCatalogos : IManageCatalogos
    {
        public readonly Context_DB_Catalogos_Nota_SAS _context;
        public ManageCatalogos(Context_DB_Catalogos_Nota_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }


        #region Search Catalogos 
        #region Get Catalogo by Id
        public async Task<Catalogo> GetCatalogoById(Guid idCatalogo)
        {
            Catalogo catalogo = await _context.Catalogos.Where(c => c.IdCatalogo == idCatalogo).FirstOrDefaultAsync();
            return catalogo;
        }
        #endregion

        #region Get Catalogos hijo por Codigo Padre


        #endregion

        #region Get All 
        public async Task<List<Catalogo>> GetAllCatalogoByCompany(Guid idEmpresa)
        {
            List<Catalogo> listCatalogo = new();
            try
            {
                listCatalogo = await _context.Catalogos.Select(p => p).Include(x => x.IdCatalogopadreNavigation).Where(x => x.IdEmpresa == idEmpresa).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listCatalogo;
        }
        #endregion

        #region Get by Name
        public async Task<List<Catalogo>> GetCatalogoByName(string name)
        {
            List<Catalogo> listCatalogo = await _context.Catalogos.Where(c => c.NombreCatalogo.Trim().ToUpper().Contains(name.Trim().ToUpper())).ToListAsync();
            return listCatalogo;
        }
        #endregion

        #region Get by Code
        public async Task<Catalogo> GetCatalogoByCodeIDEmpresa(string code, Guid idEmpresa)
        {
            Catalogo objCatalogo = await _context.Catalogos.Where(c => c.CodigoCatalogo.Trim().ToUpper().Contains(code.Trim().ToUpper()) && c.IdEmpresa == idEmpresa).Include(x => x.InverseIdCatalogopadreNavigation).FirstOrDefaultAsync();
            return objCatalogo;
        }
        #endregion

        #region Get Child by Parent Code 
        public async Task<List<Catalogo>> GetChildByParentCodeIDEmpresa(string code, Guid? idEmpresa)
        {
            List<Catalogo> listaCatalogos = new();
            if (idEmpresa != null && idEmpresa != ConstantesAplicacion.guidNulo)
            {
                listaCatalogos = await _context.Catalogos.Where(x => x.CodigoCatalogo.Trim().ToUpper() == code.Trim().ToUpper() && x.IdEmpresa == idEmpresa)
                .Include(x => x.InverseIdCatalogopadreNavigation)
                .SelectMany(y => y.InverseIdCatalogopadreNavigation).ToListAsync();
            }
            else
            {
                listaCatalogos = await _context.Catalogos.Where(x => x.CodigoCatalogo.Trim().ToUpper() == code.Trim().ToUpper())
                .Include(x => x.InverseIdCatalogopadreNavigation)
                .SelectMany(y => y.InverseIdCatalogopadreNavigation).ToListAsync();
            }


            return listaCatalogos;
        }
        #endregion

        public async Task<List<Catalogo>> GetChildsByParentCode(string codeParent, Guid? idEmpresa)
        {
            if (!string.IsNullOrEmpty(codeParent))
            {
                try
                {
                    if (idEmpresa != null && idEmpresa != ConstantesAplicacion.guidNulo)
                    {
                        List<Catalogo> listaCatalogo = await _context.Catalogos.Where(x => x.CodigoCatalogo.Trim().ToUpper() == codeParent.Trim().ToUpper() && x.IdEmpresa == idEmpresa)
                .Include(x => x.InverseIdCatalogopadreNavigation)
                .SelectMany(y => y.InverseIdCatalogopadreNavigation).ToListAsync();
                        return listaCatalogo;
                    }
                    else
                    {
                        List<Catalogo> listaCatalogo = await _context.Catalogos.Where(x => x.CodigoCatalogo.Trim().ToUpper() == codeParent.Trim().ToUpper())
                .Include(x => x.InverseIdCatalogopadreNavigation)
                .SelectMany(y => y.InverseIdCatalogopadreNavigation).ToListAsync();
                        return listaCatalogo;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return default;
        }



        #region Get Child by Parent Code by ID
        public async Task<List<Catalogo>> GetChildByParentCodeByID_IDEmpresa(Guid idPadre, Guid? idEmpresa)
        {
            List<Catalogo> listaCatalogos = new();
            if (idEmpresa != null && idEmpresa != ConstantesAplicacion.guidNulo)
            {
                listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogopadre == idPadre && x.IdEmpresa == idEmpresa).ToListAsync();
            }
            else
            {
                listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogopadre == idPadre).ToListAsync();
            }



            return listaCatalogos;
        }
        #endregion

        #region Get Child by Parent Code by Name
        public async Task<List<Catalogo>> GetChildByParentCodeByID_Nombre(string nombrePadre, Guid idEmpresa)
        {
            var catalogoPadre = await _context.Catalogos.Where(x => x.NombreCatalogo.ToLower().Trim() == nombrePadre.ToLower().Trim() && x.IdEmpresa == idEmpresa).Include(x => x.InverseIdCatalogopadreNavigation).FirstOrDefaultAsync();

            var listaCatalogos = catalogoPadre.InverseIdCatalogopadreNavigation.ToList();


            return listaCatalogos;
        }
        #endregion

        #region Get Parent by Child
        public async Task<List<Catalogo>> GetCatalogsUpLevel_Company(Guid idCatalogoHijo, Guid idEmpresa)
        {
            var listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHijo && x.IdEmpresa == idEmpresa).
                Include(x => x.IdCatalogopadreNavigation)
                .SelectMany(y => y.IdCatalogopadreNavigation.IdCatalogopadreNavigation.InverseIdCatalogopadreNavigation).ToListAsync();

            return listaCatalogos;
        }
        #endregion

        #region Get Catalogs Same Label
        public async Task<List<Catalogo>> GetCatalogsSameLevelByID_Company(Guid idCatalogoHermano, Guid idEmpresa)
        {
            try
            {
                var listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHermano && x.IdEmpresa == idEmpresa).Include(x => x.IdCatalogopadreNavigation)
                       .SelectMany(y => y.IdCatalogopadreNavigation.InverseIdCatalogopadreNavigation).ToListAsync();


                return listaCatalogos;
            }
            catch (Exception)
            {


            }

            return null;
        }
        #endregion

        public async Task<List<Catalogo>> obtenerCatalogosHermanoPorIdCatalogo(Guid idCatalogoHermano, Guid? idEmpresa)
        {
            Catalogo objCatalogoHermano = new();
            List<Catalogo> Listahermanos = new();

            try
            {
                if (idEmpresa != null)
                {
                    objCatalogoHermano = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHermano && x.IdEmpresa == idEmpresa).FirstOrDefaultAsync();
                    Listahermanos = await _context.Catalogos.Where(x => x.IdCatalogopadre == objCatalogoHermano.IdCatalogopadre).ToListAsync();
                    return Listahermanos;
                }
                else
                {
                    objCatalogoHermano = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHermano).FirstOrDefaultAsync();
                    Listahermanos = await _context.Catalogos.Where(x => x.IdCatalogopadre == objCatalogoHermano.IdCatalogopadre).ToListAsync();
                    return Listahermanos;
                }

            }
            catch (Exception ex)
            {


            }
            return null;
        }

        public async Task<Catalogo> obtenerCatalogoPadrePorIdHijo (Guid idCatalogoHijo) 
        {
            try
            {
                Catalogo catalogHijo = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHijo).FirstOrDefaultAsync();
                Catalogo catalogoPadre = await _context.Catalogos.Where(x => x.IdCatalogo == catalogHijo.IdCatalogopadre).FirstOrDefaultAsync();
                return catalogoPadre;
            }
            catch (Exception ex )
            {

                
            }
            return null;
        }
        public async Task<List<Catalogo>> obtenerCatalogosPadrePorIdHijo(Guid idCatalogoHijo, Guid? idEmpresa)
        {
            List<Catalogo> ListaCatalogosHermanosPadre = new();
            Catalogo objCatalogoHijo = new();
            Catalogo objCatalogoPadre = new();
            try
            {
                if (idEmpresa != null) 
                {
                    objCatalogoHijo = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHijo && x.IdEmpresa == idEmpresa).FirstOrDefaultAsync();
                    objCatalogoPadre = await _context.Catalogos.Where(x => x.IdCatalogo == objCatalogoHijo.IdCatalogopadre && x.IdEmpresa == idEmpresa).FirstOrDefaultAsync();
                    ListaCatalogosHermanosPadre = await _context.Catalogos.Where(x => x.IdCatalogopadre == objCatalogoPadre.IdCatalogopadre).ToListAsync();
                    return ListaCatalogosHermanosPadre;
                }
                else 
                {
                    objCatalogoHijo = await _context.Catalogos.Where(x => x.IdCatalogo == idCatalogoHijo).FirstOrDefaultAsync();
                    objCatalogoPadre = await _context.Catalogos.Where(x => x.IdCatalogo == objCatalogoHijo.IdCatalogopadre).FirstOrDefaultAsync();
                    ListaCatalogosHermanosPadre = await _context.Catalogos.Where(x => x.IdCatalogopadre == objCatalogoPadre.IdCatalogopadre).ToListAsync();
                    return ListaCatalogosHermanosPadre;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<Catalogo>> BusquedaAvanzadaCatalogo(CatalogoBusquedaDTO objBusqueda)
        {
            try
            {
                var listaCatalogos = await _context.Catalogos.Where(x => x.IdCatalogopadre == objBusqueda.IdCatalogopadre).ToListAsync();

                if (!string.IsNullOrEmpty(objBusqueda.Nombrecatalogo))
                {
                    listaCatalogos = listaCatalogos.Where(x => x.NombreCatalogo.ToUpper().Trim().Contains(objBusqueda.Nombrecatalogo.ToUpper().Trim())).ToList(); 
                }
                listaCatalogos = listaCatalogos.Where(x => x.Estado==objBusqueda.Estado).ToList();

                return listaCatalogos;
            }
            catch (Exception ex)
            {


            }
            return null;
        }

        #endregion
    }
}
