﻿using EntidadesCatalogosNotas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioCatalogoNotas.Interface
{
    public interface IManageNota
    {
        #region Search
        public Task<Nota> GetNotaById(Guid idNota);
        public Task<List<Nota>> GetAllNotas();
        public Task<Nota> GetNotaByTitulo(string titulo);

        #endregion
    }
}
