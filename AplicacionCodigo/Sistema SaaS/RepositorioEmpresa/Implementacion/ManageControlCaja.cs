﻿using EntidadesEmpresa.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioEmpresa.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Implementacion
{
    public class ManageControlCaja: IManageControlCaja
    {
        public readonly Context_DB_Empresa_SAS _contex;
        public ManageControlCaja(Context_DB_Empresa_SAS context)
        {
            this._contex = context ?? throw new ArgumentException(nameof(context));
        }

        #region Search
        public async Task<ControlCaja> GetControlCajaById(Guid idControlCaja)
        {
            ControlCaja objControlCaja = await _contex.ControlCajas.Where(C => C.IdControlCaja == idControlCaja).FirstOrDefaultAsync();
            return objControlCaja;
        }

        public async Task<List<ControlCaja>> GetAllControlCaja()
        {
            List<ControlCaja> listControlCajas = new List<ControlCaja>();
            try
            {
                listControlCajas = await _contex.ControlCajas.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listControlCajas;
        }

        #endregion

    }
}
