﻿using EntidadesEmpresa.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioEmpresa.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Empresa;

namespace RepositorioEmpresa.Implementacion
{
    public class ManageEmpresa : IManageEmpresa
    {
        public readonly Context_DB_Empresa_SAS _context;

        public ManageEmpresa(Context_DB_Empresa_SAS context)
        {
            this._context = context ??
                throw new ArgumentException(nameof(context));
        }

        #region Search

        public async Task<Empresa> GetCompanyById(Guid idCompany)
        {

            Empresa objCompany = await _context.Empresas.Where(c => c.IdEmpresa == idCompany).Include(c => c.UsuarioEmpresas).FirstOrDefaultAsync();

            return objCompany;
        }//END GetCompanyById


        public async Task<Empresa> GetCompanyByRUC(string ruc)
        {

            Empresa objCompany = await _context.Empresas.Where(c => c.Ruc.Trim().ToUpper().Contains(ruc.Trim().ToUpper())).FirstOrDefaultAsync();

            return objCompany;
        }//END GetCompanyById

        public async Task<List<Empresa>> GetAllCompany()
        {
            List<Empresa> listCompany = new List<Empresa>();
            try
            {

                listCompany = await _context.Empresas.Select(p => p).ToListAsync();

            }
            catch (Exception ex)
            {

            }
            return listCompany;
        }

        public async Task<List<Empresa>> GetCompaniesAdvanced(ObjetoBusquedaEmpresas objBusqueda)
        {
            List<Empresa> listCompany = new List<Empresa>();
            try
            {
                if (objBusqueda.estado != null)
                {
                    listCompany = _context.Empresas.Where(x => x.Estado == (bool)objBusqueda.estado).ToList();
                }
                else
                {
                    listCompany = await _context.Empresas.ToListAsync();
                }


                if (!string.IsNullOrEmpty(objBusqueda.Ruc))
                {
                    listCompany = listCompany.Where(x => x.Ruc.Contains(objBusqueda.Ruc.Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(objBusqueda.RazonSocial))
                {
                    listCompany = listCompany.Where(x => x.RazonSocial.ToUpper().Trim().Contains(objBusqueda.RazonSocial.ToUpper().Trim())).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return listCompany;
        }

        public async Task<List<UsuarioEmpresa>> GetUsuarioEmpresaByIdEmpresa(Guid IdEmpresa) 
        {
            List<UsuarioEmpresa> listaUsuariosEmpresa = await _context.UsuarioEmpresas.Where(c => c.IdEmpresa == IdEmpresa).Include(c => c.IdEmpresaNavigation).ToListAsync();
            return listaUsuariosEmpresa;
        }

        public async Task<List<UsuarioEmpresa>> GetUsuarioEmpresaByIdUsuario(Guid IdUsuario)
        {
            List<UsuarioEmpresa> usuariosEmpresa = await _context.UsuarioEmpresas.Where(c => c.IdUsuario == IdUsuario).Include(c => c.IdEmpresaNavigation).ToListAsync();
            return usuariosEmpresa;
        }
        #endregion
         
    }
}
