﻿using EntidadesEmpresa.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioEmpresa.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Local;

namespace RepositorioEmpresa.Implementacion
{
    public class ManageLocal : IManageLocal
    {
        public readonly Context_DB_Empresa_SAS _context;
        public ManageLocal(Context_DB_Empresa_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }

        #region Search
        public async Task<Local> GetLocalById(Guid idLocal)
        {
            Local objLocal = await _context.Locals.Where(c => c.IdLocal == idLocal).Include(x=> x.Ubicacions).FirstOrDefaultAsync();
            return objLocal;
        }// End GetLocalById

        public async Task<Local> GetLocalByNombre(string nombreLocal)
        {
            Local objLocal = await _context.Locals.Where(c => c.NombreLocal.Trim().ToUpper().Contains(nombreLocal.Trim().ToUpper())).FirstOrDefaultAsync();
            return objLocal;
        }
        public async Task<List<Local>> GetAllLocales()
        {
            List<Local> listLocal = new List<Local>();
            try
            {
                listLocal = await _context.Locals.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listLocal;
        }

        public async Task<List<Local>> GetLocalAdvanced(ObjetoBusquedaLocal objBusqueda)
        {
            List<Local> listLocal = new List<Local>();
            try
            {
                if (objBusqueda.IdEmpresa != null)
                {
                    listLocal = await _context.Locals.Where(x => x.IdEmpresa == objBusqueda.IdEmpresa).Include(y => y.IdEmpresaNavigation).ToListAsync();
                }

                if (objBusqueda.IdTipoLocal != null)
                {
                    listLocal = listLocal.Where(x => x.IdTipoLocal == objBusqueda.IdTipoLocal).ToList();
                }

                if (objBusqueda.estado != null)
                {
                    listLocal = listLocal.Where(x => x.Estado == (bool)objBusqueda.estado).ToList();
                }
                if (!string.IsNullOrEmpty(objBusqueda.Nombre))
                {
                    listLocal = listLocal.Where(x => x.NombreLocal
                    .ToUpper().Trim().Contains(objBusqueda.Nombre.ToUpper().Trim())).ToList();
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: "+ ex.Message);
            }

            return listLocal;
        }
        #endregion
    }
}
