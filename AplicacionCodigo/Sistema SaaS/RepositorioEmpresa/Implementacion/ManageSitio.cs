﻿using EntidadesEmpresa.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioEmpresa.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Implementacion
{
    public class ManageSitio : IManageSitio
    {
        public readonly Context_DB_Empresa_SAS _context;
        public ManageSitio(Context_DB_Empresa_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }


        public async Task<Sitio> GetSitiolById(Guid idSitio)
        {
            Sitio obj = new();
            try
            {
                obj = await _context.Sitios.Where(c => c.IdSitio == idSitio).Include(x => x.Bandejas).FirstOrDefaultAsync();
                return obj;
            }
            catch (Exception ex)
            {

            }
            return obj;
        }


        public async Task<List<Sitio>> GetSitiosByIdUbicacionl(Guid idUbicacion)
        {
            List<Sitio> ListObj = new();
            try
            {
                ListObj = await _context.Sitios.Where(x => x.IdUbicacion == idUbicacion).Include(x=>x.Bandejas).ToListAsync();
                return ListObj;
            }
            catch (Exception ex)
            {

            }
            return ListObj;
        }

    }
}
