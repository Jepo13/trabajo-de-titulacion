﻿using EntidadesEmpresa.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioEmpresa.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Implementacion
{
    public class ManageBandeja : IManageBandeja
    {
        public readonly Context_DB_Empresa_SAS _context;
        public ManageBandeja(Context_DB_Empresa_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }


        public async Task<Bandeja> GetBandejaById(Guid idBandeja)
        {
            Bandeja obj = new();
            try
            {
                obj = await _context.Bandejas.Where(c => c.IdBandeja == idBandeja).FirstOrDefaultAsync();
                return obj;
            }
            catch (Exception ex)
            {

            }
            return obj;
        }


        public async Task<List<Bandeja>> GetBandejasByIdSitio(Guid idSitio)
        {
            List<Bandeja> ListObj = new();
            try
            {
                ListObj = await _context.Bandejas.Where(x => x.IdSitio == idSitio).ToListAsync();
                return ListObj;
            }
            catch (Exception ex)
            {

            }
            return ListObj;
        }

    }
}
