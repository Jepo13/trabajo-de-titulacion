﻿using EntidadesEmpresa.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioEmpresa.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Implementacion
{
    public class ManageCaja: IManageCaja
    {
        public readonly Context_DB_Empresa_SAS _contex;
        public ManageCaja(Context_DB_Empresa_SAS context)
        {
            this._contex = context ?? throw new ArgumentException(nameof(context));
        }
        #region Search

        public async Task<Caja> GetCajaById(Guid idCaja)
        {
            Caja objCaja = await _contex.Cajas.Where(C => C.IdCaja == idCaja).FirstOrDefaultAsync();
            return objCaja;
        }
        public async Task<List<Caja>> GetAllCajas()
        {
            List<Caja> listCaja = new();
            try
            {
                listCaja = await _contex.Cajas.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listCaja;
        }
        public async Task<Caja> GetCajaByName(string nameCaja)
        {

            Caja objCaja = await _contex.Cajas.Where(c => c.NombreCaja.Trim().ToUpper().Contains(nameCaja.Trim().ToUpper())).FirstOrDefaultAsync();

            return objCaja;
        }
        public async Task<List<Caja>> GetAllCajaByIdLocal(Guid idLocal)
        {
            List<Caja> ListCajaByLocal = new List<Caja>();
            try
            {
                ListCajaByLocal = await _contex.Cajas.Where(c => c.IdLocal == idLocal).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return ListCajaByLocal;
        }

        #endregion
    }
}
