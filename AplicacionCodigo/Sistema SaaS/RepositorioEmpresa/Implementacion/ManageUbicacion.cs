﻿using EntidadesEmpresa.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioEmpresa.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Implementacion
{
    public  class ManageUbicacion : IManageUbicacion
    {
        public readonly Context_DB_Empresa_SAS _context;

        public ManageUbicacion(Context_DB_Empresa_SAS context)
        {
            this._context = context ??
                throw new ArgumentException(nameof(context));
        }

        #region Search
        public async Task<Ubicacion> GetUbicacionById (Guid idUbicacion) 
        {
            Ubicacion obj = new();
            try
            {
               obj = await _context.Ubicacions.Where(x => x.IdUbicacion == idUbicacion).Include(x=>x.Sitios).FirstOrDefaultAsync();
                return obj;
            }
            catch (Exception ex)
            {

             
            }
            return obj;
        }

        public async Task<List<Ubicacion>> GetUbicacionesByIdLocal (Guid idLocal)
        {
            List<Ubicacion> ListObj = new();
            try
            {
                ListObj = await _context.Ubicacions.Where(x => x.IdLocal == idLocal).ToListAsync();
                return ListObj;
            }
            catch (Exception ex)
            {


            }
            return ListObj;
        }

        #endregion





    }
}
