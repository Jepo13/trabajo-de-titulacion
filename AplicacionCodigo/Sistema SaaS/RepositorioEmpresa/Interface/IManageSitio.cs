﻿using EntidadesEmpresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Interface
{
    public interface IManageSitio
    {

        #region Search
        public  Task<Sitio> GetSitiolById(Guid idSitio);
        public Task<List<Sitio>> GetSitiosByIdUbicacionl(Guid idUbicacion);
        #endregion
    }
}
