﻿using EntidadesEmpresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Empresa;

namespace RepositorioEmpresa.Interface
{
    public interface IManageEmpresa
    {
        #region Search
        public Task<Empresa> GetCompanyById(Guid idCompany);
        public Task<Empresa> GetCompanyByRUC(string ruc);
        public Task<List<Empresa>> GetAllCompany();
        public Task<List<Empresa>> GetCompaniesAdvanced(ObjetoBusquedaEmpresas objBusqueda);
        public Task<List<UsuarioEmpresa>> GetUsuarioEmpresaByIdEmpresa(Guid IdEmpresa);
        public Task<List<UsuarioEmpresa>> GetUsuarioEmpresaByIdUsuario(Guid IdUsuario);
        #endregion
    }
}
