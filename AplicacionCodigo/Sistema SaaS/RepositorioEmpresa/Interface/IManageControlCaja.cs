﻿using EntidadesEmpresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Interface
{
    public interface IManageControlCaja
    {
        #region Search
        public Task<ControlCaja> GetControlCajaById(Guid idControlCaja);
        public Task<List<ControlCaja>> GetAllControlCaja();
        #endregion


    }
}
