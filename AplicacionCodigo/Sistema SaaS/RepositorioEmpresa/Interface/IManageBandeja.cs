﻿using EntidadesEmpresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Interface
{
    public interface IManageBandeja
    {
        #region Search

        public Task<Bandeja> GetBandejaById(Guid idBandeja);
        public Task<List<Bandeja>> GetBandejasByIdSitio(Guid idSitio);

        #endregion



    }
}
