﻿using EntidadesEmpresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Local;

namespace RepositorioEmpresa.Interface
{
    public interface IManageLocal
    {
        #region Search
        public Task<Local> GetLocalById(Guid idLocal);
        public Task<Local> GetLocalByNombre(string nombreLocal);
        public Task<List<Local>> GetAllLocales();
        public Task<List<Local>> GetLocalAdvanced(ObjetoBusquedaLocal objBusqueda);
        #endregion
    }
}
