﻿using EntidadesEmpresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioEmpresa.Interface
{
    public interface IManageUbicacion
    {
        public Task<Ubicacion> GetUbicacionById(Guid idUbicacion);
        public Task<List<Ubicacion>> GetUbicacionesByIdLocal(Guid idLocal);
    }
}
