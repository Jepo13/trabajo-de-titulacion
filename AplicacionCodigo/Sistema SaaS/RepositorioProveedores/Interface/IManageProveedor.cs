﻿using EntidadesProveedores.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Proveedor;

namespace RepositorioProveedores.Interface
{
    public interface IManageProveedor
    {
        
        public Task<Proveedor> GetProveedorById(Guid idProvedor);
        public Task<Proveedor> GetProveedorByRuc(string RUC, Guid idEmpresa);
        public Task<List<Proveedor>> GetAllProveedor();
        
        public Task<Proveedor> GetProveedorByName(string name);
        public Task<List<Proveedor>> GetProveedorAdvanced(ObjetoBusquedaProveedor objBusqueda);

    }
}
