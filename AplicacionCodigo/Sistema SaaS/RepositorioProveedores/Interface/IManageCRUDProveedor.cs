﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioProveedores.Interface
{
    public interface IManageCRUDProveedor<T> where T : class
    {
        public void Add(T obj);
        public void Edit(T obj);
        public void Delete(T obj);
        public Task<(bool estado, string mensajeError)> save();
    }
}
