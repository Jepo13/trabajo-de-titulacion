﻿using EntidadesProveedores.Entities;
using Microsoft.EntityFrameworkCore;
using RepositorioProveedores.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitarios.Busquedas.Proveedor;

namespace RepositorioProveedores.Implementacion
{
    public class ManageProveedor: IManageProveedor
    {
        public readonly Context_DB_Proveedores_SAS _context;
        public ManageProveedor(Context_DB_Proveedores_SAS context)
        {
            this._context = context ?? throw new ArgumentException(nameof(context));
        }
        #region Search

        #region Get Bodega by Id
        public async Task<Proveedor> GetProveedorById(Guid idProvedor)
        {
            Proveedor objProveedor = await _context.Proveedors.Where(c => c.IdProveedor == idProvedor).FirstOrDefaultAsync();
            return objProveedor;
        }
        #endregion

        #region Get All 
        public async Task<List<Proveedor>> GetAllProveedor()
        {
            List<Proveedor> listProveedor = new();
            try
            {
                listProveedor = await _context.Proveedors.Select(p => p).ToListAsync();
            }
            catch (Exception ex)
            {

            }
            return listProveedor;
        }
        #endregion

        #region Get by RUC
        public async Task<Proveedor> GetProveedorByRuc(string RUC, Guid idEmpresa)
        {
            Proveedor objProveedor = await _context.Proveedors.Where(c => c.Ruc.Trim().ToUpper().Contains(RUC.Trim().ToUpper()) && c.IdEmpresa == idEmpresa).FirstOrDefaultAsync();
            return objProveedor;
        }
        #endregion

        #region Get by Name
        public async Task<Proveedor> GetProveedorByName(string name)
        {
            Proveedor objProveedor = await _context.Proveedors.Where(c => c.NombreRepresentate.Trim().ToUpper().Contains(name.Trim().ToUpper())).FirstOrDefaultAsync();
            return objProveedor;
        }
        #endregion

        #region Busqueda avanzada
        public async Task<List<Proveedor>> GetProveedorAdvanced(ObjetoBusquedaProveedor objBusqueda)
        {
            List<Proveedor> listProveedores = new List<Proveedor>();
            try
            {
                if (objBusqueda.IdEmpresa != null)
                {
                    listProveedores = _context.Proveedors.Where(x => x.IdEmpresa == objBusqueda.IdEmpresa).ToList();
                }                
                else 
                {
                    listProveedores = await _context.Proveedors.ToListAsync();
                }

                if (objBusqueda.IdEspecialidad != null) 
                {
                    listProveedores = listProveedores.Where(x => x.IdEspecialidad == objBusqueda.IdEspecialidad).ToList();
                }
                if (objBusqueda.Estado != null)
                {
                    listProveedores = listProveedores.Where(x => x.Estado == (bool)objBusqueda.Estado).ToList();
                }
                          
                if (!string.IsNullOrEmpty(objBusqueda.Ruc))
                {
                    listProveedores = listProveedores.Where(x => x.Ruc.ToUpper().Trim().Contains(objBusqueda.Ruc.Trim())).ToList();
                }
                if (!string.IsNullOrEmpty(objBusqueda.Razonsocial))
                {
                    listProveedores = listProveedores.Where(x => x.RazonSocial.ToUpper().Trim().Contains(objBusqueda.Razonsocial.ToUpper().Trim())).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error en ManageProveedor busqueda avanzada: "+ ex.Message);
            }
            return listProveedores;
        }
        #endregion

        #endregion


    }
}
