/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2005                    */
/* Created on:     3/4/2021 11:48:04                            */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CAJA') and o.name = 'FK_CAJA_REFERENCE_LOCAL')
alter table CAJA
   drop constraint FK_CAJA_REFERENCE_LOCAL
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CANJEPRODUCTOPROVEEDOR') and o.name = 'FK_CANJEPRO_REFERENCE_PROVEEDO')
alter table CANJEPRODUCTOPROVEEDOR
   drop constraint FK_CANJEPRO_REFERENCE_PROVEEDO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATEGORIA') and o.name = 'FK_CATEGORI_REFERENCE_DATOSEMP')
alter table CATEGORIA
   drop constraint FK_CATEGORI_REFERENCE_DATOSEMP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATEGORIA') and o.name = 'FK_CATEGORI_REFERENCE_CATEGORI')
alter table CATEGORIA
   drop constraint FK_CATEGORI_REFERENCE_CATEGORI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CLIENTE') and o.name = 'FK_CLIENTE_REFERENCE_PERSONA')
alter table CLIENTE
   drop constraint FK_CLIENTE_REFERENCE_PERSONA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CLIENTE') and o.name = 'FK_CLIENTE_REFERENCE_TIPOCLIE')
alter table CLIENTE
   drop constraint FK_CLIENTE_REFERENCE_TIPOCLIE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CONTROLCAJA') and o.name = 'FK_CONTROLC_REFERENCE_CAJA')
alter table CONTROLCAJA
   drop constraint FK_CONTROLC_REFERENCE_CAJA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CONTROLCAJA') and o.name = 'FK_CONTROLC_REFERENCE_USUARIO')
alter table CONTROLCAJA
   drop constraint FK_CONTROLC_REFERENCE_USUARIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CONTROLCAJA') and o.name = 'FK_CONTROLC_REFERENCE_PERSONA')
alter table CONTROLCAJA
   drop constraint FK_CONTROLC_REFERENCE_PERSONA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETALLEFACTURA') and o.name = 'FK_DETALLEF_REFERENCE_ESTADOVE')
alter table DETALLEFACTURA
   drop constraint FK_DETALLEF_REFERENCE_ESTADOVE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETALLEFACTURA') and o.name = 'FK_DETALLEF_REFERENCE_FACTURAV')
alter table DETALLEFACTURA
   drop constraint FK_DETALLEF_REFERENCE_FACTURAV
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETALLEFACTURA') and o.name = 'FK_DETALLEF_REFERENCE_PRODUCTO')
alter table DETALLEFACTURA
   drop constraint FK_DETALLEF_REFERENCE_PRODUCTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETALLENOTACREDITO') and o.name = 'FK_DETALLEN_REFERENCE_NOTACRED')
alter table DETALLENOTACREDITO
   drop constraint FK_DETALLEN_REFERENCE_NOTACRED
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('DETALLENOTACREDITO') and o.name = 'FK_DETALLEN_REFERENCE_PRODUCTO')
alter table DETALLENOTACREDITO
   drop constraint FK_DETALLEN_REFERENCE_PRODUCTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAFORMASPAGO') and o.name = 'FK_FACTURAF_REFERENCE_FORMASPA')
alter table FACTURAFORMASPAGO
   drop constraint FK_FACTURAF_REFERENCE_FORMASPA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAFORMASPAGO') and o.name = 'FK_FACTURAF_REFERENCE_FACTURAV')
alter table FACTURAFORMASPAGO
   drop constraint FK_FACTURAF_REFERENCE_FACTURAV
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAPROVEEDOR') and o.name = 'FK_FACTURAP_REFERENCE_FACTURAF')
alter table FACTURAPROVEEDOR
   drop constraint FK_FACTURAP_REFERENCE_FACTURAF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAPROVEEDOR') and o.name = 'FK_FACTURAP_REFERENCE_FACTURAE')
alter table FACTURAPROVEEDOR
   drop constraint FK_FACTURAP_REFERENCE_FACTURAE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAPROVEEDOR') and o.name = 'FK_FACTURAP_REFERENCE_CLASECON')
alter table FACTURAPROVEEDOR
   drop constraint FK_FACTURAP_REFERENCE_CLASECON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAPROVEEDOR') and o.name = 'FK_FACTURAP_REFERENCE_PROVEEDO')
alter table FACTURAPROVEEDOR
   drop constraint FK_FACTURAP_REFERENCE_PROVEEDO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAVENTA') and o.name = 'FK_FACTURAV_REFERENCE_CLIENTE')
alter table FACTURAVENTA
   drop constraint FK_FACTURAV_REFERENCE_CLIENTE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('FACTURAVENTA') and o.name = 'FK_FACTURAV_REFERENCE_USUARIO')
alter table FACTURAVENTA
   drop constraint FK_FACTURAV_REFERENCE_USUARIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LOCAL') and o.name = 'FK_LOCAL_REFERENCE_DATOSEMP')
alter table LOCAL
   drop constraint FK_LOCAL_REFERENCE_DATOSEMP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LOGEXCEPCIONES') and o.name = 'FK_LOGEXCEP_REFERENCE_USUARIO')
alter table LOGEXCEPCIONES
   drop constraint FK_LOGEXCEP_REFERENCE_USUARIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('NOTACREDITO') and o.name = 'FK_NOTACRED_REFERENCE_USUARIO')
alter table NOTACREDITO
   drop constraint FK_NOTACRED_REFERENCE_USUARIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('NOTACREDITO') and o.name = 'FK_NOTACRED_REFERENCE_FACTURAV')
alter table NOTACREDITO
   drop constraint FK_NOTACRED_REFERENCE_FACTURAV
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('NOTACREDITO') and o.name = 'FK_NOTACRED_REFERENCE_PERSONA')
alter table NOTACREDITO
   drop constraint FK_NOTACRED_REFERENCE_PERSONA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PERSONA') and o.name = 'FK_PERSONA_REFERENCE_DATOSEMP')
alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_DATOSEMP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PERSONA') and o.name = 'FK_PERSONA_REFERENCE_TIPOIDEN')
alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_TIPOIDEN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_LOCAL')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_LOCAL
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_CONTENID')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_CONTENID
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_TIPOGANA')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_TIPOGANA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_CANJEPRO')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_CANJEPRO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_ESTADOPR')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_ESTADOPR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REF_PERSONA')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REF_PERSONA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_USUARIO')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_USUARIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_FACTURAP')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_FACTURAP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTOPROTOTIPO') and o.name = 'FK_PRODUCTO_PROTOTIPO_USUARIO')
alter table PRODUCTOPROTOTIPO
   drop constraint FK_PRODUCTO_PROTOTIPO_USUARIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTOPROTOTIPO') and o.name = 'FK_PRODUCTPROTO_REF_CONTENID')
alter table PRODUCTOPROTOTIPO
   drop constraint FK_PRODUCTPROTO_REF_CONTENID
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTOPROTOTIPO') and o.name = 'FK_PRODUCTO_REFERENCE_PORCENTA')
alter table PRODUCTOPROTOTIPO
   drop constraint FK_PRODUCTO_REFERENCE_PORCENTA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTOPROTOTIPO') and o.name = 'FK_PRODUCTO_REFERENCE_CATEGORI')
alter table PRODUCTOPROTOTIPO
   drop constraint FK_PRODUCTO_REFERENCE_CATEGORI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTOPROTOTIPO') and o.name = 'FK_PRODUCTO_REFERENCE_PERSONA')
alter table PRODUCTOPROTOTIPO
   drop constraint FK_PRODUCTO_REFERENCE_PERSONA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTOPROTOTIPO') and o.name = 'FK_PRODUCTO_REFERENCE_PROVEEDO')
alter table PRODUCTOPROTOTIPO
   drop constraint FK_PRODUCTO_REFERENCE_PROVEEDO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROVEEDOR') and o.name = 'FK_PROVEEDO_REFERENCE_TIPO_CON')
alter table PROVEEDOR
   drop constraint FK_PROVEEDO_REFERENCE_TIPO_CON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROVEEDOR') and o.name = 'FK_PROVEEDO_REFERENCE_PORCENTA')
alter table PROVEEDOR
   drop constraint FK_PROVEEDO_REFERENCE_PORCENTA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('USUARIO') and o.name = 'FK_USUARIO_REFERENCE_PERSONA')
alter table USUARIO
   drop constraint FK_USUARIO_REFERENCE_PERSONA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('USUARIO') and o.name = 'FK_USUARIO_REFERENCE_TIPOUSUA')
alter table USUARIO
   drop constraint FK_USUARIO_REFERENCE_TIPOUSUA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CAJA')
            and   type = 'U')
   drop table CAJA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CANJEPRODUCTOPROVEEDOR')
            and   type = 'U')
   drop table CANJEPRODUCTOPROVEEDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CATEGORIA')
            and   type = 'U')
   drop table CATEGORIA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CLASECONTRIBUYENTE')
            and   type = 'U')
   drop table CLASECONTRIBUYENTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CLIENTE')
            and   type = 'U')
   drop table CLIENTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CONTENIDO')
            and   type = 'U')
   drop table CONTENIDO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CONTROLCAJA')
            and   type = 'U')
   drop table CONTROLCAJA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DATOSEMPRESA')
            and   type = 'U')
   drop table DATOSEMPRESA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DETALLEFACTURA')
            and   type = 'U')
   drop table DETALLEFACTURA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DETALLENOTACREDITO')
            and   type = 'U')
   drop table DETALLENOTACREDITO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ESTADOPRODUCTO')
            and   type = 'U')
   drop table ESTADOPRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ESTADOVENTA')
            and   type = 'U')
   drop table ESTADOVENTA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURAELECTRONICAPROVEEDOR')
            and   type = 'U')
   drop table FACTURAELECTRONICAPROVEEDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURAFISICAPROVEEDOR')
            and   type = 'U')
   drop table FACTURAFISICAPROVEEDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURAFORMASPAGO')
            and   type = 'U')
   drop table FACTURAFORMASPAGO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURAPROVEEDOR')
            and   type = 'U')
   drop table FACTURAPROVEEDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FACTURAVENTA')
            and   type = 'U')
   drop table FACTURAVENTA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('FORMASPAGO')
            and   type = 'U')
   drop table FORMASPAGO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LOCAL')
            and   type = 'U')
   drop table LOCAL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LOGEXCEPCIONES')
            and   type = 'U')
   drop table LOGEXCEPCIONES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('NOTACREDITO')
            and   type = 'U')
   drop table NOTACREDITO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PERSONA')
            and   type = 'U')
   drop table PERSONA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PORCENTAJEIVA')
            and   type = 'U')
   drop table PORCENTAJEIVA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCTO')
            and   type = 'U')
   drop table PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCTOPROTOTIPO')
            and   type = 'U')
   drop table PRODUCTOPROTOTIPO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PROVEEDOR')
            and   type = 'U')
   drop table PROVEEDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TIPOCLIENTE')
            and   type = 'U')
   drop table TIPOCLIENTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TIPOGANANCIA')
            and   type = 'U')
   drop table TIPOGANANCIA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TIPOIDENTIFICACION')
            and   type = 'U')
   drop table TIPOIDENTIFICACION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TIPOUSUARIO')
            and   type = 'U')
   drop table TIPOUSUARIO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TIPO_CONTRIBUYENTE')
            and   type = 'U')
   drop table TIPO_CONTRIBUYENTE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('USUARIO')
            and   type = 'U')
   drop table USUARIO
go

/*==============================================================*/
/* Table: CAJA                                                  */
/*==============================================================*/
create table CAJA (
   ID_CAJA              uniqueidentifier     not null default newid(),
   ID_LOCAL             uniqueidentifier     null default newid(),
   NOMBRECAJA           nvarchar(10)         not null,
   FECHACREACION        datetime             not null,
   FECHAMODIFICACION    datetime             null,
   constraint PK_CAJA primary key (ID_CAJA)
)
go

/*==============================================================*/
/* Table: CANJEPRODUCTOPROVEEDOR                                */
/*==============================================================*/
create table CANJEPRODUCTOPROVEEDOR (
   ID_CANJEPRODUCTOPROVEEDOR uniqueidentifier     not null default newid(),
   ID_PROVEEDOR         uniqueidentifier     null default newid(),
   FECHACANJE           datetime             not null,
   DESCRIPCION          text                 null,
   IDENTIFICADORCANJEPRODUCTO nvarchar(30)         not null,
   FECHAINGRESO         datetime             not null,
   IMAGENCANJE          image                null,
   constraint PK_CANJEPRODUCTOPROVEEDOR primary key (ID_CANJEPRODUCTOPROVEEDOR)
)
go

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA (
   ID_CATEGORIA         uniqueidentifier     not null default newid(),
   ID_DATOSEMPRESA      uniqueidentifier     null default newid(),
   ID_CATEGORIAPADRE    uniqueidentifier     null default newid(),
   NOMBRECATEGORIA      nvarchar(45)         not null,
   constraint PK_CATEGORIA primary key (ID_CATEGORIA)
)
go

/*==============================================================*/
/* Table: CLASECONTRIBUYENTE                                    */
/*==============================================================*/
create table CLASECONTRIBUYENTE (
   ID_CLASECONTRIBUYENTE uniqueidentifier     not null default newid(),
   NOMBRECLASECONTRIBUYENTE nvarchar(10)         not null,
   constraint PK_CLASECONTRIBUYENTE primary key (ID_CLASECONTRIBUYENTE)
)
go

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           uniqueidentifier     not null default newid(),
   ID_PERSONA           uniqueidentifier     not null default newid(),
   ID_TIPOCLIENTE       uniqueidentifier     null default newid(),
   FECHAREGISTRO        datetime             not null,
   FECHAULTIMACOMPRA    datetime             null,
   VALORCUPOCREDITOPERSONAL money                not null default 0,
   VALORCUPODISPONIBLECREDITOPERSONAL money                null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
)
go

/*==============================================================*/
/* Table: CONTENIDO                                             */
/*==============================================================*/
create table CONTENIDO (
   ID_CONTENIDO         uniqueidentifier     not null default newid(),
   NOMBRECONTENIDO      varchar(15)          not null,
   constraint PK_CONTENIDO primary key (ID_CONTENIDO)
)
go

/*==============================================================*/
/* Table: CONTROLCAJA                                           */
/*==============================================================*/
create table CONTROLCAJA (
   ID_CONTROLCAJA       uniqueidentifier     not null default newid(),
   ID_CAJA              uniqueidentifier     null default newid(),
   ID_PERSONA           uniqueidentifier     null default newid(),
   ID_USUARIO           uniqueidentifier     null default newid(),
   FECHAAPERTURA        datetime             not null,
   FECHACIERRE          datetime             null,
   VALORINICIAL         money                not null,
   VALORFACTURAVENTAANULADA money                not null default 0
      constraint CKC_VALORFACTURAVENTA_CONTROLC check (VALORFACTURAVENTAANULADA >= 0),
   CANTIDADVENTASCAJA   int                  not null default 0
      constraint CKC_CANTIDADVENTASCAJ_CONTROLC check (CANTIDADVENTASCAJA >= 0),
   VENTASEFECTIVO       money                not null default 0
      constraint CKC_VENTASEFECTIVO_CONTROLC check (VENTASEFECTIVO >= 0),
   VENTASNOTACREDITO    money                not null default 0
      constraint CKC_VENTASNOTACREDITO_CONTROLC check (VENTASNOTACREDITO >= 0),
   VENTASCREDITOPERSONAL money                not null default 0
      constraint CKC_VENTASCREDITOPERS_CONTROLC check (VENTASCREDITOPERSONAL >= 0),
   VALORNOTASCREDITOGENERADAS money                not null default 0
      constraint CKC_VALORNOTASCREDITO_CONTROLC check (VALORNOTASCREDITOGENERADAS >= 0),
   REPORTECORRECTO      bit                  null,
   OBSERVACION          text                 null,
   ESTADOCAJA           nvarchar(7)          null,
   constraint PK_CONTROLCAJA primary key (ID_CONTROLCAJA)
)
go

/*==============================================================*/
/* Table: DATOSEMPRESA                                          */
/*==============================================================*/
create table DATOSEMPRESA (
   ID_DATOSEMPRESA      uniqueidentifier     not null default newid(),
   RUC                  varchar(13)          null,
   DIRECCION            varchar(80)          null,
   RAZONSOCIAL          varchar(35)          null,
   TELEFONO             varchar(35)          null,
   FECHACREACION        datetime             not null,
   FECHAMODIFICACION    datetime             null,
   constraint PK_DATOSEMPRESA primary key (ID_DATOSEMPRESA)
)
go

/*==============================================================*/
/* Table: DETALLEFACTURA                                        */
/*==============================================================*/
create table DETALLEFACTURA (
   ID_DETALLEFACTURA    uniqueidentifier     not null default newid(),
   ID_ESTADOVENTA       uniqueidentifier     null default newid(),
   ID_FACTURAVENTA      uniqueidentifier     null default newid(),
   ID_PRODUCTO          uniqueidentifier     null default user_name(),
   CANTIDAD             int                  not null,
   PRECIOUNITARIOVENTA  decimal(12,5)        not null,
   PRECIOUNITARIOADQUISICION decimal(12,5)        not null,
   VALORIVAVENTA        decimal(12,5)        not null,
   VALORIVACOMPRA       decimal(12,5)        not null,
   PORCENTAJEIVAVENTA   int                  not null,
   constraint PK_DETALLEFACTURA primary key (ID_DETALLEFACTURA)
)
go

/*==============================================================*/
/* Table: DETALLENOTACREDITO                                    */
/*==============================================================*/
create table DETALLENOTACREDITO (
   ID_DETALLENOTACREDITO uniqueidentifier     not null default newid(),
   ID_NOTACREDITO       uniqueidentifier     null default newid(),
   ID_PRODUCTO          uniqueidentifier     null default user_name(),
   CANTIDAD             int                  not null,
   PRECIOUNITARIO       money                not null,
   FECHACREACION        datetime             not null default current_timestamp,
   VALORIVA             money                not null,
   DESCRIPCION          text                 null,
   constraint PK_DETALLENOTACREDITO primary key (ID_DETALLENOTACREDITO)
)
go

/*==============================================================*/
/* Table: ESTADOPRODUCTO                                        */
/*==============================================================*/
create table ESTADOPRODUCTO (
   ID_ESTADOPRODUCTO    uniqueidentifier     not null default newid(),
   NOMBRE               varchar(20)          null,
   constraint PK_ESTADOPRODUCTO primary key (ID_ESTADOPRODUCTO)
)
go

/*==============================================================*/
/* Table: ESTADOVENTA                                           */
/*==============================================================*/
create table ESTADOVENTA (
   ID_ESTADOVENTA       uniqueidentifier     not null default newid(),
   NOMBRE               varchar(20)          null,
   FECHACREACION        datetime             not null,
   FECHAMODIFICACION    datetime             null,
   constraint PK_ESTADOVENTA primary key (ID_ESTADOVENTA)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('ESTADOVENTA') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'ESTADOVENTA' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'Sirve para no tomar en cuenta los productos que son sujetos a nota de credito.', 
   'user', @CurrentUser, 'table', 'ESTADOVENTA'
go

/*==============================================================*/
/* Table: FACTURAELECTRONICAPROVEEDOR                           */
/*==============================================================*/
create table FACTURAELECTRONICAPROVEEDOR (
   ID_FACTURAELECTRONICAPROVEEDOR uniqueidentifier     not null default newid(),
   XMLFACTURA           xml                  not null,
   ESTADO               nvarchar(12)         null,
   NUMEROAUTORIZACION   nvarchar(100)        null,
   AMBIENTE             nvarchar(11)         null,
   FECHAAUTORIZACION    datetime             null,
   constraint PK_FACTURAELECTRONICAPROVEEDOR primary key (ID_FACTURAELECTRONICAPROVEEDOR)
)
go

/*==============================================================*/
/* Table: FACTURAFISICAPROVEEDOR                                */
/*==============================================================*/
create table FACTURAFISICAPROVEEDOR (
   ID_FACTURAFISICAPROVEEDOR uniqueidentifier     not null default newid(),
   IMAGENFACTURAFISICA  image                not null,
   constraint PK_FACTURAFISICAPROVEEDOR primary key (ID_FACTURAFISICAPROVEEDOR)
)
go

/*==============================================================*/
/* Table: FACTURAFORMASPAGO                                     */
/*==============================================================*/
create table FACTURAFORMASPAGO (
   ID_FACTURAFORMAPAGO  uniqueidentifier     not null default newid(),
   ID_FORMAPAGO         uniqueidentifier     null default newid(),
   ID_FACTURAVENTA      uniqueidentifier     null default newid(),
   VALORPAGO            money                not null,
   CAMBIO               money                null,
   FECHAMAXIMAPAGO      datetime             null,
   PAGADA               bit                  not null,
   VALORABONOPAGOCREDITOPERSONAL money                not null default 0
      constraint CKC_VALORABONOPAGOCRE_FACTURAF check (VALORABONOPAGOCREDITOPERSONAL >= 0),
   constraint PK_FACTURAFORMASPAGO primary key (ID_FACTURAFORMAPAGO)
)
go

/*==============================================================*/
/* Table: FACTURAPROVEEDOR                                      */
/*==============================================================*/
create table FACTURAPROVEEDOR (
   ID_FACTURAPROVEEDOR  uniqueidentifier     not null default newid(),
   ID_FACTURAFISICAPROVEEDOR uniqueidentifier     null default newid(),
   ID_FACTURAELECTRONICAPROVEEDOR uniqueidentifier     null default newid(),
   ID_CLASECONTRIBUYENTE uniqueidentifier     null default newid(),
   ID_PROVEEDOR         uniqueidentifier     null default newid(),
   FECHAFACTURA         datetime             not null,
   NUMEROFACTURA        nchar(40)            not null,
   VALORTOTAL           decimal(12,6)        not null,
   FACTURAINGRESADA     bit                  not null,
   FACTURAPAGADA        bit                  not null,
   TIPOFACTURA          varchar(11)          null,
   FECHAINGRESOFACTURA  datetime             not null default current_timestamp,
   FECHAPAGO            datetime             null,
   constraint PK_FACTURAPROVEEDOR primary key (ID_FACTURAPROVEEDOR)
)
go

/*==============================================================*/
/* Table: FACTURAVENTA                                          */
/*==============================================================*/
create table FACTURAVENTA (
   ID_FACTURAVENTA      uniqueidentifier     not null default newid(),
   ID_CLIENTE           uniqueidentifier     null,
   ID_USUARIO           uniqueidentifier     null default newid(),
   FECHAVENTA           datetime             not null,
   SUBTOTALFACTURAIVA   decimal(12,5)        null,
   SUBTOTALFACTURASINIVA decimal(12,5)        null,
   VALORIVAFACTURA      decimal(12,5)        not null,
   PORCENTAJEIVA        int                  not null,
   TOTALFACTURA         money                not null,
   ESTADOFACTURA        nvarchar(7)          not null default 'Emitida'
      constraint CKC_ESTADOFACTURA_FACTURAV check (ESTADOFACTURA in ('Emitida','Anulada')),
   constraint PK_FACTURAVENTA primary key (ID_FACTURAVENTA)
)
go

/*==============================================================*/
/* Table: FORMASPAGO                                            */
/*==============================================================*/
create table FORMASPAGO (
   ID_FORMAPAGO         uniqueidentifier     not null default newid(),
   NOMBRE               varchar(20)          not null,
   ESTADO               bit                  not null,
   constraint PK_FORMASPAGO primary key (ID_FORMAPAGO)
)
go

/*==============================================================*/
/* Table: LOCAL                                                 */
/*==============================================================*/
create table LOCAL (
   ID_LOCAL             uniqueidentifier     not null default newid(),
   ID_DATOSEMPRESA      uniqueidentifier     null default newid(),
   NOMBRELOCAL          nvarchar(60)         not null,
   TIPOLOCAL            nvarchar(16)         not null,
   DIRECCIONLOCAL       nvarchar(80)         not null,
   ENCARGADO            nvarchar(80)         not null,
   TELEFONO             nvarchar(25)         not null,
   OBSERVACIONES        text                 null,
   FECHACREACION        datetime             not null,
   FECHAMODIFICACION    datetime             null,
   constraint PK_LOCAL primary key (ID_LOCAL)
)
go

/*==============================================================*/
/* Table: LOGEXCEPCIONES                                        */
/*==============================================================*/
create table LOGEXCEPCIONES (
   ID_LOGEXCEPCIONES    uniqueidentifier     not null default newid(),
   ID_USUARIO           uniqueidentifier     null default newid(),
   METODO               nvarchar(50)         not null,
   ERROR                text                 not null,
   FECHAERROR           datetime             not null,
   constraint PK_LOGEXCEPCIONES primary key (ID_LOGEXCEPCIONES)
)
go

/*==============================================================*/
/* Table: NOTACREDITO                                           */
/*==============================================================*/
create table NOTACREDITO (
   ID_NOTACREDITO       uniqueidentifier     not null default newid(),
   ID_FACTURAVENTA      uniqueidentifier     null default newid(),
   ID_PERSONA           uniqueidentifier     null default newid(),
   ID_USUARIO           uniqueidentifier     null default newid(),
   VALORIVANOTACREDITO  decimal(12,5)        not null,
   PORCENTAJEIVA        int                  null,
   SUBTOTALNOTACREDITOIVA decimal(12,5)        null,
   SUBTOTALNOTACREDITOSINIVA decimal(12,5)        null,
   TOTALNOTACREDITO     decimal(12,5)        not null,
   VALORDISPONIBLE      decimal(12,5)        not null,
   FECHACREACION        datetime             not null,
   FECHACADUCIDAD       datetime             not null,
   constraint PK_NOTACREDITO primary key (ID_NOTACREDITO)
)
go

/*==============================================================*/
/* Table: PERSONA                                               */
/*==============================================================*/
create table PERSONA (
   ID_PERSONA           uniqueidentifier     not null default newid(),
   ID_DATOSEMPRESA      uniqueidentifier     null default newid(),
   ID_TIPOIDENTIFICACION uniqueidentifier     null default newid(),
   NOMBRE               nvarchar(25)         null,
   APELLIDO             nvarchar(25)         null,
   NUMEROIDENTIFICACION nvarchar(15)         null,
   ESTADOCIVIL          nvarchar(12)         null,
   FECHANACIMIENTO      date                 null,
   TELEFONO1            nvarchar(11)         null,
   TELEFONO2            nvarchar(11)         null,
   CONTACTOEMERGENCIA   nvarchar(11)         null,
   DIRECCION            nvarchar(Max)        null,
   NACIONALIDAD         nvarchar(18)         null,
   PROVINCIARESIDENCIA  nvarchar(30)         null,
   CIUDADRESIDENCIA     nvarchar(30)         null,
   PROVINCIANACIMIENTO  nvarchar(30)         null,
   CIUDADNACIMIENTO     nvarchar(30)         null,
   CORREOPERSONAL       nvarchar(60)         null,
   FECHACREACION        datetime             not null,
   FECHAMODIFICACION    datetime             null,
   constraint PK_PERSONA primary key (ID_PERSONA)
)
go

/*==============================================================*/
/* Table: PORCENTAJEIVA                                         */
/*==============================================================*/
create table PORCENTAJEIVA (
   ID_PORCENAJEIVA      uniqueidentifier     not null default newid(),
   VALOR                int                  not null,
   ACTIVO               bit                  not null,
   FECHACREACION        datetime             null,
   FECHAMODIFICACION    datetime             null,
   constraint PK_PORCENTAJEIVA primary key (ID_PORCENAJEIVA)
)
go

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   ID_PRODUCTO          uniqueidentifier     not null default newid(),
   ID_CANJEPRODUCTOPROVEEDOR uniqueidentifier     null default newid(),
   ID_TIPOGANANCIA      uniqueidentifier     null default newid(),
   ID_ESTADOPRODUCTO    uniqueidentifier     null default newid(),
   ID_LOCAL             uniqueidentifier     null default newid(),
   ID_FACTURAPROVEEDOR  uniqueidentifier     null default newid(),
   ID_CONTENIDO         uniqueidentifier     null default newid(),
   ID_PERSONA           uniqueidentifier     null default newid(),
   ID_USUARIO           uniqueidentifier     null,
   NOMBREPRODUCTO       nvarchar(60)         not null,
   DETALLEPRODUCTO      varchar(Max)         null,
   COSTOADQUISICION     money                not null,
   PRECIOVENTAPUBLICO   money                not null,
   FECHAINGRESO         datetime             not null,
   IMAGENPRODUCTO       varchar(Max)         null,
   CANTIDADVENDIDOS     int                  not null,
   CANTIDADEXISTENTES   int                  not null default 0
      constraint CKC_CANTIDADEXISTENTE_PRODUCTO check (CANTIDADEXISTENTES >= 0),
   CODIGOPRODUCTO       varchar(20)          null,
   PERECIBLE            bit                  not null,
   FECHACADUCIDAD       Date                 null,
   TIENEIVA             bit                  not null,
   VALORICE             money                null,
   VALORELRBP           money                null,
   TIENEICE             bit                  not null,
   TIENELRBP            bit                  not null,
   constraint PK_PRODUCTO primary key (ID_PRODUCTO)
)
go

/*==============================================================*/
/* Table: PRODUCTOPROTOTIPO                                     */
/*==============================================================*/
create table PRODUCTOPROTOTIPO (
   ID_PRODUCTOPROTOTIPO uniqueidentifier     not null default newid(),
   ID_PROVEEDOR         uniqueidentifier     null default newid(),
   ID_PORCENAJEIVA      uniqueidentifier     null default newid(),
   ID_CONTENIDO         uniqueidentifier     null default newid(),
   ID_CATEGORIA         uniqueidentifier     null default newid(),
   ID_PERSONA           uniqueidentifier     null default newid(),
   ID_USUARIO           uniqueidentifier     null,
   NOMBREPRODUCTOPROTOTIPO nvarchar(60)         not null,
   DETALLEPRODUCTOPROTOTIPO varchar(Max)         null,
   COSTOADQUISICION     money                not null,
   PRECIOVENTAPUBLICO   money                not null,
   FECHAINGRESO         datetime             not null default current_timestamp,
   IMAGENPRODUCTOPROTOTIO varchar(Max)         null,
   PERECIBLE            bit                  not null,
   TIENEIVA             bit                  not null,
   FECHACADUCIDAD       datetime             null,
   VALORICE             money                null,
   VALORLRBP            money                null,
   TIENEICE             bit                  not null,
   TIENELRBP            bit                  not null,
   constraint PK_PRODUCTOPROTOTIPO primary key (ID_PRODUCTOPROTOTIPO)
)
go

/*==============================================================*/
/* Table: PROVEEDOR                                             */
/*==============================================================*/
create table PROVEEDOR (
   ID_PROVEEDOR         uniqueidentifier     not null default newid(),
   ID_TIPO_CONTRIBUYENTE uniqueidentifier     null default newid(),
   ID_PORCENAJEIVA      uniqueidentifier     null default newid(),
   RAZONSOCIAL          nvarchar(60)         not null,
   NOMBRECOMERCIAL      nvarchar(60)         null,
   RUC                  nvarchar(15)         not null,
   DIRECCION            nvarchar(60)         null,
   TELEFONOPROVEEDOR    nvarchar(15)         not null,
   OBLIGADOLLEVARCONTABILIDAD bit                  not null,
   NOMBRECONTACTO       nvarchar(50)         null,
   TELEFONOCONTACTO     nvarchar(15)         null,
   CODIGOCLIENTE        nvarchar(20)         null,
   FECHACREACION        datetime             not null,
   FECHAMODIFICACION    datetime             null,
   constraint PK_PROVEEDOR primary key (ID_PROVEEDOR)
)
go

/*==============================================================*/
/* Table: TIPOCLIENTE                                           */
/*==============================================================*/
create table TIPOCLIENTE (
   ID_TIPOCLIENTE       uniqueidentifier     not null default newid(),
   NOMBRETIPOCLIENTE    nvarchar(12)         not null,
   PRECIOVENTAPUBLICO   bit                  not null,
   PORCENTAJEDESCUENTO  int                  null,
   DESCRIPCIONN         text                 null,
   constraint PK_TIPOCLIENTE primary key (ID_TIPOCLIENTE)
)
go

/*==============================================================*/
/* Table: TIPOGANANCIA                                          */
/*==============================================================*/
create table TIPOGANANCIA (
   ID_TIPOGANANCIA      uniqueidentifier     not null default newid(),
   NOMBRETIPOGANACIA    varchar(10)          null,
   constraint PK_TIPOGANANCIA primary key (ID_TIPOGANANCIA)
)
go

/*==============================================================*/
/* Table: TIPOIDENTIFICACION                                    */
/*==============================================================*/
create table TIPOIDENTIFICACION (
   ID_TIPOIDENTIFICACION uniqueidentifier     not null default newid(),
   NOMBRETIPOIDENTIFICACION varchar(9)           null,
   ESTADO               bit                  not null,
   constraint PK_TIPOIDENTIFICACION primary key (ID_TIPOIDENTIFICACION)
)
go

/*==============================================================*/
/* Table: TIPOUSUARIO                                           */
/*==============================================================*/
create table TIPOUSUARIO (
   ID_TIPOUSUARIO       uniqueidentifier     not null default newid(),
   NOMBRETIPOUSUARIO    char(15)             null,
   DESCRIPCIONTIPOUSUARIO text                 null,
   ESTADO               bit                  not null,
   constraint PK_TIPOUSUARIO primary key (ID_TIPOUSUARIO)
)
go

/*==============================================================*/
/* Table: TIPO_CONTRIBUYENTE                                    */
/*==============================================================*/
create table TIPO_CONTRIBUYENTE (
   ID_TIPO_CONTRIBUYENTE uniqueidentifier     not null default newid(),
   NOMBRETIPO_CONTRIBUYENTE nvarchar(18)         not null,
   constraint PK_TIPO_CONTRIBUYENTE primary key (ID_TIPO_CONTRIBUYENTE)
)
go

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   ID_USUARIO           uniqueidentifier     not null default newid(),
   ID_PERSONA           uniqueidentifier     not null default newid(),
   ID_TIPOUSUARIO       uniqueidentifier     null default newid(),
   FECHAREGISTRO        datetime             not null default current_timestamp,
   FECHAULTIMOINGRESO   datetime             null,
   ESTADO               bit                  null,
   CORREOINSTITUCIONAL  nchar(60)            null,
   CONTRASENA           nchar(20)            not null,
   FECHAMODFICACION     datetime             null,
   INDICIOCONTRASENA    nchar(50)            null,
   constraint PK_USUARIO primary key (ID_USUARIO)
)
go

alter table CAJA
   add constraint FK_CAJA_REFERENCE_LOCAL foreign key (ID_LOCAL)
      references LOCAL (ID_LOCAL)
go

alter table CANJEPRODUCTOPROVEEDOR
   add constraint FK_CANJEPRO_REFERENCE_PROVEEDO foreign key (ID_PROVEEDOR)
      references PROVEEDOR (ID_PROVEEDOR)
go

alter table CATEGORIA
   add constraint FK_CATEGORI_REFERENCE_DATOSEMP foreign key (ID_DATOSEMPRESA)
      references DATOSEMPRESA (ID_DATOSEMPRESA)
go

alter table CATEGORIA
   add constraint FK_CATEGORI_REFERENCE_CATEGORI foreign key (ID_CATEGORIAPADRE)
      references CATEGORIA (ID_CATEGORIA)
go

alter table CLIENTE
   add constraint FK_CLIENTE_REFERENCE_PERSONA foreign key (ID_PERSONA)
      references PERSONA (ID_PERSONA)
go

alter table CLIENTE
   add constraint FK_CLIENTE_REFERENCE_TIPOCLIE foreign key (ID_TIPOCLIENTE)
      references TIPOCLIENTE (ID_TIPOCLIENTE)
go

alter table CONTROLCAJA
   add constraint FK_CONTROLC_REFERENCE_CAJA foreign key (ID_CAJA)
      references CAJA (ID_CAJA)
go

alter table CONTROLCAJA
   add constraint FK_CONTROLC_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO)
go

alter table CONTROLCAJA
   add constraint FK_CONTROLC_REFERENCE_PERSONA foreign key (ID_PERSONA)
      references PERSONA (ID_PERSONA)
go

alter table DETALLEFACTURA
   add constraint FK_DETALLEF_REFERENCE_ESTADOVE foreign key (ID_ESTADOVENTA)
      references ESTADOVENTA (ID_ESTADOVENTA)
go

alter table DETALLEFACTURA
   add constraint FK_DETALLEF_REFERENCE_FACTURAV foreign key (ID_FACTURAVENTA)
      references FACTURAVENTA (ID_FACTURAVENTA)
go

alter table DETALLEFACTURA
   add constraint FK_DETALLEF_REFERENCE_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
go

alter table DETALLENOTACREDITO
   add constraint FK_DETALLEN_REFERENCE_NOTACRED foreign key (ID_NOTACREDITO)
      references NOTACREDITO (ID_NOTACREDITO)
go

alter table DETALLENOTACREDITO
   add constraint FK_DETALLEN_REFERENCE_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
go

alter table FACTURAFORMASPAGO
   add constraint FK_FACTURAF_REFERENCE_FORMASPA foreign key (ID_FORMAPAGO)
      references FORMASPAGO (ID_FORMAPAGO)
go

alter table FACTURAFORMASPAGO
   add constraint FK_FACTURAF_REFERENCE_FACTURAV foreign key (ID_FACTURAVENTA)
      references FACTURAVENTA (ID_FACTURAVENTA)
go

alter table FACTURAPROVEEDOR
   add constraint FK_FACTURAP_REFERENCE_FACTURAF foreign key (ID_FACTURAFISICAPROVEEDOR)
      references FACTURAFISICAPROVEEDOR (ID_FACTURAFISICAPROVEEDOR)
go

alter table FACTURAPROVEEDOR
   add constraint FK_FACTURAP_REFERENCE_FACTURAE foreign key (ID_FACTURAELECTRONICAPROVEEDOR)
      references FACTURAELECTRONICAPROVEEDOR (ID_FACTURAELECTRONICAPROVEEDOR)
go

alter table FACTURAPROVEEDOR
   add constraint FK_FACTURAP_REFERENCE_CLASECON foreign key (ID_CLASECONTRIBUYENTE)
      references CLASECONTRIBUYENTE (ID_CLASECONTRIBUYENTE)
go

alter table FACTURAPROVEEDOR
   add constraint FK_FACTURAP_REFERENCE_PROVEEDO foreign key (ID_PROVEEDOR)
      references PROVEEDOR (ID_PROVEEDOR)
go

alter table FACTURAVENTA
   add constraint FK_FACTURAV_REFERENCE_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
go

alter table FACTURAVENTA
   add constraint FK_FACTURAV_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO)
go

alter table LOCAL
   add constraint FK_LOCAL_REFERENCE_DATOSEMP foreign key (ID_DATOSEMPRESA)
      references DATOSEMPRESA (ID_DATOSEMPRESA)
go

alter table LOGEXCEPCIONES
   add constraint FK_LOGEXCEP_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO)
go

alter table NOTACREDITO
   add constraint FK_NOTACRED_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO)
go

alter table NOTACREDITO
   add constraint FK_NOTACRED_REFERENCE_FACTURAV foreign key (ID_FACTURAVENTA)
      references FACTURAVENTA (ID_FACTURAVENTA)
go

alter table NOTACREDITO
   add constraint FK_NOTACRED_REFERENCE_PERSONA foreign key (ID_PERSONA)
      references PERSONA (ID_PERSONA)
go

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_DATOSEMP foreign key (ID_DATOSEMPRESA)
      references DATOSEMPRESA (ID_DATOSEMPRESA)
go

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_TIPOIDEN foreign key (ID_TIPOIDENTIFICACION)
      references TIPOIDENTIFICACION (ID_TIPOIDENTIFICACION)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_LOCAL foreign key (ID_LOCAL)
      references LOCAL (ID_LOCAL)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_CONTENID foreign key (ID_CONTENIDO)
      references CONTENIDO (ID_CONTENIDO)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_TIPOGANA foreign key (ID_TIPOGANANCIA)
      references TIPOGANANCIA (ID_TIPOGANANCIA)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_CANJEPRO foreign key (ID_CANJEPRODUCTOPROVEEDOR)
      references CANJEPRODUCTOPROVEEDOR (ID_CANJEPRODUCTOPROVEEDOR)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_ESTADOPR foreign key (ID_ESTADOPRODUCTO)
      references ESTADOPRODUCTO (ID_ESTADOPRODUCTO)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REF_PERSONA foreign key (ID_PERSONA)
      references PERSONA (ID_PERSONA)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_FACTURAP foreign key (ID_FACTURAPROVEEDOR)
      references FACTURAPROVEEDOR (ID_FACTURAPROVEEDOR)
go

alter table PRODUCTOPROTOTIPO
   add constraint FK_PRODUCTO_PROTOTIPO_USUARIO foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO)
go

alter table PRODUCTOPROTOTIPO
   add constraint FK_PRODUCTPROTO_REF_CONTENID foreign key (ID_CONTENIDO)
      references CONTENIDO (ID_CONTENIDO)
go

alter table PRODUCTOPROTOTIPO
   add constraint FK_PRODUCTO_REFERENCE_PORCENTA foreign key (ID_PORCENAJEIVA)
      references PORCENTAJEIVA (ID_PORCENAJEIVA)
go

alter table PRODUCTOPROTOTIPO
   add constraint FK_PRODUCTO_REFERENCE_CATEGORI foreign key (ID_CATEGORIA)
      references CATEGORIA (ID_CATEGORIA)
go

alter table PRODUCTOPROTOTIPO
   add constraint FK_PRODUCTO_REFERENCE_PERSONA foreign key (ID_PERSONA)
      references PERSONA (ID_PERSONA)
go

alter table PRODUCTOPROTOTIPO
   add constraint FK_PRODUCTO_REFERENCE_PROVEEDO foreign key (ID_PROVEEDOR)
      references PROVEEDOR (ID_PROVEEDOR)
go

alter table PROVEEDOR
   add constraint FK_PROVEEDO_REFERENCE_TIPO_CON foreign key (ID_TIPO_CONTRIBUYENTE)
      references TIPO_CONTRIBUYENTE (ID_TIPO_CONTRIBUYENTE)
go

alter table PROVEEDOR
   add constraint FK_PROVEEDO_REFERENCE_PORCENTA foreign key (ID_PORCENAJEIVA)
      references PORCENTAJEIVA (ID_PORCENAJEIVA)
go

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_PERSONA foreign key (ID_PERSONA)
      references PERSONA (ID_PERSONA)
go

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_TIPOUSUA foreign key (ID_TIPOUSUARIO)
      references TIPOUSUARIO (ID_TIPOUSUARIO)
go

