/********** Impuestos *************/


INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO 
 (NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
Values
('Impuestos','IMPUESTO','El impuesto es una clase de tributo regido por derecho p�blico.',1,'', SYSDATETIME(), SYSDATETIME(), 'Autom�tico', 'Autom�tico');


INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO 
 (ID_CATALOGOPADRE,NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
Values
((select ID_CATALOGO FROM SAS_Catalogos_Nota_DB.dbo.CATALOGO c WHERE c.CODIGO_CATALOGO= 'IMPUESTO'),
'IVA','IVA','El IVA es una carga fiscal sobre el consumo.',1,'Impuesto sobre el Valor A�adido', SYSDATETIME(), SYSDATETIME(), 'Autom�tico', 'Autom�tico');

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO 
 (ID_CATALOGOPADRE,NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
Values
((select ID_CATALOGO FROM SAS_Catalogos_Nota_DB.dbo.CATALOGO c WHERE c.CODIGO_CATALOGO= 'IVA'),
'12%','12%','Est�n gravados con 12% IVA, los bienes que est�n destinados para la comercializaci�n y consumo.',1,'Impuesto', SYSDATETIME(), SYSDATETIME(), 'Autom�tico', 'Autom�tico'),

((select ID_CATALOGO FROM SAS_Catalogos_Nota_DB.dbo.CATALOGO c WHERE c.CODIGO_CATALOGO= 'IVA'),
'0%','0%','La tarifa cero de IVA, grava a la transferencia de varios bienes y a la prestaci�n de varios servicios.',1,'Impuesto', SYSDATETIME(), SYSDATETIME(), 'Autom�tico', 'Autom�tico');
