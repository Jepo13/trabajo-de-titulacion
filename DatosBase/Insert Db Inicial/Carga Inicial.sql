-- Carga Inicial Empresas

INSERT INTO SAS_Empresas_DB.dbo.EMPRESA 
(RUC, RAZON_SOCIAL, TELEFONO, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)

	VALUES(N'2200025787001', N'TICS KERNEL', N'026554852', 1, getdate(),getdate(), N'Admin', N'Admin')
GO

--INSERT INTO [dbo].[EMPRESA]([RUC], [RAZONSOCIAL], [TELEFONO], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
--	VALUES(N'220054871', N'Comercio', N'062899165', 1, '2021-6-17 12:37:57', '2021-6-17 12:37:57', N'Javi', N'Javi')
--GO

--INSERT INTO [dbo].[EMPRESA]([RUC], [RAZONSOCIAL], [TELEFONO], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
--	VALUES(N'2200104578', N'Desarrollo Web', N'0266541', 1, '2021-6-17 12:37:57', '2021-6-17 12:37:57', N'PatoPC', N'PatoPC')
--GO

SELECT * FROM SAS_Empresas_DB.dbo.EMPRESA

GO
-- Carga Inicial  de Paises Provincias Ciudad
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)	
VALUES( (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'),'Paises',
N'PAS','Paises en general',1,'',SYSDATETIME(),SYSDATETIME(),N'Admin',N'Admin')
GO




INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
( ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PAS'), 
(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Ecuador', 'EC', 'Pa�s', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

go
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION ,ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Azuay','AZU','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Bol�var','BOL','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Ca�ar','CA�','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Carchi','CAR','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Chimborazo','CHI','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Cotopaxi','COT','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'El Oro','EL ','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Esmeraldas','ESM','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Gal�pagos','GAL','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Guayas','GUA','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Imbabura','IMB','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Loja','LOJ','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Los R�os','LOS','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Manab�','MAN','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Morona Santiago','MOR','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Napo','NAP','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Orellana','ORE','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Pastaza','PAZ','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Pichincha','PIC','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Santa Elena','SAN','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Sto. Domingo Ts�chilas','STO','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Sucumb�os','SUC','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Tungurahua','TUN','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='EC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Zamora Chinchipe','ZAM','Provincia', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin');		

go

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION ,ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PIC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Aloag','ALG','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PIC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Cayambe','CYB','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),	
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PIC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Machachi','MCH','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),	
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PIC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Quito','UIO','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),	
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PIC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'San Miguel De Los Bancos','CYB','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),	
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PIC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Sangolqu�','SGQ','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),	
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='PIC'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Tabacundo','TBC','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin');

go

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION ,ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='ORE'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Aguarico','AGR','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='ORE'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'La Joya De Los Sachas','JDS','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),	
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='ORE'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Coca','OCC','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin');

 go
 

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION ,ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='CAR'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA  where RUC ='2200025787001'), 'Bol�var','BLV','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='CAR'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA  where RUC ='2200025787001'), 'Carchi','CRC','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='CAR'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA  where RUC ='2200025787001'), 'El �ngel','ANG','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='CAR'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA  where RUC ='2200025787001'), 'San Gabriel','SGB','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin'),		
((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO ='CAR'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA  where RUC ='2200025787001'), 'Tulc�n','TLC','Ciudad', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin');

GO
-- fin de carga de Catalogo Paises Provincia y Ciudad
----Tipo local
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES( 
(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Tipo Local', 'TPL', 'Tipo Local', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

go
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO ([ID_CATALOGOPADRE],[ID_EMPRESA], [NOMBRE_CATALOGO], [CODIGO_CATALOGO], [DESCRIPCION], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='TPL'), 
	 (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'),
	 'Matriz', N'MTR', 'Matriz', 1, SYSDATETIME(), SYSDATETIME(), N'Admin', N'Admin'),
	 
	((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='TPL'), 
	 (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'),
	 'Sucursal', N'SCS', 'Sucursal', 1, SYSDATETIME(), SYSDATETIME(), N'Admin', N'Admin');
GO


----------Estado Civil-----------------------------------

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES( 
(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 'Estado Civil', 'ETCL', 'Tipo Local', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

GO

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO ([ID_CATALOGOPADRE],[ID_EMPRESA], [NOMBRE_CATALOGO], [CODIGO_CATALOGO], [DESCRIPCION], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES
	((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='ETCL'),(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'),
	 'Casado', 'CSD', 'Casado', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
	 ((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='ETCL'),(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'),
	 'Soltero', 'SLR', 'Soltero', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
	 ((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='ETCL'),(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'),
	 'Divorciado', 'DVD', 'Divorciado', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
	 ((select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='ETCL'),(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'),
	 'Uni�n de hecho', 'UDH', 'Uni�n de hecho', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
GO

--Fin estado civil


---- TIPO CONTRIBUYENTE-----------
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO ([ID_EMPRESA],	[NOMBRE_CATALOGO],[CODIGO_CATALOGO], [DESCRIPCION],	[ESTADO], [FECHA_CREACION],	[FECHA_MODIFICACION],	[USUARIO_CREACION],	[USUARIO_MODIFICACION])
VALUES((SELECT 	ID_EMPRESA FROM	SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200025787001'),
N'Tipo Contribuyente',
N'TCB','El SRI clasifica a los contribuyentes en cuatro clases: grandes contribuyentes, contribuyentes especiales, agentes de retenci�n y personas naturales obligadas a llevar contabilidad.',
1,'2021-7-9 10:26:59','2021-7-9 10:26:59',N'Admin',N'Admin')

GO
0
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO,
CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES((SELECT ID_CATALOGO  from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Tipo Contribuyente'), (SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200025787001'), 'Naturales',
'Nat', 'Tipo de Contribuyente', 1, 'S/N', SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');



-- Carga Inicial  Tipo identificacion 

select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'
GO
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGO,ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES(newid(), (SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200025787001'),'Tipo Identificaci�n', 'TID', 'Tipos Identificaci�n', 1, SYSDATETIME(),SYSDATETIME(), 'admin', 'admin');

GO
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, 
ID_EMPRESA, 
NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES(
(SELECT ID_CATALOGO FROM SAS_Catalogos_Nota_DB.dbo.CATALOGO  where  CODIGO_CATALOGO = 'TID'), 
(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 
'C�dula', 'CDL','Identificaci�n Ecuador', 1,  SYSDATETIME(),SYSDATETIME(), 'admin', 'admin');

GO

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, 
ID_EMPRESA, 
NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES(
(SELECT ID_CATALOGO FROM SAS_Catalogos_Nota_DB.dbo.CATALOGO  where  CODIGO_CATALOGO = 'TID'), 
(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001'), 
'Pasaporte', 'PSP','Identificaci�n extranjeros', 1,  SYSDATETIME(),SYSDATETIME(), 'admin', 'admin');

GO

-- Carga Inicial Persona 

INSERT INTO SAS_Personas_DB.dbo.PERSONA
(ID_LUGAR_NACIMIENTO, ID_RESIDENCIA, ID_PAIS_ORIGEN, ID_TIPO_IDENTIFICACION, ID_ESTADO_CIVIL, NOMBRE, APELLIDO, NUMERO_IDENTIFICACION, FECHA_NACIMIENTO, TELEFONO_1, TELEFONO_2, CONTACTO_EMERGENCIA, DIRECCION, CORREO_PERSONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)	
VALUES(
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'JDS' ), 
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'JDS' ),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'EC'), 
(select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='CDL'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'DVD'), 	
	'Jefferson', N'B�rcenas', N'2200229678', 	
	SYSDATETIME(), '0982005010', '062899165', '0980477326',
 'Av. de los Fundadores & Estefania Crespo.', 'jeffersonbarcenas_13@hotmail.com', SYSDATETIME(),SYSDATETIME(), N'Admin', N'Admin')
 
GO
INSERT INTO SAS_Personas_DB.dbo.PERSONA
(ID_LUGAR_NACIMIENTO, ID_RESIDENCIA, ID_PAIS_ORIGEN, ID_TIPO_IDENTIFICACION, ID_ESTADO_CIVIL, NOMBRE, APELLIDO, NUMERO_IDENTIFICACION, FECHA_NACIMIENTO, TELEFONO_1, TELEFONO_2, CONTACTO_EMERGENCIA, DIRECCION, CORREO_PERSONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)	
VALUES(
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'JDS' ), 
	(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'JDS' ),
	(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'EC'), 
	(select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='CDL'),	
	(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'UDH'),
	N'Patricio', N'Cordova', N'220002578', 
	SYSDATETIME(), N'0982119036', N'0982208667', N'0982208667',
	N'Av. de los Fundadores & Estefania Crespo.', N'patrico_pcs@hotmail.com', SYSDATETIME(),SYSDATETIME(), N'admin', N'admin')
GO
INSERT INTO SAS_Personas_DB.dbo.PERSONA
(ID_LUGAR_NACIMIENTO, ID_RESIDENCIA, ID_PAIS_ORIGEN, ID_TIPO_IDENTIFICACION, ID_ESTADO_CIVIL, NOMBRE, APELLIDO, NUMERO_IDENTIFICACION, 
FECHA_NACIMIENTO, TELEFONO_1, TELEFONO_2, CONTACTO_EMERGENCIA, DIRECCION, CORREO_PERSONAL, 
FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)	
	VALUES(
	(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'JDS' ), 
	(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'JDS' ),
	(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'EC'), 
	(select ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO where CODIGO_CATALOGO='CDL'),
	(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO = 'CSD'),	
	N'Javier', N'Leiton', N'0401159314',	
	SYSDATETIME(), N'0998798592', N'', N'',
	N'direcci�n.', N'xleinto@gmail.com', SYSDATETIME(),SYSDATETIME(), N'admin', N'admin')
GO



SELECT * FROM SAS_Personas_DB.dbo.PERSONA
GO



-- ROLES

INSERT INTO SAS_Roles_Usuarios_DB.dbo.ROL
(ID_EMPRESA, NOMBRE_ROL, ESTADO, FECHA_CREACION, FECHA_MODFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001')
, 'Super Administrador', 1, SYSDATETIME(), SYSDATETIME(), 'ADMIN', 'ADMIN'),
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC ='2200025787001')
, 'Administrador', 1, SYSDATETIME(), SYSDATETIME(), 'ADMIN', 'ADMIN');

select *
from ROL

/*INSERT INTO SAS_Roles_Usuarios_DB.dbo.USUARIO([ID_PERSONA] ,ID_ROL,ID_EMPRESADEFAULT,[ESTADO], [CORREOINSTITUCIONAL], [CONTRASENA], [INDICIOCONTRASENA], [FECHAULTIMOINGRESO], [FECHACRECION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES(
	(select ID_PERSONA from PERSONA where NOMBRE='Javier'),	
	(select ID_ROL from ROL where NOMBREROL='Super Administrador'),
	(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
	1, N'xleiton@ticskernel.com', N'T1CsK@J4v1.2021', N'Tic', '2021-6-24 11:2:0', '2021-6-24 11:2:0', '2021-6-24 11:2:0', N'Javi', N'Javi')
*/	




INSERT INTO SAS_Roles_Usuarios_DB.dbo.USUARIO 
( ID_PERSONA, ID_ROL, ID_EMPRESA_DEFAULT, 
ESTADO, CORREO_INSTITUCIONAL, CONTRASENA, INDICIO_CONTRASENA, FECHA_ULTIMO_INGRESO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION) 
	VALUES(
	(select ID_PERSONA from SAS_Personas_DB.dbo.PERSONA where NOMBRE='Jefferson'),
	(select ID_ROL from SAS_Roles_Usuarios_DB.dbo.ROL  where NOMBRE_ROL='Super Administrador'),
	(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
	1, N'jefferson.barcenas@ticskernel.com', N'515d298bd3065ec1d0e82a7cefd85efd2ad90954b8865387662806be8e35dbe10051005d0029008b00d30006005e00c100d000e8002a007c00ef00d8005e00fd002a00d90009005400b800860053008700660028000600be008e00', 
	N'Tic', getdate(), getdate(), getdate(), N'Admin', N'Admin')
GO

INSERT INTO SAS_Roles_Usuarios_DB.dbo.USUARIO 
( ID_PERSONA, ID_ROL, ID_EMPRESA_DEFAULT, 
ESTADO, CORREO_INSTITUCIONAL, CONTRASENA, INDICIO_CONTRASENA, FECHA_ULTIMO_INGRESO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION) 
	VALUES(
	(select ID_PERSONA from SAS_Personas_DB.dbo.PERSONA where NOMBRE='Patricio'), 	
	(select ID_ROL from SAS_Roles_Usuarios_DB.dbo.ROL where NOMBRE_ROL='Super Administrador'),
	(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
	1, N'patricio.cordova@ticskernel.com', N'515d298bd3065ec1d0e82a7cefd85efd2ad90954b8865387662806be8e35dbe10051005d0029008b00d30006005e00c100d000e8002a007c00ef00d8005e00fd002a00d90009005400b800860053008700660028000600be008e00', 
	N'Tic', getdate(), getdate(), getdate(), N'Admin', N'Admin')
	
GO




INSERT INTO SAS_Empresas_DB.dbo.USUARIO_EMPRESA 
(ID_EMPRESA, ID_USUARIO)
VALUES(
(select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(select ID_USUARIO from SAS_Roles_Usuarios_DB.dbo.USUARIO  where CORREO_INSTITUCIONAL='patricio.cordova@ticskernel.com')),

((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(select ID_USUARIO from SAS_Roles_Usuarios_DB.dbo.USUARIO where CORREO_INSTITUCIONAL='jefferson.barcenas@ticskernel.com'));

GO

SELECT * FROM SAS_Roles_Usuarios_DB.dbo.USUARIO
GO

SELECT * FROM SAS_Empresas_DB.dbo.USUARIO_EMPRESA 
GO
---CATALOGO M�dulos
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
 'M�dulos', 'MDLS','M�dulos del sistema', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
GO
--Inserto los m�dulos que existen
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, ID_CATALOGOPADRE,NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='MDLS'),
 'Usuarios', 'USRS','Usuarios Sistema', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='MDLS'),
 'Administraci�n', 'ADMI','Administraci�n M�dulo', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='MDLS'),
 'Configuraciones', 'CONFG','Configuraciones', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
GO
 select * 
 from SAS_Catalogos_Nota_DB.dbo.CATALOGO 
 where CODIGO_CATALOGO ='USRS' 
GO 
select *
from SAS_Catalogos_Nota_DB.dbo.CATALOGO 
where ID_CATALOGOPADRE = (
 select ID_CATALOGO 
 from SAS_Catalogos_Nota_DB.dbo.CATALOGO 
 where CODIGO_CATALOGO ='CONFG' )
 GO
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, ID_CATALOGOPADRE,NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION,DATO_ADICIONAL, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
 ((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='CONFG'),
  'Cat�logo', 'MACATG','Gesti�n de cat�logos','/C_Catalogo/GestionCatalogos', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='CONFG'),
  'Rol', 'MAROLES','Gesti�n de roles','/C_Rol/CrearRol', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
  ((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='USRS'),
  'Administrar Usuarios', 'MAUSERS','Menu administrador usuarios','/C_Usuario/AdministrarUsuarios', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 
  'admin'),
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='USRS'),
  'Crear Usuario', 'MAUCUSER','Menu administrador usuarios','/C_Usuario/CrearUsuario', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='ADMI'),
  'Local', 'MLCL','Menu local','/C_Local/AdministrarLocal', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
 ((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='ADMI'),
  'Empresa', 'MEMP','Menu empresa','/C_Empresa/AdministrarEmpresa', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
   
GO
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION,ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
  ((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
  'Tipo Permiso', 'TPPMS','Permisos aplicaci�n', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
 GO

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, ID_CATALOGOPADRE,NOMBRE_CATALOGO, CODIGO_CATALOGO, DESCRIPCION,DATO_ADICIONAL ,ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='TPPMS'),
  'Escritura', 'PESC','Permiso Escritura','btn btn-outline-info', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'), 
((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='TPPMS'),
  'Lectura', 'PLEC','Permiso Lectura', 'btn btn-outline-primary',1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'),
 ((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='TPPMS'),
  'Edici�n', 'PEDC','Permiso Edici�n','btn btn-outline-success', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin'), 
 ((select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE CODIGO_CATALOGO ='TPPMS'),
  'Eliminar', 'PELI','Permiso eliminar','btn btn-outline-danger', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
GO
SELECT *
from ROL r 

select *
from MENU m 
 GO
select *
from SAS_Catalogos_Nota_DB.dbo.CATALOGO c 
where c.CODIGO_CATALOGO ='EC'
GO
select *
from SAS_Catalogos_Nota_DB.dbo.CATALOGO t
where t.ID_CATALOGOPADRE = (select c.ID_CATALOGO 
from SAS_Catalogos_Nota_DB.dbo.CATALOGO c where c.CODIGO_CATALOGO ='EC')
 
 GO

 SELECT *
 from PERMISOS p 
 
---Local

 INSERT INTO SAS_Empresas_DB.dbo.[LOCAL] 
(ID_UBICACION, ID_EMPRESA, ID_TIPO_LOCAL, NOMBRE_LOCAL, DIRECCION_LOCAL, TELEFONO, OBSERVACIONES, ESTADO, FECHA_CREACION,
FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES
((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Quito'),(SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200025787001' ),
(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Matriz' ),'Isabel', 'Av.Fundadores y Estefania Crespo ','0982005010', 'Ninguna',
1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

---Bodega
INSERT INTO SAS_Empresas_DB.dbo.BODEGA
(ID_LOCAL, NOMBRE_BODEGA, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES((SELECT ID_LOCAL FROM SAS_Empresas_DB.dbo.[LOCAL]  WHERE NOMBRE_LOCAL = 'Isabel' ),
'Bodega1', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

-- Caja
INSERT INTO SAS_Empresas_DB.dbo.CAJA
(ID_LOCAL, NOMBRE_CAJA, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIOCREACION_, USUARIO_MODIFICACION)
VALUES((SELECT ID_LOCAL FROM SAS_Empresas_DB.dbo.[LOCAL]  WHERE NOMBRE_LOCAL = 'Isabel' ),
'Caja 1 ', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
INSERT INTO SAS_Empresas_DB.dbo.CAJA
(ID_LOCAL, NOMBRE_CAJA, ESTADO, FECHA_CREACION, FECHA_MODIFICACION, USUARIOCREACION_, USUARIO_MODIFICACION)
VALUES((SELECT ID_LOCAL FROM SAS_Empresas_DB.dbo.[LOCAL]  WHERE NOMBRE_LOCAL = 'Isabel' ),
'Caja 2 ', 1, SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

-- Especialidad------
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_EMPRESA, NOMBRE_CATALOGO,
CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES( (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'), 'Especialidad',
'ESP', 'Distribuidor, Artesano, Intermediario, Servicio, etc', 1, 'S/N', SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO,
CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Especialidad'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'), 'Distribuidor',
'DIST', 'Padre Especialidad', 1, 'S/N', SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO,
CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Especialidad'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'), 'Artesano',
'ART', 'Padre Especialidad', 1, 'S/N', SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO
(ID_CATALOGOPADRE, ID_EMPRESA, NOMBRE_CATALOGO,
CODIGO_CATALOGO, DESCRIPCION, ESTADO, DATO_ADICIONAL, FECHA_CREACION, FECHA_MODIFICACION, USUARIO_CREACION, USUARIO_MODIFICACION)
VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Especialidad'), (select ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA where RUC='2200025787001'), 'Intermediario',
'INTE', 'Padre Especialidad', 1, 'S/N', SYSDATETIME(), SYSDATETIME(), 'admin', 'admin');

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--HASTA AQUI SE DEBE EJECUTAR ---
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////




















SELECT *
from SAS_Catalogos_Nota_DB.dbo.CATALOGO c 
where c.ID_CATALOGO ='6d311816-c38b-4c12-b6ad-5ae4bdbef9a8'

select *
FROM  ROL r 
go
SELECT *
from OPERACION o 

SELECT *
from MODULO m 

SELECT *
FROM ROL_OPERACION 

----ROL OPERACOIN


SELECT *
FROM ROL_OPERACION ro 


----
---Empresas

INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO ([ID_EMPRESA], [NOMBRE_CATALOGO], [CODIGO_CATALOGO], [DESCRIPCION], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '22000025787001'), N'Importancia', N'Imp', 'Categoriza la importancia de las Notas', 1, '2021-7-13 10:49:14', '2021-7-13 10:49:14', N'Admin', N'Admin')
GO
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO ([ID_CATALOGOPADRE], [ID_EMPRESA], [NOMBRE_CATALOGO], [CODIGO_CATALOGO], [DESCRIPCION], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Importancia'), 
	(SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '22000025787001'), N'Urgente', N'Urg', 'Nota de maxima relevancia', 1, '2021-7-9 10:34:36', '2021-7-9 10:34:36', N'Admin', N'Admin')
GO
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO ([ID_CATALOGOPADRE], [ID_EMPRESA], [NOMBRE_CATALOGO], [CODIGO_CATALOGO], [DESCRIPCION], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Importancia'),
	(SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '22000025787001'), N'Relevante', N'Urg', 'Nota de maxima relevancia', 1, '2021-7-9 10:34:36', '2021-7-9 10:34:36', N'Admin', N'Admin')
GO
INSERT INTO SAS_Catalogos_Nota_DB.dbo.CATALOGO ([ID_CATALOGOPADRE], [ID_EMPRESA], [NOMBRE_CATALOGO], [CODIGO_CATALOGO], [DESCRIPCION], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Importancia'),
	(SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '22000025787001'), N'Media', N'Urg', 'Nota de maxima relevancia', 1, '2021-7-9 10:34:36', '2021-7-9 10:34:36', N'Admin', N'Admin')
GO


-- Carga Inicial Locales

INSERT INTO [dbo].[LOCAL]([ID_UBICACION], [ID_EMPRESA], [NOMBRE_LOCAL], [TIPOLOCAL], [DIRECCION_LOCAL], [TELEFONO], [OBSERVACIONES], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'El �ngel' ), (SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871' ), N'Comercios Ecuador', N'Oficina', N'Av. 10 de agosto', N'02887854', 'Esquina', 1, '2021-6-17 14:31:15', '2021-6-17 14:31:15', N'Javi', N'Javi')
GO
INSERT INTO [dbo].[LOCAL]([ID_UBICACION], [ID_EMPRESA], [NOMBRE_LOCAL], [TIPOLOCAL], [DIRECCION_LOCAL], [TELEFONO], [OBSERVACIONES], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Quito'), (SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '040001145' ), N'TicsKernel', N'Oficina', N'Av. 8 de octubre', N'062215574', 'Esquina', 1, '2021-6-17 14:31:15', '2021-6-17 14:31:15', N'PatoPc', N'PatoPc')
GO
INSERT INTO [dbo].[LOCAL]([ID_UBICACION], [ID_EMPRESA], [NOMBRE_LOCAL], [TIPOLOCAL], [DIRECCION_LOCAL], [TELEFONO], [OBSERVACIONES], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Joya de los sachas' ),(SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200104578' ), N'Tics', N'Oficina', N'Av.Fundadores y Estefania Crespo', N'062899165', 'Esquina', 1, '2021-6-17 14:31:15', '2021-6-17 14:31:15', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[LOCAL]
GO

-- Carga de Bodegas

INSERT INTO [dbo].[BODEGA]([ID_LOCAL], [NOMBRE_BODEGA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'TicsKernel' ), N'Bodega "1A" ', 1, '2021-6-22 15:46:13', '2021-6-22 15:46:13', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[BODEGA]([ID_LOCAL], [NOMBRE_BODEGA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'TicsKernel' ), N'Bodega "1B" ', 1, '2021-6-22 15:46:13', '2021-6-22 15:46:13', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[BODEGA]([ID_LOCAL], [NOMBRE_BODEGA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Comercios Ecuador' ), N'Bodega "A" ', 1, '2021-6-22 15:46:13', '2021-6-22 15:46:13', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[BODEGA]([ID_LOCAL], [NOMBRE_BODEGA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Comercios Ecuador' ), N'Bodega "B" ', 1, '2021-6-22 15:46:13', '2021-6-22 15:46:13', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[BODEGA]([ID_LOCAL], [NOMBRE_BODEGA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Tics' ), N'Bodega "2A" ', 1, '2021-6-22 15:46:13', '2021-6-22 15:46:13', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[BODEGA]([ID_LOCAL], [NOMBRE_BODEGA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Tics' ), N'Bodega "2B" ', 1, '2021-6-22 15:46:13', '2021-6-22 15:46:13', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[BODEGA]
GO
-- cargar Caja
INSERT INTO [dbo].[CAJA]([ID_LOCAL], [NOMBRE_CAJA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIOCREACION_], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'TicsKernel'), N'Caja A1', 1, '2021-6-24 10:58:3', '2021-6-24 10:58:3', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[CAJA]([ID_LOCAL], [NOMBRE_CAJA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIOCREACION_], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'TicsKernel'), N'Caja B2', 1, '2021-6-24 10:58:3', '2021-6-24 10:58:3', N'Admin', N'Admin')
GO

INSERT INTO [dbo].[CAJA]([ID_LOCAL], [NOMBRE_CAJA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIOCREACION_], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Comercios Ecuador'), N'Caja 1', 1, '2021-6-24 10:58:3', '2021-6-24 10:58:3', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[CAJA]([ID_LOCAL], [NOMBRE_CAJA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIOCREACION_], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Comercios Ecuador'), N'Caja 2', 1, '2021-6-24 10:58:3', '2021-6-24 10:58:3', N'Admin', N'Admin')
GO

INSERT INTO [dbo].[CAJA]([ID_LOCAL], [NOMBRE_CAJA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIOCREACION_], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Tics'), N'Caja 1A', 1, '2021-6-24 10:58:3', '2021-6-24 10:58:3', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[CAJA]([ID_LOCAL], [NOMBRE_CAJA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIOCREACION_], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_LOCAL FROM LOCAL WHERE NOMBRE_LOCAL = 'Tics'), N'Caja 2B', 1, '2021-6-24 10:58:3', '2021-6-24 10:58:3', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[CAJA]
GO
-- Carga ControlCaja

INSERT INTO [dbo].[CONTROLCAJA]([ID_CAJA], [ID_PERSONA], [ID_USUARIO], [FECHAAPERTURA], [FECHACIERRE], [VALORINICIAL], [VALORFACTURAVENTAANULADA], [CANTIDADVENTASCAJA], [VENTASEFECTIVO], [VENTASNOTACREDITO], [VENTASCREDITOPERSONAL], [VALORNOTASCREDITOGENERADAS], [REPORTECORRECTO], [OBSERVACION], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CAJA FROM CAJA WHERE NOMBRE_CAJA = 'Caja 1'),(SELECT ID_PERSONA FROM PERSONA WHERE NOMBRE = 'Patricio'),(SELECT ID_USUARIO FROM USUARIO WHERE CORREOINSTITUCIONAL = 'xleiton@ticskernel.com'), '2021-7-1 14:42:5', '2021-7-1 14:42:5', 10, 20, 30, 40, 50, 60, 10, 1, 'Ok', '2021-7-1 14:42:5', '2021-7-1 14:42:5', N'Admin', N'Admin')
GO

INSERT INTO [dbo].[CONTROLCAJA]([ID_CAJA], [ID_PERSONA], [ID_USUARIO], [FECHAAPERTURA], [FECHACIERRE], [VALORINICIAL], [VALORFACTURAVENTAANULADA], [CANTIDADVENTASCAJA], [VENTASEFECTIVO], [VENTASNOTACREDITO], [VENTASCREDITOPERSONAL], [VALORNOTASCREDITOGENERADAS], [REPORTECORRECTO], [OBSERVACION], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CAJA FROM CAJA WHERE NOMBRE_CAJA = 'Caja 1A'),(SELECT ID_PERSONA FROM PERSONA WHERE NOMBRE = 'Jefferson'),(SELECT ID_USUARIO FROM USUARIO WHERE CORREOINSTITUCIONAL = 'jefferson.barcenas@ticskernel.com'), '2021-7-1 14:56:37', '2021-7-1 14:56:37', 10, 20, 30, 40, 50, 60, 10, 1, 'OK', '2021-7-1 14:56:37', '2021-7-1 14:56:37', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[CONTROLCAJA]([ID_CAJA], [ID_PERSONA], [ID_USUARIO], [FECHAAPERTURA], [FECHACIERRE], [VALORINICIAL], [VALORFACTURAVENTAANULADA], [CANTIDADVENTASCAJA], [VENTASEFECTIVO], [VENTASNOTACREDITO], [VENTASCREDITOPERSONAL], [VALORNOTASCREDITOGENERADAS], [REPORTECORRECTO], [OBSERVACION], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CAJA FROM CAJA WHERE NOMBRE_CAJA = 'Caja 2B'),(SELECT ID_PERSONA FROM PERSONA WHERE NOMBRE = 'Leiton'),(SELECT ID_USUARIO FROM USUARIO WHERE CORREOINSTITUCIONAL = 'xleiton@ticskernel.com'), '2021-7-1 14:56:37', '2021-7-1 14:56:37', 10, 20, 30, 40, 50, 60, 10, 1, 'OK', '2021-7-1 14:56:37', '2021-7-1 14:56:37', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[CONTROLCAJA]
GO
--Fin ControlCaja
--Carga Inicial Estado Producto

INSERT INTO [dbo].[ESTADOPRODUCTO]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871' ), N'Expirado', 1, '2021-7-1 15:8:57', '2021-7-1 15:8:57', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[ESTADOPRODUCTO]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '040001145' ), N'Canje', 1, '2021-7-1 15:8:57', '2021-7-1 15:8:57', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[ESTADOPRODUCTO]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200104578' ), N'En Garantia', 1, '2021-7-1 15:8:57', '2021-7-1 15:8:57', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[ESTADOPRODUCTO]
GO
--Fin Carga Inicial Estado Producto

--Carga Inicail Grupo Producto

INSERT INTO [dbo].[GRUPOPRODUCTO]([ID_EMPRESA], [NOMBREGRUPOPRODUCTO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871' ), N'Lacteos', '2021-7-1 15:14:9', '2021-7-1 15:14:9', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[GRUPOPRODUCTO]([ID_EMPRESA], [NOMBREGRUPOPRODUCTO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '040001145' ), N'Refrescos', '2021-7-1 15:14:9', '2021-7-1 15:14:9', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[GRUPOPRODUCTO]([ID_EMPRESA], [NOMBREGRUPOPRODUCTO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200104578' ), N'Intel', '2021-7-1 15:14:9', '2021-7-1 15:14:9', N'Admin', N'Admin')
GO
SELECT * FROM [dbo].[GRUPOPRODUCTO]
GO

-- Fin Carga Inicail Grupo Product
-- Carga Inicial Impueto 
 
INSERT INTO [dbo].[IMPUESTO]([ID_EMPRESA], [NOMBREIMPUESTO], [VALOR], [TIPOIMPUESTO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871' ), 'Impuesto 1', 5, N'Nacional', '2021-7-1 15:33:16', '2021-7-1 15:33:16', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[IMPUESTO]([ID_EMPRESA], [NOMBREIMPUESTO], [VALOR], [TIPOIMPUESTO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '040001145' ), 'Impuesto 1', 4, N'Nacional', '2021-7-1 15:33:16', '2021-7-1 15:33:16', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[IMPUESTO]([ID_EMPRESA], [NOMBREIMPUESTO], [VALOR], [TIPOIMPUESTO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200104578' ), 'Impuesto 1', 2, N'Nacional', '2021-7-1 15:33:16', '2021-7-1 15:33:16', N'Admin', N'Admin')
GO
SELECT * FROM [dbo].[IMPUESTO]
GO

-- Fin Carga Inicial Impuesto 
-- Carga Inicial ProcentajeIva

INSERT INTO [dbo].[PORCENTAJEIVA]([VALOR], [ACTIVO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES( 12, 1, '2021-7-14 14:20:2', '2021-7-14 14:20:2', N'Admin', N'Admin')
GO
SELECT * FROM [dbo].[PORCENTAJEIVA]
GO

-- Fin Carga ProcentajeIVA
-- Carga Marca

INSERT INTO [dbo].[MARCA]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871'), 'Vitaleche', 1, '2021-7-1 15:37:13', '2021-7-1 15:37:13', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[MARCA]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '040001145'), 'Intel', 1, '2021-7-1 15:37:13', '2021-7-1 15:37:13', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[MARCA]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200104578'), 'AMD', 1, '2021-7-1 15:37:13', '2021-7-1 15:37:13', N'Admin', N'Admin')
GO
SELECT * FROM [dbo].[MARCA]
GO

--Fin Carga Marca 

--Carga Unicad de Entrega

INSERT INTO [dbo].[UNIDADENTREGA]([ID_EMPRESA], [NOMBRECONTENIDO], [CODIGOUNIDAD], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871'), 'Litro', 'L', '2021-7-1 15:41:17', '2021-7-1 15:41:17', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[UNIDADENTREGA]([ID_EMPRESA], [NOMBRECONTENIDO], [CODIGOUNIDAD], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '040001145'), 'Kilo', 'Kl', '2021-7-1 15:41:17', '2021-7-1 15:41:17', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[UNIDADENTREGA]([ID_EMPRESA], [NOMBRECONTENIDO], [CODIGOUNIDAD], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200104578'), 'Libra', 'Lb', '2021-7-1 15:41:17', '2021-7-1 15:41:17', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[UNIDADENTREGA]
GO

--Fin Carga Unidad de Entrega 

-- Carga EstadoProducto

INSERT INTO [dbo].[ESTADOPRODUCTO]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871'), N'DaÃƒÂ±ado', 1, '2021-7-5 12:26:19', '2021-7-5 12:26:19', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[ESTADOPRODUCTO]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '040001145'), N'Incompleto', 1, '2021-7-5 12:26:19', '2021-7-5 12:26:19', N'Admin', N'Admin')
GO
INSERT INTO [dbo].[ESTADOPRODUCTO]([ID_EMPRESA], [NOMBRE], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '2200104578'), N'Expirado', 1, '2021-7-5 12:26:19', '2021-7-5 12:26:19', N'Admin', N'Admin')
GO
SELECT * FROM [dbo].[ESTADOPRODUCTO]
GO
-- Fin Carga Estado Producto

-- Carga Categoria

INSERT INTO [dbo].[CATEGORIA]([ID_EMPRESA], [NOMBRECATEGORIA], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871'), N'Enlatados', '2021-7-6 12:40:29', '2021-7-6 12:40:29', N'Admin', N'Admin')
GO

INSERT INTO [dbo].[CATEGORIA]([ID_CATEGORIAPADRE], [ID_EMPRESA], [NOMBRECATEGORIA], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_CATEGORIA FROM CATEGORIA WHERE NOMBRECATEGORIA = 'Enlatados'), (SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871'), N'Atun', '2021-7-6 12:41:49', '2021-7-6 12:41:49', N'Admin', N'Admin')
GO
SELECT * FROM [dbo].[CATEGORIA]
GO
-- Fin Carga Categoria
-- Carga Inicial Notas

INSERT INTO [dbo].[NOTAS]([ID_EMPRESA], [ID_IMPORTANCIA], [TITULONOTA], [CONTENIDONOTA], [ESTADO], [FECHAVENCIMIENTO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871'),(SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO = 'Importancia'), 'Urgente', 'Esta es una naota de Prueba de carga Inicial', 1, '2021-7-13 11:10:57', '2021-7-13 11:10:57', '2021-7-13 11:10:57', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[NOTAS]
GO

-- finar carga Nota

-- Carga Inicial Proveedores

INSERT INTO [dbo].[PROVEEDOR]([ID_PORCENAJEIVA], [ID_TIPOCONTRIBUYENTE], [ID_ESPECIALIDAD], [ID_EMPRESA], [CONTRIBUYENTEESPECIAL], [RAZONSOCIAL],
 [NOMBRECOMERCIAL], [RUC], [DIRECCION], [TELEFONOPROVEEDOR], [TELEFONOMOVIL], [CORREOELECTRONICO], [OBLIGADOLLEVARCONTABILIDAD], [NOMBREREPRESENTATE], 
 [NOMBRECONTACTO], [TELEFONOCONTACTO], 
 [CODIGOPROVEEDOR], [NOTA], [ESTADO], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_PORCENAJEIVA FROM PORCENTAJEIVA WHERE VALOR = '12'), (SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO ='Especial'),
	 (SELECT ID_CATALOGO from SAS_Catalogos_Nota_DB.dbo.CATALOGO WHERE NOMBRE_CATALOGO ='Especial'),
	(SELECT ID_EMPRESA from SAS_Empresas_DB.dbo.EMPRESA WHERE RUC = '220054871'), 1, N'Comercio', N'TecNo', N'122242001', N'Av sn', N'0399577', N'098877545', N'tecno@hotmail.com', 1, 
	N'Juan Peres', N'Juanito AlimaÃƒÂ±a', N'0288764', N'asss', 'tes nota', 1, '2021-7-14 14:35:16', '2021-7-14 14:35:16', N'Admin', N'Admin')
GO

SELECT * FROM [dbo].[PROVEEDOR]
GO

-- Fin Carga Proveedores

-- Carga Inicial CanjeProductoProveedor
 
INSERT INTO [dbo].[CANJEPRODUCTOPROVEEDOR]([ID_PROVEEDOR], [FECHACANJE], [DESCRIPCION], [IDENTIFICADORCANJEPRODUCTO], [IMAGENCANJE], [FECHA_CREACION], [FECHA_MODIFICACION], [USUARIO_CREACION], [USUARIO_MODIFICACION]) 
	VALUES((SELECT ID_PROVEEDOR FROM PROVEEDOR WHERE NOMBRECOMERCIAL='Tecnomega'), '2021-7-14 14:35:30',N'Canje' , N'1', NULL, '2021-7-14 14:35:30', '2021-7-14 14:35:30', N'Admin', N'Admin')
GO
SELECT * FROM [dbo].[CANJEPRODUCTOPROVEEDOR]
GO
 -- Fin de Cargar CanjeProductoProveedor 
 
 --Carga Inicial EstadoVenta
 
INSERT INTO [dbo].[ESTADOVENTA]([NOMBRE], [FECHA_CREACION], [FECHA_MODIFICACION]) 
	VALUES( N'Cambio', '2021-7-16 12:39:2', '2021-7-16 12:39:2')
GO
INSERT INTO [dbo].[ESTADOVENTA]([NOMBRE], [FECHA_CREACION], [FECHA_MODIFICACION]) 
	VALUES( N'Fiado', '2021-7-16 12:39:2', '2021-7-16 12:39:2')
GO
INSERT INTO [dbo].[ESTADOVENTA]([NOMBRE], [FECHA_CREACION], [FECHA_MODIFICACION]) 
	VALUES( N'Completada', '2021-7-16 12:39:2', '2021-7-16 12:39:2')
GO

SELECT * FROM [dbo].[ESTADOVENTA]
GO
 --Finc Carga 



SELECT *
FROM USUARIO u

SELECT *
from SAS_Catalogos_Nota_DB.dbo.CATALOGO c 
where NOMBRE_CATALOGO  ='Usuarios'

select *
from SAS_Catalogos_Nota_DB.dbo.CATALOGO c
where ID_EMPRESA ='25DA37CA-17FE-4447-8E53-EB8682DCF8A6'


