/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2016                    */
/* Created on:     19/11/2022 12:03:53                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATALOGO_PRODUCTO') and o.name = 'FK_CATALOGO_REFERENCE_CATEGORI')
alter table CATALOGO_PRODUCTO
   drop constraint FK_CATALOGO_REFERENCE_CATEGORI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATALOGO_PRODUCTO') and o.name = 'FK_CATALOGO_REFERENCE_UNIDAD_E')
alter table CATALOGO_PRODUCTO
   drop constraint FK_CATALOGO_REFERENCE_UNIDAD_E
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATALOGO_PRODUCTO') and o.name = 'FK_CATALOGO_REFERENCE_GRUPO_PR')
alter table CATALOGO_PRODUCTO
   drop constraint FK_CATALOGO_REFERENCE_GRUPO_PR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATALOGO_PRODUCTO') and o.name = 'FK_CATALOGO_REFERENCE_MARCA')
alter table CATALOGO_PRODUCTO
   drop constraint FK_CATALOGO_REFERENCE_MARCA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATALOGO_PRODUCTO_IMPUESTO') and o.name = 'FK_CATALOGO_FK_C_PROD_CATALOGO')
alter table CATALOGO_PRODUCTO_IMPUESTO
   drop constraint FK_CATALOGO_FK_C_PROD_CATALOGO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATALOGO_PRODUCTO_IMPUESTO') and o.name = 'FK_CATALOGO_REFERENCE_IMPUESTO')
alter table CATALOGO_PRODUCTO_IMPUESTO
   drop constraint FK_CATALOGO_REFERENCE_IMPUESTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CATEGORIA_PRODUCTO') and o.name = 'FK_CATEGORI_REFERENCE_CATEGORI')
alter table CATEGORIA_PRODUCTO
   drop constraint FK_CATEGORI_REFERENCE_CATEGORI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRECIO_TIPO_VENTA_CATALOGO') and o.name = 'FK_PRECIO_T_REFERENCE_CATALOGO')
alter table PRECIO_TIPO_VENTA_CATALOGO
   drop constraint FK_PRECIO_T_REFERENCE_CATALOGO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRECIO_TIPO_VENTA_PRODUCTO') and o.name = 'FK_PRECIO_T_REFERENCE_PRODUCTO')
alter table PRECIO_TIPO_VENTA_PRODUCTO
   drop constraint FK_PRECIO_T_REFERENCE_PRODUCTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_UNIDAD_E')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_UNIDAD_E
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_CATEGORI')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_CATEGORI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_GRUPO_PR')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_GRUPO_PR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_CANJE_PR')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_CANJE_PR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_ESTADO_P')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_ESTADO_P
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO') and o.name = 'FK_PRODUCTO_REFERENCE_MARCA')
alter table PRODUCTO
   drop constraint FK_PRODUCTO_REFERENCE_MARCA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO_IMPUESTO') and o.name = 'FK_PRODUCTO_PRODUCTO__PRODUCTO')
alter table PRODUCTO_IMPUESTO
   drop constraint FK_PRODUCTO_PRODUCTO__PRODUCTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO_IMPUESTO') and o.name = 'FK_PRODUCTO_REFERENCE_IMPUESTO')
alter table PRODUCTO_IMPUESTO
   drop constraint FK_PRODUCTO_REFERENCE_IMPUESTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRODUCTO_STOCK') and o.name = 'FK_PRODUCTO_REFERENCE_PRODUCTO')
alter table PRODUCTO_STOCK
   drop constraint FK_PRODUCTO_REFERENCE_PRODUCTO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UNIDAD_CATALOGO_PRODUCTO') and o.name = 'FK_UNIDAD_C_REFERENCE_CATALOGO')
alter table UNIDAD_CATALOGO_PRODUCTO
   drop constraint FK_UNIDAD_C_REFERENCE_CATALOGO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UNIDAD_PRODUCTO') and o.name = 'FK_UNIDAD_P_REFERENCE_PRODUCTO')
alter table UNIDAD_PRODUCTO
   drop constraint FK_UNIDAD_P_REFERENCE_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CANJE_PRODUCTO_PROVEEDOR')
            and   type = 'U')
   drop table CANJE_PRODUCTO_PROVEEDOR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CATALOGO_PRODUCTO')
            and   type = 'U')
   drop table CATALOGO_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CATALOGO_PRODUCTO_IMPUESTO')
            and   type = 'U')
   drop table CATALOGO_PRODUCTO_IMPUESTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CATEGORIA_PRODUCTO')
            and   type = 'U')
   drop table CATEGORIA_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ESTADO_PRODUCTO')
            and   type = 'U')
   drop table ESTADO_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('GRUPO_PRODUCTO')
            and   type = 'U')
   drop table GRUPO_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('IMPUESTO')
            and   type = 'U')
   drop table IMPUESTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MARCA')
            and   type = 'U')
   drop table MARCA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRECIO_TIPO_VENTA_CATALOGO')
            and   type = 'U')
   drop table PRECIO_TIPO_VENTA_CATALOGO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRECIO_TIPO_VENTA_PRODUCTO')
            and   type = 'U')
   drop table PRECIO_TIPO_VENTA_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCTO')
            and   type = 'U')
   drop table PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCTO_IMPUESTO')
            and   type = 'U')
   drop table PRODUCTO_IMPUESTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRODUCTO_STOCK')
            and   type = 'U')
   drop table PRODUCTO_STOCK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UNIDAD_CATALOGO_PRODUCTO')
            and   type = 'U')
   drop table UNIDAD_CATALOGO_PRODUCTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UNIDAD_ENTREGA')
            and   type = 'U')
   drop table UNIDAD_ENTREGA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UNIDAD_PRODUCTO')
            and   type = 'U')
   drop table UNIDAD_PRODUCTO
go

/*==============================================================*/
/* Table: CANJE_PRODUCTO_PROVEEDOR                              */
/*==============================================================*/
create table CANJE_PRODUCTO_PROVEEDOR (
   ID_CANJE_PRODUCTO_PROVEEDOR uniqueidentifier     not null default newid(),
   ID_PROVEEDOR         uniqueidentifier     null,
   FECHA_CANJE          datetime             not null,
   DESCRIPCION          varchar(1)           null,
   IDENTIFICADOR_CANJE_PRODUCTO nvarchar(30)         not null,
   IMAGEN_CANJE         image                null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(36)         not null,
   USUARIO_MODIFICACION nvarchar(36)         not null,
   constraint PK_CANJE_PRODUCTO_PROVEEDOR primary key (ID_CANJE_PRODUCTO_PROVEEDOR)
)
go

/*==============================================================*/
/* Table: CATALOGO_PRODUCTO                                     */
/*==============================================================*/
create table CATALOGO_PRODUCTO (
   ID_CATALOGO_PRODUCTO uniqueidentifier     not null default newid(),
   ID_PORCENTAJE_IVA    uniqueidentifier     null,
   ID_CATEGORIA         uniqueidentifier     null,
   ID_UNIDAD_ENTREGA    uniqueidentifier     null,
   ID_GRUPO_PRODUCTO    uniqueidentifier     null,
   ID_MARCA             uniqueidentifier     null,
   ID_EMPRESA           uniqueidentifier     null,
   ID_PERSONA           uniqueidentifier     null,
   ID_PROVEEDOR         uniqueidentifier     null,
   NOMBRE_PRODUCTO      nvarchar(60)         not null,
   DETALLE_CATALOGO_PRODUCTO nvarchar(Max)        null,
   COSTO_ULTIMA_ADQUISICION decimal(12,5)        not null,
   IMAGEN_PRODUCTO      nvarchar(Max)        null,
   TIENE_IVA            bit                  not null,
   FECHA_CADUCIDAD      datetime             null,
   CANTIDAD_MAXIMA      int                  not null default 0,
   CANTIDAD_MINIMA      int                  not null default 0,
   NOMBRE_CORTO         varchar(20)          null,
   CODIGO_PERSONALIZADO binary(15)           null,
   URL_IMAGEN_CATALOGO_PRODUCTO varchar(1)           null,
   ESTADO               bit                  not null,
   ID_USUARIO_CREACION  uniqueidentifier     null,
   ID_USUARIO_MODIFICACION uniqueidentifier     null,
   FECHA_CREACION       datetime             not null default current_timestamp,
   FECHA_MODIFICACION   datetime             not null,
   constraint PK_CATALOGO_PRODUCTO primary key (ID_CATALOGO_PRODUCTO)
)
go

/*==============================================================*/
/* Table: CATALOGO_PRODUCTO_IMPUESTO                            */
/*==============================================================*/
create table CATALOGO_PRODUCTO_IMPUESTO (
   ID_CATALOGOPRODUCTOIMPUESTO uniqueidentifier     not null default newid(),
   ID_IMPUESTO          uniqueidentifier     null,
   ID_CATALOGO_PRODUCTO uniqueidentifier     null,
   constraint PK_CATALOGO_PRODUCTO_IMPUESTO primary key (ID_CATALOGOPRODUCTOIMPUESTO)
)
go

/*==============================================================*/
/* Table: CATEGORIA_PRODUCTO                                    */
/*==============================================================*/
create table CATEGORIA_PRODUCTO (
   ID_CATEGORIA_PRODUCTO uniqueidentifier     not null default newid(),
   ID_CATEGORIA_PRODUCTO_PADRE uniqueidentifier     null,
   ID_EMPRESA           uniqueidentifier     not null,
   NOMBRE_CATEGORIA_PRODUCTO nvarchar(45)         not null,
   ESTADO_CATEGORIA_PRODUCTO bit                  not null,
   DESCRIPCION_PRODUCTO nvarchar(100)        null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(100)        not null,
   USUARIO_MODIFICACION nvarchar(100)        not null,
   constraint PK_CATEGORIA_PRODUCTO primary key (ID_CATEGORIA_PRODUCTO)
)
go

/*==============================================================*/
/* Table: ESTADO_PRODUCTO                                       */
/*==============================================================*/
create table ESTADO_PRODUCTO (
   ID_ESTADO_PRODUCTO   uniqueidentifier     not null default newid(),
   ID_EMPRESA           uniqueidentifier     null,
   NOMBRE               nvarchar(20)         null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(36)         not null,
   USUARIO_MODIFICACION nvarchar(36)         not null,
   constraint PK_ESTADO_PRODUCTO primary key (ID_ESTADO_PRODUCTO)
)
go

/*==============================================================*/
/* Table: GRUPO_PRODUCTO                                        */
/*==============================================================*/
create table GRUPO_PRODUCTO (
   ID_GRUPO_PRODUCTO    uniqueidentifier     not null default newid(),
   ID_EMPRESA           uniqueidentifier     null,
   NOMBRE_GRUPO_PRODUCTO nvarchar(30)         not null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(100)        not null,
   USUARIO_MODIFICACION nvarchar(100)        not null,
   constraint PK_GRUPO_PRODUCTO primary key (ID_GRUPO_PRODUCTO)
)
go

/*==============================================================*/
/* Table: IMPUESTO                                              */
/*==============================================================*/
create table IMPUESTO (
   ID_IMPUESTO          uniqueidentifier     not null default newid(),
   ID_EMPRESA           uniqueidentifier     null,
   NOMBRE_IMPUESTO      varchar(30)          not null,
   VALOR                decimal(12,5)        not null,
   TIPO_IMPUESTO        nvarchar(10)         not null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(100)        not null,
   USUARIO_MODIFICACION nvarchar(100)        not null,
   constraint PK_IMPUESTO primary key (ID_IMPUESTO)
)
go

/*==============================================================*/
/* Table: MARCA                                                 */
/*==============================================================*/
create table MARCA (
   ID_MARCA             uniqueidentifier     not null default newid(),
   ID_EMPRESA           uniqueidentifier     null,
   NOMBRE               varchar(25)          not null,
   ESTADO               bit                  not null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(100)        not null,
   USUARIO_MODIFICACION nvarchar(100)        not null,
   constraint PK_MARCA primary key (ID_MARCA)
)
go

/*==============================================================*/
/* Table: PRECIO_TIPO_VENTA_CATALOGO                            */
/*==============================================================*/
create table PRECIO_TIPO_VENTA_CATALOGO (
   ID_PRECIO_TIPO_VENTA_CATALOGO uniqueidentifier     not null default newid(),
   ID_CATALOGO_PRODUCTO uniqueidentifier     null default newid(),
   ID_TIPO_CLIENTE_COMPRA uniqueidentifier     not null,
   PORCENTAJE_UTILIDAD  decimal(2,2)         null,
   VALOR                money                null,
   PORCENTAJE_DESCUENTO decimal(2,2)         null,
   VALOR_COMPRA_PRODUCTO money                not null,
   constraint PK_PRECIO_TIPO_VENTA_CATALOGO primary key (ID_PRECIO_TIPO_VENTA_CATALOGO)
)
go

/*==============================================================*/
/* Table: PRECIO_TIPO_VENTA_PRODUCTO                            */
/*==============================================================*/
create table PRECIO_TIPO_VENTA_PRODUCTO (
   ID_PRECIO_TIPO_VENTA_PRODUCTO uniqueidentifier     not null default newid(),
   ID_PRODUCTO          uniqueidentifier     null default newid(),
   ID_TIPO_CLIENTE_COMPRA uniqueidentifier     not null,
   PORCENTAJE_UTILIDAD  decimal(2,2)         null,
   VALOR                money                null,
   PORCENTAJE_DESCUENTO decimal(2,2)         null,
   VALOR_COMPRA_PRODUCTO money                not null,
   constraint PK_PRECIO_TIPO_VENTA_PRODUCTO primary key (ID_PRECIO_TIPO_VENTA_PRODUCTO)
)
go

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   ID_PRODUCTO          uniqueidentifier     not null default newid(),
   ID_CANJE_PRODUCTO_PROVEEDOR uniqueidentifier     null,
   ID_ESTADO_PRODUCTO   uniqueidentifier     null,
   ID_FACTURA_PROVEEDOR uniqueidentifier     null,
   ID_MARCA             uniqueidentifier     null,
   ID_PROCEDENCIA       uniqueidentifier     null,
   ID_UNIDAD_ENTREGA    uniqueidentifier     null,
   ID_CATEGORIA         uniqueidentifier     null,
   ID_GRUPO_PRODUCTO    uniqueidentifier     null,
   ID_ESTADO            uniqueidentifier     null,
   ES_MEDICINA          bit                  not null,
   NOMBRE_PRODUCTO      nvarchar(60)         not null,
   DETALLE_PRODUCTO     text                 null,
   COSTO_ADQUISICION    money                not null,
   CANTIDAD_VENDIDOS    int                  not null,
   CANTIDAD_EXISTENTES  int                  not null default 0
      constraint CKC_CANTIDAD_EXISTENT_PRODUCTO check (CANTIDAD_EXISTENTES >= 0),
   CODIGO_PRODUCTO      nvarchar(20)         not null,
   FECHA_CADUCIDAD      datetime             null,
   REGISTRO_SANITARIO   varchar(25)          null,
   TIENE_IVA            bit                  not null,
   CANTIDAD_INSERVIBLE  int                  null,
   NOMBRE_CORTO         varchar(20)          null,
   CODIGO_BARRAS        varchar(15)          not null,
   CODIGO_PERSONALIZADO nvarchar(15)         null,
   URL_IMAGEN_PRODUCTO  varchar(180)         null,
   USUARIO_CREACION     varchar(100)         null,
   USUARIO_MODIFICACION varchar(100)         null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   constraint PK_PRODUCTO primary key (ID_PRODUCTO)
)
go

/*==============================================================*/
/* Table: PRODUCTO_IMPUESTO                                     */
/*==============================================================*/
create table PRODUCTO_IMPUESTO (
   ID_PRODUCTO_IMPUESTO uniqueidentifier     not null default newid(),
   ID_IMPUESTO          uniqueidentifier     null,
   ID_PRODUCTO          uniqueidentifier     null,
   constraint PK_PRODUCTO_IMPUESTO primary key (ID_PRODUCTO_IMPUESTO)
)
go

/*==============================================================*/
/* Table: PRODUCTO_STOCK                                        */
/*==============================================================*/
create table PRODUCTO_STOCK (
   PRODUCTO_STOCK       uniqueidentifier     not null default newid(),
   ID_PRODUCTO          uniqueidentifier     not null default newid(),
   ID_LOCAL             uniqueidentifier     null,
   CANTIDAD_PRODUCTOS   int                  not null,
   FECHA_CADUCIDAD      datetime             null,
   ID_BODEGA            uniqueidentifier     null,
   USUARIO_CREACION     varchar(100)         null,
   USUARIO_MODIFICACION varchar(100)         null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   constraint PK_PRODUCTO_STOCK primary key (PRODUCTO_STOCK)
)
go

/*==============================================================*/
/* Table: UNIDAD_CATALOGO_PRODUCTO                              */
/*==============================================================*/
create table UNIDAD_CATALOGO_PRODUCTO (
   ID_UNIDAD_CATALOGO_PRODUCTO uniqueidentifier     not null default newid(),
   ID_CATALOGO_PRODUCTO uniqueidentifier     not null default newid(),
   MEDIDA_SUPERIOR      varchar(12)          not null,
   CANTIDAD_SUPERIOR    int                  not null,
   CODIGO_BARRAS_SUPERIOR varchar(15)          not null,
   MEDIDA_INFERIOR      varchar(12)          not null,
   CANTIDAD_INFERIOR    decimal(12,5)        not null,
   CODIGO_BARRAS_INFERIOR varchar(15)          not null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(100)        not null,
   USUARIO_MODIFICACION nvarchar(100)        not null,
   constraint PK_UNIDAD_CATALOGO_PRODUCTO primary key (ID_UNIDAD_CATALOGO_PRODUCTO)
)
go

/*==============================================================*/
/* Table: UNIDAD_ENTREGA                                        */
/*==============================================================*/
create table UNIDAD_ENTREGA (
   ID_UNIDADENTREGA     uniqueidentifier     not null default newid(),
   ID_EMPRESA           uniqueidentifier     null,
   NOMBRE_CONTENIDO     varchar(15)          not null,
   CODIGO_UNIDAD        char(5)              not null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(100)        not null,
   USUARIO_MODIFICACION nvarchar(100)        not null,
   constraint PK_UNIDAD_ENTREGA primary key (ID_UNIDADENTREGA)
)
go

/*==============================================================*/
/* Table: UNIDAD_PRODUCTO                                       */
/*==============================================================*/
create table UNIDAD_PRODUCTO (
   ID_UNIDAD_PRODUCTO   uniqueidentifier     not null default newid(),
   ID_PRODUCTO          uniqueidentifier     not null default newid(),
   MEDIDA_SUPERIOR      varchar(12)          not null,
   CANTIDAD_SUPERIOR    int                  not null,
   CODIGO_BARRAS_SUPERIOR varchar(15)          not null,
   MEDIDA_INFERIOR      varchar(12)          not null,
   CANTIDAD_INFERIOR    decimal(15,5)        not null,
   CODIGO_BARRAS_INFERIOR varchar(15)          not null,
   FECHA_CREACION       datetime             not null,
   FECHA_MODIFICACION   datetime             not null,
   USUARIO_CREACION     nvarchar(100)        not null,
   USUARIO_MODIFICACION nvarchar(100)        not null,
   constraint PK_UNIDAD_PRODUCTO primary key (ID_UNIDAD_PRODUCTO)
)
go

alter table CATALOGO_PRODUCTO
   add constraint FK_CATALOGO_REFERENCE_CATEGORI foreign key (ID_CATEGORIA)
      references CATEGORIA_PRODUCTO (ID_CATEGORIA_PRODUCTO)
go

alter table CATALOGO_PRODUCTO
   add constraint FK_CATALOGO_REFERENCE_UNIDAD_E foreign key (ID_UNIDAD_ENTREGA)
      references UNIDAD_ENTREGA (ID_UNIDADENTREGA)
go

alter table CATALOGO_PRODUCTO
   add constraint FK_CATALOGO_REFERENCE_GRUPO_PR foreign key (ID_GRUPO_PRODUCTO)
      references GRUPO_PRODUCTO (ID_GRUPO_PRODUCTO)
go

alter table CATALOGO_PRODUCTO
   add constraint FK_CATALOGO_REFERENCE_MARCA foreign key (ID_MARCA)
      references MARCA (ID_MARCA)
go

alter table CATALOGO_PRODUCTO_IMPUESTO
   add constraint FK_CATALOGO_FK_C_PROD_CATALOGO foreign key (ID_CATALOGO_PRODUCTO)
      references CATALOGO_PRODUCTO (ID_CATALOGO_PRODUCTO)
go

alter table CATALOGO_PRODUCTO_IMPUESTO
   add constraint FK_CATALOGO_REFERENCE_IMPUESTO foreign key (ID_IMPUESTO)
      references IMPUESTO (ID_IMPUESTO)
go

alter table CATEGORIA_PRODUCTO
   add constraint FK_CATEGORI_REFERENCE_CATEGORI foreign key (ID_CATEGORIA_PRODUCTO_PADRE)
      references CATEGORIA_PRODUCTO (ID_CATEGORIA_PRODUCTO)
go

alter table PRECIO_TIPO_VENTA_CATALOGO
   add constraint FK_PRECIO_T_REFERENCE_CATALOGO foreign key (ID_CATALOGO_PRODUCTO)
      references CATALOGO_PRODUCTO (ID_CATALOGO_PRODUCTO)
go

alter table PRECIO_TIPO_VENTA_PRODUCTO
   add constraint FK_PRECIO_T_REFERENCE_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_UNIDAD_E foreign key (ID_UNIDAD_ENTREGA)
      references UNIDAD_ENTREGA (ID_UNIDADENTREGA)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_CATEGORI foreign key (ID_CATEGORIA)
      references CATEGORIA_PRODUCTO (ID_CATEGORIA_PRODUCTO)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_GRUPO_PR foreign key (ID_GRUPO_PRODUCTO)
      references GRUPO_PRODUCTO (ID_GRUPO_PRODUCTO)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_CANJE_PR foreign key (ID_CANJE_PRODUCTO_PROVEEDOR)
      references CANJE_PRODUCTO_PROVEEDOR (ID_CANJE_PRODUCTO_PROVEEDOR)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_ESTADO_P foreign key (ID_ESTADO_PRODUCTO)
      references ESTADO_PRODUCTO (ID_ESTADO_PRODUCTO)
go

alter table PRODUCTO
   add constraint FK_PRODUCTO_REFERENCE_MARCA foreign key (ID_MARCA)
      references MARCA (ID_MARCA)
go

alter table PRODUCTO_IMPUESTO
   add constraint FK_PRODUCTO_PRODUCTO__PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
go

alter table PRODUCTO_IMPUESTO
   add constraint FK_PRODUCTO_REFERENCE_IMPUESTO foreign key (ID_IMPUESTO)
      references IMPUESTO (ID_IMPUESTO)
go

alter table PRODUCTO_STOCK
   add constraint FK_PRODUCTO_REFERENCE_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
go

alter table UNIDAD_CATALOGO_PRODUCTO
   add constraint FK_UNIDAD_C_REFERENCE_CATALOGO foreign key (ID_CATALOGO_PRODUCTO)
      references CATALOGO_PRODUCTO (ID_CATALOGO_PRODUCTO)
go

alter table UNIDAD_PRODUCTO
   add constraint FK_UNIDAD_P_REFERENCE_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
go

