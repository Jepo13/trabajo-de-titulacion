﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApiComercios.DTO
{
    public class CompanyDTO
    {
        public string Ruc { get; set; }
        public string Direccion { get; set; }
        public string Razonsocial { get; set; }
        public string Telefono { get; set; }
    }
}
