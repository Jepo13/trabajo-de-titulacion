﻿using Marvin.StreamExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using TestApiComercios.DTO;
using TestApiComercios.Http;
using TestApiComercios;

namespace TestAPI
{
    [TestClass]
    public class CompanyTest
    {

        [TestMethod]
        public async Task test1()
        {
            string resposeResult = "";

            try
            {
                ClientHttpCreatePost objHttp = new ClientHttpCreatePost();
                CompanyDTO objCompany = new CompanyDTO();

                objCompany.Direccion = "Calderon Carapungo";
                objCompany.Razonsocial = "TicsKernel";
                objCompany.Ruc = Constantes.ruc;
                objCompany.Telefono = "0982119036";

                var response = await objHttp.requestCreate<CompanyDTO>(objCompany, "/api/Empresa/Create");
                if (response.IsSuccessStatusCode)
                {
                    resposeResult = await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {

            }

            Assert.IsTrue(!string.IsNullOrEmpty(resposeResult));
        }

        [TestMethod]
        public async Task test2()
        {
            List<CompanyDTO> listCompanies = new List<CompanyDTO>();
            try
            {
                ClientHttpCreatePost objHttp = new ClientHttpCreatePost();

                var response = await objHttp.getAll("api/Empresa/GetAllCompany");
                if (response.IsSuccessStatusCode)
                {
                    var responseStream = await response.Content.ReadAsStringAsync();
                    listCompanies = JsonConvert.DeserializeObject<List<CompanyDTO>>(responseStream);
                }
            }
            catch (Exception ex)
            {

            }

            Assert.IsTrue(listCompanies.Count() > 0);
        }

        [TestMethod]
        public async Task test3()
        {
            CompanyDTO objecto = new CompanyDTO();
            try
            {
                ClientHttpCreatePost objHttp = new ClientHttpCreatePost();

                var response = await objHttp.getAll("api/Empresa/getCompanyByRUC?rucCompany="+Constantes.ruc);
                if (response.IsSuccessStatusCode)
                {
                    var responseStream = await response.Content.ReadAsStringAsync();
                    objecto = JsonConvert.DeserializeObject<CompanyDTO>(responseStream);
                }
            }
            catch (Exception ex)
            {

            }

            Assert.IsTrue(objecto!=null);
        }

    }//class
}
