﻿using Marvin.StreamExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using TestApiComercios.DTO;

namespace TestApiComercios.Http
{
    public class ClientHttpCreatePost
    {
        //api= "/api/Empresa/Create"
        public async Task<HttpResponseMessage> requestCreate<T>(T objCompany, string api)
        {
            MemoryStream memoryContentStream = new MemoryStream();
            memoryContentStream.SerializeToJsonAndWrite(objCompany, new System.Text.UTF8Encoding(), 1024, true);

            //BORRAR
            var temp = JsonConvert.SerializeObject(objCompany);

            memoryContentStream.Seek(0, SeekOrigin.Begin);

            using (var request = new HttpRequestMessage(HttpMethod.Post, api))
            {
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var streamContent = new StreamContent(memoryContentStream))
                {
                    try
                    {
                        request.Content = streamContent;
                        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(Constantes.uri);

                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.SendAsync(request);

                        return response;
                    }
                    catch (Exception ex)
                    {
                    }

                }
            }

            return null;
        }

        public async Task<HttpResponseMessage> getAll(string uri)
        {
            try
            {
                string url = Constantes.uri + uri;
                HttpClient client = new HttpClient();

                var request = new HttpRequestMessage(HttpMethod.Get, url);
                var response = await client.SendAsync(request);

                return response;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

    }
}
